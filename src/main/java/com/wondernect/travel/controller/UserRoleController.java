package com.wondernect.travel.controller;

import com.wondernect.elements.authorize.context.WondernectCommonContext;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeRoleType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeUserRole;
import com.wondernect.elements.common.error.BusinessError;
import com.wondernect.elements.common.response.BusinessData;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.rdb.request.PageRequestData;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.services.rbac.model.Role;
import com.wondernect.services.rbac.model.UserRole;
import com.wondernect.services.rbac.service.RolesService;
import com.wondernect.services.session.service.impl.DefaultTokenSessionService;
import com.wondernect.services.user.model.User;
import com.wondernect.services.user.service.UserService;
import com.wondernect.travel.dto.login.UserLoginResponseDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: UserRoleController
 * Author: chenxun
 * Date: 2019/4/8 10:38
 * Description:
 */
@RestController
@RequestMapping(value = "/v5/rbac/user/role")
@Validated
@Api(tags = {"rbac-user-role"}, description = "rbac-用户角色服务")
public class UserRoleController {

    @Autowired
    private RolesService rolesService;

    @Autowired
    private UserService userService;

    @Autowired
    private DefaultTokenSessionService tokenSessionService;

    @Autowired
    private WondernectCommonContext wondernectCommonContext;

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ApiOperation(value = "rbac_save_user_role", notes = "创建更新用户角色", httpMethod = "POST")
    public BusinessData<Role> createUserRole(
            @ApiParam(required = true) @NotBlank(message = "用户id不能为空") @RequestParam(value = "user_id", required = false) String userId,
            @ApiParam(required = true) @NotBlank(message = "角色id不能为空") @RequestParam(value = "role_id", required = false) String roleId
    ) {
        return new BusinessData<>(rolesService.saveUserRole(wondernectCommonContext.getAuthorizeData().getUserId(), userId, roleId));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ApiOperation(value = "rbac_delete_user_role", notes = "删除用户角色", httpMethod = "POST")
    public BusinessData deleteUserRole(
            @ApiParam(required = true) @NotBlank(message = "用户id不能为空") @RequestParam(value = "user_id", required = false) String userId,
            @ApiParam(required = true) @NotBlank(message = "角色id不能为空") @RequestParam(value = "role_id", required = false) String roleId
    ) {
        rolesService.deleteUserRole(wondernectCommonContext.getAuthorizeData().getUserId(), userId, roleId);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    @ApiOperation(value = "rbac_get_role_list", notes = "获取用户所有角色列表", httpMethod = "POST")
    public BusinessData<List<Role>> getRoleList(
            @ApiParam(required = false) @RequestBody(required = false) List<SortData> sortDataList
    ) {
        return new BusinessData<>(rolesService.findAllByUserId(wondernectCommonContext.getAuthorizeData().getUserId(), sortDataList));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/page", method = RequestMethod.POST)
    @ApiOperation(value = "rbac_get_role_page", notes = "获取用户所有角色分页", httpMethod = "POST")
    public BusinessData<PageResponseData<Role>> getRolePage(
            @ApiParam(required = true) @NotNull(message = "分页请求参数不能为空") @Validated @RequestBody(required = false) PageRequestData pageRequestData
    ) {
        return new BusinessData<>(rolesService.findAllByUserId(wondernectCommonContext.getAuthorizeData().getUserId(), pageRequestData));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/user_list", method = RequestMethod.POST)
    @ApiOperation(value = "rbac_get_role_list", notes = "获取角色所有用户列表", httpMethod = "POST")
    public BusinessData<List<User>> getUserList(
            @ApiParam(required = true) @NotNull(message = "角色列表不能为空") @RequestParam(value = "role_ids", required = false) String[] roleIds,
            @ApiParam(required = false) @RequestBody(required = false) List<SortData> sortDataList
    ) {
        if (roleIds == null || roleIds.length <= 0) {
            return new BusinessData<>(new ArrayList<>());
        }
        List<User> userList = new ArrayList<>();
        List<UserRole> userRoleList = rolesService.findListByRoleIdIn(Arrays.asList(roleIds), sortDataList);
        if (CollectionUtils.isNotEmpty(userRoleList)) {
            for (UserRole userRole : userRoleList) {
                userList.add(userService.findByUserId(userRole.getUserId()));
            }
        }
        return new BusinessData<>(userList);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/user_page", method = RequestMethod.POST)
    @ApiOperation(value = "rbac_get_role_page", notes = "获取角色所有用户分页", httpMethod = "POST")
    public BusinessData<PageResponseData<UserLoginResponseDTO>> getUserPage(
            @ApiParam(required = false) @RequestParam(value = "role_ids", required = false) String[] roleIds,
            @ApiParam(required = true) @NotNull(message = "分页请求参数不能为空") @Validated @RequestBody(required = false) PageRequestData pageRequestData
    ) {
        PageResponseData<UserRole> userRolePageResponseData;
        if (roleIds == null || roleIds.length <= 0) {
            userRolePageResponseData = rolesService.findPage(pageRequestData);
        } else {
            userRolePageResponseData = rolesService.findPageByRoleIdIn(Arrays.asList(roleIds), pageRequestData);
        }
        List<UserLoginResponseDTO> userLoginResponseDTOList = new ArrayList<>();
        List<String> userIdList = new ArrayList<>();
        List<UserRole> userRoleList = userRolePageResponseData.getDataList();
        if (CollectionUtils.isNotEmpty(userRoleList)) {
            for (UserRole userRole : userRoleList) {
                User user = userService.findByUserId(userRole.getUserId());
                if (ESObjectUtils.isNotNull(user) && !userIdList.contains(user.getId())) {
                    userLoginResponseDTOList.add(new UserLoginResponseDTO(user, rolesService.findAllByUserId(user.getId(), new ArrayList<>()), tokenSessionService.findLatestUserTokenSession(user.getId())));
                    userIdList.add(user.getId());
                }
            }
        }
        return new BusinessData<>(new PageResponseData<>(pageRequestData.getPage(), pageRequestData.getSize(), userRolePageResponseData.getTotalPages(), userRolePageResponseData.getTotalElements(), userLoginResponseDTOList));
    }
}
