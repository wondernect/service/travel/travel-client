package com.wondernect.travel.controller;

import com.alibaba.fastjson.JSONObject;
import com.wondernect.elements.common.exception.BusinessException;
import com.wondernect.elements.common.response.BusinessData;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.common.utils.ESStringUtils;
import com.wondernect.elements.http.client.HttpClient;
import com.wondernect.travel.api.CaptchaSessionAPI;
import com.wondernect.travel.dto.captcha.*;
import com.wondernect.travel.wechat.WechatAESUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: CaptchaSessionController
 * Author: chenxun
 * Date: 2019/3/18 14:11
 * Description: captcha session controller
 */
@RestController
@RequestMapping(value = "/v5/session")
@Validated
@Api(tags = {"session-captcha"}, description = "验证码会话服务")
public class CaptchaSessionController {

    @Autowired
    private CaptchaSessionAPI captchaSessionAPI;

    @Autowired
    private HttpClient httpClient;

    private static final String wechat_user_get_url = "https://api.weixin.qq.com/sns/jscode2session";

    private static final String appId = "wx7b18e1845642939f";

    private static final String appSecret = "201c43094688e9e121e9acb1409cd619";

    @RequestMapping(value = "/captcha", method = RequestMethod.POST)
    @ApiOperation(value = "session_request_captcha", notes = "请求手机短信验证码", httpMethod = "POST")
    public BusinessData<CaptchaResponseDTO> requestRegistCaptcha(
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @Validated @RequestBody(required = false) CaptchaRequestDTO captchaRequestDTO
    ) {
        return new BusinessData<>(captchaSessionAPI.requestCaptchaSession(captchaRequestDTO));
    }

    @RequestMapping(value = "/wechat_captcha", method = RequestMethod.POST)
    @ApiOperation(value = "小程序code请求", notes = "小程序code请求", httpMethod = "POST")
    public BusinessData<String> wechatRequestRegistCaptcha(
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @Validated @RequestBody(required = false) WeChatCaptchaRequestDTO captchaRequestDTO
    ) {
        String url = wechat_user_get_url + "?appid="+ appId +"&secret="+ appSecret +"&js_code="+ captchaRequestDTO.getCode() +"&grant_type=authorization_code";
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        String response = httpClient.getForJson(url, headers, null);
        return new BusinessData<>(response);
    }

    @RequestMapping(value = "/decrypt_user_mobile", method = RequestMethod.POST)
    @ApiOperation(value = "微信加密信息解密接口", notes = "微信加密信息解密接口", httpMethod = "POST")
    public BusinessData<String> decryptWeChatUserPhone(
            @ApiParam(required = true) @NotNull(message = "用户登录请求数据不能为空") @Validated @RequestBody(required = false) WechatSecretRequestDTO wechatSecretRequestDTO
    ) {
        String result = WechatAESUtils.wxDecrypt(wechatSecretRequestDTO.getEncryptedData(), wechatSecretRequestDTO.getSessionKey(), wechatSecretRequestDTO.getIv());
        JSONObject json = JSONObject.parseObject(result);
        if (ESObjectUtils.isNull(json)) {
            throw new BusinessException("获取的手机号等信息的json数据为空");
        }
        String phone = null;
        if (json.containsKey("phoneNumber")) {
            phone = json.getString("phoneNumber");
            if (ESStringUtils.isRealEmpty(phone)) {
                throw new BusinessException("获取失败！用户未绑定手机号");
            }
        }
        return new BusinessData<>(phone);
    }
}
