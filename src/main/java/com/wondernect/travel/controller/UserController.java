package com.wondernect.travel.controller;

import com.wondernect.elements.authorize.context.interceptor.AuthorizeRoleType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeUserRole;
import com.wondernect.elements.common.error.BusinessError;
import com.wondernect.elements.common.response.BusinessData;
import com.wondernect.elements.rdb.request.PageRequestData;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.services.user.model.User;
import com.wondernect.travel.api.UserAPI;
import com.wondernect.travel.dto.login.UserLoginResponseDTO;
import com.wondernect.travel.dto.user.UserRoleAddRequestDTO;
import com.wondernect.travel.dto.user.UserRoleCreateRequestDTO;
import com.wondernect.travel.dto.user.UserRoleResponseDTO;
import com.wondernect.travel.dto.user.UserUpdateRequestDTO;
import com.wondernect.travel.model.Shop;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: UserController
 * Author: chenxun
 * Date: 2019/4/7 13:30
 * Description:
 */
@RestController
@RequestMapping(value = "/v5/console/user")
@Validated
@Api(tags = {"console-user"}, description = "管理后台用户服务")
public class UserController {

    @Autowired
    private UserAPI userAPI;

    // @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/check", method = RequestMethod.GET)
    @ApiOperation(value = "手机号码或邮箱是否注册", notes = "手机号码或邮箱是否注册", httpMethod = "GET")
    public BusinessData<UserRoleResponseDTO> check(
            @ApiParam(required = true) @NotBlank(message = "手机号码或邮箱不能为空") @RequestParam(value = "username", required = false) String username
    ) {
        return new BusinessData<>(userAPI.findByUsername(username));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ApiOperation(value = "user_create", notes = "管理员创建用户", httpMethod = "POST")
    public BusinessData<UserRoleResponseDTO> createUser(
            @ApiParam(required = true) @NotNull(message = "创建请求数据不能为空") @Validated @RequestBody(required = false) UserRoleCreateRequestDTO userRoleCreateRequestDTO
    ) {
        return new BusinessData<>(userAPI.createUser(userRoleCreateRequestDTO));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ApiOperation(value = "user_add", notes = "管理员添加用户角色", httpMethod = "POST")
    public BusinessData<UserRoleResponseDTO> addUser(
            @ApiParam(required = true) @NotNull(message = "添加请求数据不能为空") @Validated @RequestBody(required = false) UserRoleAddRequestDTO userRoleAddRequestDTO
    ) {
        return new BusinessData<>(userAPI.addUser(userRoleAddRequestDTO));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ApiOperation(value = "user_update", notes = "管理员更新用户", httpMethod = "POST")
    public BusinessData<UserRoleResponseDTO> updateUser(
            @ApiParam(required = true) @NotNull(message = "更新请求数据不能为空") @Validated @RequestBody(required = false) UserUpdateRequestDTO userUpdateRequestDTO
    ) {
        return new BusinessData<>(userAPI.updateUser(userUpdateRequestDTO));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ApiOperation(value = "user_delete", notes = "管理员删除用户", httpMethod = "POST")
    public BusinessData deleteUser(
            @ApiParam(required = true) @NotBlank(message = "用户id不能为空") @RequestParam(value = "user_id", required = false) String userId
    ) {
        userAPI.deleteByUserId(userId);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    @ApiOperation(value = "rbac_get_user_list", notes = "获取用户列表", httpMethod = "POST")
    public BusinessData<List<UserLoginResponseDTO>> getUserList(
            @ApiParam(required = false) @RequestParam(value = "username", required = false) String username,
            @ApiParam(required = false) @RequestBody(required = false) List<SortData> sortDataList
    ) {
        return new BusinessData<>(userAPI.findList(username, sortDataList));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/page", method = RequestMethod.POST)
    @ApiOperation(value = "rbac_get_user_page", notes = "获取用户分页", httpMethod = "POST")
    public BusinessData<PageResponseData<UserLoginResponseDTO>> getUserPage(
            @ApiParam(required = false) @RequestParam(value = "username", required = false) String username,
            @ApiParam(required = true) @NotNull(message = "分页请求参数不能为空") @Validated @RequestBody(required = false) PageRequestData pageRequestData
    ) {
        return new BusinessData<>(userAPI.findPage(username, pageRequestData));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/shop_list", method = RequestMethod.POST)
    @ApiOperation(value = "获取用户商家列表", notes = "获取用户商家列表", httpMethod = "POST")
    public BusinessData<List<Shop>> getShopList(
            @ApiParam(required = false) @RequestParam(value = "user_id", required = false) String userId
    ) {
        return new BusinessData<>(userAPI.findUserShopList(userId));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/shop_user_list", method = RequestMethod.POST)
    @ApiOperation(value = "获取商家用户列表", notes = "获取商家用户列表", httpMethod = "POST")
    public BusinessData<List<User>> getShopUserList(
            @ApiParam(required = false) @RequestParam(value = "shop_id", required = false) String shopId,
            @ApiParam(required = false) @RequestBody(required = false) List<SortData> sortDataList
    ) {
        return new BusinessData<>(userAPI.findShopUserList(shopId));
    }
}
