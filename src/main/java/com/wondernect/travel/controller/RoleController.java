package com.wondernect.travel.controller;

import com.wondernect.elements.authorize.context.WondernectCommonContext;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeRoleType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeUserRole;
import com.wondernect.elements.common.error.BusinessError;
import com.wondernect.elements.common.response.BusinessData;
import com.wondernect.elements.rdb.request.PageRequestData;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.services.rbac.model.Role;
import com.wondernect.services.rbac.service.RolesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: RoleController
 * Author: chenxun
 * Date: 2019/4/6 14:27
 * Description:
 */
@RestController
@RequestMapping(value = "/v5/rbac/role")
@Validated
@Api(tags = {"rbac-role"}, description = "rbac-角色服务")
public class RoleController {

    @Autowired
    private RolesService rolesService;

    @Autowired
    private WondernectCommonContext wondernectCommonContext;

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ApiOperation(value = "创建角色", notes = "创建角色", httpMethod = "POST")
    public BusinessData<Role> save(
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @Validated @RequestBody Role role
    ) {
        return new BusinessData<>(rolesService.save(wondernectCommonContext.getAuthorizeData().getUserId(), role));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ApiOperation(value = "删除角色", notes = "删除角色", httpMethod = "POST")
    public BusinessData deleteRole(
            @ApiParam(required = true) @NotBlank(message = "请求参数不能为空") @RequestParam(value = "role_id", required = false) String roleId
    ) {
        rolesService.deleteRole(wondernectCommonContext.getAuthorizeData().getUserId(), roleId);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    @ApiOperation(value = "获取用户可见的角色列表", notes = "获取用户可见的角色列表", httpMethod = "POST")
    public BusinessData<List<Role>> getRoleList(
            @ApiParam(required = false) @RequestBody(required = false) List<SortData> sortDataList
    ) {
        return new BusinessData<>(rolesService.findUserVisibleRoleList(wondernectCommonContext.getAuthorizeData().getUserId(), sortDataList));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/page", method = RequestMethod.POST)
    @ApiOperation(value = "获取用户可见的角色分页", notes = "获取用户可见的角色分页", httpMethod = "POST")
    public BusinessData<PageResponseData<Role>> getRolePage(
            @ApiParam(required = true) @NotNull(message = "分页请求参数不能为空") @Validated @RequestBody(required = false) PageRequestData pageRequestData
    ) {
        return new BusinessData<>(rolesService.findUserVisibleRolePage(wondernectCommonContext.getAuthorizeData().getUserId(), pageRequestData));
    }
}
