package com.wondernect.travel.controller;

import com.wondernect.elements.authorize.context.interceptor.AuthorizeRoleType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeUserRole;
import com.wondernect.elements.common.error.BusinessError;
import com.wondernect.elements.common.response.BusinessData;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.services.user.model.User;
import com.wondernect.services.user.model.em.AppType;
import com.wondernect.travel.api.UserAPI;
import com.wondernect.travel.dto.login.*;
import com.wondernect.travel.dto.user.AppUserRegistRequestDTO;
import com.wondernect.travel.dto.user.UserPasswordNewRequestDTO;
import com.wondernect.travel.dto.user.UserRoleResponseDTO;
import com.wondernect.travel.dto.user.UserUpdateRequestDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: UserController
 * Author: chenxun
 * Date: 2019/4/7 13:30
 * Description:
 */
@RestController
@RequestMapping(value = "/v5/user")
@Validated
@Api(tags = {"user"}, description = "用户会话服务")
public class UserSessionController {

    @Autowired
    private UserAPI userAPI;

    @RequestMapping(value = "/check_app_user", method = RequestMethod.GET)
    @ApiOperation(value = "第三方应用用户是否注册", notes = "第三方应用用户是否注册", httpMethod = "GET")
    public BusinessData<String> checkAppUser(
            @ApiParam(required = true) @NotBlank(message = "用户标示") @RequestParam(value = "app_user_id", required = false) String appUserId
    ) {
        User user = userAPI.findAppUser(AppType.WECHAT, appUserId);
        if (ESObjectUtils.isNull(user)) {
            return new BusinessData<>(BusinessError.SUCCESS);
        }
        return new BusinessData<>(user.getId());
    }

    @RequestMapping(value = "/app_user_regist", method = RequestMethod.POST)
    @ApiOperation(value = "第三方应用用户注册", notes = "第三方应用用户注册", httpMethod = "POST")
    public BusinessData<UserLoginResponseDTO> appRegist(
            @ApiParam(required = true) @NotNull(message = "请求数据不能为空") @Validated @RequestBody(required = false) AppUserRegistRequestDTO appUserRegistRequestDTO
    ) {
        return new BusinessData<>(userAPI.appRegist(appUserRegistRequestDTO));
    }

    @RequestMapping(value = "/password/set", method = RequestMethod.POST)
    @ApiOperation(value = "设置用户密码", notes = "设置用户密码", httpMethod = "POST")
    public BusinessData setUserPassword(
            @ApiParam(required = true) @NotNull(message = "请求数据不能为空") @Validated @RequestBody(required = false) UserPasswordNewRequestDTO userPasswordNewRequestDTO
    ) {
        userAPI.setUserPassword(userPasswordNewRequestDTO);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ApiOperation(value = "用户登录", notes = "用户登录", httpMethod = "POST")
    public BusinessData<UserLoginResponseDTO> login(
            @ApiParam(required = true) @NotNull(message = "用户登录请求数据不能为空") @Validated @RequestBody(required = false) UserLoginRequestDTO userLoginRequestDTO
    ) {
        return new BusinessData<>(userAPI.login(userLoginRequestDTO));
    }

    @RequestMapping(value = "/captcha_login", method = RequestMethod.POST)
    @ApiOperation(value = "司机端短信验证码登录", notes = "司机端短信验证码登录", httpMethod = "POST")
    public BusinessData<UserLoginResponseDTO> captchaLogin(
            @ApiParam(required = true) @NotNull(message = "登录请求数据不能为空") @Validated @RequestBody(required = false) CaptchaLoginRequestDTO captchaLoginRequestDTO
    ) {
        return new BusinessData<>(userAPI.driverCaptchalogin(captchaLoginRequestDTO));
    }

    @RequestMapping(value = "/wechat_captcha_login", method = RequestMethod.POST)
    @ApiOperation(value = "微信用户端短信验证码登录", notes = "微信用户端短信验证码登录", httpMethod = "POST")
    public BusinessData<UserLoginResponseDTO> wechatCaptchaLogin(
            @ApiParam(required = true) @NotNull(message = "登录请求数据不能为空") @Validated @RequestBody(required = false) WechatCaptchaLoginRequestDTO wechatCaptchaLoginRequestDTO
    ) {
        return new BusinessData<>(userAPI.wechatCaptchalogin(wechatCaptchaLoginRequestDTO));
    }

    @RequestMapping(value = "/app_user_login", method = RequestMethod.POST)
    @ApiOperation(value = "第三方用户登录", notes = "第三方用户登录", httpMethod = "POST")
    public BusinessData<UserLoginResponseDTO> appUserLogin(
            @ApiParam(required = true) @NotNull(message = "用户登录请求数据不能为空") @Validated @RequestBody(required = false) AppUserLoginRequestDTO appUserLoginRequestDTO
    ) {
        return new BusinessData<>(userAPI.appUserlogin(appUserLoginRequestDTO));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    @ApiOperation(value = "用户登出", notes = "用户登出", httpMethod = "POST")
    public BusinessData logout() {
        userAPI.logout();
        return new BusinessData(BusinessError.SUCCESS);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    @ApiOperation(value = "获取用户", notes = "获取用户", httpMethod = "GET")
    public BusinessData<UserRoleResponseDTO> getUser(
            @ApiParam(required = false) @RequestParam(value = "user_id", required = false) String userId
    ) {
        return new BusinessData<>(userAPI.findByUserId(userId));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ApiOperation(value = "更新用户信息", notes = "更新用户信息", httpMethod = "POST")
    public BusinessData<UserRoleResponseDTO> getUser(
            @ApiParam(required = true) @NotNull(message = "请求数据不能为空") @Validated @RequestBody(required = false) UserUpdateRequestDTO userUpdateRequestDTO
    ) {
        return new BusinessData<>(userAPI.updateUser(userUpdateRequestDTO));
    }
}
