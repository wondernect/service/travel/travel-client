package com.wondernect.travel.controller.base;

import com.wondernect.elements.authorize.context.interceptor.AuthorizeRoleType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeUserRole;
import com.wondernect.elements.common.error.BusinessError;
import com.wondernect.elements.common.exception.BusinessException;
import com.wondernect.elements.common.response.BusinessData;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.travel.api.PriceAPI;
import com.wondernect.travel.dto.base.PriceCountRequestDTO;
import com.wondernect.travel.manager.ConsumeCarLevelManager;
import com.wondernect.travel.manager.PriceStrategyManager;
import com.wondernect.travel.model.PriceStrategy;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: PriceController
 * Author: chenxun
 * Date: 2019/5/11 18:22
 * Description:
 */
@RestController
@RequestMapping(value = "/v5/travel/price")
@Validated
@Api(tags = {"price"}, description = "价格|价格策略服务")
public class PriceController {

    @Autowired
    private PriceAPI priceAPI;

    @Autowired
    private PriceStrategyManager priceStrategyManager;

    @Autowired
    private ConsumeCarLevelManager consumeCarLevelManager;

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/strategy/save", method = RequestMethod.POST)
    @ApiOperation(value = "新增/编辑", notes = "新增/编辑", httpMethod = "POST")
    public BusinessData<PriceStrategy> save(
            @ApiParam(required = true) @NotNull(message = "策略不能为空") @Validated @RequestBody PriceStrategy priceStrategy
    ) {
        return new BusinessData<>(priceAPI.save(priceStrategy));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/strategy/delete", method = RequestMethod.DELETE)
    @ApiOperation(value = "删除", notes = "删除", httpMethod = "DELETE")
    public BusinessData delete(
            @ApiParam(required = true) @NotBlank(message = "策略id不能为空") @RequestParam(value = "id", required = false) String strategyId
    ) {
        if (consumeCarLevelManager.existsByPriceStrategyId(strategyId)) {
            throw new BusinessException("价格策略正在使用，不可删除");
        }
        priceStrategyManager.deleteById(strategyId);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/strategy/list", method = RequestMethod.POST)
    @ApiOperation(value = "列表", notes = "列表", httpMethod = "POST")
    public BusinessData<List<PriceStrategy>> page(
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @RequestBody(required = false) List<SortData> sortDataList
    ) {
        return new BusinessData<>(priceStrategyManager.findAll(sortDataList));
    }

    // @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/count", method = RequestMethod.POST)
    @ApiOperation(value = "获取价格", notes = "获取价格", httpMethod = "POST")
    public BusinessData<Double> count(
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @Validated @RequestBody(required = false) PriceCountRequestDTO priceCountRequestDTO
    ) {
        return new BusinessData<>(
                priceAPI.getPrice(
                        priceCountRequestDTO.getPriceStrategyId(),
                        priceCountRequestDTO.getStartTime(),
                        priceCountRequestDTO.getKilo(),
                        priceCountRequestDTO.getTime()
                )
        );
    }
}
