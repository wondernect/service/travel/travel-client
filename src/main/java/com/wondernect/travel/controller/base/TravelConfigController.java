package com.wondernect.travel.controller.base;

import com.wondernect.elements.authorize.context.interceptor.AuthorizeRoleType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeUserRole;
import com.wondernect.elements.common.error.BusinessError;
import com.wondernect.elements.common.response.BusinessData;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.travel.manager.TravelConfigManager;
import com.wondernect.travel.model.TravelConfig;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: AddressController
 * Author: chenxun
 * Date: 2019/5/11 17:07
 * Description:
 */
@RestController
@RequestMapping(value = "/v5/travel/config")
@Validated
@Api(tags = {"travel_config"}, description = "配置服务")
public class TravelConfigController {

    @Autowired
    private TravelConfigManager travelConfigManager;

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ApiOperation(value = "新增/编辑", notes = "新增/编辑", httpMethod = "POST")
    public BusinessData<TravelConfig> save(
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @Validated @RequestBody TravelConfig travelConfig
    ) {
        return new BusinessData<>(travelConfigManager.save(travelConfig));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    @ApiOperation(value = "删除", notes = "删除", httpMethod = "DELETE")
    public BusinessData delete(
            @ApiParam(required = true) @NotBlank(message = "配置id不能为空") @RequestParam(value = "config_id", required = false) String configId
    ) {
        travelConfigManager.deleteById(configId);
        return new BusinessData(BusinessError.SUCCESS);
    }

    // @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    @ApiOperation(value = "列表", notes = "列表", httpMethod = "POST")
    public BusinessData<List<TravelConfig>> list(
            @ApiParam(required = true) @NotBlank(message = "配置代码不能为空") @RequestParam(value = "code", required = false) String code,
            @ApiParam(required = true) @NotNull(message = "列表请求参数不能为空") @RequestBody(required = false) List<SortData> sortDataList
    ) {
        return new BusinessData<>(travelConfigManager.findAllByCode(code, sortDataList));
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    @ApiOperation(value = "获取所有配置", notes = "获取所有配置", httpMethod = "GET")
    public BusinessData<List<TravelConfig>> get(
            @ApiParam(required = true) @NotBlank(message = "配置代码不能为空") @RequestParam(value = "code", required = false) String code
    ) {
        return new BusinessData<>(travelConfigManager.findAllByCode(code, new ArrayList<>()));
    }
}
