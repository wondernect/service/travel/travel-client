package com.wondernect.travel.controller.base;

import com.wondernect.elements.authorize.context.interceptor.AuthorizeRoleType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeUserRole;
import com.wondernect.elements.common.error.BusinessError;
import com.wondernect.elements.common.response.BusinessData;
import com.wondernect.elements.rdb.request.PageRequestData;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.travel.api.CarAPI;
import com.wondernect.travel.dto.base.CarResponseDTO;
import com.wondernect.travel.model.Car;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: AddressController
 * Author: chenxun
 * Date: 2019/5/11 17:07
 * Description:
 */
@RestController
@RequestMapping(value = "/v5/travel/car")
@Validated
@Api(tags = {"car"}, description = "车辆服务")
public class CarController {

    @Autowired
    private CarAPI carAPI;

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ApiOperation(value = "新增/编辑", notes = "新增/编辑", httpMethod = "POST")
    public BusinessData<CarResponseDTO> save(
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @Validated @RequestBody Car car
    ) {
        return new BusinessData<>(carAPI.save(car));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    @ApiOperation(value = "删除", notes = "删除", httpMethod = "DELETE")
    public BusinessData delete(
            @ApiParam(required = true) @NotBlank(message = "车辆id不能为空") @RequestParam(value = "car_id", required = false) String carId
    ) {
        carAPI.delete(carId);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    @ApiOperation(value = "列表", notes = "列表", httpMethod = "POST")
    public BusinessData<List<CarResponseDTO>> list(
            @ApiParam(required = true) @NotNull(message = "列表请求参数不能为空") @RequestBody(required = false) List<SortData> sortDataList
    ) {
        return new BusinessData<>(carAPI.list(sortDataList));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/page", method = RequestMethod.POST)
    @ApiOperation(value = "分页", notes = "分页", httpMethod = "POST")
    public BusinessData<PageResponseData<CarResponseDTO>> page(
            @ApiParam(required = false) @RequestParam(value = "city_id", required = false) String cityId,
            @ApiParam(required = false) @RequestParam(value = "car_no", required = false) String carNo,
            @ApiParam(required = true) @NotNull(message = "page请求参数不能为空") @RequestBody(required = false) PageRequestData pageRequestData
            ) {
        return new BusinessData<>(carAPI.page(cityId, carNo, pageRequestData));
    }
}
