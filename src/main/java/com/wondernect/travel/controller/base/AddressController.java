package com.wondernect.travel.controller.base;

import com.wondernect.elements.authorize.context.interceptor.AuthorizeRoleType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeUserRole;
import com.wondernect.elements.common.error.BusinessError;
import com.wondernect.elements.common.response.BusinessData;
import com.wondernect.elements.rdb.request.PageRequestData;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.travel.manager.AddressManager;
import com.wondernect.travel.model.Address;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: AddressController
 * Author: chenxun
 * Date: 2019/5/11 17:07
 * Description:
 */
@RestController
@RequestMapping(value = "/v5/travel/address")
@Validated
@Api(tags = {"address"}, description = "接送地址")
public class AddressController {

    @Autowired
    private AddressManager addressManager;

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ApiOperation(value = "新增/编辑地址", notes = "新增/编辑地址", httpMethod = "POST")
    public BusinessData<Address> save(
            @ApiParam(required = true) @NotNull(message = "地址不能为空") @Validated @RequestBody Address address
    ) {
        return new BusinessData<>(addressManager.save(address));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    @ApiOperation(value = "删除地址", notes = "删除地址", httpMethod = "DELETE")
    public BusinessData delete(
            @ApiParam(required = true) @NotBlank(message = "地址id不能为空") @RequestParam(value = "id", required = false) String addressId
    ) {
        addressManager.deleteById(addressId);
        return new BusinessData(BusinessError.SUCCESS);
    }

    // @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/page", method = RequestMethod.POST)
    @ApiOperation(value = "获取地址分页", notes = "获取地址分页", httpMethod = "POST")
    public BusinessData<PageResponseData<Address>> page(
            @ApiParam(required = false) @RequestParam(value = "city_id", required = false) String cityId,
            @ApiParam(required = false) @RequestParam(value = "input", required = false) String input,
            @ApiParam(required = true) @NotNull(message = "分页请求参数不能为空") @RequestBody(required = false) PageRequestData pageRequestData
    ) {
        return new BusinessData<>(addressManager.page(cityId, input, pageRequestData));
    }
}
