package com.wondernect.travel.controller.base;

import com.wondernect.elements.authorize.context.WondernectCommonContext;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeRoleType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeUserRole;
import com.wondernect.elements.common.error.BusinessError;
import com.wondernect.elements.common.exception.BusinessException;
import com.wondernect.elements.common.response.BusinessData;
import com.wondernect.elements.common.utils.ESDateTimeUtils;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.common.utils.ESStringUtils;
import com.wondernect.elements.rdb.request.PageRequestData;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.travel.api.TravelOrderAPI;
import com.wondernect.travel.business.youhui.manager.CouponManager;
import com.wondernect.travel.business.youhui.model.Coupon;
import com.wondernect.travel.dto.base.BatchOrderRequestDTO;
import com.wondernect.travel.dto.base.WarningOrderResponseDTO;
import com.wondernect.travel.manager.TravelOrderHistoryManager;
import com.wondernect.travel.manager.TravelOrderManager;
import com.wondernect.travel.model.TravelOrder;
import com.wondernect.travel.model.TravelOrderHistory;
import com.wondernect.travel.model.em.OrderSource;
import com.wondernect.travel.model.em.OrderStatus;
import com.wondernect.travel.model.em.Scene;
import com.wondernect.travel.model.em.WarningStatus;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: OrderStrategyController
 * Author: chenxun
 * Date: 2019/5/13 11:01
 * Description:
 */
@RestController
@RequestMapping(value = "/v5/travel/order")
@Validated
@Api(tags = "订单服务", description = "订单服务")
public class OrderController {

    @Autowired
    private WondernectCommonContext wondernectCommonContext;

    @Autowired
    private TravelOrderManager travelOrderManager;

    @Autowired
    private TravelOrderHistoryManager travelOrderHistoryManager;

    @Autowired
    private TravelOrderAPI travelOrderAPI;

    @Autowired
    private CouponManager couponManager;

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ApiOperation(value = "创建订单", notes = "创建订单", httpMethod = "POST")
    public BusinessData<TravelOrder> createOrder(
            @ApiParam(required = true) @NotNull(message = "参数不能为空") @Validated @RequestBody TravelOrder travelOrder
    ) {
        return new BusinessData<>(travelOrderAPI.createOrder(wondernectCommonContext.getAuthorizeData().getUserId(), travelOrder));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ApiOperation(value = "编辑订单", notes = "编辑订单", httpMethod = "POST")
    public BusinessData<TravelOrder> saveOrder(
            @ApiParam(required = true) @NotNull(message = "参数不能为空") @Validated @RequestBody TravelOrder travelOrder
    ) {
        return new BusinessData<>(travelOrderAPI.saveOrder(travelOrder));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    @ApiOperation(value = "获取订单详情", notes = "获取订单详情", httpMethod = "GET")
    public BusinessData<TravelOrder> getOrder(
            @ApiParam(required = true) @NotBlank(message = "订单id不能为空") @RequestParam(value = "order_id") String orderId
    ) {
        return new BusinessData<>(travelOrderManager.findById(orderId));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/get_history", method = RequestMethod.GET)
    @ApiOperation(value = "获取订单历史记录", notes = "获取订单历史记录", httpMethod = "GET")
    public BusinessData<List<TravelOrderHistory>> getOrderHistory(
            @ApiParam(required = true) @NotBlank(message = "订单id不能为空") @RequestParam(value = "order_id") String orderId
    ) {
        return new BusinessData<>(travelOrderHistoryManager.list(orderId));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/wait_pay", method = RequestMethod.GET)
    @ApiOperation(value = "待支付订单详情获取(重新生成订单签名&时间戳)", notes = "待支付订单详情获取", httpMethod = "GET")
    public BusinessData<TravelOrder> waitPay(
            @ApiParam(required = true) @NotBlank(message = "订单id不能为空") @RequestParam(value = "order_id", required = false) String orderId
    ) {
        return new BusinessData<>(travelOrderAPI.waitPayOrder(orderId));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/shop_approval", method = RequestMethod.POST)
    @ApiOperation(value = "商家审核完成", notes = "商家审核完成", httpMethod = "POST")
    public BusinessData approval(
            @ApiParam(required = true) @NotBlank(message = "订单id不能为空") @RequestParam(value = "order_id", required = false) String orderId
    ) {
        travelOrderAPI.confirmShopApproval(orderId);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/batch_shop_approval", method = RequestMethod.POST)
    @ApiOperation(value = "商家批量审核完成", notes = "商家批量审核完成", httpMethod = "POST")
    public BusinessData batchApproval(
            @ApiParam(required = true) @NotNull(message = "订单id列表不能为空") @RequestBody(required = false) BatchOrderRequestDTO batchOrderRequestDTO
    ) {
        travelOrderAPI.batchconfirmShopApproval(batchOrderRequestDTO.getOrderIds());
        return new BusinessData(BusinessError.SUCCESS);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/settled", method = RequestMethod.POST)
    @ApiOperation(value = "结算", notes = "结算", httpMethod = "POST")
    public BusinessData settled(
            @ApiParam(required = true) @NotBlank(message = "订单id不能为空") @RequestParam(value = "order_id", required = false) String orderId
    ) {
        travelOrderAPI.orderSettled(orderId);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/batch_settled", method = RequestMethod.POST)
    @ApiOperation(value = "批量结算", notes = "批量结算(多个id间使用逗号分割)", httpMethod = "POST")
    public BusinessData batchSettled(
            @ApiParam(required = true) @NotNull(message = "订单id列表不能为空") @RequestBody(required = false) BatchOrderRequestDTO batchOrderRequestDTO
    ) {
        travelOrderAPI.orderBatchSettled(batchOrderRequestDTO.getOrderIds());
        return new BusinessData(BusinessError.SUCCESS);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/refund", method = RequestMethod.POST)
    @ApiOperation(value = "退款", notes = "退款", httpMethod = "POST")
    public BusinessData refund(
            @ApiParam(required = true) @NotBlank(message = "订单id不能为空") @RequestParam(value = "order_id", required = false) String orderId
    ) {
        travelOrderAPI.refund(orderId);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/console_cancel", method = RequestMethod.POST)
    @ApiOperation(value = "后台取消订单", notes = "后台取消订单", httpMethod = "POST")
    public BusinessData consoleCancel(
            @ApiParam(required = true) @NotBlank(message = "订单id不能为空") @RequestParam(value = "order_id", required = false) String orderId
    ) {
        travelOrderAPI.orderConsoleCancel(orderId);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/shop_cancel", method = RequestMethod.POST)
    @ApiOperation(value = "商家取消订单", notes = "商家取消订单", httpMethod = "POST")
    public BusinessData shopCancel(
            @ApiParam(required = true) @NotBlank(message = "订单id不能为空") @RequestParam(value = "order_id", required = false) String orderId
    ) {
        travelOrderAPI.orderShopCancel(orderId);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/fake_user_cancel", method = RequestMethod.POST)
    @ApiOperation(value = "用户取消订单", notes = "用户取消订单", httpMethod = "POST")
    public BusinessData fakeUserCancel(
            @ApiParam(required = true) @NotBlank(message = "订单id不能为空") @RequestParam(value = "order_id", required = false) String orderId
    ) {
        TravelOrder travelOrder = travelOrderManager.findById(orderId);
        if (ESObjectUtils.isNull(travelOrder)) {
            throw new BusinessException("订单不存在");
        }
        if (ESObjectUtils.isNull(travelOrder.getOrderSource()) ||
                travelOrder.getOrderSource() != OrderSource.USER) {
            throw new BusinessException("非法来源订单或非用户订单");
        }
        Long currentTime = ESDateTimeUtils.getCurrentTimestamp();
        travelOrder.setOrderStatus(OrderStatus.CANCEL_USER);
        travelOrder.setWarningStatus(WarningStatus.NORMAL);
        travelOrder.setCancelTime(currentTime);
        travelOrder = travelOrderManager.save(travelOrder);
        travelOrderHistoryManager.save(new TravelOrderHistory(travelOrder.getId(), travelOrder.getOrderStatus(), "用户取消订单"));
        // 已付款订单退款检查优惠券使用情况执行回退操作
        if (ESStringUtils.isNotBlank(travelOrder.getCouponId()) && travelOrder.getCouponPrice() > 0) {
            Coupon coupon = couponManager.findById(travelOrder.getCouponId());
            if (ESObjectUtils.isNotNull(coupon) && ESStringUtils.isNotBlank(coupon.getUserId())) {
                coupon.setHasUsed(false);
                couponManager.save(coupon);
            }
        }
        return new BusinessData(BusinessError.SUCCESS);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/user_cancel", method = RequestMethod.POST)
    @ApiOperation(value = "用户取消订单", notes = "用户取消订单", httpMethod = "POST")
    public BusinessData userCancel(
            @ApiParam(required = true) @NotBlank(message = "订单id不能为空") @RequestParam(value = "order_id", required = false) String orderId
    ) {
        travelOrderAPI.orderUserCancel(orderId);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/change_status", method = RequestMethod.POST)
    @ApiOperation(value = "更新用户订单状态", notes = "更新用户订单状态(只能修改为ON_THE_WAY||DONE)", httpMethod = "POST")
    public BusinessData changeOrderStatus(
            @ApiParam(required = true) @NotBlank(message = "订单id不能为空") @RequestParam(value = "order_id", required = false) String orderId,
            @ApiParam(required = false) @RequestParam(value = "express_number", required = false) String expressNumber,
            @ApiParam(required = true) @NotNull(message = "订单状态不能为空") @RequestParam(value = "order_status", required = false) OrderStatus orderStatus
    ) {
        travelOrderAPI.changeOrderStatus(orderId, expressNumber, orderStatus);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/change_express_number", method = RequestMethod.POST)
    @ApiOperation(value = "更新用户快递单号", notes = "更新用户快递单号", httpMethod = "POST")
    public BusinessData changeExpressNumber(
            @ApiParam(required = true) @NotBlank(message = "订单id不能为空") @RequestParam(value = "order_id", required = false) String orderId,
            @ApiParam(required = true) @NotBlank(message = "快递单号不能为空") @RequestParam(value = "express_number", required = false) String expressNumber
    ) {
        travelOrderAPI.changeExpressNumber(orderId, expressNumber);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/order_page", method = RequestMethod.POST)
    @ApiOperation(value = "订单分页", notes = "订单分页(1-下单时间；2-订单开始时间；)", httpMethod = "POST")
    public BusinessData<PageResponseData<TravelOrder>> orderPage(
            @ApiParam(required = false) @RequestParam(value = "scene", required = false) List<Scene> sceneList,
            @ApiParam(required = false) @RequestParam(value = "order_status", required = false) OrderStatus orderStatus,
            @ApiParam(required = false) @RequestParam(value = "order_source", required = false) OrderSource orderSource,
            @ApiParam(required = false) @RequestParam(value = "shop_id", required = false) String shopId,
            @ApiParam(required = false) @RequestParam(value = "source_shop_id", required = false) String sourceShopId,
            @ApiParam(required = false) @RequestParam(value = "source_driver_user_id", required = false) String sourceDriverUserId,
            @ApiParam(required = false) @RequestParam(value = "driver_user_id", required = false) String driverUserId,
            @ApiParam(required = false) @RequestParam(value = "type", required = false) Integer type,
            @ApiParam(required = false) @RequestParam(value = "start", required = false) Long start,
            @ApiParam(required = false) @RequestParam(value = "end", required = false) Long end,
            @ApiParam(required = false) @RequestParam(value = "value", required = false) String value,
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @RequestBody(required = false) PageRequestData pageRequestData
    ) {
        return new BusinessData<>(
                travelOrderManager.orderPage(sceneList, orderStatus, orderSource, shopId, sourceShopId, sourceDriverUserId, driverUserId, type, start, end, value, pageRequestData)
        );
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/export_order_page", method = RequestMethod.GET)
    @ApiOperation(value = "导出订单分页(最多1000条)", notes = "导出订单分页(1-下单时间；2-订单开始时间；)", httpMethod = "GET")
    public BusinessData exportOrderPage(
            @ApiParam(required = false) @RequestParam(value = "scene", required = false) List<Scene> sceneList,
            @ApiParam(required = false) @RequestParam(value = "order_status", required = false) OrderStatus orderStatus,
            @ApiParam(required = false) @RequestParam(value = "order_source", required = false) OrderSource orderSource,
            @ApiParam(required = false) @RequestParam(value = "shop_id", required = false) String shopId,
            @ApiParam(required = false) @RequestParam(value = "source_shop_id", required = false) String sourceShopId,
            @ApiParam(required = false) @RequestParam(value = "source_driver_user_id", required = false) String sourceDriverUserId,
            @ApiParam(required = false) @RequestParam(value = "driver_user_id", required = false) String driverUserId,
            @ApiParam(required = false) @RequestParam(value = "type", required = false) Integer type,
            @ApiParam(required = false) @RequestParam(value = "start", required = false) Long start,
            @ApiParam(required = false) @RequestParam(value = "end", required = false) Long end,
            @ApiParam(required = false) @RequestParam(value = "value", required = false) String value,
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        travelOrderManager.exportOrderPage(sceneList, orderStatus, orderSource, shopId, sourceShopId, sourceDriverUserId, driverUserId, type, start, end, value, request, response);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/settled_page", method = RequestMethod.POST)
    @ApiOperation(value = "结算订单分页", notes = "结算订单分页(1-结算时间；2-下单时间；)", httpMethod = "POST")
    public BusinessData<PageResponseData<TravelOrder>> settledPage(
            @ApiParam(required = false) @RequestParam(value = "settled_type", required = false) Integer settledType,
            @ApiParam(required = false) @RequestParam(value = "order_source", required = false) OrderSource orderSource,
            @ApiParam(required = false) @RequestParam(value = "shop_id", required = false) String shopId,
            @ApiParam(required = false) @RequestParam(value = "source_shop_id", required = false) String sourceShopId,
            @ApiParam(required = false) @RequestParam(value = "order_id", required = false) String orderId,
            @ApiParam(required = false) @RequestParam(value = "value", required = false) String value,
            @ApiParam(required = false) @RequestParam(value = "target_place", required = false)  String targetPlace,
            @ApiParam(required = false) @RequestParam(value = "settled_start", required = false) Long settledStart,
            @ApiParam(required = false) @RequestParam(value = "settled_end", required = false) Long settledEnd,
            @ApiParam(required = false) @RequestParam(value = "create_start", required = false) Long createStart,
            @ApiParam(required = false) @RequestParam(value = "create_end", required = false) Long createEnd,
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @RequestBody(required = false) PageRequestData pageRequestData
    ) {
        if (ESObjectUtils.isNull(orderSource)) {
            orderSource = OrderSource.SHOP;
        }
        return new BusinessData<>(
                travelOrderManager.settledPage(settledType, orderSource, shopId, sourceShopId, orderId, value, targetPlace, settledStart, settledEnd, createStart, createEnd, pageRequestData)
        );
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/export_settled_page", method = RequestMethod.GET)
    @ApiOperation(value = "导出结算订单分页(最多1000条)", notes = "导出结算订单分页(1-结算时间；2-下单时间；)", httpMethod = "GET")
    public BusinessData exportSettledPage(
            @ApiParam(required = false) @RequestParam(value = "settled_type", required = false) Integer settledType,
            @ApiParam(required = false) @RequestParam(value = "order_source", required = false) OrderSource orderSource,
            @ApiParam(required = false) @RequestParam(value = "shop_id", required = false) String shopId,
            @ApiParam(required = false) @RequestParam(value = "source_shop_id", required = false) String sourceShopId,
            @ApiParam(required = false) @RequestParam(value = "order_id", required = false) String orderId,
            @ApiParam(required = false) @RequestParam(value = "value", required = false) String value,
            @ApiParam(required = false) @RequestParam(value = "target_place", required = false)  String targetPlace,
            @ApiParam(required = false) @RequestParam(value = "settled_start", required = false) Long settledStart,
            @ApiParam(required = false) @RequestParam(value = "settled_end", required = false) Long settledEnd,
            @ApiParam(required = false) @RequestParam(value = "create_start", required = false) Long createStart,
            @ApiParam(required = false) @RequestParam(value = "create_end", required = false) Long createEnd,
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        if (ESObjectUtils.isNull(orderSource)) {
            orderSource = OrderSource.SHOP;
        }
        travelOrderManager.exportSettledPage(settledType, orderSource, shopId, sourceShopId, orderId, value, targetPlace, settledStart, settledEnd, createStart, createEnd, request, response);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/warning_page", method = RequestMethod.POST)
    @ApiOperation(value = "预警订单分页", notes = "预警订单分页", httpMethod = "POST")
    public BusinessData<WarningOrderResponseDTO> warningPage(
            @ApiParam(required = false) @RequestParam(value = "warning_status", required = false) WarningStatus warningStatus,
            @ApiParam(required = false) @RequestParam(value = "username", required = false) String username,
            @ApiParam(required = false) @RequestParam(value = "order_id", required = false) String orderId,
            @ApiParam(required = false) @RequestParam(value = "start_place", required = false) String startPlace,
            @ApiParam(required = false) @RequestParam(value = "target_place", required = false) String targetPlace,
            @ApiParam(required = false) @RequestParam(value = "create_start", required = false) Long createStart,
            @ApiParam(required = false) @RequestParam(value = "create_end", required = false) Long createEnd,
            @ApiParam(required = false) @RequestParam(value = "execute_start", required = false) Long executeStart,
            @ApiParam(required = false) @RequestParam(value = "execute_end", required = false) Long executeEnd,
            @ApiParam(required = false) @RequestParam(value = "low_price", required = false) Double lowPrice,
            @ApiParam(required = false) @RequestParam(value = "high_price", required = false) Double highPrice,
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @RequestBody(required = false) PageRequestData pageRequestData
    ) {
        PageResponseData<TravelOrder> travelOrderPageResponseData =
                travelOrderManager.warningPage(warningStatus, username, orderId, startPlace, targetPlace, createStart, createEnd, executeStart, executeEnd, lowPrice, highPrice, pageRequestData);
        Long warningCount = travelOrderManager.countOrder(WarningStatus.WARNING);
        Long urgentCount = travelOrderManager.countOrder(WarningStatus.URGENT);
        return new BusinessData<>(
                new WarningOrderResponseDTO(
                        travelOrderPageResponseData,
                        warningCount + urgentCount,
                        warningCount,
                        urgentCount
                )
        );
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/export_warning_page", method = RequestMethod.GET)
    @ApiOperation(value = "导出预警订单分页(最多1000条)", notes = "预警订单分页", httpMethod = "GET")
    public BusinessData exportWarningPage(
            @ApiParam(required = false) @RequestParam(value = "warning_status", required = false) WarningStatus warningStatus,
            @ApiParam(required = false) @RequestParam(value = "username", required = false) String username,
            @ApiParam(required = false) @RequestParam(value = "order_id", required = false) String orderId,
            @ApiParam(required = false) @RequestParam(value = "start_place", required = false) String startPlace,
            @ApiParam(required = false) @RequestParam(value = "target_place", required = false) String targetPlace,
            @ApiParam(required = false) @RequestParam(value = "create_start", required = false) Long createStart,
            @ApiParam(required = false) @RequestParam(value = "create_end", required = false) Long createEnd,
            @ApiParam(required = false) @RequestParam(value = "execute_start", required = false) Long executeStart,
            @ApiParam(required = false) @RequestParam(value = "execute_end", required = false) Long executeEnd,
            @ApiParam(required = false) @RequestParam(value = "low_price", required = false) Double lowPrice,
            @ApiParam(required = false) @RequestParam(value = "high_price", required = false) Double highPrice,
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        travelOrderManager.exportWarningPage(warningStatus, username, orderId, startPlace, targetPlace, createStart, createEnd, executeStart, executeEnd, lowPrice, highPrice, request, response);
        return new BusinessData(BusinessError.SUCCESS);
    }
}
