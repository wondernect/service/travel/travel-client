package com.wondernect.travel.controller.base;

import com.wondernect.elements.authorize.context.interceptor.AuthorizeRoleType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeUserRole;
import com.wondernect.elements.common.error.BusinessError;
import com.wondernect.elements.common.response.BusinessData;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.travel.api.ConsumeAPI;
import com.wondernect.travel.dto.base.ConsumeRequestDTO;
import com.wondernect.travel.dto.base.WeChatConsumeResponseDTO;
import com.wondernect.travel.model.em.Scene;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: AddressController
 * Author: chenxun
 * Date: 2019/5/11 17:07
 * Description:
 */
@RestController
@RequestMapping(value = "/v5/travel/consume")
@Validated
@Api(tags = {"consume"}, description = "用车服务")
public class ConsumeController {

    @Autowired
    private ConsumeAPI consumeAPI;

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ApiOperation(value = "新增/编辑", notes = "新增/编辑", httpMethod = "POST")
    public BusinessData<WeChatConsumeResponseDTO> save(
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @Validated @RequestBody ConsumeRequestDTO consumeRequestDTO
    ) {
        return new BusinessData<>(consumeAPI.saveConsume(consumeRequestDTO));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    @ApiOperation(value = "删除", notes = "删除", httpMethod = "DELETE")
    public BusinessData delete(
            @ApiParam(required = true) @NotBlank(message = "请求参数不能为空") @RequestParam(value = "consume_id", required = false) String consumeId
    ) {
        consumeAPI.deleteConsume(consumeId);
        return new BusinessData(BusinessError.SUCCESS);
    }

    // @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    @ApiOperation(value = "列表", notes = "列表", httpMethod = "POST")
    public BusinessData<List<WeChatConsumeResponseDTO>> list(
            @ApiParam(required = true) @RequestParam(value = "city_id", required = false) String cityId,
            @ApiParam(required = true, allowableValues = "JIEJI, SONGJI, DAY_PRIVATE") @RequestParam(value = "scene", required = false) Scene scene,
            @ApiParam(required = true) @RequestParam(value = "taocan", required = false) String taocan,
            @ApiParam(required = true) @NotNull(message = "列表请求参数不能为空") @RequestBody(required = false) List<SortData> sortDataList
    ) {
        return new BusinessData<>(consumeAPI.list(cityId, scene, taocan, sortDataList));
    }

    @RequestMapping(value = "/service", method = RequestMethod.POST)
    @ApiOperation(value = "获取用车服务", notes = "获取用车服务", httpMethod = "POST")
    public BusinessData<WeChatConsumeResponseDTO> get(
            @ApiParam(required = true) @RequestParam(value = "city_id", required = false) String cityId,
            @ApiParam(required = true, allowableValues = "JIEJI, SONGJI, ORDER_SCENE") @NotNull(message = "用车场景不能为空") @RequestParam(value = "scene", required = false) Scene scene,
            @ApiParam(required = true) @RequestParam(value = "taocan", required = false) String taocan,
            @ApiParam(required = false) @RequestParam(value = "user_id", required = false) String userId
    ) {
        return new BusinessData<>(consumeAPI.getService(cityId, scene, taocan, userId));
    }
}
