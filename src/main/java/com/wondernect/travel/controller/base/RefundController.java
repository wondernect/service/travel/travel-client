package com.wondernect.travel.controller.base;

import com.wondernect.elements.authorize.context.interceptor.AuthorizeRoleType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeUserRole;
import com.wondernect.elements.common.response.BusinessData;
import com.wondernect.elements.rdb.request.PageRequestData;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.travel.api.TravelOrderAPI;
import com.wondernect.travel.dto.base.RefundResponseDTO;
import com.wondernect.travel.manager.RefundConfigManager;
import com.wondernect.travel.manager.RefundOrderManager;
import com.wondernect.travel.model.RefundConfig;
import com.wondernect.travel.model.RefundOrder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: OrderStrategyController
 * Author: chenxun
 * Date: 2019/5/13 11:01
 * Description:
 */
@RestController
@RequestMapping(value = "/v5/travel/refund")
@Validated
@Api(tags = {"refund"}, description = "退款|退款配置服务")
public class RefundController {

    @Autowired
    private RefundConfigManager refundConfigManager;

    @Autowired
    private RefundOrderManager refundOrderManager;

    @Autowired
    private TravelOrderAPI travelOrderAPI;

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/config/save", method = RequestMethod.POST)
    @ApiOperation(value = "新增/编辑", notes = "新增/编辑", httpMethod = "POST")
    public BusinessData<RefundConfig> save(
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @Validated @RequestBody RefundConfig refundConfig
    ) {
        return new BusinessData<>(refundConfigManager.save(refundConfig));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/config/get", method = RequestMethod.GET)
    @ApiOperation(value = "获取接送机预约退款配置", notes = "获取接送机预约退款配置", httpMethod = "GET")
    public BusinessData<RefundConfig> get() {
        return new BusinessData<>(refundConfigManager.getOrderRefundConfig());
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/config/get_private", method = RequestMethod.GET)
    @ApiOperation(value = "获取包车退款配置", notes = "获取包车退款配置", httpMethod = "GET")
    public BusinessData<RefundConfig> getPrivate() {
        return new BusinessData<>(refundConfigManager.getPrivateRefundConfig());
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/page", method = RequestMethod.POST)
    @ApiOperation(value = "获取", notes = "获取", httpMethod = "POST")
    public BusinessData<PageResponseData<RefundOrder>> page(
            @ApiParam(required = false) @RequestParam(value = "has_refund", required = false) Boolean hasRefund,
            @ApiParam(required = false) @RequestParam(value = "value", required = false) String value,
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @RequestBody(required = false) PageRequestData pageRequestData
    ) {
        return new BusinessData<>(refundOrderManager.page(value, hasRefund, pageRequestData));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/refund_count", method = RequestMethod.GET)
    @ApiOperation(value = "获取退款手续费", notes = "获取退款手续费", httpMethod = "GET")
    public BusinessData<RefundResponseDTO> refundCount(
            @ApiParam(required = true) @NotBlank(message = "订单id不能为空") @RequestParam(value = "order_id", required = false) String orderId
    ) {
        return new BusinessData<>(travelOrderAPI.refundCount(orderId));
    }
}
