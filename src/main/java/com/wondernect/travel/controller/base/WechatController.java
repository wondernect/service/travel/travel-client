package com.wondernect.travel.controller.base;

import com.wondernect.elements.common.error.BusinessError;
import com.wondernect.elements.common.response.BusinessData;
import com.wondernect.travel.api.TravelOrderAPI;
import com.wondernect.travel.api.WechatAPI;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: AddressController
 * Author: chenxun
 * Date: 2019/5/11 17:07
 * Description:
 */
@RestController
@RequestMapping(value = "/v5/travel/weixin")
@Validated
@Api(tags = {"weixin_notify"}, description = "微信支付|退款通知")
public class WechatController {

    private static final Logger logger = LoggerFactory.getLogger(WechatController.class);

    @Autowired
    private WechatAPI wechatAPI;

    @Autowired
    private TravelOrderAPI travelOrderAPI;

    @RequestMapping(value = "/shop_code", method = RequestMethod.GET)
    @ApiOperation(value = "商家小程序码", notes = "商家小程序码", httpMethod = "GET")
    public void getWxacode(
            @ApiParam(required = true) @NotBlank(message = "场景不能为空") @RequestParam(value = "scene", required = false) String scene,
            HttpServletRequest request, HttpServletResponse response
    ) {
        wechatAPI.getWxacode(scene, response);
    }

    @RequestMapping(value = "/pay/fake_notify", method = RequestMethod.POST)
    @ApiOperation(value = "模拟微信支付通知", notes = "模拟微信支付通知", httpMethod = "POST")
    public BusinessData payNotify(
            @ApiParam(required = true) @NotBlank(message = "场景不能为空") @RequestParam(value = "order_id", required = false) String orderId
    ) {
        travelOrderAPI.confirmUserWechatPay(orderId);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @RequestMapping(value = "/pay/notify", method = RequestMethod.POST)
    @ApiOperation(value = "支付通知", notes = "支付通知", httpMethod = "POST", hidden = true)
    public String payNotify(HttpServletRequest request, HttpServletResponse response) {
        String resXml="";
        try{
            //
            InputStream is = request.getInputStream();
            //将InputStream转换成String
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();
            String line;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            resXml=sb.toString();
            System.out.println("微信支付异步通知请求包:" + resXml);
            return wechatAPI.payNotify(resXml);
        }catch (Exception e){
            logger.error("微信支付回调通知失败",e);
            return "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>" + "<return_msg><![CDATA[报文为空]]></return_msg>" + "</xml> ";
        }
    }

    @RequestMapping(value = "/refund/notify", method = RequestMethod.POST)
    @ApiOperation(value = "退款通知", notes = "退款通知", httpMethod = "POST", hidden = true)
    public String refundNotify(HttpServletRequest request, HttpServletResponse response) {
        String resXml="";
        try{
            //
            InputStream is = request.getInputStream();
            //将InputStream转换成String
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();
            String line;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            resXml=sb.toString();
            System.out.println("微信退款异步通知请求包:" + resXml);
            return wechatAPI.refundNotify(resXml);
        }catch (Exception e){
            logger.error("微信退款回调通知失败",e);
            return "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>" + "<return_msg><![CDATA[报文为空]]></return_msg>" + "</xml> ";
        }
    }
}
