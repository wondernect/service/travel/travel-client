package com.wondernect.travel.controller.base;

import com.wondernect.elements.authorize.context.interceptor.AuthorizeRoleType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeUserRole;
import com.wondernect.elements.common.error.BusinessError;
import com.wondernect.elements.common.response.BusinessData;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.travel.manager.OrderStrategyManager;
import com.wondernect.travel.model.OrderStrategy;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C), 2017-2020, wondernect.com
 * FileName: OrderStrategyController
 * Author: chenxun
 * Date: 2020/1/5 9:53
 * Description: 订单派送策略
 */
@RestController
@RequestMapping(value = "/v5/travel/order/strategy")
@Validated
@Api(tags = "派送策略服务", description = "派送策略服务")
public class OrderStrategyController {

    @Autowired
    private OrderStrategyManager orderStrategyManager;

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ApiOperation(value = "新增/编辑", notes = "新增/编辑", httpMethod = "POST")
    public BusinessData<OrderStrategy> save(
            @ApiParam(required = true) @NotNull(message = "策略不能为空") @Validated @RequestBody OrderStrategy orderStrategy
    ) {
        return new BusinessData<>(orderStrategyManager.save(orderStrategy));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    @ApiOperation(value = "删除", notes = "删除", httpMethod = "DELETE")
    public BusinessData delete(
            @ApiParam(required = true) @NotBlank(message = "策略id不能为空") @RequestParam(value = "id", required = false) String strategyId
    ) {
        orderStrategyManager.deleteById(strategyId);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    @ApiOperation(value = "列表", notes = "列表", httpMethod = "POST")
    public BusinessData<List<OrderStrategy>> page(
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @RequestBody(required = false) List<SortData> sortDataList
    ) {
        if (CollectionUtils.isEmpty(sortDataList)) {
            sortDataList = new ArrayList<>();
            sortDataList.add(new SortData("weight", "DESC"));
        }
        return new BusinessData<>(orderStrategyManager.findAll(sortDataList));
    }
}
