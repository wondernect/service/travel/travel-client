package com.wondernect.travel.controller.base;

import com.wondernect.elements.authorize.context.interceptor.AuthorizeRoleType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeUserRole;
import com.wondernect.elements.common.response.BusinessData;
import com.wondernect.elements.rdb.request.PageRequestData;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.travel.dto.base.DriverSettledOrderResponseDTO;
import com.wondernect.travel.dto.base.DriverSettledRequestDTO;
import com.wondernect.travel.manager.TravelOrderManager;
import com.wondernect.travel.model.TravelOrder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Copyright (C), 2017-2020, wondernect.com
 * FileName: DriverOrderController
 * Author: chenxun
 * Date: 2020/1/5 9:58
 * Description: 司机订单服务
 */
@RestController
@RequestMapping(value = "/v5/travel/order/driver")
@Validated
@Api(tags = "司机订单服务", description = "司机订单服务")
public class DriverOrderController {

    @Autowired
    private TravelOrderManager travelOrderManager;

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/accepted_list", method = RequestMethod.POST)
    @ApiOperation(value = "司机已接单列表", notes = "司机已接单列表", httpMethod = "POST")
    public BusinessData<List<TravelOrder>> driverAcceptedList(
            @ApiParam(required = true) @NotBlank(message = "司机id不能为空") @RequestParam(value = "driver_user_id", required = false) String driverUserId
    ) {
        return new BusinessData<>(travelOrderManager.findAcceptedOrderList(driverUserId));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/now", method = RequestMethod.POST)
    @ApiOperation(value = "司机立即叫车(抢单)", notes = "司机立即叫车(抢单)", httpMethod = "POST")
    public BusinessData<List<TravelOrder>> driverNowList(
            @ApiParam(required = true) @NotBlank(message = "司机id不能为空") @RequestParam(value = "driver_user_id", required = false) String driverUserId
    ) {
        return new BusinessData<>(travelOrderManager.findNowAndOrder(driverUserId));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/order_list", method = RequestMethod.POST)
    @ApiOperation(value = "司机待接单列表", notes = "司机待接单列表", httpMethod = "POST")
    public BusinessData<List<TravelOrder>> driverOrderList(
            @ApiParam(required = true) @NotBlank(message = "司机id不能为空") @RequestParam(value = "driver_user_id", required = false) String driverUserId
    ) {
        return new BusinessData<>(travelOrderManager.findOrder(driverUserId));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/task_list", method = RequestMethod.POST)
    @ApiOperation(value = "司机任务列表", notes = "司机任务列表", httpMethod = "POST")
    public BusinessData<PageResponseData<TravelOrder>> driverTaskList(
            @ApiParam(required = true) @NotBlank(message = "司机id不能为空") @RequestParam(value = "driver_user_id", required = false) String driverUserId,
            @ApiParam(required = true) @NotNull(message = "分页请求参数不能为空") @RequestBody(required = false) PageRequestData pageRequestData
    ) {
        List<SortData> sortDataList = pageRequestData.getSortDataList();
        if (CollectionUtils.isEmpty(sortDataList)) {
            sortDataList = new ArrayList<>();
            sortDataList.add(new SortData("startTime", "DESC"));
            pageRequestData.setSortDataList(sortDataList);
        }
        return new BusinessData<>(travelOrderManager.driverOrderList(driverUserId, pageRequestData));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/task_list_test", method = RequestMethod.POST)
    @ApiOperation(value = "司机任务列表测试", notes = "司机任务列表测试", httpMethod = "POST")
    public BusinessData<PageResponseData<TravelOrder>> driverTaskListTest(
            @ApiParam(required = true) @NotBlank(message = "司机id不能为空") @RequestParam(value = "driver_user_id", required = false) String driverUserId,
            @ApiParam(required = true) @NotNull(message = "分页请求参数不能为空") @RequestBody(required = false) PageRequestData pageRequestData
    ) {
        List<SortData> sortDataList = pageRequestData.getSortDataList();
        if (CollectionUtils.isEmpty(sortDataList)) {
            sortDataList = new ArrayList<>();
            sortDataList.add(new SortData("startTime", "DESC"));
            pageRequestData.setSortDataList(sortDataList);
        }
        return new BusinessData<>(travelOrderManager.driverOrderListTest(driverUserId, pageRequestData));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/settled_list", method = RequestMethod.POST)
    @ApiOperation(value = "司机已结算订单列表", notes = "司机已结算订单列表", httpMethod = "POST")
    public BusinessData<DriverSettledOrderResponseDTO> driverSettledOrderList(
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @RequestBody DriverSettledRequestDTO driverSettledRequestDTO
    ) {
        List<SortData> sortDataList = Arrays.asList(new SortData("createTime", "DESC"));
        List<TravelOrder> travelOrderList = travelOrderManager.driverSettledOrderList(
                driverSettledRequestDTO.getDriverUserId(),
                driverSettledRequestDTO.getStart(),
                driverSettledRequestDTO.getEnd(),
                sortDataList);
        Double total = 0d;
        if (CollectionUtils.isNotEmpty(travelOrderList)) {
            for (TravelOrder travelOrder : travelOrderList) {
                total += travelOrder.getPrice();
            }
        }
        return new BusinessData<>(
                new DriverSettledOrderResponseDTO(travelOrderList, total)
        );
    }
}
