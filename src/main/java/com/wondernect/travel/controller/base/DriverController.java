package com.wondernect.travel.controller.base;

import com.wondernect.elements.authorize.context.WondernectCommonContext;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeRoleType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeUserRole;
import com.wondernect.elements.common.error.BusinessError;
import com.wondernect.elements.common.response.BusinessData;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.common.utils.ESStringUtils;
import com.wondernect.elements.rdb.request.PageRequestData;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.travel.cache.model.DriverCurrent;
import com.wondernect.travel.api.DriverAPI;
import com.wondernect.travel.api.TravelOrderAPI;
import com.wondernect.travel.dto.base.DriverEvaluateResponseDTO;
import com.wondernect.travel.dto.base.DriverResponseDTO;
import com.wondernect.travel.model.DriverEvaluate;
import com.wondernect.travel.model.TravelOrder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: PriceController
 * Author: chenxun
 * Date: 2019/5/11 18:22
 * Description:
 */
@RestController
@RequestMapping(value = "/v5/travel/driver")
@Validated
@Api(tags = {"driver"}, description = "司机服务")
public class DriverController {

    @Autowired
    private WondernectCommonContext wondernectCommonContext;

    @Autowired
    private DriverAPI driverAPI;

    @Autowired
    private TravelOrderAPI travelOrderAPI;

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    @ApiOperation(value = "获取司机信息", notes = "获取司机信息", httpMethod = "GET")
    public BusinessData<DriverResponseDTO> get(
            @ApiParam(required = true) @NotBlank(message = "用户id不能为空") @RequestParam(value = "user_id", required = false) String userId
    ) {
        return new BusinessData<>(driverAPI.findByUserId(userId));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/sync", method = RequestMethod.POST)
    @ApiOperation(value = "同步司机信息", notes = "同步司机信息", httpMethod = "POST")
    public BusinessData sync(
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @Validated @RequestBody(required = false) DriverCurrent driverCurrent
    ) {
        driverAPI.saveDriverStatusAndLocation(driverCurrent);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/evaluate", method = RequestMethod.POST)
    @ApiOperation(value = "司机评价", notes = "司机评价", httpMethod = "POST")
    public BusinessData evaluate(
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @Validated @RequestBody(required = false) DriverEvaluate driverEvaluate
    ) {
        driverAPI.saveDriverEvaluate(driverEvaluate);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/list_driver", method = RequestMethod.POST)
    @ApiOperation(value = "司机列表", notes = "司机列表", httpMethod = "POST")
    public BusinessData<List<DriverResponseDTO>> listDriver(
            @ApiParam(required = false) @RequestParam(value = "value", required = false) String value,
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @RequestBody(required = false) List<SortData> sortDataList
    ) {
        return new BusinessData<>(driverAPI.listDriver(value, sortDataList));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/page_driver_evaluate", method = RequestMethod.POST)
    @ApiOperation(value = "司机评价分页", notes = "司机评价分页(1-下单时间；2-订单开始时间；)", httpMethod = "POST")
    public BusinessData<PageResponseData<DriverEvaluateResponseDTO>> pageDriverEvaluate(
            @ApiParam(required = false) @RequestParam(value = "driver_user_id", required = false) String driverUserId,
            @ApiParam(required = false) @RequestParam(value = "type", required = false) Integer type,
            @ApiParam(required = false) @RequestParam(value = "start", required = false) Long start,
            @ApiParam(required = false) @RequestParam(value = "end", required = false) Long end,
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @RequestBody(required = false) PageRequestData pageRequestData
    ) {
        return new BusinessData<>(driverAPI.pageDriverEvaluate(driverUserId, type, start, end, pageRequestData));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/accept_order", method = RequestMethod.POST)
    @ApiOperation(value = "司机接单", notes = "司机接单", httpMethod = "POST")
    public BusinessData acceptOrder(
            @ApiParam(required = true) @NotBlank(message = "订单不能为空") @RequestParam(value = "order_id", required = false) String orderId,
            @ApiParam(required = true) @NotBlank(message = "司机不能为空") @RequestParam(value = "driver_user_id", required = false) String driverUserId
    ) {
        travelOrderAPI.orderAccept(orderId, driverUserId);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/force_accept_order", method = RequestMethod.POST)
    @ApiOperation(value = "后台指定司机接单", notes = "后台指定司机接单", httpMethod = "POST")
    public BusinessData forceAcceptOrder(
            @ApiParam(required = true) @NotBlank(message = "订单不能为空") @RequestParam(value = "order_id", required = false) String orderId,
            @ApiParam(required = true) @NotBlank(message = "司机不能为空") @RequestParam(value = "driver_user_id", required = false) String driverUserId
    ) {
        travelOrderAPI.forceOrderAccept(orderId, driverUserId);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/execute_order", method = RequestMethod.POST)
    @ApiOperation(value = "司机到达约定地点,开始订单", notes = "司机到达约定地点,开始订单", httpMethod = "POST")
    public BusinessData executeOrder(
            @ApiParam(required = true) @NotBlank(message = "订单不能为空") @RequestParam(value = "order_id", required = false) String orderId,
            @ApiParam(required = true) @NotBlank(message = "司机不能为空") @RequestParam(value = "driver_user_id", required = false) String driverUserId
    ) {
        travelOrderAPI.orderExecute(orderId, driverUserId);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/done_order", method = RequestMethod.POST)
    @ApiOperation(value = "司机结束行程,完成订单", notes = "司机结束行程,完成订单", httpMethod = "POST")
    public BusinessData doneOrder(
            @ApiParam(required = true) @NotBlank(message = "司机不能为空") @RequestParam(value = "driver_user_id", required = false) String driverUserId,
            @ApiParam(required = true) @NotBlank(message = "订单不能为空") @RequestParam(value = "order_id", required = false) String orderId
    ) {
        travelOrderAPI.orderDone(orderId, driverUserId);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/change_order", method = RequestMethod.POST)
    @ApiOperation(value = "司机申请改派订单", notes = "司机申请改派订单", httpMethod = "POST")
    public BusinessData changeOrderDriver(
            @ApiParam(required = true) @NotBlank(message = "司机不能为空") @RequestParam(value = "driver_user_id", required = false) String driverUserId,
            @ApiParam(required = true) @NotBlank(message = "订单不能为空") @RequestParam(value = "order_id", required = false) String orderId,
            @ApiParam(required = false) @RequestParam(value = "description", required = false) String description
    ) {
        travelOrderAPI.changeOrderDriver(orderId, driverUserId, description);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/driver_cancel_page", method = RequestMethod.POST)
    @ApiOperation(value = "获取司机已取消订单", notes = "获取司机已取消订单", httpMethod = "POST")
    public BusinessData<List<TravelOrder>> getDriverCancelPage(
            @ApiParam(required = false) @RequestParam(value = "driver_user_id", required = false) String driverUserId,
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @RequestBody(required = false) List<SortData> sortDataList
    ) {
        if (ESStringUtils.isBlank(driverUserId)) {
            driverUserId = wondernectCommonContext.getAuthorizeData().getUserId();
        }
        if (ESObjectUtils.isNull(sortDataList)) {
            sortDataList = new ArrayList<>();
            sortDataList.add(new SortData("cancelTime", "DESC"));
        }
        return new BusinessData<>(travelOrderAPI.getDriverCancelList(driverUserId, sortDataList));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/driver_order_page", method = RequestMethod.POST)
    @ApiOperation(value = "获取司机历史订单", notes = "获取司机历史订单", httpMethod = "POST")
    public BusinessData<PageResponseData<TravelOrder>> getDriverOrderPage(
            @ApiParam(required = false) @RequestParam(value = "driver_user_id", required = false) String driverUserId,
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @RequestBody(required = false) PageRequestData pageRequestData
    ) {
        if (ESStringUtils.isBlank(driverUserId)) {
            driverUserId = wondernectCommonContext.getAuthorizeData().getUserId();
        }
        if (ESObjectUtils.isNull(pageRequestData.getSortDataList())) {
            List<SortData> sortDataList = new ArrayList<>();
            sortDataList.add(new SortData("createTime", "DESC"));
            pageRequestData.setSortDataList(sortDataList);
        }
        return new BusinessData<>(travelOrderAPI.getDriverOrderPage(driverUserId, pageRequestData));
    }
}
