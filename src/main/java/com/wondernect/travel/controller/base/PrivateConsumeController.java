package com.wondernect.travel.controller.base;

import com.wondernect.elements.authorize.context.WondernectCommonContext;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeRoleType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeUserRole;
import com.wondernect.elements.common.error.BusinessError;
import com.wondernect.elements.common.response.BusinessData;
import com.wondernect.elements.rdb.request.PageRequestData;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.travel.api.PrivateConsumeAPI;
import com.wondernect.travel.dto.base.PrivateConsumeRequestDTO;
import com.wondernect.travel.dto.base.WeChatPrivateConsumeResponseDTO;
import com.wondernect.travel.model.PrivateConsumeEvaluate;
import com.wondernect.travel.model.em.Scene;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: PrivateConsumeController
 * Author: chenxun
 * Date: 2019/5/11 17:07
 * Description:
 */
@RestController
@RequestMapping(value = "/v5/travel/private_consume")
@Validated
@Api(tags = {"包车服务"}, description = "包车服务")
public class PrivateConsumeController {

    @Autowired
    private PrivateConsumeAPI privateConsumeAPI;

    @Autowired
    private WondernectCommonContext wondernectCommonContext;

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/evaluate", method = RequestMethod.POST)
    @ApiOperation(value = "用户评价", notes = "用户评价", httpMethod = "POST")
    public BusinessData<PrivateConsumeEvaluate> evaluate(
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @Validated @RequestBody(required = false) PrivateConsumeEvaluate privateConsumeEvaluate
    ) {
        privateConsumeEvaluate.setUserId(wondernectCommonContext.getAuthorizeData().getUserId());
        return new BusinessData<>(privateConsumeAPI.evaluate(privateConsumeEvaluate));
    }

    // @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/evaluate_page", method = RequestMethod.POST)
    @ApiOperation(value = "用户评价分页", notes = "用户评价分页", httpMethod = "POST")
    public BusinessData<PageResponseData<PrivateConsumeEvaluate>> evaluatePage(
            @ApiParam(required = false) @RequestParam(value = "private_consume_id", required = false) String privateConsumeId,
            @ApiParam(required = false) @RequestParam(value = "status", required = false) Integer status,
            @ApiParam(required = true) @NotNull(message = "分页请求参数不能为空") @RequestBody(required = false) PageRequestData pageRequestData
    ) {
        return new BusinessData<>(privateConsumeAPI.evaluatePage(privateConsumeId, status, pageRequestData));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/evaluate_status_change", method = RequestMethod.POST)
    @ApiOperation(value = "用户评价审核", notes = "用户评价审核", httpMethod = "POST")
    public BusinessData evaluateStatusChange(
            @ApiParam(required = false) @RequestParam(value = "status", required = false) Integer status,
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @Validated @RequestBody(required = false) List<String> evaluateList
    ) {
        privateConsumeAPI.evaluateStatusChange(status, evaluateList);
        return new BusinessData<>(BusinessError.SUCCESS);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ApiOperation(value = "新增/编辑", notes = "新增/编辑", httpMethod = "POST")
    public BusinessData<WeChatPrivateConsumeResponseDTO> save(
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @Validated @RequestBody PrivateConsumeRequestDTO privateConsumeRequestDTO
    ) {
        return new BusinessData<>(privateConsumeAPI.savePrivateConsume(privateConsumeRequestDTO));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ApiOperation(value = "删除", notes = "删除", httpMethod = "POST")
    public BusinessData delete(
            @ApiParam(required = true) @NotBlank(message = "请求参数不能为空") @RequestParam(value = "private_consume_id", required = false) String privateConsumeId
    ) {
        privateConsumeAPI.deletePrivateConsume(privateConsumeId);
        return new BusinessData(BusinessError.SUCCESS);
    }

    // @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    @ApiOperation(value = "列表", notes = "列表", httpMethod = "POST")
    public BusinessData<List<WeChatPrivateConsumeResponseDTO>> list(
            @ApiParam(required = true) @RequestParam(value = "city_id", required = false) String cityId,
            @ApiParam(required = true) @RequestParam(value = "scene", required = false) List<Scene> sceneList,
            @ApiParam(required = true) @NotNull(message = "列表请求参数不能为空") @RequestBody(required = false) List<SortData> sortDataList
    ) {
        return new BusinessData<>(privateConsumeAPI.list(cityId, sceneList, sortDataList));
    }

    // @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/page", method = RequestMethod.POST)
    @ApiOperation(value = "分页", notes = "分页", httpMethod = "POST")
    public BusinessData<PageResponseData<WeChatPrivateConsumeResponseDTO>> page(
            @ApiParam(required = true) @RequestParam(value = "city_id", required = false) String cityId,
            @ApiParam(required = false) @RequestParam(value = "scene", required = false) List<Scene> sceneList,
            @ApiParam(required = false) @RequestParam(value = "value", required = false) String value,
            @ApiParam(required = false) @RequestParam(value = "start", required = false) Long start,
            @ApiParam(required = false) @RequestParam(value = "end", required = false) Long end,
            @ApiParam(required = true) @NotNull(message = "分页请求参数不能为空") @RequestBody(required = false) PageRequestData pageRequestData
    ) {
        return new BusinessData<>(privateConsumeAPI.page(cityId, sceneList, value, start, end, pageRequestData));
    }

    // @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    @ApiOperation(value = "获取", notes = "获取", httpMethod = "GET")
    public BusinessData<WeChatPrivateConsumeResponseDTO> get(
            @ApiParam(required = true) @NotBlank(message = "请求参数不能为空") @RequestParam(value = "private_consume_id", required = false) String privateConsumeId
    ) {
        return new BusinessData<>(privateConsumeAPI.getPrivateConsume(privateConsumeId));
    }
}
