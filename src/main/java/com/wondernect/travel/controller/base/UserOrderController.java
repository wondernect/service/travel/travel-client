package com.wondernect.travel.controller.base;

import com.wondernect.elements.authorize.context.WondernectCommonContext;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeRoleType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeUserRole;
import com.wondernect.elements.common.response.BusinessData;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.common.utils.ESStringUtils;
import com.wondernect.elements.rdb.request.PageRequestData;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.travel.dto.base.TravelOrderResponseDTO;
import com.wondernect.travel.manager.TravelOrderManager;
import com.wondernect.travel.model.em.OrderStatus;
import com.wondernect.travel.model.em.Scene;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Copyright (C), 2017-2020, wondernect.com
 * FileName: UserOrderController
 * Author: chenxun
 * Date: 2020/1/5 10:00
 * Description: 用户订单服务
 */
@RestController
@RequestMapping(value = "/v5/travel/order/user")
@Validated
@Api(tags = "用户订单服务", description = "用户订单服务")
public class UserOrderController {

    @Autowired
    private TravelOrderManager travelOrderManager;

    @Autowired
    private WondernectCommonContext wondernectCommonContext;

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/page", method = RequestMethod.POST)
    @ApiOperation(value = "用户行程", notes = "用户行程", httpMethod = "POST")
    public BusinessData<PageResponseData<TravelOrderResponseDTO>> userPage(
            @ApiParam(required = false) @RequestParam(value = "user_id", required = false) String userId,
            @ApiParam(required = false) @RequestParam(value = "scene", required = false) List<Scene> sceneList,
            @ApiParam(required = false) @RequestParam(value = "status", required = false) List<OrderStatus> statusList,
            @ApiParam(required = false) @RequestParam(value = "evaluate", required = false) Boolean evaluate,
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @RequestBody(required = false) PageRequestData pageRequestData
    ) {
        if (ESObjectUtils.isNull(pageRequestData.getSortDataList())) {
            List<SortData> sortData = new ArrayList<>();
            sortData.add(new SortData("createTime", "DESC"));
            pageRequestData.setSortDataList(sortData);
        }
        if (CollectionUtils.isEmpty(sceneList)) {
            sceneList = Arrays.asList(Scene.JIEJI, Scene.SONGJI, Scene.ORDER_SCENE, Scene.DAY_PRIVATE, Scene.ROAD_PRIVATE, Scene.JINGDIAN_PRIVATE, Scene.MEISHI_PRIVATE, Scene.BANSHOU_PRIVATE);
        }
        return new BusinessData<>(travelOrderManager.userPage(userId, sceneList, statusList, evaluate, pageRequestData));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/bill_page", method = RequestMethod.POST)
    @ApiOperation(value = "用户可/已开具发票行程信息(1-可开具发票；2-已开具发票；)", notes = "用户可/已开具发票行程信息(1-可开具发票；2-已开具发票；)", httpMethod = "POST")
    public BusinessData<PageResponseData<TravelOrderResponseDTO>> userBillPage(
            @ApiParam(required = false) @RequestParam(value = "user_id", required = false) String userId,
            @ApiParam(required = true) @NotNull(message = "类型不能为空") @RequestParam(value = "type", required = false) Integer type,
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @RequestBody(required = false) PageRequestData pageRequestData
    ) {
        if (ESStringUtils.isBlank(userId)) {
            userId = wondernectCommonContext.getAuthorizeData().getUserId();
        }
        if (ESObjectUtils.isNull(pageRequestData.getSortDataList())) {
            List<SortData> sortData = new ArrayList<>();
            sortData.add(new SortData("createTime", "DESC"));
            pageRequestData.setSortDataList(sortData);
        }
        return new BusinessData<>(travelOrderManager.billPage(userId, type, pageRequestData));
    }
}
