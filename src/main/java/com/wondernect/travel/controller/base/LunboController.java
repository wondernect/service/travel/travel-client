package com.wondernect.travel.controller.base;

import com.wondernect.elements.authorize.context.interceptor.AuthorizeRoleType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeUserRole;
import com.wondernect.elements.common.error.BusinessError;
import com.wondernect.elements.common.exception.BusinessException;
import com.wondernect.elements.common.response.BusinessData;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.rdb.request.PageRequestData;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.travel.manager.LunboManager;
import com.wondernect.travel.model.Lunbo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: LunboController
 * Author: chenxun
 * Date: 2019/5/11 17:07
 * Description:
 */
@RestController
@RequestMapping(value = "/v5/travel/lunbo")
@Validated
@Api(tags = {"首页轮播图"}, description = "首页轮播图")
public class LunboController {

    @Autowired
    private LunboManager lunboManager;

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ApiOperation(value = "新增/编辑", notes = "新增/编辑", httpMethod = "POST")
    public BusinessData<Lunbo> save(
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @Validated @RequestBody(required = false) Lunbo lunbo
    ) {
        return new BusinessData<>(lunboManager.save(lunbo));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ApiOperation(value = "删除", notes = "删除", httpMethod = "POST")
    public BusinessData delete(
            @ApiParam(required = true) @NotBlank(message = "轮播配置id不能为空") @RequestParam(value = "lunbo_id", required = false) String lunboId
    ) {
        if (ESObjectUtils.isNull(lunboManager.findById(lunboId))) {
            throw new BusinessException("轮播配置不存在");
        }
        lunboManager.deleteById(lunboId);
        return new BusinessData(BusinessError.SUCCESS);
    }

    // @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    @ApiOperation(value = "列表", notes = "列表", httpMethod = "POST")
    public BusinessData<List<Lunbo>> list(
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @RequestBody(required = false) List<SortData> sortDataList
    ) {
        return new BusinessData<>(lunboManager.findAll(sortDataList));
    }

    // @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/page", method = RequestMethod.POST)
    @ApiOperation(value = "分页", notes = "分页", httpMethod = "POST")
    public BusinessData<PageResponseData<Lunbo>> page(
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @RequestBody(required = false) PageRequestData pageRequestData
    ) {
        return new BusinessData<>(lunboManager.findAll(pageRequestData));
    }
}
