package com.wondernect.travel.controller.base;

import com.wondernect.elements.authorize.context.interceptor.AuthorizeRoleType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeUserRole;
import com.wondernect.elements.common.response.BusinessData;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.travel.manager.ShopManager;
import com.wondernect.travel.model.Shop;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: ShopController
 * Author: chenxun
 * Date: 2019-05-25 10:32
 * Description:
 */
@RestController
@RequestMapping(value = "/v5/travel/shop")
@Validated
@Api(tags = {"shop"}, description = "司机服务")
public class ShopController {

    @Autowired
    private ShopManager shopManager;

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.CONFIG)
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    @ApiOperation(value = "商家列表", notes = "商家列表", httpMethod = "POST")
    public BusinessData<List<Shop>> list(
            @ApiParam(required = false) @RequestParam(value = "name", required = false) String name,
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @RequestBody(required = false) List<SortData> sortDataList
    ) {
        return new BusinessData<>(shopManager.list(name, null, sortDataList));
    }
}
