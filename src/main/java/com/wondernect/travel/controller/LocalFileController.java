package com.wondernect.travel.controller;

import com.google.gson.Gson;
import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeRoleType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeUserRole;
import com.wondernect.elements.common.error.BusinessError;
import com.wondernect.elements.common.exception.BusinessException;
import com.wondernect.elements.common.response.BusinessData;
import com.wondernect.elements.common.utils.ESJSONObjectUtils;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.common.utils.ESStringUtils;
import com.wondernect.elements.rdb.request.PageRequestData;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.services.file.model.File;
import com.wondernect.services.file.service.FileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: LocalFileController
 * Author: chenxun
 * Date: 2019/3/28 16:12
 * Description: local file controller
 */
@RestController
@RequestMapping(value = "/v5/file/local")
@Validated
@Api(tags = {"local_file"}, description = "local文件服务")
public class LocalFileController {

    @Autowired
    @Qualifier("local")
    private FileService fileService;

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/qiniu_upload", method = RequestMethod.POST)
    @ApiOperation(value = "七牛上传文件", notes = "七牛上传文件", httpMethod = "POST")
    public BusinessData<File> qiniuUpload(
            @ApiParam(required = true) @RequestParam(value = "file", required = false) MultipartFile file
    ) {
        //构造一个带指定 Region 对象的配置类
        Configuration cfg = new Configuration(Region.huanan());
        cfg.useHttpsDomains = false;
        //...其他参数参考类注释
        UploadManager uploadManager = new UploadManager(cfg);
        //...生成上传凭证，然后准备上传
        String accessKey = "PQcXwfhI7UTQygepM-FtaqYryE012sSwRz_5Qbfq";
        String secretKey = "B7eJUShsuH4SB-JFJcnDkXrz5mGpuFSBjVzzVr8q";
        String bucket = "kingtrip";
        //默认不指定key的情况下，以文件内容的hash值作为文件名
        String key = null;
        Auth auth = Auth.create(accessKey, secretKey);
        String upToken = auth.uploadToken(bucket);
        String fileName = "";
        try {
            Response response = uploadManager.put(file.getInputStream(), key, upToken,null, null);
            //解析上传成功的结果
            DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
            fileName = putRet.key;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        if (ESStringUtils.isBlank(fileName)) {
            throw new BusinessException("文件上传失败，请重试");
        }
        File fileResponse = new File();
        fileResponse.setId(fileName);
        fileResponse.setPath("http://qiniu.kingtrip.vip/" + fileName);
        return new BusinessData<>(fileResponse);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/qiniu_wechat_upload", method = RequestMethod.POST)
    @ApiOperation(value = "七牛上传文件(微信小程序)", notes = "七牛上传文件(微信小程序)", httpMethod = "POST")
    public BusinessData<File> wechatUpload(
            @ApiParam(required = true) @NotBlank(message = "文件获取标识不能为空") @RequestParam(value = "file_key", required = false) String fileKey,
            HttpServletRequest httpServletRequest
    ) {
        MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) httpServletRequest;
        MultipartFile file = multipartHttpServletRequest.getFile(fileKey);
        //构造一个带指定 Region 对象的配置类
        Configuration cfg = new Configuration(Region.huanan());
        cfg.useHttpsDomains = false;
        //...其他参数参考类注释
        UploadManager uploadManager = new UploadManager(cfg);
        //...生成上传凭证，然后准备上传
        String accessKey = "PQcXwfhI7UTQygepM-FtaqYryE012sSwRz_5Qbfq";
        String secretKey = "B7eJUShsuH4SB-JFJcnDkXrz5mGpuFSBjVzzVr8q";
        String bucket = "kingtrip";
        //默认不指定key的情况下，以文件内容的hash值作为文件名
        String key = null;
        Auth auth = Auth.create(accessKey, secretKey);
        String upToken = auth.uploadToken(bucket);
        String fileName = "";
        try {
            Response response = uploadManager.put(file.getInputStream(), key, upToken,null, null);
            //解析上传成功的结果
            DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
            fileName = putRet.key;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        if (ESStringUtils.isBlank(fileName)) {
            throw new BusinessException("文件上传失败，请重试");
        }
        File fileResponse = new File();
        fileResponse.setId(fileName);
        fileResponse.setPath("http://qiniu.kingtrip.vip/" + fileName);
        return new BusinessData<>(fileResponse);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @ApiOperation(value = "local_file_upload", notes = "上传文件", httpMethod = "POST")
    public BusinessData<File> upload(
            @ApiParam(required = true, allowableValues = "IMAGE, IMAGE_FILE, VOICE, VIDEO, FILE") @NotBlank(message = "文件类型不能为空") @RequestParam(value = "file_type", required = false) String fileType,
            @ApiParam(required = true) @RequestParam(value = "file_meta_data", required = false) String fileMetaData,
            @ApiParam(required = true) @RequestParam(value = "file", required = false) MultipartFile file
    ) {
        Map<String, String> metaData = null;
        if (ESStringUtils.isNotRealEmpty(fileMetaData)) {
            metaData = ESJSONObjectUtils.jsonStringToMapClassObject(fileMetaData, String.class);
        }
        return new BusinessData<>(fileService.upload(file, fileType, metaData));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    @ApiOperation(value = "local_file_delete", notes = "删除文件及文件信息", httpMethod = "DELETE")
    public BusinessData delete(
            @ApiParam(required = true) @NotBlank(message = "文件id不能为空") @RequestParam(value = "file_id", required = false) String fileId
    ) {
        fileService.delete(fileId);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    @ApiOperation(value = "local_file_get", notes = "获取文件信息", httpMethod = "GET")
    public BusinessData<File> findByFileId(
            @ApiParam(required = true) @NotBlank(message = "文件id不能为空") @RequestParam(value = "file_id", required = false) String fileId
    ) {
        return new BusinessData<>(fileService.findByFileId(fileId));
    }

    @RequestMapping(value = "/page", method = RequestMethod.POST)
    @ApiOperation(value = "local_file_get_user_page", notes = "获取用户文件分页", httpMethod = "POST")
    public BusinessData<PageResponseData<File>> findAllByApplicationIdAndUserId(
            @ApiParam(required = false) @RequestParam(value = "user_id", required = false) String userId,
            @ApiParam(required = true) @NotNull(message = "分页请求参数不能为空") @RequestBody(required = false) PageRequestData pageRequestData
    ) {
        return new BusinessData<>(fileService.findAllByUserId(userId, pageRequestData));
    }
}
