package com.wondernect.travel.api;

import com.wondernect.elements.common.exception.BusinessException;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.common.utils.ESStringUtils;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.travel.business.chexing.manager.ChexingManager;
import com.wondernect.travel.business.chexing.model.Chexing;
import com.wondernect.travel.business.zuowei.manager.ZuoweiManager;
import com.wondernect.travel.business.zuowei.model.Zuowei;
import com.wondernect.travel.dto.base.ConsumeRequestDTO;
import com.wondernect.travel.dto.base.ConsumeCarLevelResponseDTO;
import com.wondernect.travel.dto.base.WeChatConsumeResponseDTO;
import com.wondernect.travel.manager.*;
import com.wondernect.travel.model.Consume;
import com.wondernect.travel.model.ConsumeCarLevel;
import com.wondernect.travel.model.PriceStrategy;
import com.wondernect.travel.model.em.Scene;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: ConsumeAPI
 * Author: chenxun
 * Date: 2019-05-12 13:39
 * Description:
 */
@Service
public class ConsumeAPI {

    @Autowired
    private ConsumeManager consumeManager;

    @Autowired
    private ConsumeCarLevelManager consumeCarLevelManager;

    @Autowired
    private ZuoweiManager zuoweiManager;

    @Autowired
    private ChexingManager chexingManager;

    @Autowired
    private PriceStrategyManager priceStrategyManager;

    @Autowired
    private TravelOrderManager travelOrderManager;

    @Transactional
    public WeChatConsumeResponseDTO saveConsume(ConsumeRequestDTO consumeRequestDTO) {
        Consume consume;
        if (ESStringUtils.isNotBlank(consumeRequestDTO.getConsume().getId())) {
            // 更新
            consume = consumeManager.findById(consumeRequestDTO.getConsume().getId());
            if (ESObjectUtils.isNull(consume)) {
                throw new BusinessException("用车服务不存在");
            }
            consumeCarLevelManager.deleteAllByConsumeId(consumeRequestDTO.getConsume().getId());
            consume.setCityId(consumeRequestDTO.getConsume().getCityId());
            consume.setScene(consumeRequestDTO.getConsume().getScene());
            consume.setCommonScene(consumeRequestDTO.getConsume().getCommonScene());
            consume.setTaocan(consumeRequestDTO.getConsume().getTaocan());
            consume.setTitle(consumeRequestDTO.getConsume().getTitle());
            consume.setShowPrice(consumeRequestDTO.getConsume().getShowPrice());
            consume.setDescription(consumeRequestDTO.getConsume().getDescription());
            consume.setContain(consumeRequestDTO.getConsume().getContain());
            consume.setUncontain(consumeRequestDTO.getConsume().getUncontain());
        } else {
            // 新增
            consume = new Consume(
                    consumeRequestDTO.getConsume().getCityId(),
                    consumeRequestDTO.getConsume().getScene(),
                    consumeRequestDTO.getConsume().getCommonScene(),
                    consumeRequestDTO.getConsume().getTaocan(),
                    consumeRequestDTO.getConsume().getTitle(),
                    consumeRequestDTO.getConsume().getShowPrice(),
                    consumeRequestDTO.getConsume().getDescription(),
                    consumeRequestDTO.getConsume().getContain(),
                    consumeRequestDTO.getConsume().getUncontain()
            );
        }
        consume = consumeManager.save(consume);
        List<ConsumeCarLevelResponseDTO> consumeCarLevelResponseDTOList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(consumeRequestDTO.getCarLevels())) {
            for (ConsumeCarLevel carLevel : consumeRequestDTO.getCarLevels()) {
                carLevel.setConsumeId(consume.getId());
                carLevel = consumeCarLevelManager.save(carLevel);
                Zuowei zuowei = zuoweiManager.findById(carLevel.getZuoweiId());
                Chexing chexing = chexingManager.findById(carLevel.getChexingId());
                PriceStrategy priceStrategy = null;
                if (ESStringUtils.isNotBlank(carLevel.getPriceStrategyId())) {
                    priceStrategy = priceStrategyManager.findById(carLevel.getPriceStrategyId());
                }
                consumeCarLevelResponseDTOList.add(
                        new ConsumeCarLevelResponseDTO(
                                zuowei,
                                chexing,
                                carLevel.getPrice(),
                                carLevel.getPriceStrategyId(),
                                ESObjectUtils.isNotNull(priceStrategy) ? priceStrategy.getStrategyName() : null,
                                ESObjectUtils.isNotNull(priceStrategy) ? priceStrategy.getPlanDescription() : null
                        )
                );
            }
        }
        return new WeChatConsumeResponseDTO(consume, consumeCarLevelResponseDTOList, 0L);
    }

    @Transactional
    public void deleteConsume(String consumeId) {
        Consume consume = consumeManager.findById(consumeId);
        if (ESObjectUtils.isNull(consume)) {
            throw new BusinessException("用车服务不存在");
        }
        consumeCarLevelManager.deleteAllByConsumeId(consumeId);
        consumeManager.deleteById(consumeId);
    }

    public List<WeChatConsumeResponseDTO> list(String cityId, Scene scene, String taocan, List<SortData> sortDataList) {
        List<WeChatConsumeResponseDTO> weChatConsumeResponseDTOList = new ArrayList<>();
        List<Consume> consumeList;
        if (ESObjectUtils.isNotNull(scene)) {
            consumeList = consumeManager.findAllByScene(cityId, scene, taocan, sortDataList);
        } else {
            consumeList = consumeManager.findAll(sortDataList);
        }
        if (CollectionUtils.isNotEmpty(consumeList)) {
            for (Consume consume : consumeList) {
                List<ConsumeCarLevelResponseDTO> consumeCarLevelResponseDTOList = new ArrayList<>();
                List<ConsumeCarLevel> carLevels = consumeCarLevelManager.findAllByConsumeId(consume.getId(), new ArrayList<>());
                if (CollectionUtils.isNotEmpty(carLevels)) {
                    for (ConsumeCarLevel carLevel : carLevels) {
                        Zuowei zuowei = zuoweiManager.findById(carLevel.getZuoweiId());
                        Chexing chexing = chexingManager.findById(carLevel.getChexingId());
                        PriceStrategy priceStrategy = null;
                        if (ESStringUtils.isNotBlank(carLevel.getPriceStrategyId())) {
                            priceStrategy = priceStrategyManager.findById(carLevel.getPriceStrategyId());
                        }
                        consumeCarLevelResponseDTOList.add(
                                new ConsumeCarLevelResponseDTO(
                                        zuowei,
                                        chexing,
                                        carLevel.getPrice(),
                                        carLevel.getPriceStrategyId(),
                                        ESObjectUtils.isNotNull(priceStrategy) ? priceStrategy.getStrategyName() : null,
                                        ESObjectUtils.isNotNull(priceStrategy) ? priceStrategy.getPlanDescription() : null
                                )
                        );
                    }
                }
                weChatConsumeResponseDTOList.add(new WeChatConsumeResponseDTO(consume, consumeCarLevelResponseDTOList, 0L));
            }
        }
        return weChatConsumeResponseDTOList;
    }

    public WeChatConsumeResponseDTO getService(String cityId, Scene scene, String taocan, String userId) {
        Long count = 0L;
        if (ESStringUtils.isNotBlank(userId)) {
            count = travelOrderManager.countAllByUserIdAndOrderStatusIn(userId);
        }
        List<Consume> consumeList = consumeManager.findAllByScene(cityId, scene, taocan, new ArrayList<>());
        if (CollectionUtils.isEmpty(consumeList)) {
            throw new BusinessException("用车服务不存在，请通知服务端进行配置");
        }
        Consume consume = consumeList.get(0);
        List<ConsumeCarLevelResponseDTO> consumeCarLevelResponseDTOList = new ArrayList<>();
        List<ConsumeCarLevel> carLevels = consumeCarLevelManager.findAllByConsumeId(consume.getId(), new ArrayList<>());
        if (CollectionUtils.isNotEmpty(carLevels)) {
            for (ConsumeCarLevel carLevel : carLevels) {
                Zuowei zuowei = zuoweiManager.findById(carLevel.getZuoweiId());
                Chexing chexing = chexingManager.findById(carLevel.getChexingId());
                PriceStrategy priceStrategy = null;
                if (ESStringUtils.isNotBlank(carLevel.getPriceStrategyId())) {
                    priceStrategy = priceStrategyManager.findById(carLevel.getPriceStrategyId());
                }
                consumeCarLevelResponseDTOList.add(
                        new ConsumeCarLevelResponseDTO(
                                zuowei,
                                chexing,
                                carLevel.getPrice(),
                                carLevel.getPriceStrategyId(),
                                ESObjectUtils.isNotNull(priceStrategy) ? priceStrategy.getStrategyName() : null,
                                ESObjectUtils.isNotNull(priceStrategy) ? priceStrategy.getPlanDescription() : null
                        )
                );
            }
        }
        return new WeChatConsumeResponseDTO(consume, consumeCarLevelResponseDTOList, count);
    }
}
