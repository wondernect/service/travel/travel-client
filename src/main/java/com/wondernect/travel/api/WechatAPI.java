package com.wondernect.travel.api;

import com.alibaba.fastjson.JSON;
import com.wondernect.elements.common.exception.BusinessException;
import com.wondernect.elements.common.utils.ESJSONObjectUtils;
import com.wondernect.elements.common.utils.ESStringUtils;
import com.wondernect.elements.http.client.HttpClient;
import com.wondernect.travel.wechat.WXPay;
import com.wondernect.travel.wechat.WXPayConfigImpl;
import com.wondernect.travel.wechat.util.AESUtil;
import com.wondernect.travel.wechat.util.WXPayUtil;
import com.wondernect.travel.wechat.util.WXUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: WechatUtil
 * Author: chenxun
 * Date: 2019-05-24 18:26
 * Description:
 */
@Service
public class WechatAPI {

    private static final Logger logger = LoggerFactory.getLogger(WechatAPI.class);

    @Autowired
    private HttpClient httpClient;

    private static final String wechat_user_get_url = "https://api.weixin.qq.com/sns/jscode2session";

    private static final String appId = "wx7b18e1845642939f";

    private static final String appSecret = "201c43094688e9e121e9acb1409cd619";

    @Autowired
    private TravelOrderAPI travelOrderAPI;

    @Autowired
    private WXUtils wxUtils;

    public void getWxacode(String scene, HttpServletResponse response) {
        org.apache.http.client.HttpClient httpClient = HttpClientBuilder.create()
                .setConnectionManager(new BasicHttpClientConnectionManager(
                        RegistryBuilder.<ConnectionSocketFactory>create()
                                .register("http", PlainConnectionSocketFactory.getSocketFactory())
                                .register("https", SSLConnectionSocketFactory.getSocketFactory())
                                .build(),
                        null,
                        null,
                        null
                ))
                .build();
        String url = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=" + wxUtils.getAccessToken();
        HttpPost httpPost = new HttpPost(url);
        Map<String, String> bodyParams = new HashMap<>();
        bodyParams.put("scene", scene);
        StringEntity postEntity = new StringEntity(JSON.toJSONString(bodyParams), "UTF-8");
        httpPost.addHeader("Content-Type", "application/json");
        httpPost.setEntity(postEntity);
        try {
            HttpResponse httpResponse = httpClient.execute(httpPost);
            InputStream inputStream = httpResponse.getEntity().getContent();
            byte[] buffer = new byte[4096];
            int length;
            while((length = inputStream.read(buffer)) > 0) {
                response.getOutputStream().write(buffer, 0, length);
            }
            inputStream.close();
            response.getOutputStream().close();
        } catch (Exception e) {
            logger.error("获取微信小程序码失败", e);
            throw new BusinessException("商家小程序码获取失败，请稍后重试");
        }
    }

    public String getOpenId(String code) {
        String url = wechat_user_get_url + "?appid="+ appId +"&secret="+ appSecret +"&js_code="+ code +"&grant_type=authorization_code";
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        String response = httpClient.getForJson(url, headers, null);
        Map<String, Object> map = ESJSONObjectUtils.jsonStringToMapClassObject(response, Object.class);
        String openId = (String) map.get("openid");
        return ESStringUtils.isBlank(openId) ? null : openId;
    }

    public String payNotify(String notifyData) {
        String xmlBack;
        Map<String, String> notifyMap;
        try {
            WXPay wxpay = new WXPay(WXPayConfigImpl.getInstance());
            notifyMap = WXPayUtil.xmlToMap(notifyData);
            if (wxpay.isPayResultNotifySignatureValid(notifyMap)) {
                // 签名正确
                // 注意特殊情况：订单已经退款，但收到了支付结果成功的通知，不应把商户侧订单状态从退款改成支付成功
                String return_code = notifyMap.get("return_code");//状态
                if (ESStringUtils.equalsIgnoreCase(return_code, "SUCCESS")) {
                    String out_trade_no = notifyMap.get("out_trade_no");//订单号
                    if (ESStringUtils.isBlank(out_trade_no)) {
                        logger.error("微信支付回调失败: {}", notifyMap);
                        xmlBack = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>" + "<return_msg><![CDATA[报文为空]]></return_msg>" + "</xml> ";
                    } else {
                        logger.info("微信支付回调成功: {}", notifyMap);
                        if (!travelOrderAPI.confirmUserWechatPay(out_trade_no)) {
                            xmlBack = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>" + "<return_msg><![CDATA[报文为空]]></return_msg>" + "</xml> ";
                        } else {
                            xmlBack = "<xml>" + "<return_code><![CDATA[SUCCESS]]></return_code>" + "<return_msg><![CDATA[SUCCESS]]></return_msg>" + "</xml> ";
                        }
                    }
                } else {
                    logger.error("微信支付回调失败");
                    xmlBack = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>" + "<return_msg><![CDATA[报文为空]]></return_msg>" + "</xml> ";
                }
            } else {
                logger.error("微信支付回调失败：签名错误");
                xmlBack = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>" + "<return_msg><![CDATA[报文为空]]></return_msg>" + "</xml> ";
            }
        } catch (Exception e) {
            logger.error("微信支付回调失败", e);
            xmlBack = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>" + "<return_msg><![CDATA[报文为空]]></return_msg>" + "</xml> ";
        }
        return xmlBack;
    }

    public String refundNotify(String notifyData) {
        String xmlBack;
        Map<String, String> notifyMap;
        try {
            notifyMap = WXPayUtil.xmlToMap(notifyData);
            String return_code = notifyMap.get("return_code");
            if (ESStringUtils.equalsIgnoreCase(return_code, "SUCCESS")) {
                String reqInfo = notifyMap.get("req_info");
                if (ESStringUtils.isBlank(reqInfo)) {
                    logger.info("微信退款回调失败: {}", notifyMap);
                    xmlBack = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>" + "<return_msg><![CDATA[报文为空]]></return_msg>" + "</xml> ";
                } else {
                    String decryptData = AESUtil.decryptData(reqInfo);
                    if (ESStringUtils.isBlank(decryptData)) {
                        logger.info("微信退款回调失败: {}", notifyMap);
                        xmlBack = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>" + "<return_msg><![CDATA[报文为空]]></return_msg>" + "</xml> ";
                    } else {
                        Map<String, String> refundInfo = WXPayUtil.xmlToMap(decryptData);
                        logger.info("微信退款回调成功: {}", refundInfo);
                        String out_trade_no = refundInfo.get("out_trade_no");//订单号
                        if (!travelOrderAPI.confirmRefund(out_trade_no)) {
                            xmlBack = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>" + "<return_msg><![CDATA[报文为空]]></return_msg>" + "</xml> ";
                        } else {
                            xmlBack = "<xml>" + "<return_code><![CDATA[SUCCESS]]></return_code>" + "<return_msg><![CDATA[SUCCESS]]></return_msg>" + "</xml> ";
                        }
                    }
                }
            } else {
                logger.error("微信退款回调失败");
                xmlBack = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>" + "<return_msg><![CDATA[报文为空]]></return_msg>" + "</xml> ";
            }
        } catch (Exception e) {
            logger.error("微信退款回调失败", e);
            xmlBack = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>" + "<return_msg><![CDATA[报文为空]]></return_msg>" + "</xml> ";
        }
        return xmlBack;
    }
}
