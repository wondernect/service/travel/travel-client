package com.wondernect.travel.api;

import com.wondernect.elements.common.exception.BusinessException;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.rdb.request.PageRequestData;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.services.user.service.UserService;
import com.wondernect.travel.business.chexing.manager.ChexingManager;
import com.wondernect.travel.business.chexing.model.Chexing;
import com.wondernect.travel.business.city.manager.CityManager;
import com.wondernect.travel.business.city.model.City;
import com.wondernect.travel.business.zuowei.manager.ZuoweiManager;
import com.wondernect.travel.business.zuowei.model.Zuowei;
import com.wondernect.travel.cache.DriverCurrentCache;
import com.wondernect.travel.cache.model.DriverCurrent;
import com.wondernect.travel.dto.base.CarResponseDTO;
import com.wondernect.travel.manager.CarManager;
import com.wondernect.travel.manager.DriverManager;
import com.wondernect.travel.model.Car;
import com.wondernect.travel.model.Driver;
import com.wondernect.travel.model.em.DriverStatus;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: CarAPI
 * Author: chenxun
 * Date: 2019-05-12 17:54
 * Description:
 */
@Service
public class CarAPI {

    @Autowired
    private UserService userService;

    @Autowired
    private DriverManager driverManager;

    @Autowired
    private ZuoweiManager zuoweiManager;

    @Autowired
    private ChexingManager chexingManager;

    @Autowired
    private CityManager cityManager;

    @Autowired
    private CarManager carManager;

    @Autowired
    private DriverCurrentCache driverCurrentCache;

    @Transactional
    public CarResponseDTO save(Car car) {
        Driver driver = driverManager.findByUserId(car.getDriverUserId());
        if (ESObjectUtils.isNull(driver)) {
            throw new BusinessException("司机不存在");
        }
        City city = cityManager.findById(car.getCityId());
        if (ESObjectUtils.isNull(city)) {
            throw new BusinessException("城市不存在");
        }
        Zuowei zuowei = zuoweiManager.findById(car.getZuoweiId());
        if (ESObjectUtils.isNull(zuowei)) {
            throw new BusinessException("座位不存在");
        }
        Chexing chexing = chexingManager.findById(car.getChexingId());
        if (ESObjectUtils.isNull(chexing)) {
            throw new BusinessException("车型不存在");
        }
        Car carSave = carManager.save(car);
        DriverCurrent driverCurrent = driverCurrentCache.get(car.getDriverUserId());
        if (ESObjectUtils.isNotNull(driverCurrent)) {
            driverCurrent.setCityId(carSave.getCityId());
            driverCurrent.setZuoweiId(carSave.getZuoweiId());
            driverCurrent.setChexingId(carSave.getChexingId());
        } else {
            driverCurrent = new DriverCurrent(car.getDriverUserId(), carSave.getCityId(), carSave.getZuoweiId(), carSave.getChexingId(), DriverStatus.UNKNOWN, null, null, driver.getEvaluate());
        }
        driverCurrentCache.save(driverCurrent);
        return new CarResponseDTO(
                carSave,
                cityManager.findById(carSave.getCityId()),
                zuoweiManager.findById(carSave.getZuoweiId()),
                chexingManager.findById(carSave.getChexingId()),
                driverManager.findByUserId(carSave.getDriverUserId()),
                userService.findByUserId(carSave.getDriverUserId())
        );
    }

    @Transactional
    public void delete(String carId) {
        Car car = carManager.findById(carId);
        if (ESObjectUtils.isNotNull(car)) {
            carManager.deleteById(carId);
            DriverCurrent driverCurrent = driverCurrentCache.get(car.getDriverUserId());
            if (ESObjectUtils.isNotNull(driverCurrent)) {
                driverCurrent.setCityId(null);
                driverCurrent.setZuoweiId(null);
                driverCurrent.setChexingId(null);
                driverCurrentCache.save(driverCurrent);
            }
        }
    }

    public List<CarResponseDTO> list(List<SortData> sortDataList) {
        List<CarResponseDTO> carResponseDTOList = new ArrayList<>();
        List<Car> carList = carManager.findAll(sortDataList);
        if (CollectionUtils.isNotEmpty(carList)) {
            for (Car car : carList) {
                carResponseDTOList.add(new CarResponseDTO(
                        car,
                        cityManager.findById(car.getCityId()),
                        zuoweiManager.findById(car.getZuoweiId()),
                        chexingManager.findById(car.getChexingId()),
                        driverManager.findByUserId(car.getDriverUserId()),
                        userService.findByUserId(car.getDriverUserId())
                ));
            }
        }
        return carResponseDTOList;
    }

    public PageResponseData<CarResponseDTO> page(String cityId, String carNo, PageRequestData pageRequestData) {
        PageResponseData<Car> carResponseDTOPageResponseData = carManager.page(cityId, carNo, pageRequestData);
        List<CarResponseDTO> carResponseDTOList = new ArrayList<>();
        List<Car> carList = carResponseDTOPageResponseData.getDataList();
        if (CollectionUtils.isNotEmpty(carList)) {
            for (Car car : carList) {
                carResponseDTOList.add(new CarResponseDTO(
                        car,
                        cityManager.findById(car.getCityId()),
                        zuoweiManager.findById(car.getZuoweiId()),
                        chexingManager.findById(car.getChexingId()),
                        driverManager.findByUserId(car.getDriverUserId()),
                        userService.findByUserId(car.getDriverUserId())
                ));
            }
        }
        return new PageResponseData<>(pageRequestData.getPage(), pageRequestData.getSize(), carResponseDTOPageResponseData.getTotalPages(), carResponseDTOPageResponseData.getTotalElements(), carResponseDTOList);
    }
}
