package com.wondernect.travel.api;

import com.wondernect.elements.common.exception.BusinessException;
import com.wondernect.elements.common.utils.*;
import com.wondernect.travel.manager.PriceStrategyManager;
import com.wondernect.travel.model.PriceStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: PriceAPI
 * Author: chenxun
 * Date: 2019/5/11 17:22
 * Description:
 */
@Service
public class PriceAPI {

    @Autowired
    private PriceStrategyManager priceStrategyManager;

    @Transactional
    public PriceStrategy save(PriceStrategy priceStrategy) {
        switch (priceStrategy.getPlanType()) {
            case ONE:
            {
                if (ESObjectUtils.isNull(priceStrategy.getKiloPrice()) ||
                        priceStrategy.getKiloPrice() == 0 ||
                        ESObjectUtils.isNull(priceStrategy.getNightPercent()) ||
                        priceStrategy.getNightPercent() == 0) {
                    throw new BusinessException("里程费&夜间服务费百分比不能为空");
                }
                priceStrategy.setPlanDescription("(" + "公里数x" + priceStrategy.getKiloPrice() + ")" + "x(1 + " + priceStrategy.getNightPercent() +"%)");
                break;
            }
            case TWO:
            {
                if (ESObjectUtils.isNull(priceStrategy.getBasePrice()) ||
                        priceStrategy.getBasePrice() == 0 ||
                        ESObjectUtils.isNull(priceStrategy.getKiloPrice()) ||
                        priceStrategy.getKiloPrice() == 0 ||
                        ESObjectUtils.isNull(priceStrategy.getNightPercent()) ||
                        priceStrategy.getNightPercent() == 0) {
                    throw new BusinessException("基础费&里程费&夜间服务费百分比不能为空");
                }
                priceStrategy.setPlanDescription("(" + priceStrategy.getBasePrice() + "+公里数x" + priceStrategy.getKiloPrice() + ")" + "x(1 + " + priceStrategy.getNightPercent() +"%)");
                break;
            }
            case THREE:
            {
                if (ESObjectUtils.isNull(priceStrategy.getBasePrice()) ||
                        priceStrategy.getBasePrice() == 0 ||
                        ESObjectUtils.isNull(priceStrategy.getTimePrice()) ||
                        priceStrategy.getTimePrice() == 0 ||
                        ESObjectUtils.isNull(priceStrategy.getNightPercent()) ||
                        priceStrategy.getNightPercent() == 0) {
                    throw new BusinessException("基础费&时长费&夜间服务费百分比不能为空");
                }
                priceStrategy.setPlanDescription("(" + priceStrategy.getBasePrice() + "+时长x" + priceStrategy.getTimePrice() + ")" + "x(1 + " + priceStrategy.getNightPercent() +"%)");
                break;
            }
            case FOUR:
            {
                if (ESObjectUtils.isNull(priceStrategy.getBasePrice()) ||
                        priceStrategy.getBasePrice() == 0 ||
                        ESObjectUtils.isNull(priceStrategy.getStartKilo()) ||
                        priceStrategy.getStartKilo() == 0 ||
                        ESObjectUtils.isNull(priceStrategy.getKiloPrice()) ||
                        priceStrategy.getKiloPrice() == 0 ||
                        ESObjectUtils.isNull(priceStrategy.getNightPercent()) ||
                        priceStrategy.getNightPercent() == 0) {
                    throw new BusinessException("基础费&起步公里数&里程费&夜间服务费百分比不能为空");
                }
                priceStrategy.setPlanDescription("(" + priceStrategy.getBasePrice() + "+(公里数-" + priceStrategy.getStartKilo() + ")x" + priceStrategy.getKiloPrice() + ")" + "x(1 + " + priceStrategy.getNightPercent() +"%)");
                break;
            }
            case FIVE:
            {
                if (ESObjectUtils.isNull(priceStrategy.getBasePrice()) ||
                        priceStrategy.getBasePrice() == 0 ||
                        ESObjectUtils.isNull(priceStrategy.getKiloPrice()) ||
                        priceStrategy.getKiloPrice() == 0 ||
                        ESObjectUtils.isNull(priceStrategy.getTimePrice()) ||
                        priceStrategy.getTimePrice() == 0 ||
                        ESObjectUtils.isNull(priceStrategy.getLimitKilo()) ||
                        priceStrategy.getLimitKilo() == 0 ||
                        ESObjectUtils.isNull(priceStrategy.getLongPrice()) ||
                        priceStrategy.getLongPrice() == 0 ||
                        ESObjectUtils.isNull(priceStrategy.getNightPercent()) ||
                        priceStrategy.getNightPercent() == 0) {
                    throw new BusinessException("基础费&里程费&时长费&远途界限&远途费&夜间服务费百分比不能为空");
                }
                priceStrategy.setPlanDescription("(" + priceStrategy.getBasePrice() + "+公里数x" + priceStrategy.getKiloPrice() + "+时长x" + priceStrategy.getTimePrice() + "+(公里数-"+ priceStrategy.getLimitKilo() + ")x" + priceStrategy.getLongPrice() + ")" + "x(1 + " + priceStrategy.getNightPercent() +"%)");
                break;
            }
            case SIX:
            {
                if (ESStringUtils.isBlank(priceStrategy.getDistance()) ||
                        ESObjectUtils.isNull(priceStrategy.getKiloPrice()) ||
                        priceStrategy.getKiloPrice() == 0 ||
                        ESObjectUtils.isNull(priceStrategy.getNightPercent()) ||
                        priceStrategy.getNightPercent() == 0) {
                    throw new BusinessException("公里价格区间&里程费&夜间服务费百分比不能为空");
                }
                priceStrategy.setPlanDescription("(价格区间收费 + 超出公里数×" + priceStrategy.getKiloPrice() + ")x(1 + " + priceStrategy.getNightPercent() +"%)");
                break;
            }
            default:
            {
                throw new BusinessException("价格方案非法");
            }
        }
        return priceStrategyManager.save(priceStrategy);
    }

    public Double getPrice(String strategyId, Long inputStartTime, Double kilo, Double time) {
        PriceStrategy priceStrategy = priceStrategyManager.findById(strategyId);
        if (ESObjectUtils.isNull(priceStrategy)) {
            throw new BusinessException("价格策略不存在");
        }
        Long startTime = ESDateTimeUtils.getCurrentTimestamp();
        if (ESObjectUtils.isNotNull(inputStartTime) && inputStartTime > 0) {
            startTime = inputStartTime;
        }
        Long dayStartTime = ESDateTimeUtils.dayStartTimeMillis(startTime);
        Long nightDayStartTime = dayStartTime + 7 * 60 * 60 * 1000;
        Long dayEndTime = ESDateTimeUtils.dayEndTimeMillis(startTime);
        Long nightDayEndtime = dayEndTime - 2 * 60 * 60 * 1000;
        Boolean night = false;
        if (startTime > dayStartTime && startTime < nightDayStartTime) {
            // 当天 0 ~ 7
            night = true;
        } else if (startTime > nightDayEndtime && startTime < dayEndTime) {
            // 当天 22 ~ 0
            night = true;
        }
        Double price = 0d;
        switch (priceStrategy.getPlanType()) {
            case ONE:
            {
                if (ESObjectUtils.isNotNull(kilo) && kilo > 0) {
                    kilo = kilo / 1000;
                    price = kilo * priceStrategy.getKiloPrice();
                    if (night && ESObjectUtils.isNotNull(priceStrategy.getNightPercent()) && priceStrategy.getNightPercent() > 0) {
                        price = price * (1 + priceStrategy.getNightPercent() / 100);
                    }
                }
                break;
            }
            case TWO:
            {
                if (ESObjectUtils.isNotNull(kilo) && kilo > 0) {
                    kilo = kilo / 1000;
                    price = priceStrategy.getBasePrice() + kilo * priceStrategy.getKiloPrice();
                    if (night && ESObjectUtils.isNotNull(priceStrategy.getNightPercent()) && priceStrategy.getNightPercent() > 0) {
                        price = price * (1 + priceStrategy.getNightPercent() / 100);
                    }
                }
                break;
            }
            case THREE:
            {
                if (ESObjectUtils.isNotNull(time) && time > 0) {
                    time = time / 3600;
                    price = priceStrategy.getBasePrice() + time * priceStrategy.getTimePrice();
                    if (night && ESObjectUtils.isNotNull(priceStrategy.getNightPercent()) && priceStrategy.getNightPercent() > 0) {
                        price = price * (1 + priceStrategy.getNightPercent() / 100);
                    }
                }
                break;
            }
            case FOUR:
            {
                if (ESObjectUtils.isNotNull(kilo) && kilo > 0) {
                    kilo = kilo / 1000;
                    Double temp = kilo - priceStrategy.getStartKilo();
                    Double kilos = temp <= 0 ? 0 : temp;
                    price = priceStrategy.getBasePrice() + kilos * priceStrategy.getKiloPrice();
                    if (night && ESObjectUtils.isNotNull(priceStrategy.getNightPercent()) && priceStrategy.getNightPercent() > 0) {
                        price = price * (1 + priceStrategy.getNightPercent() / 100);
                    }
                }
                break;
            }
            case FIVE:
            {
                if (ESObjectUtils.isNotNull(kilo) && kilo > 0 &&
                        ESObjectUtils.isNotNull(time) && time > 0) {
                    kilo = kilo / 1000;
                    time = time / 3600;
                    Double temp = kilo - priceStrategy.getLimitKilo();
                    Double kilos = temp <= 0 ? 0 : temp;
                    price = priceStrategy.getBasePrice() + kilo * priceStrategy.getKiloPrice() + time * priceStrategy.getTimePrice() + kilos * priceStrategy.getLongPrice();
                    if (night && ESObjectUtils.isNotNull(priceStrategy.getNightPercent()) && priceStrategy.getNightPercent() > 0) {
                        price = price * (1 + priceStrategy.getNightPercent() / 100);
                    }
                }
                break;
            }
            case SIX:
            {
                if (ESObjectUtils.isNotNull(kilo) && kilo > 0) {
                    kilo = kilo / 1000;
                    Map<String, Double> map = ESJSONObjectUtils.jsonStringToMapClassObject(priceStrategy.getDistance(), Double.class);
                    Boolean flag = true;
                    Double max = 0d;
                    Double maxKilo = 0d;
                    for (String k : map.keySet()) {
                        Double kd = Double.valueOf(k);
                        if (kilo <= kd) {
                            price = map.get(k);
                            flag = false;
                            break;
                        }
                        if (maxKilo <= kd) {
                            maxKilo = kd;
                        }
                        if (max <= map.get(k)) {
                            max = map.get(k);
                        }
                    }
                    if (flag) {
                        // 超出最大公里数
                        price = max + (kilo - maxKilo) * priceStrategy.getKiloPrice();
                    }
                    if (night && ESObjectUtils.isNotNull(priceStrategy.getNightPercent()) && priceStrategy.getNightPercent() > 0) {
                        price = price * (1 + priceStrategy.getNightPercent() / 100);
                    }
                }
                break;
            }
        }
        return ESNumberFormatUtils.formatDouble(price, 2);
    }
}
