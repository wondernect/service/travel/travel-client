package com.wondernect.travel.api;

import com.wondernect.elements.common.exception.BusinessException;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.common.utils.ESStringUtils;
import com.wondernect.elements.rdb.request.PageRequestData;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.services.user.service.UserService;
import com.wondernect.travel.cache.DriverCurrentCache;
import com.wondernect.travel.cache.model.DriverCurrent;
import com.wondernect.travel.dto.base.DriverEvaluateResponseDTO;
import com.wondernect.travel.dto.base.DriverResponseDTO;
import com.wondernect.travel.manager.CarManager;
import com.wondernect.travel.manager.DriverEvaluateManager;
import com.wondernect.travel.manager.DriverManager;
import com.wondernect.travel.manager.TravelOrderManager;
import com.wondernect.travel.model.Car;
import com.wondernect.travel.model.Driver;
import com.wondernect.travel.model.DriverEvaluate;
import com.wondernect.travel.model.TravelOrder;
import com.wondernect.travel.model.em.DriverStatus;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: DriverAPI
 * Author: chenxun
 * Date: 2019-05-12 16:42
 * Description:
 */
@Service
public class DriverAPI {

    @Autowired
    private UserService userService;

    @Autowired
    private DriverManager driverManager;

    @Autowired
    private TravelOrderManager travelOrderManager;

    @Autowired
    private CarManager carManager;

    @Autowired
    private DriverEvaluateManager driverEvaluateManager;

    @Autowired
    private DriverCurrentCache driverCurrentCache;

    public DriverResponseDTO findByUserId(String userId) {
        return new DriverResponseDTO(driverManager.findByUserId(userId), userService.findByUserId(userId));
    }

    @Transactional
    public void saveDriverStatusAndLocation(DriverCurrent driverCurrent) {
        DriverCurrent driverCurrentGet = driverCurrentCache.get(driverCurrent.getDriverUserId());
        if (ESObjectUtils.isNull(driverCurrentGet)) {
            Driver driver = driverManager.findByUserId(driverCurrent.getDriverUserId());
            if (ESObjectUtils.isNull(driver)) {
                throw new BusinessException("司机不存在");
            }
            Car car = carManager.findByDriverUserId(driver.getUserId());
            driverCurrentGet = new DriverCurrent(driver.getUserId(), ESObjectUtils.isNull(car) ? null : car.getCityId(), ESObjectUtils.isNull(car) ? null : car.getZuoweiId(), ESObjectUtils.isNull(car) ? null : car.getChexingId(), driverCurrent.getStatus(), driverCurrent.getLongitude(), driverCurrent.getLatitude(), driver.getEvaluate());
        } else {
            driverCurrentGet.setStatus(driverCurrent.getStatus());
            driverCurrentGet.setLongitude(driverCurrent.getLongitude());
            driverCurrentGet.setLatitude(driverCurrent.getLatitude());
        }
        driverCurrentCache.save(driverCurrentGet);
    }

    @Transactional
    public void saveDriverEvaluate(DriverEvaluate driverEvaluate) {
        Driver driver = driverManager.findByUserId(driverEvaluate.getDriverUserId());
        if (ESObjectUtils.isNull(driver)) {
            throw new BusinessException("司机不存在");
        }
        TravelOrder travelOrder = travelOrderManager.findById(driverEvaluate.getOrderId());
        if (ESObjectUtils.isNull(travelOrder)) {
            throw new BusinessException("订单不存在");
        }
        travelOrder.setEvaluate(true);
        travelOrderManager.save(travelOrder);
        driverEvaluate.setOrderCreateTime(travelOrder.getCreateTime());
        driverEvaluate.setOrderExcuteTime(travelOrder.getExecuteTime());
        driverEvaluateManager.save(driverEvaluate);
        // driver缓存
        DriverCurrent driverCurrentGet = driverCurrentCache.get(driverEvaluate.getDriverUserId());
        if (ESObjectUtils.isNull(driverCurrentGet)) {
            Car car = carManager.findByDriverUserId(driver.getUserId());
            driverCurrentGet = new DriverCurrent(driver.getUserId(), ESObjectUtils.isNull(car) ? null : car.getCityId(), ESObjectUtils.isNull(car) ? null : car.getZuoweiId(), ESObjectUtils.isNull(car) ? null : car.getChexingId(), DriverStatus.UNKNOWN, null, null, 10);
        }
        // driver评价
        if (ESStringUtils.isNotBlank(driverEvaluate.getContent())) {
            int count = driver.getTotalEvaluateCount() + 1;
            driver.setTotalEvaluateCount(count);
            int totalEvaluate = driver.getTotalEvaluate() + driverEvaluate.getEvaluate();
            driver.setTotalEvaluate(totalEvaluate);
            driver.setEvaluate(totalEvaluate / count);
        }
        driverManager.save(driver);
        driverCurrentGet.setEvaluate(driver.getEvaluate());
        driverCurrentCache.save(driverCurrentGet);
    }

    public List<DriverResponseDTO> listDriver(String value, List<SortData> sortDataList) {
        List<DriverResponseDTO> driverResponseDTOList = new ArrayList<>();
        List<Driver> driverList = driverManager.driverList(value, sortDataList);
        if (CollectionUtils.isNotEmpty(driverList)) {
            for (Driver driver : driverList) {
                driverResponseDTOList.add(new DriverResponseDTO(driver, userService.findByUserId(driver.getUserId())));
            }
        }
        return driverResponseDTOList;
    }

    public PageResponseData<DriverEvaluateResponseDTO> pageDriverEvaluate(String driverUserId, Integer type, Long start, Long end, PageRequestData pageRequestData) {
        List<DriverEvaluateResponseDTO> driverEvaluateResponseDTOList = new ArrayList<>();
        PageResponseData<DriverEvaluate> driverEvaluatePageResponseData = driverEvaluateManager.findAllByDriverUserId(driverUserId, type, start, end, pageRequestData);
        List<DriverEvaluate> driverEvaluateList = driverEvaluatePageResponseData.getDataList();
        if (CollectionUtils.isNotEmpty(driverEvaluateList)) {
            for (DriverEvaluate driverEvaluate : driverEvaluateList) {
                driverEvaluateResponseDTOList.add(
                        new DriverEvaluateResponseDTO(
                                driverEvaluate,
                                driverManager.findByUserId(driverEvaluate.getDriverUserId()),
                                userService.findByUserId(driverEvaluate.getDriverUserId()),
                                userService.findByUserId(driverEvaluate.getUserId())
                        )
                );
            }
        }
        return new PageResponseData<>(pageRequestData.getPage(), pageRequestData.getSize(), driverEvaluatePageResponseData.getTotalPages(), driverEvaluatePageResponseData.getTotalElements(), driverEvaluateResponseDTOList);
    }
}
