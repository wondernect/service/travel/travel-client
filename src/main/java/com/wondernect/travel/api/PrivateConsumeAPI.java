package com.wondernect.travel.api;

import com.wondernect.elements.common.exception.BusinessException;
import com.wondernect.elements.common.utils.ESBeanUtils;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.common.utils.ESStringUtils;
import com.wondernect.elements.rdb.criteria.Criteria;
import com.wondernect.elements.rdb.criteria.Restrictions;
import com.wondernect.elements.rdb.request.PageRequestData;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.services.user.model.User;
import com.wondernect.services.user.service.UserService;
import com.wondernect.travel.business.chexing.manager.ChexingManager;
import com.wondernect.travel.business.chexing.model.Chexing;
import com.wondernect.travel.business.zuowei.manager.ZuoweiManager;
import com.wondernect.travel.business.zuowei.model.Zuowei;
import com.wondernect.travel.dto.base.PrivateConsumeCarLevelResponseDTO;
import com.wondernect.travel.dto.base.PrivateConsumeRequestDTO;
import com.wondernect.travel.dto.base.WeChatPrivateConsumeResponseDTO;
import com.wondernect.travel.manager.*;
import com.wondernect.travel.model.*;
import com.wondernect.travel.model.em.Scene;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: PrivateConsumeAPI
 * Author: chenxun
 * Date: 2019-05-12 13:39
 * Description:
 */
@Service
public class PrivateConsumeAPI {

    @Autowired
    private DriverAPI driverAPI;

    @Autowired
    private PrivateConsumeManager privateConsumeManager;

    @Autowired
    private PrivateConsumeCarLevelManager privateConsumeCarLevelManager;

    @Autowired
    private PrivateConsumeRoadManager privateConsumeRoadManager;

    @Autowired
    private PrivateConsumeEvaluateManager privateConsumeEvaluateManager;

    @Autowired
    private ZuoweiManager zuoweiManager;

    @Autowired
    private ChexingManager chexingManager;

    @Autowired
    private PriceStrategyManager priceStrategyManager;

    @Autowired
    private TravelOrderManager travelOrderManager;

    @Autowired
    private UserService userService;

    @Transactional
    public WeChatPrivateConsumeResponseDTO savePrivateConsume(PrivateConsumeRequestDTO privateConsumeRequestDTO) {
        PrivateConsume privateConsume;
        if (ESStringUtils.isNotBlank(privateConsumeRequestDTO.getPrivateConsume().getId())) {
            // 更新
            privateConsume = privateConsumeManager.findById(privateConsumeRequestDTO.getPrivateConsume().getId());
            if (ESObjectUtils.isNull(privateConsume)) {
                throw new BusinessException("包车服务不存在");
            }
            privateConsumeCarLevelManager.deleteAllByPrivateConsumeId(privateConsumeRequestDTO.getPrivateConsume().getId());
            privateConsumeRoadManager.deleteAllByPrivateConsumeId(privateConsumeRequestDTO.getPrivateConsume().getId());
        } else {
            // 新增
            privateConsume = new PrivateConsume();
        }
        ESBeanUtils.copyProperties(privateConsumeRequestDTO.getPrivateConsume(), privateConsume);
        if (ESObjectUtils.isNotNull(privateConsumeRequestDTO.getPrivateConsume().getWeight())) {
            privateConsume.setWeight(privateConsumeRequestDTO.getPrivateConsume().getWeight());
        }
        privateConsume = privateConsumeManager.save(privateConsume);
        List<PrivateConsumeCarLevelResponseDTO> privateConsumeCarLevelResponseDTOList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(privateConsumeRequestDTO.getCarLevels())) {
            for (PrivateConsumeCarLevel carLevel : privateConsumeRequestDTO.getCarLevels()) {
                carLevel.setPrivateConsumeId(privateConsume.getId());
                carLevel = privateConsumeCarLevelManager.save(carLevel);
                Zuowei zuowei = zuoweiManager.findById(carLevel.getZuoweiId());
                Chexing chexing = chexingManager.findById(carLevel.getChexingId());
                PriceStrategy priceStrategy = null;
                if (ESStringUtils.isNotBlank(carLevel.getPriceStrategyId())) {
                    priceStrategy = priceStrategyManager.findById(carLevel.getPriceStrategyId());
                }
                privateConsumeCarLevelResponseDTOList.add(
                        new PrivateConsumeCarLevelResponseDTO(
                                zuowei,
                                chexing,
                                carLevel.getPrice(),
                                carLevel.getPriceStrategyId(),
                                ESObjectUtils.isNotNull(priceStrategy) ? priceStrategy.getStrategyName() : null,
                                ESObjectUtils.isNotNull(priceStrategy) ? priceStrategy.getPlanDescription() : null
                        )
                );
            }
        }
        List<PrivateConsumeRoad> privateConsumeRoadList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(privateConsumeRequestDTO.getRoads())) {
            for (PrivateConsumeRoad privateConsumeRoad : privateConsumeRequestDTO.getRoads()) {
                privateConsumeRoad.setPrivateConsumeId(privateConsume.getId());
                privateConsumeRoadList.add(privateConsumeRoadManager.save(privateConsumeRoad));
            }
        }
        return new WeChatPrivateConsumeResponseDTO(privateConsume, privateConsumeCarLevelResponseDTOList, privateConsumeRoadList);
    }

    @Transactional
    public void deletePrivateConsume(String privateConsumeId) {
        PrivateConsume privateConsume = privateConsumeManager.findById(privateConsumeId);
        if (ESObjectUtils.isNull(privateConsume)) {
            throw new BusinessException("包车服务不存在");
        }
        privateConsumeCarLevelManager.deleteAllByPrivateConsumeId(privateConsumeId);
        privateConsumeRoadManager.deleteAllByPrivateConsumeId(privateConsumeId);
        privateConsumeManager.deleteById(privateConsumeId);
    }

    public WeChatPrivateConsumeResponseDTO getPrivateConsume(String privateConsumeId) {
        PrivateConsume privateConsume = privateConsumeManager.findById(privateConsumeId);
        if (ESObjectUtils.isNull(privateConsume)) {
            return null;
        }
        List<PrivateConsumeCarLevelResponseDTO> privateConsumeCarLevelResponseDTOList = new ArrayList<>();
        List<PrivateConsumeCarLevel> carLevels = privateConsumeCarLevelManager.findAllByConsumeId(privateConsumeId, new ArrayList<>());
        if (CollectionUtils.isNotEmpty(carLevels)) {
            for (PrivateConsumeCarLevel carLevel : carLevels) {
                Zuowei zuowei = zuoweiManager.findById(carLevel.getZuoweiId());
                Chexing chexing = chexingManager.findById(carLevel.getChexingId());
                PriceStrategy priceStrategy = null;
                if (ESStringUtils.isNotBlank(carLevel.getPriceStrategyId())) {
                    priceStrategy = priceStrategyManager.findById(carLevel.getPriceStrategyId());
                }
                privateConsumeCarLevelResponseDTOList.add(
                        new PrivateConsumeCarLevelResponseDTO(
                                zuowei,
                                chexing,
                                carLevel.getPrice(),
                                carLevel.getPriceStrategyId(),
                                ESObjectUtils.isNotNull(priceStrategy) ? priceStrategy.getStrategyName() : null,
                                ESObjectUtils.isNotNull(priceStrategy) ? priceStrategy.getPlanDescription() : null
                        )
                );
            }
        }
        List<PrivateConsumeRoad> roads = privateConsumeRoadManager.list(privateConsume.getId(), new ArrayList<>());
        return new WeChatPrivateConsumeResponseDTO(privateConsume, privateConsumeCarLevelResponseDTOList, roads);
    }

    public List<WeChatPrivateConsumeResponseDTO> list(String cityId, List<Scene> sceneList, List<SortData> sortDataList) {
        List<WeChatPrivateConsumeResponseDTO> weChatPrivateConsumeResponseDTOList = new ArrayList<>();
        List<PrivateConsume> privateConsumeList = privateConsumeManager.list(cityId, sceneList, sortDataList);
        if (CollectionUtils.isNotEmpty(privateConsumeList)) {
            for (PrivateConsume privateConsume : privateConsumeList) {
                List<PrivateConsumeCarLevelResponseDTO> privateConsumeCarLevelResponseDTOList = new ArrayList<>();
                List<PrivateConsumeCarLevel> carLevels = privateConsumeCarLevelManager.findAllByConsumeId(privateConsume.getId(), new ArrayList<>());
                if (CollectionUtils.isNotEmpty(carLevels)) {
                    for (PrivateConsumeCarLevel carLevel : carLevels) {
                        Zuowei zuowei = zuoweiManager.findById(carLevel.getZuoweiId());
                        Chexing chexing = chexingManager.findById(carLevel.getChexingId());
                        PriceStrategy priceStrategy = null;
                        if (ESStringUtils.isNotBlank(carLevel.getPriceStrategyId())) {
                            priceStrategy = priceStrategyManager.findById(carLevel.getPriceStrategyId());
                        }
                        privateConsumeCarLevelResponseDTOList.add(
                                new PrivateConsumeCarLevelResponseDTO(
                                        zuowei,
                                        chexing,
                                        carLevel.getPrice(),
                                        carLevel.getPriceStrategyId(),
                                        ESObjectUtils.isNotNull(priceStrategy) ? priceStrategy.getStrategyName() : null,
                                        ESObjectUtils.isNotNull(priceStrategy) ? priceStrategy.getPlanDescription() : null
                                )
                        );
                    }
                }
                List<PrivateConsumeRoad> roads = privateConsumeRoadManager.list(privateConsume.getId(), new ArrayList<>());
                weChatPrivateConsumeResponseDTOList.add(
                        new WeChatPrivateConsumeResponseDTO(privateConsume, privateConsumeCarLevelResponseDTOList, roads)
                );
            }
        }
        return weChatPrivateConsumeResponseDTOList;
    }

    public PageResponseData<WeChatPrivateConsumeResponseDTO> page(String cityId, List<Scene> sceneList, String value, Long start, Long end, PageRequestData pageRequestData) {
        List<WeChatPrivateConsumeResponseDTO> weChatPrivateConsumeResponseDTOList = new ArrayList<>();
        PageResponseData<PrivateConsume> pageResponseData = privateConsumeManager.page(cityId, sceneList, value, start, end, pageRequestData);
        List<PrivateConsume> privateConsumeList = pageResponseData.getDataList();
        if (CollectionUtils.isNotEmpty(privateConsumeList)) {
            for (PrivateConsume privateConsume : privateConsumeList) {
                List<PrivateConsumeCarLevelResponseDTO> privateConsumeCarLevelResponseDTOList = new ArrayList<>();
                List<PrivateConsumeCarLevel> carLevels = privateConsumeCarLevelManager.findAllByConsumeId(privateConsume.getId(), new ArrayList<>());
                if (CollectionUtils.isNotEmpty(carLevels)) {
                    for (PrivateConsumeCarLevel carLevel : carLevels) {
                        Zuowei zuowei = zuoweiManager.findById(carLevel.getZuoweiId());
                        Chexing chexing = chexingManager.findById(carLevel.getChexingId());
                        PriceStrategy priceStrategy = null;
                        if (ESStringUtils.isNotBlank(carLevel.getPriceStrategyId())) {
                            priceStrategy = priceStrategyManager.findById(carLevel.getPriceStrategyId());
                        }
                        privateConsumeCarLevelResponseDTOList.add(
                                new PrivateConsumeCarLevelResponseDTO(
                                        zuowei,
                                        chexing,
                                        carLevel.getPrice(),
                                        carLevel.getPriceStrategyId(),
                                        ESObjectUtils.isNotNull(priceStrategy) ? priceStrategy.getStrategyName() : null,
                                        ESObjectUtils.isNotNull(priceStrategy) ? priceStrategy.getPlanDescription() : null
                                )
                        );
                    }
                }
                List<PrivateConsumeRoad> roads = privateConsumeRoadManager.list(privateConsume.getId(), new ArrayList<>());
                weChatPrivateConsumeResponseDTOList.add(
                        new WeChatPrivateConsumeResponseDTO(privateConsume, privateConsumeCarLevelResponseDTOList, roads)
                );
            }
        }
        return new PageResponseData<>(
                pageResponseData.getPage(),
                pageResponseData.getSize(),
                pageResponseData.getTotalPages(),
                pageResponseData.getTotalElements(),
                weChatPrivateConsumeResponseDTOList
        );
    }

    @Transactional
    public PrivateConsumeEvaluate evaluate(PrivateConsumeEvaluate privateConsumeEvaluate) {
        PrivateConsume privateConsume = privateConsumeManager.findById(privateConsumeEvaluate.getPrivateConsumeId());
        if (ESObjectUtils.isNull(privateConsume)) {
            throw new BusinessException("包车服务不存在");
        }
        TravelOrder travelOrder = travelOrderManager.findById(privateConsumeEvaluate.getOrderId());
        if (ESObjectUtils.isNull(travelOrder)) {
            throw new BusinessException("订单不存在");
        }
        travelOrder.setEvaluate(true);
        travelOrderManager.save(travelOrder);
        User user = userService.findByUserId(privateConsumeEvaluate.getUserId());
        if (ESObjectUtils.isNull(user)) {
            throw new BusinessException("用户信息不存在");
        }
        privateConsumeEvaluate.setStatus(0);
        if (privateConsume.getScene() != Scene.BANSHOU_PRIVATE) {
            DriverEvaluate driverEvaluate = new DriverEvaluate(
                    privateConsumeEvaluate.getDriverUserId(), privateConsumeEvaluate.getEvaluate(), privateConsumeEvaluate.getImage(),
                    privateConsumeEvaluate.getContent(), privateConsumeEvaluate.getUserId(), privateConsumeEvaluate.getOrderId()
            );
            driverAPI.saveDriverEvaluate(driverEvaluate);
        }
        return privateConsumeEvaluateManager.save(privateConsumeEvaluate);
    }

    public PageResponseData<PrivateConsumeEvaluate> evaluatePage(String privateConsumeId, Integer status, PageRequestData pageRequestData) {
        PrivateConsume privateConsume = privateConsumeManager.findById(privateConsumeId);
        Criteria<PrivateConsumeEvaluate> privateConsumeEvaluateCriteria = new Criteria<>();
        privateConsumeEvaluateCriteria.add(Restrictions.eq("privateConsumeId", privateConsumeId));
        privateConsumeEvaluateCriteria.add(Restrictions.eq("status", status));
        if (CollectionUtils.isEmpty(pageRequestData.getSortDataList())) {
            pageRequestData.setSortDataList(Arrays.asList(new SortData("createTime", "DESC")));
        }
        PageResponseData<PrivateConsumeEvaluate> pageResponseData = privateConsumeEvaluateManager.findAll(privateConsumeEvaluateCriteria, pageRequestData);
        if (CollectionUtils.isNotEmpty(pageResponseData.getDataList())) {
            for (PrivateConsumeEvaluate privateConsumeEvaluate : pageResponseData.getDataList()) {
                privateConsumeEvaluate.setPrivateConsume(privateConsume);
                privateConsumeEvaluate.setUser(userService.findByUserId(privateConsumeEvaluate.getUserId()));
                privateConsumeEvaluate.setOrder(travelOrderManager.findById(privateConsumeEvaluate.getOrderId()));
            }
        }
        return pageResponseData;
    }

    public void evaluateStatusChange(Integer status, List<String> evaluateIdList) {
        if (CollectionUtils.isNotEmpty(evaluateIdList)) {
            for (String id : evaluateIdList) {
                PrivateConsumeEvaluate privateConsumeEvaluate = privateConsumeEvaluateManager.findById(id);
                if (ESObjectUtils.isNotNull(privateConsumeEvaluate)) {
                    Integer originStatus = privateConsumeEvaluate.getStatus();
                    if (originStatus == 0) {
                        // 待审核
                        if (status == 1) {
                            // 审核通过
                            PrivateConsume privateConsume = privateConsumeManager.findById(privateConsumeEvaluate.getPrivateConsumeId());
                            if (ESObjectUtils.isNull(privateConsume)) {
                                throw new BusinessException("包车服务不存在");
                            }
                            int privateCount = ESObjectUtils.isNotNull(privateConsume.getEvaluateCount()) ? privateConsume.getEvaluateCount() : 0;
                            int count = privateCount + 1;
                            privateConsume.setEvaluateCount(count);
                            int evaluateScore = ESObjectUtils.isNotNull(privateConsume.getEvaluateScore()) ? privateConsume.getEvaluateScore() : 0;
                            int totalEvaluate = evaluateScore + privateConsumeEvaluate.getEvaluate();
                            privateConsume.setEvaluateScore(totalEvaluate / count);
                            privateConsumeManager.save(privateConsume);
                        }
                    } else if (originStatus == 1) {
                        // 审核通过
                        if (status != 1) {
                            // 待审核、审核拒绝
                            PrivateConsume privateConsume = privateConsumeManager.findById(privateConsumeEvaluate.getPrivateConsumeId());
                            if (ESObjectUtils.isNull(privateConsume)) {
                                throw new BusinessException("包车服务不存在");
                            }
                            int preTotalScore = privateConsume.getEvaluateScore() * privateConsume.getEvaluateCount();
                            int preCount = privateConsume.getEvaluateCount();
                            int count = preCount - 1;
                            privateConsume.setEvaluateCount(count);
                            int totalEvaluate = preTotalScore - privateConsumeEvaluate.getEvaluate();
                            privateConsume.setEvaluateScore(totalEvaluate / count);
                            privateConsumeManager.save(privateConsume);
                        }
                    } else if (originStatus == 2) {
                        // 审核拒绝
                        if (status == 1) {
                            // 审核通过
                            PrivateConsume privateConsume = privateConsumeManager.findById(privateConsumeEvaluate.getPrivateConsumeId());
                            if (ESObjectUtils.isNull(privateConsume)) {
                                throw new BusinessException("包车服务不存在");
                            }
                            int privateCount = ESObjectUtils.isNotNull(privateConsume.getEvaluateCount()) ? privateConsume.getEvaluateCount() : 0;
                            int count = privateCount + 1;
                            privateConsume.setEvaluateCount(count);
                            int evaluateScore = ESObjectUtils.isNotNull(privateConsume.getEvaluateScore()) ? privateConsume.getEvaluateScore() : 0;
                            int totalEvaluate = evaluateScore + privateConsumeEvaluate.getEvaluate();
                            privateConsume.setEvaluateScore(totalEvaluate / count);
                            privateConsumeManager.save(privateConsume);
                        }
                    }
                    privateConsumeEvaluate.setStatus(status);
                    privateConsumeEvaluateManager.save(privateConsumeEvaluate);
                }
            }
        }
    }
}
