package com.wondernect.travel.api;

import com.wondernect.elements.authorize.context.AuthorizeData;
import com.wondernect.elements.authorize.context.WondernectCommonContext;
import com.wondernect.elements.common.exception.BusinessException;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.common.utils.ESRegexUtils;
import com.wondernect.elements.common.utils.ESStringUtils;
import com.wondernect.elements.rdb.request.PageRequestData;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.services.rbac.model.Role;
import com.wondernect.services.rbac.model.em.RoleType;
import com.wondernect.services.rbac.service.RolesService;
import com.wondernect.services.session.model.CaptchaSession;
import com.wondernect.services.session.model.TokenSession;
import com.wondernect.services.session.service.impl.DefaultCaptchaSessionService;
import com.wondernect.services.session.service.impl.DefaultTokenSessionService;
import com.wondernect.services.user.common.error.UserErrorEnum;
import com.wondernect.services.user.common.exception.UserException;
import com.wondernect.services.user.manager.OpenUserManager;
import com.wondernect.services.user.model.OpenUser;
import com.wondernect.services.user.model.User;
import com.wondernect.services.user.model.UserAuth;
import com.wondernect.services.user.model.em.AppType;
import com.wondernect.services.user.model.em.Gender;
import com.wondernect.services.user.model.em.PasswordType;
import com.wondernect.services.user.model.em.UserRegistType;
import com.wondernect.services.user.service.UserService;
import com.wondernect.travel.cache.DriverCurrentCache;
import com.wondernect.travel.cache.model.DriverCurrent;
import com.wondernect.travel.dto.login.*;
import com.wondernect.travel.dto.user.*;
import com.wondernect.travel.manager.CarManager;
import com.wondernect.travel.manager.DriverManager;
import com.wondernect.travel.manager.ShopManager;
import com.wondernect.travel.manager.UserShopManager;
import com.wondernect.travel.model.Car;
import com.wondernect.travel.model.Driver;
import com.wondernect.travel.model.Shop;
import com.wondernect.travel.model.UserShop;
import com.wondernect.travel.model.em.DriverStatus;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: UserAPI
 * Author: chenxun
 * Date: 2019/4/13 13:31
 * Description:
 */
@Service
public class UserAPI {

    @Autowired
    private WondernectCommonContext wondernectCommonContext;

    @Autowired
    private UserService userService;

    @Autowired
    private OpenUserManager openUserManager;

    @Autowired
    private DefaultCaptchaSessionService captchaSessionService;

    @Autowired
    private DefaultTokenSessionService tokenSessionService;

    @Autowired
    private RolesService rolesService;

    @Autowired
    private ShopManager shopManager;

    @Autowired
    private UserShopManager userShopManager;

    @Autowired
    private DriverManager driverManager;

    @Autowired
    private CarManager carManager;

    @Autowired
    private DriverCurrentCache driverCurrentCache;

    @Autowired
    private WechatAPI wechatAPI;

    public UserRoleResponseDTO findByUsername(String username) {
        User user = userService.findByUsername(username);
        if (ESObjectUtils.isNull(user)) {
            return null;
        }
        return new UserRoleResponseDTO(user, rolesService.findAllByUserId(user.getId(), new ArrayList<>()));
    }

    public User findAppUser(AppType appType, String appUserId) {
        OpenUser openUser = openUserManager.findByAppTypeAndAppUserId(appType, appUserId);
        if (ESObjectUtils.isNull(openUser)) {
            return null;
        }
        return userService.findByUserId(openUser.getUserId());
    }

    @Transactional
    public UserLoginResponseDTO appRegist(AppUserRegistRequestDTO appUserRegistRequestDTO) {
        User user;
        if (ESRegexUtils.isMobile(appUserRegistRequestDTO.getUsername())) {
            if (ESObjectUtils.isNotNull(userService.findByUsername(appUserRegistRequestDTO.getUsername()))) {
                throw new BusinessException("手机号码已存在");
            }
            user = new User(UserRegistType.APP, Gender.UNKNOWN, appUserRegistRequestDTO.getAppUserName(), appUserRegistRequestDTO.getAppUserName(), appUserRegistRequestDTO.getAppUserAvatar(), appUserRegistRequestDTO.getUsername(), null, null, null);
        } else {
            throw new UserException(UserErrorEnum.USER_MOBILE_EMAIL_INVALID);
        }
        if (ESObjectUtils.isNotNull(openUserManager.findByAppTypeAndAppUserId(AppType.WECHAT, appUserRegistRequestDTO.getAppUserId()))) {
            throw new BusinessException("微信用户已注册");
        }
        OpenUser openUser = new OpenUser(AppType.WECHAT, appUserRegistRequestDTO.getAppUserId(), appUserRegistRequestDTO.getAppUserName(), appUserRegistRequestDTO.getAppUserAvatar(), null);
        User userRegist = userService.userRegist(user, openUser, null);
        wondernectCommonContext.setAuthorizeData(new AuthorizeData(null, userRegist.getId(), null));
        TokenSession tokenSession = tokenSessionService.requestTokenSession(
                "登录",
                wondernectCommonContext.getRequestIp(),
                wondernectCommonContext.getDevicePlatform(),
                wondernectCommonContext.getRequestDevice(),
                wondernectCommonContext.getDeviceIdentifier()
        );
        return new UserLoginResponseDTO(userRegist, rolesService.findAllByUserId(user.getId(), new ArrayList<>()), tokenSession);
    }

    @Transactional
    public UserRoleResponseDTO createUser(UserRoleCreateRequestDTO userRoleCreateRequestDTO) {
        if (!ESRegexUtils.isMobile(userRoleCreateRequestDTO.getMobile())) {
            throw new UserException(UserErrorEnum.USER_MOBILE_EMAIL_INVALID);
        }
        User user = new User(UserRegistType.MOBILE, Gender.UNKNOWN, userRoleCreateRequestDTO.getName(), userRoleCreateRequestDTO.getNickName(), null, userRoleCreateRequestDTO.getMobile(), null, null, null);
        user = userService.userRegist(user, null, new UserAuth(null, PasswordType.USERNAME_PASSWORD, userRoleCreateRequestDTO.getPassword()));
        Role role = rolesService.saveUserRole(wondernectCommonContext.getAuthorizeData().getUserId(), user.getId(), userRoleCreateRequestDTO.getRoleId());
        switch (role.getRoleType()) {
            case PLATFORM_ADMIN_INIT:
            {
                break;
            }
            case PLATFORM_USER_INIT:
            {
                // 注册商家
                shopManager.save(new Shop(userRoleCreateRequestDTO.getShopName(), user.getId()));
                break;
            }
            case APPLICATION_ADMIN_INIT:
            {
                // 注册员工
                if (ESObjectUtils.isNull(shopManager.findById(userRoleCreateRequestDTO.getShopId()))) {
                    throw new BusinessException("商家信息不存在");
                }
                userShopManager.save(new UserShop(user.getId(), userRoleCreateRequestDTO.getShopId()));
                break;
            }
            case APPLICATION_USER_CUSTOM:
            {
                // 注册司机
                if (role.getIsInit()) {
                    driverManager.save(new Driver(user.getId(), user.getName(), user.getMobile(), userRoleCreateRequestDTO.getDriverEvaluate(), 0, 0));
                    driverCurrentCache.save(new DriverCurrent(user.getId(), null, null, null, DriverStatus.UNKNOWN, null, null, userRoleCreateRequestDTO.getDriverEvaluate()));
                }
                break;
            }
        }
        return new UserRoleResponseDTO(user, rolesService.findAllByUserId(user.getId(), new ArrayList<>()));
    }

    @Transactional
    public UserRoleResponseDTO addUser(UserRoleAddRequestDTO userRoleAddRequestDTO) {
        User user = userService.findByUserId(userRoleAddRequestDTO.getUserId());
        if (ESObjectUtils.isNull(user)) {
            throw new BusinessException("用户不存在");
        }
        Role role = rolesService.saveUserRole(wondernectCommonContext.getAuthorizeData().getUserId(), user.getId(), userRoleAddRequestDTO.getRoleId());
        switch (role.getRoleType()) {
            case PLATFORM_ADMIN_INIT:
            {
                break;
            }
            case PLATFORM_USER_INIT:
            {
                // 注册商家
                shopManager.save(new Shop(userRoleAddRequestDTO.getShopName(), user.getId()));
                break;
            }
            case APPLICATION_ADMIN_INIT:
            {
                // 注册员工
                if (ESObjectUtils.isNull(shopManager.findById(userRoleAddRequestDTO.getShopId()))) {
                    throw new BusinessException("商家信息不存在");
                }
                userShopManager.save(new UserShop(user.getId(), userRoleAddRequestDTO.getShopId()));
                break;
            }
            case APPLICATION_USER_CUSTOM:
            {
                // 注册司机
                if (role.getIsInit()) {
                    driverManager.save(new Driver(user.getId(), user.getName(), user.getMobile(), userRoleAddRequestDTO.getDriverEvaluate(), 0, 0));
                    driverCurrentCache.save(new DriverCurrent(user.getId(), null, null, null, DriverStatus.UNKNOWN, null, null, userRoleAddRequestDTO.getDriverEvaluate()));
                }
                break;
            }
        }
        return new UserRoleResponseDTO(user, rolesService.findAllByUserId(user.getId(), new ArrayList<>()));
    }

    @Transactional
    public UserRoleResponseDTO updateUser(UserUpdateRequestDTO userUpdateRequestDTO) {
        User user = userService.findByUserId(userUpdateRequestDTO.getUserId());
        user.setName(userUpdateRequestDTO.getName());
        user.setNickName(userUpdateRequestDTO.getNickName());
        user.setAvatar(userUpdateRequestDTO.getAvatar());
        user.setMobile(userUpdateRequestDTO.getMobile());
        user.setRemark(userUpdateRequestDTO.getRemark());
        user.setGender(userUpdateRequestDTO.getGender());
        user.setLocation(userUpdateRequestDTO.getLocation());
        user = userService.updateUser(user);
        Driver driver = driverManager.findByUserId(userUpdateRequestDTO.getUserId());
        if (ESObjectUtils.isNotNull(driver)) {
            driver.setName(user.getName());
            driver.setMobile(user.getMobile());
            driverManager.save(driver);
        }
        if (ESStringUtils.isNotBlank(userUpdateRequestDTO.getPassword())) {
            userService.setUserPassword(new UserAuth(user.getId(), PasswordType.USERNAME_PASSWORD, userUpdateRequestDTO.getPassword()));
        }
        return new UserRoleResponseDTO(user, rolesService.findAllByUserId(userUpdateRequestDTO.getUserId(), new ArrayList<>()));
    }

    @Transactional
    public void setUserPassword(UserPasswordNewRequestDTO userPasswordNewRequestDTO) {
        User user = userService.findByUsername(userPasswordNewRequestDTO.getUsername());
        if (ESObjectUtils.isNull(user)) {
            throw new UserException(UserErrorEnum.USER_NOT_FOUND);
        }
        CaptchaSession captchaSession = captchaSessionService.authCaptchaSession(userPasswordNewRequestDTO.getUsername(), userPasswordNewRequestDTO.getCaptchaSessionId(), userPasswordNewRequestDTO.getCaptcha());
        UserAuth userAuth = new UserAuth(user.getId(), PasswordType.USERNAME_PASSWORD, userPasswordNewRequestDTO.getPassword());
        userService.setUserPassword(userAuth);
        captchaSessionService.deleteByCaptchaSessionId(captchaSession.getUsername(), captchaSession.getId());
    }

    @Transactional
    public void deleteByUserId(String userId) {
        userService.deleteByUserId(wondernectCommonContext.getAuthorizeData().getUserId(), userId);
        Driver driver = driverManager.findByUserId(userId);
        if (ESObjectUtils.isNotNull(driver)) {
            driverCurrentCache.remove(driver.getUserId());
            driverManager.deleteById(driver.getId());
            Car car = carManager.findByDriverUserId(driver.getUserId());
            if (ESObjectUtils.isNotNull(car)) {
                car.setDriverUserId(null);
                carManager.save(car);
            }
        }
        List<Shop> shopList = shopManager.list(null, userId, new ArrayList<>());
        if (CollectionUtils.isNotEmpty(shopList)) {
            throw new BusinessException("商家负责人,不能删除");
        }
        rolesService.deleteUserRoles(wondernectCommonContext.getAuthorizeData().getUserId(), userId);
    }

    public UserRoleResponseDTO findByUserId(String userId) {
        if (ESStringUtils.isRealEmpty(userId)) {
            userId = wondernectCommonContext.getAuthorizeData().getUserId();
        }
        User user = userService.findByUserId(userId);
        if (ESObjectUtils.isNull(user)) {
            return null;
        }
        return new UserRoleResponseDTO(user, rolesService.findAllByUserId(userId, new ArrayList<>()));
    }

    public List<Shop> findUserShopList(String userId) {
        List<Shop> shopList = shopManager.list(null, userId, new ArrayList<>());
        List<UserShop> userShopList = userShopManager.findAllByUserId(userId, new ArrayList<>());
        if (CollectionUtils.isEmpty(shopList)) {
            shopList = new ArrayList<>();
        }
        if (CollectionUtils.isNotEmpty(userShopList)) {
            for (UserShop userShop : userShopList) {
                shopList.add(shopManager.findById(userShop.getShopId()));
            }
        }
        return shopList;
    }

    public List<User> findShopUserList(String shopId) {
        Shop shop = shopManager.findById(shopId);
        if (ESObjectUtils.isNull(shop)) {
            throw new BusinessException("商家不存在");
        }
        List<User> userList = new ArrayList<>();
        List<UserShop> userShopList = userShopManager.findAllByShopId(shopId, new ArrayList<>());
        if (CollectionUtils.isNotEmpty(userShopList)) {
            for (UserShop userShop : userShopList) {
                userList.add(userService.findByUserId(userShop.getUserId()));
            }
        }
        return userList;
    }

    public List<UserLoginResponseDTO> findList(String username, List<SortData> sortDataList) {
        List<User> userList = userService.findList(null, username, username, username, sortDataList);
        List<UserLoginResponseDTO> userLoginResponseDTOList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(userList)) {
            for (User user : userList) {
                userLoginResponseDTOList.add(new UserLoginResponseDTO(user, rolesService.findAllByUserId(user.getId(), new ArrayList<>()), tokenSessionService.findLatestUserTokenSession(user.getId())));
            }
        }
        return userLoginResponseDTOList;
    }

    public PageResponseData<UserLoginResponseDTO> findPage(String username, PageRequestData pageRequestData) {
        PageResponseData<User> userPageResponseData = userService.findPage(null, username, username, username, pageRequestData);
        List<UserLoginResponseDTO> userLoginResponseDTOList = new ArrayList<>();
        List<User> userList = userPageResponseData.getDataList();
        if (CollectionUtils.isNotEmpty(userList)) {
            for (User user : userList) {
                userLoginResponseDTOList.add(new UserLoginResponseDTO(user, rolesService.findAllByUserId(user.getId(), new ArrayList<>()), tokenSessionService.findLatestUserTokenSession(user.getId())));
            }
        }
        return new PageResponseData<>(userPageResponseData.getPage(), userPageResponseData.getSize(), userPageResponseData.getTotalPages(), userPageResponseData.getTotalElements(), userLoginResponseDTOList);
    }

    @Transactional
    public UserLoginResponseDTO login(UserLoginRequestDTO userLoginRequestDTO) {
        User user = userService.findByUsername(userLoginRequestDTO.getUsername());
        if (ESObjectUtils.isNull(user)) {
            throw new UserException(UserErrorEnum.USER_NOT_FOUND);
        }
        wondernectCommonContext.setAuthorizeData(new AuthorizeData(null, user.getId(), null));
        UserAuth userAuth = new UserAuth(user.getId(), PasswordType.USERNAME_PASSWORD, userLoginRequestDTO.getPassword());
        userService.authUserPassword(userAuth);
        TokenSession tokenSession = tokenSessionService.requestTokenSession(
                "登录",
                wondernectCommonContext.getRequestIp(),
                wondernectCommonContext.getDevicePlatform(),
                wondernectCommonContext.getRequestDevice(),
                wondernectCommonContext.getDeviceIdentifier()
        );
        return new UserLoginResponseDTO(user, rolesService.findAllByUserId(user.getId(), new ArrayList<>()), tokenSession);
    }

    @Transactional
    public UserLoginResponseDTO driverCaptchalogin(CaptchaLoginRequestDTO captchaLoginRequestDTO) {
        User user = userService.findByUsername(captchaLoginRequestDTO.getUsername());
        if (ESObjectUtils.isNull(user)) {
            throw new BusinessException("司机信息不存在");
        }
        List<Role> roleList = rolesService.findAllByUserId(user.getId(), new ArrayList<>());
        if (CollectionUtils.isEmpty(roleList)) {
            throw new BusinessException("非司机账号");
        }
        boolean isDriver = false;
        for (Role role : roleList) {
            if (role.getRoleType() == RoleType.APPLICATION_USER_CUSTOM && role.getIsInit()) {
                isDriver = true;
                break;
            }
        }
        if (!isDriver) {
            throw new BusinessException("非司机账号");
        }
        captchaSessionService.authCaptchaSession(captchaLoginRequestDTO.getUsername(), captchaLoginRequestDTO.getCaptchaSessionId(), captchaLoginRequestDTO.getCaptcha());
        wondernectCommonContext.setAuthorizeData(new AuthorizeData(null, user.getId(), null));
        TokenSession tokenSession = tokenSessionService.requestTokenSession(
                "登录",
                wondernectCommonContext.getRequestIp(),
                wondernectCommonContext.getDevicePlatform(),
                wondernectCommonContext.getRequestDevice(),
                wondernectCommonContext.getDeviceIdentifier()
        );
        captchaSessionService.deleteByCaptchaSessionId(captchaLoginRequestDTO.getUsername(), captchaLoginRequestDTO.getCaptchaSessionId());
        return new UserLoginResponseDTO(
                user,
                rolesService.findAllByUserId(user.getId(), new ArrayList<>()),
                tokenSession
        );
    }

    @Transactional
    public UserLoginResponseDTO wechatCaptchalogin(WechatCaptchaLoginRequestDTO wechatCaptchaLoginRequestDTO) {
        User user = userService.findByUsername(wechatCaptchaLoginRequestDTO.getMobile());
        if (ESObjectUtils.isNull(user)) {
            // 用户尚未注册
            user = userService.userRegist(
                    new User(UserRegistType.APP, Gender.UNKNOWN, wechatCaptchaLoginRequestDTO.getUsername(), wechatCaptchaLoginRequestDTO.getUsername(), null, wechatCaptchaLoginRequestDTO.getMobile(), null, null, null),
                    new OpenUser(AppType.WECHAT, wechatCaptchaLoginRequestDTO.getOpenId(), wechatCaptchaLoginRequestDTO.getUsername(), null, null),
                    null
            );
        }
        OpenUser openUser = openUserManager.findByUserId(user.getId());
        if (ESObjectUtils.isNull(openUser)) {
            // 开放用户为空
            openUserManager.save(new OpenUser(AppType.WECHAT, wechatCaptchaLoginRequestDTO.getOpenId(), wechatCaptchaLoginRequestDTO.getUsername(), null, user.getId()));
        }
        captchaSessionService.authCaptchaSession(wechatCaptchaLoginRequestDTO.getMobile(), wechatCaptchaLoginRequestDTO.getCaptchaSessionId(), wechatCaptchaLoginRequestDTO.getCaptcha());
        wondernectCommonContext.setAuthorizeData(new AuthorizeData(null, user.getId(), null));
        TokenSession tokenSession = tokenSessionService.requestTokenSession(
                "登录",
                wondernectCommonContext.getRequestIp(),
                wondernectCommonContext.getDevicePlatform(),
                wondernectCommonContext.getRequestDevice(),
                wondernectCommonContext.getDeviceIdentifier()
        );
        captchaSessionService.deleteByCaptchaSessionId(wechatCaptchaLoginRequestDTO.getMobile(), wechatCaptchaLoginRequestDTO.getCaptchaSessionId());
        return new UserLoginResponseDTO(
                user,
                null,
                tokenSession
        );
    }

    @Transactional
    public UserLoginResponseDTO appUserlogin(AppUserLoginRequestDTO appUserLoginRequestDTO) {
        OpenUser openUser = openUserManager.findByAppTypeAndAppUserId(AppType.WECHAT, appUserLoginRequestDTO.getAppUserId());
        if (ESObjectUtils.isNull(openUser)) {
            throw new BusinessException("用户尚未注册");
        }
        wondernectCommonContext.setAuthorizeData(new AuthorizeData(null, openUser.getUserId(), null));
        TokenSession tokenSession = tokenSessionService.requestTokenSession(
                "登录",
                wondernectCommonContext.getRequestIp(),
                wondernectCommonContext.getDevicePlatform(),
                wondernectCommonContext.getRequestDevice(),
                wondernectCommonContext.getDeviceIdentifier()
        );
        return new UserLoginResponseDTO(
                userService.findByUserId(openUser.getUserId()),
                rolesService.findAllByUserId(openUser.getUserId(), new ArrayList<>()),
                tokenSession
        );
    }

    @Transactional
    public void logout() {
        tokenSessionService.deleteByToken(wondernectCommonContext.getAuthorizeData().getUserId(), wondernectCommonContext.getAuthorizeData().getToken());
    }
}
