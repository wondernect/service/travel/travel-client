package com.wondernect.travel.api;

import com.wondernect.elements.authorize.context.AuthorizeData;
import com.wondernect.elements.authorize.context.WondernectCommonContext;
import com.wondernect.elements.common.exception.BusinessException;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.common.utils.ESRegexUtils;
import com.wondernect.elements.common.utils.ESStringUtils;
import com.wondernect.services.session.model.CaptchaSession;
import com.wondernect.services.session.service.impl.DefaultCaptchaSessionService;
import com.wondernect.services.user.manager.OpenUserManager;
import com.wondernect.services.user.model.OpenUser;
import com.wondernect.services.user.model.User;
import com.wondernect.services.user.model.em.AppType;
import com.wondernect.services.user.service.UserService;
import com.wondernect.travel.dto.captcha.CaptchaRequestDTO;
import com.wondernect.travel.dto.captcha.CaptchaResponseDTO;
import com.wondernect.travel.dto.captcha.WeChatCaptchaRequestDTO;
import com.wondernect.travel.dto.captcha.WechatCaptchaResponseDTO;
import com.wondernect.travel.model.schedule.SMSSchedule;
import com.wondernect.travel.task.SMSScheduleTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: DefaultAdministratorCaptchaSessionService
 * Author: chenxun
 * Date: 2019/4/4 10:22
 * Description:
 */
@Service
public class CaptchaSessionAPI {

    @Autowired
    private DefaultCaptchaSessionService captchaSessionService;

    @Autowired
    private UserService userService;

    @Autowired
    private OpenUserManager openUserManager;

    @Autowired
    private WondernectCommonContext wondernectCommonContext;

    @Autowired
    private SMSScheduleTask smsScheduleTask;

    @Autowired
    private WechatAPI wechatAPI;

    public CaptchaResponseDTO requestCaptchaSession(CaptchaRequestDTO captchaRequestDTO) {
        wondernectCommonContext.setAuthorizeData(new AuthorizeData(null, captchaRequestDTO.getUsername(), null));
        if (ESStringUtils.isBlank(captchaRequestDTO.getCaptchaType())) {
            captchaRequestDTO.setCaptchaType("MOBILE");
        }
        if (ESStringUtils.isBlank(captchaRequestDTO.getDescription())) {
            captchaRequestDTO.setDescription("手机验证码登录");
        }
        if (ESObjectUtils.isNull(captchaRequestDTO.getExpires()) || captchaRequestDTO.getExpires() == 0) {
            captchaRequestDTO.setExpires(300L);
        }
        CaptchaSession captchaSession;
        switch (captchaRequestDTO.getCaptchaType()) {
            case "MOBILE":
            {
                if (!ESRegexUtils.isMobile(captchaRequestDTO.getUsername())) {
                    throw new RuntimeException("手机号码格式有误");
                }
                captchaSession = captchaSessionService.requestCaptchaSession(
                        captchaRequestDTO.getUsername(),
                        captchaRequestDTO.getDescription(),
                        captchaRequestDTO.getExpires(),
                        wondernectCommonContext.getRequestIp(),
                        wondernectCommonContext.getDevicePlatform(),
                        wondernectCommonContext.getRequestDevice()
                );
                smsScheduleTask.addSMS(new SMSSchedule(0, captchaRequestDTO.getUsername(), null, captchaSession.getCaptcha()));
                captchaSession.setCaptcha("");
                break;
            }
            default:
            {
                throw new RuntimeException("请求验证码方式有误");
            }
        }
        return new CaptchaResponseDTO(captchaSession.getUsername(), captchaSession.getId(), captchaSession.getCaptcha(), captchaSession.getExpires(), captchaSession.getDescription());
    }

    // public WechatCaptchaResponseDTO requestCaptchaSession(WeChatCaptchaRequestDTO captchaRequestDTO) {
    //     String openId = wechatAPI.getOpenId(captchaRequestDTO.getCode());
    //     if (ESStringUtils.isBlank(openId)) {
    //         throw new BusinessException("用户openId获取失败");
    //     }
    //     OpenUser openUser = openUserManager.findByAppTypeAndAppUserId(AppType.WECHAT, openId);
    //     if (ESObjectUtils.isNotNull(openUser)) {
    //         User user = userService.findByUserId(openUser.getUserId());
    //         if (ESObjectUtils.isNotNull(user) && !ESStringUtils.equalsIgnoreCase(user.getMobile(), captchaRequestDTO.getUsername())) {
    //             throw new BusinessException("当前微信用户登录手机号码有误，请重新输入");
    //         }
    //     }
    //     wondernectCommonContext.setAuthorizeData(new AuthorizeData(null, captchaRequestDTO.getUsername(), null));
    //     CaptchaSession captchaSession;
    //     if (!ESRegexUtils.isMobile(captchaRequestDTO.getUsername())) {
    //         throw new RuntimeException("手机号码格式有误");
    //     }
    //     captchaSession = captchaSessionService.requestCaptchaSession(
    //             captchaRequestDTO.getUsername(),
    //             "手机验证码登录",
    //             300L,
    //             wondernectCommonContext.getRequestIp(),
    //             wondernectCommonContext.getDevicePlatform(),
    //             wondernectCommonContext.getRequestDevice()
    //     );
    //     smsScheduleTask.addSMS(new SMSSchedule(0, captchaRequestDTO.getUsername(), null, captchaSession.getCaptcha()));
    //     captchaSession.setCaptcha("");
    //     return new WechatCaptchaResponseDTO(openId, captchaSession.getUsername(), captchaSession.getId(), captchaSession.getCaptcha(), captchaSession.getExpires(), captchaSession.getDescription());
    // }
}
