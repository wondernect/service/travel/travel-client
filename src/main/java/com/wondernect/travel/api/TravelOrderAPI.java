package com.wondernect.travel.api;

import com.wondernect.elements.authorize.context.WondernectCommonContext;
import com.wondernect.elements.common.exception.BusinessException;
import com.wondernect.elements.common.utils.*;
import com.wondernect.elements.rdb.request.PageRequestData;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.services.user.manager.OpenUserManager;
import com.wondernect.services.user.model.OpenUser;
import com.wondernect.services.user.model.User;
import com.wondernect.services.user.service.UserService;
import com.wondernect.travel.business.chexing.manager.ChexingManager;
import com.wondernect.travel.business.chexing.model.Chexing;
import com.wondernect.travel.business.user.manager.WalletManager;
import com.wondernect.travel.business.user.model.Wallet;
import com.wondernect.travel.business.youhui.manager.CouponManager;
import com.wondernect.travel.business.youhui.model.Coupon;
import com.wondernect.travel.business.youhui.model.CouponCategory;
import com.wondernect.travel.business.zuowei.manager.ZuoweiManager;
import com.wondernect.travel.business.zuowei.model.Zuowei;
import com.wondernect.travel.constant.CarConfigConstant;
import com.wondernect.travel.dto.base.RefundResponseDTO;
import com.wondernect.travel.model.em.*;
import com.wondernect.travel.model.schedule.AutoSchedule;
import com.wondernect.travel.model.schedule.SMSSchedule;
import com.wondernect.travel.task.AutoScheduleTask;
import com.wondernect.travel.task.SMSScheduleTask;
import com.wondernect.travel.wechat.WXPay;
import com.wondernect.travel.wechat.WXPayConfigImpl;
import com.wondernect.travel.wechat.constants.WXPayConstants;
import com.wondernect.travel.wechat.util.DateTimeUtil;
import com.wondernect.travel.wechat.util.WXPayUtil;
import com.wondernect.travel.manager.*;
import com.wondernect.travel.model.*;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

import static com.wondernect.travel.wechat.constants.WXPayConstants.SUCCESS;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: TravelOrderAPI
 * Author: chenxun
 * Date: 2019/5/13 16:53
 * Description:
 */
@Service
public class TravelOrderAPI {

    private static final Logger logger = LoggerFactory.getLogger(TravelOrderAPI.class);

    @Autowired
    private CouponManager couponManager;

    @Autowired
    private WalletManager walletManager;

    @Autowired
    private TravelOrderManager travelOrderManager;

    @Autowired
    private RefundConfigManager refundConfigManager;

    @Autowired
    private RefundOrderManager refundOrderManager;

    @Autowired
    private UserService userService;

    @Autowired
    private OpenUserManager openUserManager;

    @Autowired
    private AutoScheduleTask autoScheduleTask;

    @Autowired
    private OrderStrategyManager orderStrategyManager;

    @Autowired
    private ShopManager shopManager;

    @Autowired
    private DriverManager driverManager;

    @Autowired
    private WondernectCommonContext wondernectCommonContext;

    @Autowired
    private SMSScheduleTask smsScheduleTask;

    @Autowired
    private CarManager carManager;

    @Autowired
    private ZuoweiManager zuoweiManager;

    @Autowired
    private ChexingManager chexingManager;

    @Autowired
    private TravelOrderHistoryManager travelOrderHistoryManager;

    // 用户/商家创建订单
    public TravelOrder createOrder(String operatorId, TravelOrder travelOrder) {
        switch (travelOrder.getOrderSource()) {
            case SHOP:
            {
                if (ESStringUtils.isBlank(travelOrder.getShopId())) {
                    throw new BusinessException("商家不能为空");
                }
                Shop shop = shopManager.findById(travelOrder.getShopId());
                if (ESObjectUtils.isNull(shop)) {
                    throw new BusinessException("商家不存在");
                }
                Shop sourceShop = null;
                if (ESStringUtils.isNotBlank(travelOrder.getSourceShopId())) {
                    sourceShop = shopManager.findById(travelOrder.getSourceShopId());
                }
                Driver sourceDriver = null;
                if (ESStringUtils.isNotBlank(travelOrder.getSourceDriverUserId())) {
                    sourceDriver = driverManager.findByUserId(travelOrder.getSourceDriverUserId());
                }
                User user = userService.findByUserId(operatorId);
                if (ESObjectUtils.isNull(user)) {
                    throw new BusinessException("商家负责人不存在");
                }
                travelOrder.setShopName(shop.getName());
                travelOrder.setSourceShopName(ESObjectUtils.isNotNull(sourceShop) ? sourceShop.getName() : null);
                travelOrder.setSourceDriverUserName(ESObjectUtils.isNotNull(sourceDriver) ? sourceDriver.getName() : null);
                travelOrder.setUserId(operatorId);
                travelOrder.setUserMobile(user.getMobile());
                break;
            }
            case USER:
            {
                if (ESStringUtils.isBlank(travelOrder.getUserId())) {
                    throw new BusinessException("用户不能为空");
                }
                Shop sourceShop = null;
                if (ESStringUtils.isNotBlank(travelOrder.getSourceShopId())) {
                    sourceShop = shopManager.findById(travelOrder.getSourceShopId());
                }
                Driver sourceDriver = null;
                if (ESStringUtils.isNotBlank(travelOrder.getSourceDriverUserId())) {
                    sourceDriver = driverManager.findByUserId(travelOrder.getSourceDriverUserId());
                }
                User user = userService.findByUserId(operatorId);
                OpenUser openUser = openUserManager.findByUserId(operatorId);
                if (ESObjectUtils.isNull(user) || ESObjectUtils.isNull(openUser)) {
                    throw new BusinessException("用户不存在");
                }
                travelOrder.setUserId(operatorId);
                travelOrder.setUserMobile(user.getMobile());
                travelOrder.setUserWXName(user.getName());
                travelOrder.setOpenId(openUser.getAppUserId());
                travelOrder.setShopId(null);
                travelOrder.setShopName(null);
                travelOrder.setSourceShopName(ESObjectUtils.isNotNull(sourceShop) ? sourceShop.getName() : null);
                travelOrder.setSourceDriverUserName(ESObjectUtils.isNotNull(sourceDriver) ? sourceDriver.getName() : null);
                break;
            }
            default:
            {
                throw new BusinessException("订单来源非法");
            }
        }
        if (ESStringUtils.isBlank(travelOrder.getZuoweiId())) {
            travelOrder.setZuoweiId(CarConfigConstant.ALL_CAR_CONFIG);
        }
        if (ESStringUtils.isBlank(travelOrder.getChexingId())) {
            travelOrder.setChexingId(CarConfigConstant.ALL_CAR_CONFIG);
        }
        travelOrder.setOrderStatus(OrderStatus.WAIT_APPROVAL_OR_PAY);
        travelOrder.setWarningStatus(WarningStatus.NORMAL);
        travelOrder.setId(null);
        Long currentTime = ESDateTimeUtils.getCurrentTimestamp();
        switch (travelOrder.getScene()) {
            case JIEJI:
            case SONGJI:
            case DAY_PRIVATE:
            case ORDER_SCENE:
            case ROAD_PRIVATE:
            case JINGDIAN_PRIVATE:
            case MEISHI_PRIVATE:
            {
                if (travelOrder.getCommonScene() == CommonScene.ORDER) {
                    OrderStrategy orderStrategy = orderStrategyManager.findNextOrderStrategy(null);
                    if (ESObjectUtils.isNotNull(orderStrategy)) {
                        travelOrder.setBeforeUseDispatchHour(orderStrategy.getBeforeUseDispatchHour());
                        Double beforeTime = orderStrategy.getBeforeUseDispatchHour()*3600*1000;
                        travelOrder.setAutoDispatchTime(travelOrder.getStartTime() - beforeTime.longValue());
                    } else {
                        // 没有派单策略则自动派单时间设为当前时间
                        travelOrder.setAutoDispatchTime(currentTime);
                    }
                } else {
                    travelOrder.setStartTime(currentTime);
                    travelOrder.setAutoDispatchTime(currentTime);
                }
                break;
            }
            case BANSHOU_PRIVATE:
            default:
            {
                break;
            }
        }
        travelOrder.setAcceptedTime(null);
        travelOrder.setExecuteTime(null);
        travelOrder.setDoneTime(null);
        travelOrder.setCancelTime(null);
        travelOrder.setSettledTime(null);
        travelOrder.setDriverUserId(null);
        travelOrder.setDriverUserName(null);
        travelOrder.setDriverMobile(null);
        travelOrder.setDriverCarNo(null);
        travelOrder = travelOrderManager.save(travelOrder);
        if (travelOrder.getOrderSource() == OrderSource.USER) {
            // 用户来源订单统一下单
            long startTime = ESDateTimeUtils.getCurrentTimestamp();
            logger.info("微信统一下单开始:{}", travelOrder.getUsername());
            Map<String, String> payResult = wechatPay(travelOrder.getOpenId(), travelOrder.getId(), travelOrder.getPrice());
            logger.info("微信统一下单响应:{}，耗时:{}", travelOrder.getUsername(), ESDateTimeUtils.getCurrentTimestamp() - startTime);
            if (ESObjectUtils.isNull(payResult)) {
                logger.info("微信统一下单失败:{}", travelOrder.getUsername());
                if (ESObjectUtils.isNotNull(travelOrderManager.findById(travelOrder.getId()))) {
                    travelOrderManager.deleteById(travelOrder.getId());
                }
                throw new BusinessException("微信统一下单失败");
            } else {
                if (ESStringUtils.isBlank(payResult.get("prepay_id"))
                        || ESStringUtils.isBlank(payResult.get("nonceStr"))
                        || ESStringUtils.isBlank(payResult.get("timeStamp"))
                        || ESStringUtils.isBlank(payResult.get("sign"))) {
                    logger.info("微信统一下单失败:{}", travelOrder.getUsername());
                    if (ESObjectUtils.isNotNull(travelOrderManager.findById(travelOrder.getId()))) {
                        travelOrderManager.deleteById(travelOrder.getId());
                    }
                    throw new BusinessException("微信统一下单失败");
                }
                travelOrder.setWechatOrderId(payResult.get("prepay_id"));
                travelOrder.setWechatNonceStr(payResult.get("nonceStr"));
                travelOrder.setWechatTimestamp(payResult.get("timeStamp"));
                travelOrder.setWechatPaySign(payResult.get("sign"));
            }
            travelOrder = travelOrderManager.save(travelOrder);
        }
        travelOrderHistoryManager.save(new TravelOrderHistory(travelOrder.getId(), travelOrder.getOrderStatus(), "下单"));
        // 是否使用优惠券
        if (ESStringUtils.isNotBlank(travelOrder.getCouponId()) && travelOrder.getCouponPrice() > 0) {
            Coupon coupon = couponManager.findById(travelOrder.getCouponId());
            if (ESObjectUtils.isNotNull(coupon)) {
                if (coupon.getHasUsed()) {
                    throw new BusinessException("当前您选择的优惠券已使用，请选择其他优惠券");
                }
                coupon.setHasUsed(true);
                couponManager.save(coupon);
            }
        }
        return travelOrder;
    }

    // 微信统一下单
    public Map<String, String> wechatPay(String openId, String travelOrderId, Double price) {
        WXPay wxpay;
        Map<String, String> result = new HashMap<>();
        try {
            wxpay = new WXPay(WXPayConfigImpl.getInstance());
            Map<String, String> data = new HashMap<String, String>();
            data.put("body", "旅王出行-订单费用");
            data.put("out_trade_no", travelOrderId); // 订单唯一编号, 不允许重复
            data.put("total_fee", String.valueOf(new BigDecimal(price * 100).intValue())); // 订单金额, 单位分
            data.put("spbill_create_ip", wondernectCommonContext.getRequestIp()); // 下单ip
            data.put("openid", openId); // 微信公众号统一标示openid
            data.put("notify_url", WXPayConstants.NOTIFY_URL_PAY); // 订单结果通知, 微信主动回调此接口
            data.put("trade_type", "JSAPI"); // 固定填写
            logger.info("发起微信支付下单接口, request={}", data);
            Map<String, String> response = wxpay.unifiedOrder(data); // 微信sdk集成方法, 统一下单接口unifiedOrder, 此处请求   MD5加密   加密方式
            logger.info("微信支付下单返回值, response={}", response);
            String returnCode = response.get("return_code");
            if (!SUCCESS.equals(returnCode)) {
                return null;
            }
            String resultCode = response.get("result_code");
            if (!SUCCESS.equals(resultCode)) {
                return null;
            }
            String prepay_id = response.get("prepay_id");
            if (prepay_id == null) {
                return null;
            }
            // ******************************************
            //
            //  前端调起微信支付必要参数
            //
            // ******************************************
            String packages = "prepay_id=" + prepay_id;
            Map<String, String> wxPayMap = new HashMap<>();
            wxPayMap.put("appId", WXPayConfigImpl.getInstance().getAppID());
            wxPayMap.put("timeStamp", String.valueOf(DateTimeUtil.getTenTimeByDate(new Date())));
            wxPayMap.put("nonceStr", WXPayUtil.generateNonceStr());
            wxPayMap.put("package", packages);
            wxPayMap.put("signType", "MD5");
            // 加密串中包括 appId timeStamp nonceStr package signType 5个参数, 通过sdk WXPayUtil类加密, 注意, 此处使用  MD5加密  方式
            String sign = WXPayUtil.generateSignature(wxPayMap, WXPayConfigImpl.getInstance().getKey());
            // ******************************************
            //
            //  返回给前端调起微信支付的必要参数
            //
            // ******************************************
            result.put("prepay_id", prepay_id);
            result.put("sign", sign);
            result.putAll(wxPayMap);
        } catch (Exception e) {
            result = null;
        }
        return result;
    }

    // 生成订单待付款信息
    @Transactional
    public TravelOrder waitPayOrder(String orderId) {
        TravelOrder travelOrder = travelOrderManager.findById(orderId);
        if (ESObjectUtils.isNull(travelOrder)) {
            throw new BusinessException("订单不存在");
        }
        if (travelOrder.getOrderStatus() != OrderStatus.WAIT_APPROVAL_OR_PAY) {
            throw new BusinessException("订单非待支付状态");
        }
        if (ESStringUtils.isBlank(travelOrder.getWechatOrderId())) {
            throw new BusinessException("微信统一下单失败，请取消后重新下单");
        }
        try {
            String packages = "prepay_id=" + travelOrder.getWechatOrderId();
            Map<String, String> wxPayMap = new HashMap<>();
            wxPayMap.put("appId", WXPayConfigImpl.getInstance().getAppID());
            wxPayMap.put("timeStamp", String.valueOf(DateTimeUtil.getTenTimeByDate(new Date())));
            wxPayMap.put("nonceStr", WXPayUtil.generateNonceStr());
            wxPayMap.put("package", packages);
            wxPayMap.put("signType", "MD5");
            // 加密串中包括 appId timeStamp nonceStr package signType 5个参数, 通过sdk WXPayUtil类加密, 注意, 此处使用  MD5加密  方式
            String sign = WXPayUtil.generateSignature(wxPayMap, WXPayConfigImpl.getInstance().getKey());
            travelOrder.setWechatNonceStr(wxPayMap.get("nonceStr"));
            travelOrder.setWechatTimestamp(wxPayMap.get("timeStamp"));
            travelOrder.setWechatPaySign(sign);
            travelOrderManager.save(travelOrder);
        } catch (Exception e) {
            travelOrder = null;
        }
        return travelOrder;
    }

    // 监听用户微信付款后订单通知
    public boolean confirmUserWechatPay(String orderId) {
        boolean pay = true;
        try {
            TravelOrder travelOrder = travelOrderManager.findById(orderId);
            if (ESObjectUtils.isNull(travelOrder) || travelOrder.getOrderStatus() != OrderStatus.WAIT_APPROVAL_OR_PAY) {
                logger.error("微信支付回调成功===>处理订单状态，订单{}不存在或状态不是待付款", orderId);
                pay = false;
            } else {
                travelOrder.setOrderStatus(OrderStatus.WAIT_ACCEPT);
                travelOrder = travelOrderManager.save(travelOrder);
                // 将该订单添加到auto自动派单队列
                if (travelOrder.getScene() != Scene.BANSHOU_PRIVATE) {
                    autoScheduleTask.add(new AutoSchedule(travelOrder.getId(), null, ESDateTimeUtils.getCurrentTimestamp()));
                    smsScheduleTask.addSMS(new SMSSchedule(1, travelOrder.getMobile(), travelOrder.getId(), null));
                    travelOrderHistoryManager.save(new TravelOrderHistory(travelOrder.getId(), travelOrder.getOrderStatus(), "用户付款成功，进入派单队列等待派单"));
                } else {
                    smsScheduleTask.addSMS(new SMSSchedule(4, travelOrder.getMobile(), travelOrder.getId(), null));
                    travelOrderHistoryManager.save(new TravelOrderHistory(travelOrder.getId(), travelOrder.getOrderStatus(), "用户付款成功"));
                }
            }
        } catch (Exception e) {
            logger.error("微信支付回调成功===>处理订单状态，出现异常", e);
            pay = false;
        }
        return pay;
    }

    // 用户或商家修改订单
    @Transactional
    public TravelOrder saveOrder(TravelOrder travelOrder) {
        TravelOrder travelOrderSave;
        if (ESStringUtils.isBlank(travelOrder.getId())) {
            throw new BusinessException("订单id不能为空");
        }
        travelOrderSave = travelOrderManager.findById(travelOrder.getId());
        if (ESObjectUtils.isNull(travelOrderSave)) {
            throw new BusinessException("订单不存在");
        }
        if (travelOrderSave.getOrderStatus() != OrderStatus.WAIT_APPROVAL_OR_PAY) {
            throw new BusinessException("订单非待付款或待审核状态，不可修改");
        }
        travelOrderSave.setScene(travelOrder.getScene());
        travelOrderSave.setCommonScene(travelOrder.getCommonScene());
        travelOrderSave.setWarningStatus(WarningStatus.NORMAL);
        travelOrderSave.setUsername(travelOrder.getUsername());
        travelOrderSave.setMobile(travelOrder.getMobile());
        travelOrderSave.setContact(travelOrder.getContact());
        travelOrderSave.setContactMobile(travelOrder.getContactMobile());
        travelOrderSave.setStartTime(travelOrder.getStartTime());
        travelOrderSave.setStartPlace(travelOrder.getStartPlace());
        travelOrderSave.setStartLongitude(travelOrder.getStartLongitude());
        travelOrderSave.setStartLatitude(travelOrder.getStartLatitude());
        travelOrderSave.setTargetPlace(travelOrder.getTargetPlace());
        travelOrderSave.setTargetLongitude(travelOrder.getTargetLongitude());
        travelOrderSave.setTargetLatitude(travelOrder.getTargetLatitude());
        travelOrderSave.setAirNo(travelOrder.getAirNo());
        travelOrderSave.setRemark(travelOrder.getRemark());
        travelOrderSave.setZuoweiId(travelOrder.getZuoweiId());
        travelOrderSave.setChexingId(travelOrder.getChexingId());
        travelOrderSave.setKilo(travelOrder.getKilo());
        travelOrderSave.setTime(travelOrder.getTime());
        travelOrderSave.setPrice(travelOrder.getPrice());
        travelOrderSave = travelOrderManager.save(travelOrderSave);
        switch (travelOrderSave.getOrderSource()) {
            case SHOP:
            {
                travelOrderHistoryManager.save(new TravelOrderHistory(travelOrderSave.getId(), travelOrder.getOrderStatus(), "商家修改订单"));
                break;
            }
            case USER:
            {
                travelOrderHistoryManager.save(new TravelOrderHistory(travelOrderSave.getId(), travelOrder.getOrderStatus(), "用户修改订单"));
                break;
            }
        }
        return travelOrderSave;
    }

    // 商家审核完成
    @Transactional
    public void confirmShopApproval(String orderId) {
        TravelOrder travelOrder = travelOrderManager.findById(orderId);
        if (ESObjectUtils.isNull(travelOrder)) {
            throw new BusinessException("订单不存在");
        }
        if (travelOrder.getOrderSource() != OrderSource.SHOP) {
            throw new BusinessException("非商家来源订单");
        }
        if (travelOrder.getOrderStatus() != OrderStatus.WAIT_APPROVAL_OR_PAY) {
            throw new BusinessException("当前订单状态非待审核(" + travelOrder.getOrderStatus().toString() + ")");
        }
        travelOrder.setOrderStatus(OrderStatus.WAIT_ACCEPT);
        travelOrder = travelOrderManager.save(travelOrder);
        // 将该订单添加到auto自动派单队列
        if (travelOrder.getScene() != Scene.BANSHOU_PRIVATE) {
            autoScheduleTask.add(new AutoSchedule(travelOrder.getId(), null, ESDateTimeUtils.getCurrentTimestamp()));
            smsScheduleTask.addSMS(new SMSSchedule(1, travelOrder.getMobile(), travelOrder.getId(), null));
            travelOrderHistoryManager.save(new TravelOrderHistory(travelOrder.getId(), travelOrder.getOrderStatus(), "商家审核推送订单，进入派单队列等待派单"));
        }
    }

    // 商家批量审核完成
    @Transactional
    public void batchconfirmShopApproval(String[] orderIds) {
        List<String> orderIdList = Arrays.asList(orderIds);
        if (CollectionUtils.isNotEmpty(orderIdList)) {
            for (String orderId : orderIdList) {
                TravelOrder travelOrder = travelOrderManager.findById(orderId);
                if (ESObjectUtils.isNotNull(travelOrder) &&
                        travelOrder.getOrderSource() == OrderSource.SHOP &&
                        travelOrder.getOrderStatus() == OrderStatus.WAIT_APPROVAL_OR_PAY) {
                    travelOrder.setOrderStatus(OrderStatus.WAIT_ACCEPT);
                    travelOrder = travelOrderManager.save(travelOrder);
                    // 将该订单添加到auto自动派单队列
                    if (travelOrder.getScene() != Scene.BANSHOU_PRIVATE) {
                        autoScheduleTask.add(new AutoSchedule(travelOrder.getId(), null, ESDateTimeUtils.getCurrentTimestamp()));
                        travelOrderHistoryManager.save(new TravelOrderHistory(travelOrder.getId(), travelOrder.getOrderStatus(), "商家审核推送订单，进入派单队列等待派单"));
                    }
                }
            }
        }
    }

    // 司机接单
    @Transactional
    public synchronized void orderAccept(String orderId, String driverUserId) {
        User driver = userService.findByUserId(driverUserId);
        if (ESObjectUtils.isNull(driver)) {
            throw new BusinessException("司机不存在");
        }
        Car car = carManager.findByDriverUserId(driverUserId);
        if (ESObjectUtils.isNull(car)) {
            throw new BusinessException("司机对应车辆信息不存在");
        }
        Zuowei zuowei = zuoweiManager.findById(car.getZuoweiId());
        if (ESObjectUtils.isNull(zuowei)) {
            throw new BusinessException("座位id不存在");
        }
        Chexing chexing = chexingManager.findById(car.getChexingId());
        if (ESObjectUtils.isNull(chexing)) {
            throw new BusinessException("车型id不存在");
        }
        TravelOrder travelOrder = travelOrderManager.findById(orderId);
        if (ESObjectUtils.isNull(travelOrder)) {
            throw new BusinessException("订单不存在");
        }
        if (travelOrder.getOrderStatus() != OrderStatus.AUTO && travelOrder.getOrderStatus() != OrderStatus.WAIT_ACCEPT) {
            throw new BusinessException("当前订单状态非自动派单中(" + travelOrder.getOrderStatus().toString() + ")");
        }
        travelOrder.setDriverUserId(driverUserId);
        travelOrder.setDriverUserName(driver.getName());
        travelOrder.setDriverMobile(driver.getMobile());
        travelOrder.setDriverCarNo(car.getCarNo());
        travelOrder.setOrderStatus(OrderStatus.ACCEPTED);
        travelOrder.setWarningStatus(WarningStatus.NORMAL);
        travelOrder.setAcceptedTime(ESDateTimeUtils.getCurrentTimestamp());
        travelOrder = travelOrderManager.save(travelOrder);
        travelOrderManager.driverAcceptOrder(driverUserId, orderId, travelOrder.getCommonScene());
        if (travelOrder.getScene() != Scene.BANSHOU_PRIVATE) {
            // 发送短信给乘客
            smsScheduleTask.addSMS(new SMSSchedule(2, travelOrder.getMobile(), travelOrder.getId(), null));
            // 发送短信给司机
            smsScheduleTask.addSMS(new SMSSchedule(3, driver.getMobile(), travelOrder.getId(), null));
            // 接单历史记录
            travelOrderHistoryManager.save(new TravelOrderHistory(travelOrder.getId(), travelOrder.getOrderStatus(), "司机 " + driver.getName() + ":" + driver.getMobile() + " 接单"));
        }
    }

    // 后台指定司机接单
    @Transactional
    public synchronized void forceOrderAccept(String orderId, String driverUserId) {
        User driver = userService.findByUserId(driverUserId);
        if (ESObjectUtils.isNull(driver)) {
            throw new BusinessException("司机不存在");
        }
        Car car = carManager.findByDriverUserId(driverUserId);
        if (ESObjectUtils.isNull(car)) {
            throw new BusinessException("司机对应车辆信息不存在");
        }
        Zuowei zuowei = zuoweiManager.findById(car.getZuoweiId());
        if (ESObjectUtils.isNull(zuowei)) {
            throw new BusinessException("座位id不存在");
        }
        Chexing chexing = chexingManager.findById(car.getChexingId());
        if (ESObjectUtils.isNull(chexing)) {
            throw new BusinessException("车型id不存在");
        }
        TravelOrder travelOrder = travelOrderManager.findById(orderId);
        if (ESObjectUtils.isNull(travelOrder)) {
            throw new BusinessException("订单不存在");
        }
        if (travelOrder.getOrderStatus() != OrderStatus.AUTO) {
            throw new BusinessException("当前订单状态非自动派单中(" + travelOrder.getOrderStatus().toString() + ")");
        }
        travelOrder.setDriverUserId(driverUserId);
        travelOrder.setDriverUserName(driver.getName());
        travelOrder.setDriverMobile(driver.getMobile());
        travelOrder.setDriverCarNo(car.getCarNo());
        travelOrder.setForceAccept(true);
        travelOrder.setOrderStatus(OrderStatus.ACCEPTED);
        travelOrder.setWarningStatus(WarningStatus.NORMAL);
        travelOrder.setAcceptedTime(ESDateTimeUtils.getCurrentTimestamp());
        travelOrder = travelOrderManager.save(travelOrder);
        travelOrderManager.driverAcceptOrder(driverUserId, orderId, travelOrder.getCommonScene());
        if (travelOrder.getScene() != Scene.BANSHOU_PRIVATE) {
            // 发送短信给乘客
            smsScheduleTask.addSMS(new SMSSchedule(2, travelOrder.getMobile(), travelOrder.getId(), null));
            // 发送短信给司机
            smsScheduleTask.addSMS(new SMSSchedule(3, driver.getMobile(), travelOrder.getId(), null));
            // 后台指定司机接单
            travelOrderHistoryManager.save(new TravelOrderHistory(travelOrder.getId(), travelOrder.getOrderStatus(), "后台指定司机 " + driver.getName() + ":" + driver.getMobile() + " 接单"));
        }
    }

    // 司机申请改派订单
    @Transactional
    public void changeOrderDriver(String orderId, String driverUserId, String description) {
        User driver = userService.findByUserId(driverUserId);
        if (ESObjectUtils.isNull(driver)) {
            throw new BusinessException("司机不存在");
        }
        TravelOrder travelOrder = travelOrderManager.findById(orderId);
        if (ESObjectUtils.isNull(travelOrder)) {
            throw new BusinessException("订单不存在");
        }
        if (!ESStringUtils.equalsIgnoreCase(driverUserId, travelOrder.getDriverUserId())) {
            throw new BusinessException("非当前司机订单");
        }
        if (travelOrder.getOrderStatus() != OrderStatus.ACCEPTED) {
            throw new BusinessException("当前订单状态非已接单(" + travelOrder.getOrderStatus().toString() + ")");
        }
        travelOrder.setDriverUserId(null);
        travelOrder.setDriverUserName(null);
        travelOrder.setDriverMobile(null);
        travelOrder.setDriverCarNo(null);
        travelOrder.setOrderStatus(OrderStatus.WAIT_ACCEPT);
        travelOrder.setAcceptedTime(null);
        travelOrder = travelOrderManager.save(travelOrder);
        // 将该订单添加到auto自动派单队列
        if (travelOrder.getScene() != Scene.BANSHOU_PRIVATE) {
            autoScheduleTask.add(new AutoSchedule(travelOrder.getId(), null, ESDateTimeUtils.getCurrentTimestamp()));
            // 司机申请改派订单
            if (ESStringUtils.isBlank(description)) {
                travelOrderHistoryManager.save(new TravelOrderHistory(travelOrder.getId(), travelOrder.getOrderStatus(), "司机 " + driver.getName() + ":" + driver.getMobile() + " 申请改派订单"));
            } else {
                travelOrderHistoryManager.save(new TravelOrderHistory(travelOrder.getId(), travelOrder.getOrderStatus(), "因为 " + description + ",司机 " + driver.getName() + ":" + driver.getMobile() + " 申请改派订单"));
            }
        }
    }

    // 司机到达约定地点,开始订单
    @Transactional
    public void orderExecute(String orderId, String driverUserId) {
        User driver = userService.findByUserId(driverUserId);
        if (ESObjectUtils.isNull(driver)) {
            throw new BusinessException("司机不存在");
        }
        TravelOrder travelOrder = travelOrderManager.findById(orderId);
        if (ESObjectUtils.isNull(travelOrder)) {
            throw new BusinessException("订单不存在");
        }
        if (!ESStringUtils.equalsIgnoreCase(driverUserId, travelOrder.getDriverUserId())) {
            throw new BusinessException("非当前司机订单");
        }
        if (travelOrder.getOrderStatus() != OrderStatus.ACCEPTED) {
            throw new BusinessException("当前订单状态非已接单(" + travelOrder.getOrderStatus().toString() + ")");
        }
        travelOrder.setOrderStatus(OrderStatus.ON_THE_WAY);
        travelOrder.setExecuteTime(ESDateTimeUtils.getCurrentTimestamp());
        travelOrder = travelOrderManager.save(travelOrder);
        if (travelOrder.getScene() != Scene.BANSHOU_PRIVATE) {
            // 司机开始订单
            travelOrderHistoryManager.save(new TravelOrderHistory(travelOrder.getId(), travelOrder.getOrderStatus(), "司机 " + driver.getName() + ":" + driver.getMobile() + " 到达约定地点开始订单"));
        }
    }

    // 司机结束行程,完成订单
    @Transactional
    public void orderDone(String orderId, String driverUserId) {
        User driver = userService.findByUserId(driverUserId);
        if (ESObjectUtils.isNull(driver)) {
            throw new BusinessException("司机不存在");
        }
        TravelOrder travelOrder = travelOrderManager.findById(orderId);
        if (ESObjectUtils.isNull(travelOrder)) {
            throw new BusinessException("订单不存在");
        }
        if (!ESStringUtils.equalsIgnoreCase(driverUserId, travelOrder.getDriverUserId())) {
            throw new BusinessException("非当前司机订单");
        }
        if (travelOrder.getOrderStatus() != OrderStatus.ON_THE_WAY) {
            throw new BusinessException("当前订单状态非进行中(" + travelOrder.getOrderStatus().toString() + ")");
        }
        travelOrder.setOrderStatus(OrderStatus.DONE);
        travelOrder.setDoneTime(ESDateTimeUtils.getCurrentTimestamp());
        travelOrder = travelOrderManager.save(travelOrder);
        travelOrderManager.driverDoneOrder(driverUserId, orderId);
        if (travelOrder.getScene() != Scene.BANSHOU_PRIVATE) {
            // 司机完成订单
            travelOrderHistoryManager.save(new TravelOrderHistory(travelOrder.getId(), travelOrder.getOrderStatus(), "司机 " + driver.getName() + ":" + driver.getMobile() + " 结束行程完成订单"));
        }
        // 司机完成订单，检查是否使用优惠券，是否有分享用户id
        if (ESStringUtils.isNotBlank(travelOrder.getCouponId()) && travelOrder.getCouponPrice() > 0) {
            Coupon coupon = couponManager.findById(travelOrder.getCouponId());
            if (ESObjectUtils.isNotNull(coupon) && ESStringUtils.isNotBlank(coupon.getUserId())) {
                // 推荐用户不为空，下单成功，推荐用户将获得奖励
                if (coupon.getCouponCategory() == CouponCategory.FANLI) {
                    User user = userService.findByUserId(coupon.getUserId());
                    if (ESObjectUtils.isNotNull(user)) {
                        Wallet wallet = walletManager.findById(user.getId());
                        if (ESObjectUtils.isNotNull(wallet)) {
                            Double money = wallet.getMoney() + coupon.getFanli();
                            wallet.setMoney(money);
                        } else {
                            wallet = new Wallet(user.getId(), coupon.getFanli());
                        }
                        walletManager.save(wallet);
                    }
                }
            }
        }
    }

    // 获取司机已取消订单
    public List<TravelOrder> getDriverCancelList(String driverUserId, List<SortData> sortDataList) {
        return travelOrderManager.driverCancelList(driverUserId, sortDataList);
    }

    // 获取司机历史订单
    public PageResponseData<TravelOrder> getDriverOrderPage(String driverUserId, PageRequestData pageRequestData) {
        return travelOrderManager.driverDonePage(driverUserId, pageRequestData);
    }

    // 财务人员后台点击已结算
    @Transactional
    public void orderSettled(String orderId) {
        TravelOrder travelOrder = travelOrderManager.findById(orderId);
        if (ESObjectUtils.isNull(travelOrder)) {
            throw new BusinessException("订单不存在");
        }
        if (travelOrder.getOrderStatus() != OrderStatus.DONE) {
            throw new BusinessException("当前订单状态非已完成(" + travelOrder.getOrderStatus().toString() + ")");
        }
        travelOrder.setOrderStatus(OrderStatus.SETTLED);
        travelOrder.setSettledTime(ESDateTimeUtils.getCurrentTimestamp());
        travelOrder = travelOrderManager.save(travelOrder);
        if (travelOrder.getScene() != Scene.BANSHOU_PRIVATE) {
            travelOrderHistoryManager.save(new TravelOrderHistory(travelOrder.getId(), travelOrder.getOrderStatus(), "后台结算订单"));
        }
    }

    // 财务人员后台点击批量结算
    @Transactional
    public void orderBatchSettled(String[] orderIds) {
        List<String> orderIdList = Arrays.asList(orderIds);
        if (CollectionUtils.isNotEmpty(orderIdList)) {
            for (String orderId : orderIdList) {
                TravelOrder travelOrder = travelOrderManager.findById(orderId);
                if (ESObjectUtils.isNotNull(travelOrder)) {
                    if (travelOrder.getOrderStatus() == OrderStatus.DONE) {
                        travelOrder.setOrderStatus(OrderStatus.SETTLED);
                        travelOrder.setSettledTime(ESDateTimeUtils.getCurrentTimestamp());
                        travelOrder = travelOrderManager.save(travelOrder);
                        if (travelOrder.getScene() != Scene.BANSHOU_PRIVATE) {
                            travelOrderHistoryManager.save(new TravelOrderHistory(travelOrder.getId(), travelOrder.getOrderStatus(), "后台结算订单"));
                        }
                    }
                }
            }
        }
    }

    // 后台执行退款
    @Transactional
    public void refund(String orderId) {
        TravelOrder travelOrder = travelOrderManager.findById(orderId);
        if (ESObjectUtils.isNull(travelOrder)) {
            throw new BusinessException("订单不存在");
        }
        RefundOrder refundOrder = refundOrderManager.findByOrderId(orderId);
        if (ESObjectUtils.isNull(refundOrder)) {
            throw new BusinessException("退款订单不存在");
        }
        if (refundOrder.getHasRefund()) {
            throw new BusinessException("订单已完成退款");
        }
        // 微信申请退款
        Map<String, String> refundResponse = wechatRefund(orderId, refundOrder.getId(), refundOrder.getPrice(), refundOrder.getRefund());
        if (MapUtils.isEmpty(refundResponse)) {
            throw new BusinessException("微信申请退款失败");
        }
        travelOrderHistoryManager.save(new TravelOrderHistory(travelOrder.getId(), travelOrder.getOrderStatus(), "后台执行订单退款"));
    }

    // 微信申请退款
    public Map<String, String> wechatRefund(String travelOrderId, String refundId, Double total_fee, Double refund_fee) {
        WXPay wxpay;
        Map<String, String> result = new HashMap<>();
        try {
            wxpay = new WXPay(WXPayConfigImpl.getInstance());
            Map<String, String> data = new HashMap<>();
            data.put("out_trade_no", travelOrderId); // 订单唯一编号, 不允许重复
            data.put("out_refund_no",refundId); // 退款订单id
            data.put("total_fee", String.valueOf(new BigDecimal(total_fee * 100).intValue())); // 订单金额, 单位分
            data.put("refund_fee", String.valueOf(new BigDecimal(refund_fee*100).intValue())); // 订单金额, 单位分
            data.put("refund_desc", "取消订单"); // 微信公众号统一标示openid
            data.put("notify_url", WXPayConstants.NOTIFY_URL_REFUND); // 订单结果通知, 微信主动回调此接口
            // logger.info("发起微信退款接口调用, 请求参数 request={}", data);
            Map<String, String> response = wxpay.refund(data); // 微信sdk集成方法, 统一下单接口unifiedOrder, 此处请求   MD5加密   加密方式
            // logger.info("微信退款接口调用成功, 返回值 response={}", response);
            String returnCode = response.get("return_code");
            if (!SUCCESS.equals(returnCode)) {
                return null;
            }
            String resultCode = response.get("result_code");
            if (!SUCCESS.equals(resultCode)) {
                return null;
            }
            result.putAll(response);
        } catch (Exception e) {
            result = null;
        }
        return result;
    }

    // 接收微信退款通知
    public boolean confirmRefund(String orderId) {
        boolean refund = true;
        try {
            RefundOrder refundOrder = refundOrderManager.findByOrderId(orderId);
            if (ESObjectUtils.isNotNull(refundOrder)) {
                if (!refundOrder.getHasRefund()) {
                    refundOrder.setHasRefund(true);
                    refundOrderManager.save(refundOrder);
                }
            } else {
                logger.error("微信退款回调成功===>处理退款订单状态，订单{}对应退款信息不存在", orderId);
            }
        } catch (Exception e) {
            logger.error("微信退款回调成功===>处理退款订单状态，出现异常", e);
            refund = false;
        }
        return refund;
    }

    // 后台取消订单
    @Transactional
    public void orderConsoleCancel(String orderId) {
        TravelOrder travelOrder = travelOrderManager.findById(orderId);
        if (ESObjectUtils.isNull(travelOrder)) {
            throw new BusinessException("订单不存在");
        }
        if (ESObjectUtils.isNull(travelOrder.getOrderSource())) {
            throw new BusinessException("非法来源订单");
        }
        // 后台取消用户订单
        Long currentTime = ESDateTimeUtils.getCurrentTimestamp();
        switch (travelOrder.getOrderStatus()) {
            case ON_THE_WAY:
            case DONE:
            case SETTLED:
            case CANCEL_CONSOLE:
            case CANCEL_SHOP:
            case CANCEL_USER: {
                throw new BusinessException("不能取消订单,当前订单状态为" + travelOrder.getOrderStatus().toString());
            }
            case WAIT_APPROVAL_OR_PAY: {
                travelOrder.setRefundFee(0d);
                break;
            }
            case WAIT_ACCEPT:
            case AUTO:
            case ACCEPTED: {
                switch (travelOrder.getOrderSource()) {
                    case USER:
                    {
                        // 取消用户订单
                        OpenUser openUser = openUserManager.findByUserId(travelOrder.getUserId());
                        RefundOrder refundOrder = new RefundOrder(
                                travelOrder.getId(),
                                travelOrder.getUserId(),
                                travelOrder.getUsername(),
                                travelOrder.getMobile(),
                                ESObjectUtils.isNull(openUser) ? null : openUser.getAppUserName(),
                                travelOrder.getPrice(),
                                travelOrder.getPrice(),
                                null,
                                0d,
                                0d,
                                false
                        );
                        RefundConfig refundConfig;
                        switch (travelOrder.getScene()) {
                            case JIEJI:
                            case SONGJI:
                            case ORDER_SCENE:
                            case JINGDIAN_PRIVATE:
                            {
                                refundConfig = refundConut(RefundConfig.ORDER_CODE, currentTime, travelOrder.getStartTime(), refundOrder);
                                break;
                            }
                            case DAY_PRIVATE:
                            case ROAD_PRIVATE:
                            {
                                refundConfig = refundConut(RefundConfig.PRIVATE_CODE, currentTime, travelOrder.getStartTime(), refundOrder);
                                break;
                            }
                            case BANSHOU_PRIVATE:
                            {
                                refundConfig = refundConut(RefundConfig.ZERO_REFUND_CODE, null, null, refundOrder);
                                break;
                            }
                            default:
                            {
                                throw new BusinessException("订单用车场景非法:" + travelOrder.getScene());
                            }
                        }
                        refundOrder = refundOrderManager.save(refundOrder);
                        if (refundOrder.getRefund() > 0) {
                            if (ESObjectUtils.isNull(refundConfig.getLimitFund()) ||
                                    refundConfig.getLimitFund() <= 0 ||
                                    refundOrder.getRefund() < refundConfig.getLimitFund()) {
                                // 微信申请退款
                                Map<String, String> refundResponse = wechatRefund(orderId, refundOrder.getId(), refundOrder.getPrice(), refundOrder.getRefund());
                                if (MapUtils.isEmpty(refundResponse)) {
                                    throw new BusinessException("微信申请退款失败");
                                }
                            }
                        } else {
                            refundOrder.setHasRefund(true);
                            refundOrderManager.save(refundOrder);
                        }
                        travelOrder.setRefundFee(refundOrder.getFee());
                        break;
                    }
                    case SHOP:
                    {
                        // 取消商家订单
                        RefundOrder refundOrder = new RefundOrder(
                                travelOrder.getId(),
                                travelOrder.getUserId(),
                                travelOrder.getUsername(),
                                travelOrder.getMobile(),
                                null,
                                travelOrder.getPrice(),
                                travelOrder.getPrice(),
                                null,
                                0d,
                                0d,
                                false
                        );
                        switch (travelOrder.getScene()) {
                            case JIEJI:
                            case SONGJI:
                            case ORDER_SCENE:
                            case JINGDIAN_PRIVATE:
                            {
                                refundConut(RefundConfig.ORDER_CODE, currentTime, travelOrder.getStartTime(), refundOrder);
                                break;
                            }
                            case DAY_PRIVATE:
                            case ROAD_PRIVATE:
                            {
                                refundConut(RefundConfig.PRIVATE_CODE, currentTime, travelOrder.getStartTime(), refundOrder);
                                break;
                            }
                            case BANSHOU_PRIVATE:
                            {
                                refundConut(RefundConfig.ZERO_REFUND_CODE, null, null, refundOrder);
                                break;
                            }
                            default:
                            {
                                throw new BusinessException("订单用车场景非法:" + travelOrder.getScene());
                            }
                        }
                        travelOrder.setRefundFee(refundOrder.getFee());
                        break;
                    }
                }
            }
        }
        travelOrder.setOrderStatus(OrderStatus.CANCEL_CONSOLE);
        travelOrder.setWarningStatus(WarningStatus.NORMAL);
        travelOrder.setCancelTime(currentTime);
        travelOrder = travelOrderManager.save(travelOrder);
        travelOrderHistoryManager.save(new TravelOrderHistory(travelOrder.getId(), travelOrder.getOrderStatus(), "后台取消订单"));
        // 已付款订单退款检查优惠券使用情况执行回退操作
        if (ESStringUtils.isNotBlank(travelOrder.getCouponId()) && travelOrder.getCouponPrice() > 0) {
            Coupon coupon = couponManager.findById(travelOrder.getCouponId());
            if (ESObjectUtils.isNotNull(coupon) && ESStringUtils.isNotBlank(coupon.getUserId())) {
                coupon.setHasUsed(false);
                couponManager.save(coupon);
            }
        }
    }

    // 商家取消订单
    @Transactional
    public void orderShopCancel(String orderId) {
        TravelOrder travelOrder = travelOrderManager.findById(orderId);
        if (ESObjectUtils.isNull(travelOrder)) {
            throw new BusinessException("订单不存在");
        }
        if (ESObjectUtils.isNull(travelOrder.getOrderSource()) ||
                travelOrder.getOrderSource() != OrderSource.SHOP) {
            throw new BusinessException("非法来源订单或非商户订单");
        }
        // 商家取消订单
        Long currentTime = ESDateTimeUtils.getCurrentTimestamp();
        switch (travelOrder.getOrderStatus()) {
            case ON_THE_WAY:
            case DONE:
            case SETTLED:
            case CANCEL_CONSOLE:
            case CANCEL_SHOP:
            case CANCEL_USER: {
                throw new BusinessException("不能取消订单,当前订单状态为" + travelOrder.getOrderStatus().toString());
            }
            case WAIT_APPROVAL_OR_PAY: {
                travelOrder.setRefundFee(0d);
                break;
            }
            case WAIT_ACCEPT:
            case AUTO:
            case ACCEPTED:
            {
                RefundOrder refundOrder = new RefundOrder(
                        travelOrder.getId(),
                        travelOrder.getUserId(),
                        travelOrder.getUsername(),
                        travelOrder.getMobile(),
                        null,
                        travelOrder.getPrice(),
                        travelOrder.getPrice(),
                        null,
                        0d,
                        0d,
                        false
                );
                switch (travelOrder.getScene()) {
                    case JIEJI:
                    case SONGJI:
                    case ORDER_SCENE:
                    case JINGDIAN_PRIVATE:
                    {
                        refundConut(RefundConfig.ORDER_CODE, currentTime, travelOrder.getStartTime(), refundOrder);
                        break;
                    }
                    case DAY_PRIVATE:
                    case ROAD_PRIVATE:
                    {
                        refundConut(RefundConfig.PRIVATE_CODE, currentTime, travelOrder.getStartTime(), refundOrder);
                        break;
                    }
                    case BANSHOU_PRIVATE:
                    {
                        refundConut(RefundConfig.ZERO_REFUND_CODE, null, null, refundOrder);
                        break;
                    }
                    default:
                    {
                        throw new BusinessException("订单用车场景非法:" + travelOrder.getScene());
                    }
                }
                travelOrder.setRefundFee(refundOrder.getFee());
            }
        }
        travelOrder.setOrderStatus(OrderStatus.CANCEL_SHOP);
        travelOrder.setWarningStatus(WarningStatus.NORMAL);
        travelOrder.setCancelTime(currentTime);
        travelOrder = travelOrderManager.save(travelOrder);
        travelOrderHistoryManager.save(new TravelOrderHistory(travelOrder.getId(), travelOrder.getOrderStatus(), "商家取消订单"));
        // 已付款订单退款检查优惠券使用情况执行回退操作
        if (ESStringUtils.isNotBlank(travelOrder.getCouponId()) && travelOrder.getCouponPrice() > 0) {
            Coupon coupon = couponManager.findById(travelOrder.getCouponId());
            if (ESObjectUtils.isNotNull(coupon) && ESStringUtils.isNotBlank(coupon.getUserId())) {
                coupon.setHasUsed(false);
                couponManager.save(coupon);
            }
        }
    }

    // 用户取消订单
    @Transactional
    public void orderUserCancel(String orderId) {
        TravelOrder travelOrder = travelOrderManager.findById(orderId);
        if (ESObjectUtils.isNull(travelOrder)) {
            throw new BusinessException("订单不存在");
        }
        if (ESObjectUtils.isNull(travelOrder.getOrderSource()) ||
                travelOrder.getOrderSource() != OrderSource.USER) {
            throw new BusinessException("非法来源订单或非用户订单");
        }
        // 用户取消订单
        Long currentTime = ESDateTimeUtils.getCurrentTimestamp();
        switch (travelOrder.getOrderStatus()) {
            case ON_THE_WAY:
            case DONE:
            case SETTLED:
            case CANCEL_CONSOLE:
            case CANCEL_SHOP:
            case CANCEL_USER:
            {
                throw new BusinessException("不能取消订单,当前订单状态为" + travelOrder.getOrderStatus().toString());
            }
            case WAIT_APPROVAL_OR_PAY:
            {
                travelOrder.setRefundFee(0d);
                break;
            }
            case WAIT_ACCEPT:
            case AUTO:
            case ACCEPTED:
            {
                // 用户已付款,取消订单需要进入退款流程
                OpenUser openUser = openUserManager.findByUserId(travelOrder.getUserId());
                RefundOrder refundOrder = new RefundOrder(
                        travelOrder.getId(),
                        travelOrder.getUserId(),
                        travelOrder.getUsername(),
                        travelOrder.getMobile(),
                        ESObjectUtils.isNull(openUser) ? null : openUser.getAppUserName(),
                        travelOrder.getPrice(),
                        travelOrder.getPrice(),
                        null,
                        0d,
                        0d,
                        false
                );
                RefundConfig refundConfig;
                switch (travelOrder.getScene()) {
                    case JIEJI:
                    case SONGJI:
                    case ORDER_SCENE:
                    case JINGDIAN_PRIVATE:
                    {
                        refundConfig = refundConut(RefundConfig.ORDER_CODE, currentTime, travelOrder.getStartTime(), refundOrder);
                        break;
                    }
                    case DAY_PRIVATE:
                    case ROAD_PRIVATE:
                    {
                        refundConfig = refundConut(RefundConfig.PRIVATE_CODE, currentTime, travelOrder.getStartTime(), refundOrder);
                        break;
                    }
                    case BANSHOU_PRIVATE:
                    {
                        refundConfig = refundConut(RefundConfig.ZERO_REFUND_CODE, null, null, refundOrder);
                        break;
                    }
                    default:
                    {
                        throw new BusinessException("订单用车场景非法:" + travelOrder.getScene());
                    }
                }
                refundOrder = refundOrderManager.save(refundOrder);
                travelOrder.setRefundFee(refundOrder.getFee());
                if (refundOrder.getRefund() > 0) {
                    if (ESObjectUtils.isNull(refundConfig.getLimitFund()) ||
                            refundConfig.getLimitFund() <= 0 ||
                            refundOrder.getRefund() < refundConfig.getLimitFund()) {
                        // 微信申请退款
                        Map<String, String> refundResponse = wechatRefund(orderId, refundOrder.getId(), refundOrder.getPrice(), refundOrder.getRefund());
                        if (MapUtils.isEmpty(refundResponse)) {
                            throw new BusinessException("微信申请退款失败");
                        }
                    }
                } else {
                    refundOrder.setHasRefund(true);
                    refundOrderManager.save(refundOrder);
                }
                travelOrder.setRefundFee(refundOrder.getFee());
                break;
            }
        }
        travelOrder.setOrderStatus(OrderStatus.CANCEL_USER);
        travelOrder.setWarningStatus(WarningStatus.NORMAL);
        travelOrder.setCancelTime(currentTime);
        travelOrder = travelOrderManager.save(travelOrder);
        travelOrderHistoryManager.save(new TravelOrderHistory(travelOrder.getId(), travelOrder.getOrderStatus(), "用户取消订单"));
        // 已付款订单退款检查优惠券使用情况执行回退操作
        if (ESStringUtils.isNotBlank(travelOrder.getCouponId()) && travelOrder.getCouponPrice() > 0) {
            Coupon coupon = couponManager.findById(travelOrder.getCouponId());
            if (ESObjectUtils.isNotNull(coupon) && ESStringUtils.isNotBlank(coupon.getUserId())) {
                coupon.setHasUsed(false);
                couponManager.save(coupon);
            }
        }
    }

    /**
     * 更新用户订单状态(目前只适用于伴手礼订单)
     */
    @Transactional
    public void changeOrderStatus(String orderId, String expressNumber, OrderStatus orderStatus) {
        TravelOrder travelOrder = travelOrderManager.findById(orderId);
        if (ESObjectUtils.isNull(travelOrder)) {
            throw new BusinessException("订单不存在");
        }
        if (ESObjectUtils.isNull(travelOrder.getScene()) ||
                travelOrder.getScene() != Scene.BANSHOU_PRIVATE) {
            throw new BusinessException("非法订单来源或非伴手礼订单");
        }
        switch (orderStatus) {
            case SETTLED:
            case CANCEL_CONSOLE:
            case CANCEL_SHOP:
            case CANCEL_USER:
            case WAIT_APPROVAL_OR_PAY:
            case WAIT_ACCEPT:
            case AUTO:
            case ACCEPTED:
            {
                throw new BusinessException("订单状态不能修改为" + travelOrder.getOrderStatus().toString());
            }
            case ON_THE_WAY:
            {
                if (ESStringUtils.isBlank(expressNumber)) {
                    throw new BusinessException("快递单号不能为空");
                }
                travelOrder.setExpressNumber(expressNumber);
                travelOrder.setOrderStatus(orderStatus);
                travelOrder.setExecuteTime(ESDateTimeUtils.getCurrentTimestamp());
                travelOrderManager.save(travelOrder);
                smsScheduleTask.addSMS(new SMSSchedule(5, travelOrder.getMobile(), travelOrder.getId(), null));
                travelOrderHistoryManager.save(new TravelOrderHistory(travelOrder.getId(), travelOrder.getOrderStatus(), "订单已发货"));
                break;
            }
            case DONE:
            {
                travelOrder.setOrderStatus(orderStatus);
                travelOrder.setDoneTime(ESDateTimeUtils.getCurrentTimestamp());
                travelOrderManager.save(travelOrder);
                travelOrderHistoryManager.save(new TravelOrderHistory(travelOrder.getId(), travelOrder.getOrderStatus(), "订单已完成"));
                break;
            }
        }
    }

    /**
     * 更新用户快递单号(目前只适用于伴手礼订单)
     */
    @Transactional
    public void changeExpressNumber(String orderId, String expressNumber) {
        TravelOrder travelOrder = travelOrderManager.findById(orderId);
        if (ESObjectUtils.isNull(travelOrder)) {
            throw new BusinessException("订单不存在");
        }
        if (ESObjectUtils.isNull(travelOrder.getScene()) ||
                travelOrder.getScene() != Scene.BANSHOU_PRIVATE) {
            throw new BusinessException("非法订单来源或非伴手礼订单");
        }
        switch (travelOrder.getOrderStatus()) {
            case SETTLED:
            case CANCEL_CONSOLE:
            case CANCEL_SHOP:
            case CANCEL_USER:
            case WAIT_APPROVAL_OR_PAY:
            case WAIT_ACCEPT:
            case AUTO:
            case ACCEPTED:
            case DONE:
            default:
            {
                throw new BusinessException("订单状态为" + travelOrder.getOrderStatus().toString() + ",不可修改快递单号");
            }
            case ON_THE_WAY:
            {
                if (ESStringUtils.isBlank(expressNumber)) {
                    throw new BusinessException("快递单号不能为空");
                }
                travelOrder.setExpressNumber(expressNumber);
                travelOrderManager.save(travelOrder);
                travelOrderHistoryManager.save(new TravelOrderHistory(travelOrder.getId(), travelOrder.getOrderStatus(), "修改快递单号"));
                break;
            }
        }
    }

    public RefundResponseDTO refundCount(String orderId) {
        TravelOrder travelOrder = travelOrderManager.findById(orderId);
        if (ESObjectUtils.isNull(travelOrder)) {
            throw new BusinessException("订单不存在");
        }
        RefundOrder refundOrder = new RefundOrder(
                travelOrder.getId(),
                travelOrder.getUserId(),
                travelOrder.getUsername(),
                travelOrder.getMobile(),
                null,
                travelOrder.getPrice(),
                travelOrder.getPrice(),
                null,
                0d,
                0d,
                false
        );
        switch (travelOrder.getScene()) {
            case JIEJI:
            case SONGJI:
            case ORDER_SCENE:
            case JINGDIAN_PRIVATE:
            {
                refundConut(RefundConfig.ORDER_CODE, ESDateTimeUtils.getCurrentTimestamp(), travelOrder.getStartTime(), refundOrder);
                break;
            }
            case DAY_PRIVATE:
            case ROAD_PRIVATE:
            {
                refundConut(RefundConfig.PRIVATE_CODE, ESDateTimeUtils.getCurrentTimestamp(), travelOrder.getStartTime(), refundOrder);
                break;
            }
            case BANSHOU_PRIVATE:
            {
                refundConut(RefundConfig.ZERO_REFUND_CODE, null, null, refundOrder);
                break;
            }
            default:
            {
                throw new BusinessException("订单用车场景非法:" + travelOrder.getScene());
            }
        }
        return new RefundResponseDTO(orderId, refundOrder.getBeforeTime(), new BigDecimal(refundOrder.getRefundPercent()).intValue(), refundOrder.getFee());
    }

    private RefundConfig refundConut(String code, Long currentTime, Long startTime, RefundOrder refundOrder) {
        RefundConfig refundConfig = null;
        switch (code) {
            case RefundConfig.ORDER_CODE:
            {
                refundConfig = refundConfigManager.getOrderRefundConfig();
                break;
            }
            case RefundConfig.PRIVATE_CODE:
            {
                refundConfig = refundConfigManager.getPrivateRefundConfig();
                break;
            }
            case RefundConfig.ZERO_REFUND_CODE:
            {
                return refundConfigManager.getZeroRefundConfig();
            }
        }
        if (ESObjectUtils.isNull(refundConfig)) {
            throw new BusinessException("退款配置不存在");
        }
        Double time = Double.valueOf((startTime - currentTime) / 3600000);
        Double firstRefundFee = 0d;
        Double secondRefundFee = 0d;
        if (time < refundConfig.getBeforeFirstTime()) {
            firstRefundFee = ESNumberFormatUtils.formatDouble(refundOrder.getPrice() * refundConfig.getFirstFund() / 100, 2);
        }
        if (time <refundConfig.getBeforeSecondTime()) {
            secondRefundFee = ESNumberFormatUtils.formatDouble(refundOrder.getPrice() * refundConfig.getSecondFund() / 100, 2);
        }
        if (firstRefundFee >= secondRefundFee) {
            refundOrder.setBeforeTime(refundConfig.getBeforeFirstTime());
            refundOrder.setRefundPercent(refundConfig.getFirstFund());
            refundOrder.setFee(firstRefundFee);
            Double firstRefund = refundOrder.getPrice() - firstRefundFee;
            if (firstRefund > 0) {
                refundOrder.setRefund(firstRefund);
            } else {
                refundOrder.setRefund(0d);
            }
        } else {
            refundOrder.setBeforeTime(refundConfig.getBeforeSecondTime());
            refundOrder.setRefundPercent(refundConfig.getSecondFund());
            refundOrder.setFee(secondRefundFee);
            Double secondRefund = refundOrder.getPrice() - secondRefundFee;
            if (secondRefund > 0) {
                refundOrder.setRefund(secondRefund);
            } else {
                refundOrder.setRefund(0d);
            }
        }
        return refundConfig;
    }
}
