package com.wondernect.travel.cache;

import com.wondernect.elements.redis.base.HashRedisCache;
import com.wondernect.travel.cache.model.DriverCurrent;
import org.springframework.stereotype.Repository;

import java.util.Set;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: DriverCurrentCache
 * Author: chenxun
 * Date: 2019/5/17 14:45
 * Description:
 */
@Repository
public class DriverCurrentCache extends HashRedisCache<DriverCurrent> {

    private static final String DRIVER_CACHE_HASH_KEY = "driver_current_status";

    public void save(DriverCurrent driverCurrent) {
        super.put(DRIVER_CACHE_HASH_KEY, driverCurrent.getDriverUserId(), driverCurrent);
    }

    public void remove(String driverUserId) {
        super.delete(DRIVER_CACHE_HASH_KEY, driverUserId);
    }

    public DriverCurrent get(String driverUserId) {
        return super.get(DRIVER_CACHE_HASH_KEY, driverUserId);
    }

    public Set<String> driverList() {
        return super.keys(DRIVER_CACHE_HASH_KEY);
    }
}
