package com.wondernect.travel.cache;

import com.wondernect.elements.common.utils.ESDateTimeUtils;
import com.wondernect.elements.redis.base.ZSetRedisCache;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: DriverCurrentCache
 * Author: chenxun
 * Date: 2019/5/17 14:45
 * Description:
 */
@Repository
public class DriverNowCache extends ZSetRedisCache {

    private static final String DRIVER_NOW_CACHE_ZSET_KEY = "now";

    public void addOrder(String driverUserId, String orderId) {
        super.add(DRIVER_NOW_CACHE_ZSET_KEY + "|" + driverUserId, orderId, ESDateTimeUtils.getCurrentTimestamp());
    }

    public void removeOrder(String driverUserId, String orderId) {
        super.remove(DRIVER_NOW_CACHE_ZSET_KEY + "|" + driverUserId, orderId);
    }

    public List<String> getOrderList(String driverUserId) {
        List<String> list = new ArrayList<>();
        Set<ZSetOperations.TypedTuple<Object>> orderSet = super.reverseRangeWithScores(DRIVER_NOW_CACHE_ZSET_KEY + "|" + driverUserId, 0, -1);
        if (orderSet.isEmpty()) {
            return list;
        }
        Iterator<ZSetOperations.TypedTuple<Object>> iterator = orderSet.iterator();
        while (iterator.hasNext()) {
            ZSetOperations.TypedTuple<Object> objectTypedTuple = iterator.next();
            Object object = objectTypedTuple.getValue();
            String orderId = "";
            if (object instanceof String) {
                orderId = (String) object;
                list.add(orderId);
            }
        }
        return list;
    }
}
