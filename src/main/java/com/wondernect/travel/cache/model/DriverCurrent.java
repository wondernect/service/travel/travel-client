package com.wondernect.travel.cache.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.travel.model.em.DriverStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: DriverStatus
 * Author: chenxun
 * Date: 2019/5/17 14:39
 * Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(description = "司机当前状态")
public class DriverCurrent {

    @NotBlank(message = "司机不能为空")
    @JsonProperty("driver_user_id")
    @ApiModelProperty(notes = "司机对应用户id")
    private String driverUserId;

    @JsonProperty("city_id")
    @ApiModelProperty(value = "城市id")
    private String cityId;

    @JsonProperty("zuowei_id")
    @ApiModelProperty(value = "座位id")
    private String zuoweiId;

    @JsonProperty("chexing_id")
    @ApiModelProperty(value = "车型id")
    private String chexingId;

    @NotNull(message = "司机状态不能为空")
    @Enumerated(EnumType.STRING)
    @JsonProperty("status")
    @ApiModelProperty(notes = "司机当前状态", allowableValues = "UNKNOWN, WORK, IDLE")
    private DriverStatus status;

    @NotNull(message = "司机坐标经度不能为空")
    @JsonProperty("longitude")
    @ApiModelProperty(notes = "司机最近一次发送的坐标经度")
    private Double longitude;

    @NotNull(message = "司机坐标纬度不能为空")
    @JsonProperty("latitude")
    @ApiModelProperty(notes = "司机最近一次发送的坐标纬度")
    private Double latitude;

    @JsonProperty("evaluate")
    @ApiModelProperty(notes = "司机评分(有效评分平均值)")
    private int evaluate;
}
