package com.wondernect.travel.cache;

import com.wondernect.elements.common.utils.ESDateTimeUtils;
import com.wondernect.elements.redis.base.ZSetRedisCache;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: DriverCurrentCache
 * Author: chenxun
 * Date: 2019/5/17 14:45
 * Description:
 */
@Repository
public class DriverAcceptedOrderCache extends ZSetRedisCache {

    private static final String DRIVER_ACCEPTED_ORDER_CACHE_ZSET_KEY = "accepted_order";

    public void addOrder(String driverUserId, String orderId) {
        super.add(DRIVER_ACCEPTED_ORDER_CACHE_ZSET_KEY + "|" + driverUserId, orderId, ESDateTimeUtils.getCurrentTimestamp());
    }

    public void removeOrder(String driverUserId, String orderId) {
        super.remove(DRIVER_ACCEPTED_ORDER_CACHE_ZSET_KEY + "|" + driverUserId, orderId);
    }

    public List<Object> getOrderList(String driverUserId) {
        List<Object> objectList = new ArrayList<>();
        Long size = super.zCard(DRIVER_ACCEPTED_ORDER_CACHE_ZSET_KEY + "|" + driverUserId);
        if (size > 0) {
            Set<ZSetOperations.TypedTuple<Object>> orderSet = super.reverseRangeWithScores(DRIVER_ACCEPTED_ORDER_CACHE_ZSET_KEY + "|" + driverUserId, 0, size - 1);
            Iterator<ZSetOperations.TypedTuple<Object>> iterator = orderSet.iterator();
            while (iterator.hasNext()) {
                ZSetOperations.TypedTuple<Object> objectTypedTuple =iterator.next();
                objectList.add(objectTypedTuple.getValue());
            }
        }
        return objectList;
    }
}
