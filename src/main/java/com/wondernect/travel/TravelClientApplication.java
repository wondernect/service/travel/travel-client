package com.wondernect.travel;

import com.wondernect.elements.common.utils.ESDateTimeUtils;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.services.rbac.service.RBACOpenService;
import com.wondernect.services.user.model.User;
import com.wondernect.services.user.service.UserOpenService;
import com.wondernect.travel.model.schedule.AutoSchedule;
import com.wondernect.travel.task.AutoScheduleTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScans({
        @ComponentScan(basePackages = "com.wondernect.*")
})
@EntityScan(basePackages = {
        "com.wondernect.*"
})
@EnableJpaRepositories(basePackages = {
        "com.wondernect.*"
})
public class TravelClientApplication implements CommandLineRunner {

    @Autowired
    private RBACOpenService rbacOpenService;

    @Autowired
    private UserOpenService userOpenService;

    @Autowired
    private AutoScheduleTask autoScheduleTask;

    public static void main(String[] args) {
        SpringApplication.run(TravelClientApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        rbacOpenService.initPlatformAndApplicationRole();
        User user = userOpenService.initPlatformAdmin();
        if (ESObjectUtils.isNotNull(user)) {
            rbacOpenService.initPlatformAdminRole(user.getId());
        }
        System.out.println("发送测试消息");
        autoScheduleTask.add(new AutoSchedule("1", null, ESDateTimeUtils.getCurrentTimestamp()));
        System.out.println("测试消息发送完毕");
    }
}
