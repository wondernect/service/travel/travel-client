package com.wondernect.travel.sms;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: SMSConstant
 * Author: chenxun
 * Date: 2019-06-07 10:38
 * Description:
 */
public final class SMSConstant {

    public static final String CAPTCHA_SMS_TEMPLATE_CODE = "SMS_165677433";

    // public static final String CAPTCHA_SMS_SIGN_NAME = "旅王驾到";

    public static final String COMMON_SMS_TEMPLATE_CODE_ORDER_CREATE_USER = "SMS_207746001";

    public static final String COMMON_SMS_TEMPLATE_CODE_BANSHOU_ORDER_CREATE_USER = "SMS_182677629";

    public static final String COMMON_SMS_TEMPLATE_CODE_BANSHOU_ORDER_SEND_USER = "SMS_182667757";

    public static final String COMMON_SMS_TEMPLATE_CODE_ORDER_ACCEPTED_USER = "SMS_172011835";

    public static final String COMMON_SMS_TEMPLATE_CODE_ORDER_ACCEPTED_DRIVER = "SMS_168591343";

    public static final String COMMON_SMS_SIGN_NAME = "旅王出行";
}
