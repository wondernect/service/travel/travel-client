package com.wondernect.travel.sms;

import com.alibaba.fastjson.JSONObject;
import com.wondernect.elements.common.exception.BusinessException;
import com.wondernect.elements.common.utils.ESDateTimeUtils;
import com.wondernect.elements.sms.client.AliyunSMSClient;
import com.wondernect.elements.sms.client.util.SMSSendResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: TravelSMSService
 * Author: chenxun
 * Date: 2019-06-07 11:06
 * Description:
 */
@Service
public class TravelSMSService {

    private static final Logger logger = LoggerFactory.getLogger(TravelSMSService.class);

    @Autowired
    private AliyunSMSClient aliyunSMSClient;

    /**
     * 发送短信验证码
     * @param phoneNumber 手机号码
     * @param captcha 短信验证码
     */
    public void sendCaptcha(String phoneNumber, String captcha) {
        Map<String, String> captchaMap = new HashMap<>();
        captchaMap.put("code", captcha);
        SMSSendResult smsSendResult = aliyunSMSClient.send(
                phoneNumber,
                SMSConstant.CAPTCHA_SMS_TEMPLATE_CODE,
                JSONObject.toJSONString(captchaMap),
                SMSConstant.COMMON_SMS_SIGN_NAME
        );
        if (!smsSendResult.getResult()) {
            logger.error("发送短信验证码失败:{}", smsSendResult);
            throw new BusinessException("短信验证码发送失败");
        } else {
            logger.info("发送短信验证码成功:{}, {}, {}", phoneNumber, captchaMap, smsSendResult);
        }
    }

    /**
     * 预约成功短信通知 - 乘客端
     * @param phoneNumber 手机号码
     * @param startTime 预约时间
     */
    public void orderCreateForUser(String phoneNumber, Long startTime) {
        Map<String, String> captchaMap = new HashMap<>();
        captchaMap.put("start_time", ESDateTimeUtils.formatDate(startTime, "MM-dd HH:mm"));
        SMSSendResult smsSendResult = aliyunSMSClient.send(
                phoneNumber,
                SMSConstant.COMMON_SMS_TEMPLATE_CODE_ORDER_CREATE_USER,
                JSONObject.toJSONString(captchaMap),
                SMSConstant.COMMON_SMS_SIGN_NAME
        );
        if (!smsSendResult.getResult()) {
            logger.error("预约成功短信通知(乘客端)失败:{}", smsSendResult);
            throw new BusinessException("预约成功短信通知(乘客端)失败");
        } else {
            logger.info("预约成功短信通知(乘客端)成功:{}, {}, {}", phoneNumber, captchaMap, smsSendResult);
        }
    }

    /**
     * 伴手礼下单成功短信通知 - 用户
     * @param phoneNumber 手机号码
     * @param startTime 预约时间
     */
    public void banshouOrderCreateForUser(String phoneNumber, Long startTime) {
        Map<String, String> captchaMap = new HashMap<>();
        captchaMap.put("start_time", ESDateTimeUtils.formatDate(startTime, "MM-dd HH:mm"));
        SMSSendResult smsSendResult = aliyunSMSClient.send(
                phoneNumber,
                SMSConstant.COMMON_SMS_TEMPLATE_CODE_BANSHOU_ORDER_CREATE_USER,
                JSONObject.toJSONString(captchaMap),
                SMSConstant.COMMON_SMS_SIGN_NAME
        );
        if (!smsSendResult.getResult()) {
            logger.error("伴手礼下单成功短信通知(用户)失败:{}", smsSendResult);
            throw new BusinessException("伴手礼下单成功短信通知(用户)失败");
        } else {
            logger.info("伴手礼下单成功短信通知(用户)成功:{}, {}, {}", phoneNumber, captchaMap, smsSendResult);
        }
    }

    /**
     * 伴手礼发货成功短信通知 - 用户
     * @param phoneNumber 手机号码
     * @param startTime 发货时间
     */
    public void banshouOrderSendForUser(String phoneNumber, Long startTime) {
        Map<String, String> captchaMap = new HashMap<>();
        captchaMap.put("start_time", ESDateTimeUtils.formatDate(startTime, "MM-dd HH:mm"));
        SMSSendResult smsSendResult = aliyunSMSClient.send(
                phoneNumber,
                SMSConstant.COMMON_SMS_TEMPLATE_CODE_BANSHOU_ORDER_SEND_USER,
                JSONObject.toJSONString(captchaMap),
                SMSConstant.COMMON_SMS_SIGN_NAME
        );
        if (!smsSendResult.getResult()) {
            logger.error("伴手礼发货成功短信通知(用户)失败:{}", smsSendResult);
            throw new BusinessException("伴手礼发货成功短信通知(用户)失败");
        } else {
            logger.info("伴手礼发货成功短信通知(用户)成功:{}, {}, {}", phoneNumber, captchaMap, smsSendResult);
        }
    }

    /**
     * 司机接单短信通知 - 乘客端
     * @param phoneNumber 手机号码
     * @param startTime 预约时间
     * @param driver 司机姓名
     * @param car 车牌号
     * @param color 车型颜色
     * @param mobile 司机手机号码
     */
    public void orderAcceptedForUser(String phoneNumber, Long startTime, String driver, String car, String color, String mobile) {
        Map<String, String> captchaMap = new HashMap<>();
        captchaMap.put("start_time", ESDateTimeUtils.formatDate(startTime, "MM-dd HH:mm"));
        captchaMap.put("driver", driver);
        captchaMap.put("car_no", car);
        captchaMap.put("car_color", color);
        captchaMap.put("user_mobile", mobile);
        SMSSendResult smsSendResult = aliyunSMSClient.send(
                phoneNumber,
                SMSConstant.COMMON_SMS_TEMPLATE_CODE_ORDER_ACCEPTED_USER,
                JSONObject.toJSONString(captchaMap),
                SMSConstant.COMMON_SMS_SIGN_NAME
        );
        if (!smsSendResult.getResult()) {
            logger.error("司机接单短信通知(乘客端)失败:{}", smsSendResult);
            throw new BusinessException("司机接单短信通知(乘客端)失败");
        } else {
            logger.info("司机接单短信通知(乘客端)成功:{}, {}, {}", phoneNumber, captchaMap, smsSendResult);
        }
    }

    /**
     * 司机接单短信通知 - 司机端
     * @param phoneNumber 手机号码
     * @param startTime 预约时间
     * @param user 乘客姓名
     * @param mobile 乘客手机号码
     */
    public void orderAcceptedForDriver(String phoneNumber, Long startTime, String user, String mobile) {
        Map<String, String> captchaMap = new HashMap<>();
        captchaMap.put("start_time", ESDateTimeUtils.formatDate(startTime, "MM-dd HH:mm"));
        captchaMap.put("user", user);
        captchaMap.put("user_mobile", mobile);
        SMSSendResult smsSendResult = aliyunSMSClient.send(
                phoneNumber,
                SMSConstant.COMMON_SMS_TEMPLATE_CODE_ORDER_ACCEPTED_DRIVER,
                JSONObject.toJSONString(captchaMap),
                SMSConstant.COMMON_SMS_SIGN_NAME
        );
        if (!smsSendResult.getResult()) {
            logger.error("司机接单短信通知(司机端)失败:{}", smsSendResult);
            throw new BusinessException("司机接单短信通知(司机端)失败");
        } else {
            logger.info("司机接单短信通知(司机端)成功:{}, {}, {}", phoneNumber, captchaMap, smsSendResult);
        }
    }
}
