package com.wondernect.travel.wechat.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wondernect.elements.http.client.HttpClient;
import com.wondernect.travel.wechat.constants.WXPayConstants;
import com.wondernect.travel.wechat.constants.WXURL;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * 微信小工具类
 *
 * @author yclimb
 * @date 2018/8/17
 */
@Slf4j
@Component
public class WXUtils {

    @Autowired
    private HttpClient httpClient;

    /**
     * 获取微信全局accessToken
     */
    public String getAccessToken() {
        // 通过接口取得access_token
        String response = httpClient.getForJson(MessageFormat.format(WXURL.BASE_ACCESS_TOKEN, WXPayConstants.APP_ID, WXPayConstants.SECRET), null, null);
        JSONObject jsonObject = JSON.parseObject(response);
        String token = (String) jsonObject.get("access_token");
        if (StringUtils.isNotBlank(token)) {
            return token;
        } else {
            log.error("获取微信accessToken出错，微信返回信息为：[{}]", response);
        }
        return null;
    }

    /**
     * 获取小程序静默登录返回信息
     */
    public JSONObject getMiniBaseUserInfo(String code, String appId, String appSecret) {
        log.info("getMiniBaseUserInfo:params:[{}]", code);
        String data = httpClient.getForJson(MessageFormat.format(WXURL.WX_MINI_LOGIN, appId, appSecret, code), null, null);
        log.info("getMiniBaseUserInfo:result:[{}]", data);
        return JSONObject.parseObject(data);

    }

    /**
     * 通过access_token和openid请求获取用户信息
     */
    public JSONObject getJsapiUserinfo(String access_token, String openid) {
        if (StringUtils.isBlank(access_token) || StringUtils.isBlank(openid)) {
            return null;
        }
        try {
            String userinfo_json = httpClient.getForJson(MessageFormat.format(WXURL.OAUTH_GET_USERINFO_URL, access_token, openid), null, null);
            log.info("getUserinfo:userinfo_json:{}", userinfo_json);
            if (StringUtils.isBlank(userinfo_json)) {
                return null;
            }
            JSONObject jsonObject = JSON.parseObject(userinfo_json);
            if (0 != jsonObject.getIntValue("errcode")) {
                return null;
            }
            return jsonObject;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    /**
     * 获取 application/json;charset=UTF-8 的 HttpHeaders 对象
     */
    public HttpHeaders getHttpHeadersUTF8JSON() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        return headers;
    }

    /**
     * 获取 jsapi_ticket
     * 请求路径：https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token={0}&type=jsapi
     */
    public String getWxApiTicket(String access_token) {
        if (StringUtils.isBlank(access_token)) {
            return null;
        }
        try {
            String api_ticket = httpClient.getForJson(MessageFormat.format(WXURL.BASE_JSAPI_TICKET, access_token), null, null);
            WXPayUtil.getLogger().info("getWxApiTicket:api_ticket:{}", api_ticket);
            if (StringUtils.isBlank(api_ticket)) {
                return null;
            }
            JSONObject jsonObject = JSON.parseObject(api_ticket);
            if (0 != jsonObject.getIntValue("errcode")) {
                return null;
            }
            return jsonObject.getString("ticket");
        } catch (Exception e) {
            WXPayUtil.getLogger().error(e.getMessage(), e);
        }
        return null;
    }

    /**
     * 获取微信签名信息
     */
    public Map<String, Object> getSignature(String requestUrl, String appid, String code) {
        Map<String, Object> map = new HashMap<>();
        try {
            // 获取公众号的 access_token、jsapi_ticket
            String accessToken = this.getAccessToken();
            String jsapi_ticket = this.getWxApiTicket(accessToken);
            String nonce_str = WXPayUtil.generateNonceStr();
            String timestamp = Long.toString(WXPayUtil.getCurrentTimestamp());
            // 注意这里参数名必须全部小写，且必须有序
            String dataStr = "jsapi_ticket=" + jsapi_ticket +
                    "&noncestr=" + nonce_str +
                    "&timestamp=" + timestamp +
                    "&url=" + requestUrl;
            WXPayUtil.getLogger().info(dataStr);

            String signature = WXPayUtil.SHA1(dataStr);
            map.put("noncestr", nonce_str);
            map.put("timestamp", timestamp);
            map.put("appid", appid);
            map.put("signature", signature);
        } catch (Exception e) {
            WXPayUtil.getLogger().error(e.getMessage(), e);
        }
        return map;
    }

}
