package com.wondernect.travel.manager;

import com.wondernect.elements.common.exception.BusinessException;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.common.utils.ESStringUtils;
import com.wondernect.elements.rdb.base.manager.BaseSortManager;
import com.wondernect.travel.model.OrderStrategy;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: OrderStrategyManager
 * Author: chenxun
 * Date: 2019/5/11 16:59
 * Description:
 */
@Service
public class OrderStrategyManager extends BaseSortManager<OrderStrategy, String> {

    public OrderStrategy save(OrderStrategy orderStrategy) {
        OrderStrategy orderStrategySave;
        if (ESStringUtils.isNotBlank(orderStrategy.getId())) {
            orderStrategySave = super.findById(orderStrategy.getId());
            if (ESObjectUtils.isNull(orderStrategySave)) {
                throw new BusinessException("订单派送策略不存在");
            }
            orderStrategySave.setStrategyName(orderStrategy.getStrategyName());
            orderStrategySave.setStrategyDescription(orderStrategy.getStrategyDescription());
            orderStrategySave.setBeforeUseDispatchHour(orderStrategy.getBeforeUseDispatchHour());
            orderStrategySave.setDriverStatus(orderStrategy.getDriverStatus());
            orderStrategySave.setDistanceScope(orderStrategy.getDistanceScope());
            orderStrategySave.setDriverEvaluate(orderStrategy.getDriverEvaluate());
            orderStrategySave.setAutoTime(orderStrategy.getAutoTime());
        } else {
            orderStrategySave = new OrderStrategy(
                    orderStrategy.getStrategyName(),
                    orderStrategy.getStrategyDescription(),
                    orderStrategy.getBeforeUseDispatchHour(),
                    orderStrategy.getDriverStatus(),
                    orderStrategy.getDistanceScope(),
                    orderStrategy.getDriverEvaluate(),
                    orderStrategy.getAutoTime()
            );
        }
        if (ESObjectUtils.isNotNull(orderStrategy.getWeight())) {
            orderStrategySave.setWeight(orderStrategy.getWeight());
        }
        return super.save(orderStrategySave);
    }

    /**
     * 获取订单派送策略
     * @param orderStrategyId null或空值时表示订单刚开始派送，取第一条派送策略
     * @return null 表示策略已用完，订单放弃派送，等待预警人工派送
     */
    public OrderStrategy findNextOrderStrategy(String orderStrategyId) {
        List<OrderStrategy> orderStrategyList = super.findAll(new ArrayList<>());
        OrderStrategy orderStrategy = null;
        if (CollectionUtils.isNotEmpty(orderStrategyList)) {
            if (ESStringUtils.isBlank(orderStrategyId)) {
                orderStrategy = orderStrategyList.get(0);
            } else {
                int size = orderStrategyList.size();
                int index = size;
                for (int tempIndex = 0; tempIndex < size; tempIndex++) {
                    if (ESStringUtils.equalsIgnoreCase(orderStrategyId, orderStrategyList.get(tempIndex).getId())) {
                        index = tempIndex;
                        index++;
                        break;
                    }
                }
                if (index < size) {
                    orderStrategy = orderStrategyList.get(index);
                } else {
                    orderStrategy = orderStrategyList.get(size - 1);
                }
            }
        }
        return orderStrategy;
    }
}
