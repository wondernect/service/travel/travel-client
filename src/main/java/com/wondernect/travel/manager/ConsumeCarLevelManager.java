package com.wondernect.travel.manager;

import com.wondernect.elements.rdb.base.manager.BaseStringManager;
import com.wondernect.elements.rdb.criteria.Criteria;
import com.wondernect.elements.rdb.criteria.Restrictions;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.travel.model.ConsumeCarLevel;
import com.wondernect.travel.repository.ConsumeCarLevelDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: ConsumeCarLevelManager
 * Author: chenxun
 * Date: 2019/5/11 16:56
 * Description:
 */
@Service
public class ConsumeCarLevelManager extends BaseStringManager<ConsumeCarLevel> {

    @Autowired
    private ConsumeCarLevelDao consumeCarLevelDao;

    public void deleteAllByConsumeId(String consumeId) {
        consumeCarLevelDao.deleteAllByConsumeId(consumeId);
    }

    public boolean existsByZuoweiIdOrChexingId(String zuoweiId, String chexingId) {
        return consumeCarLevelDao.existsByZuoweiIdOrChexingId(zuoweiId, chexingId);
    }

    public boolean existsByPriceStrategyId(String priceStrategyId) {
        return consumeCarLevelDao.existsByPriceStrategyId(priceStrategyId);
    }

    public List<ConsumeCarLevel> findAllByConsumeId(String consumeId, List<SortData> sortDataList) {
        Criteria<ConsumeCarLevel> consumeCarLevelCriteria = new Criteria<>();
        consumeCarLevelCriteria.add(Restrictions.eq("consumeId", consumeId));
        return super.findAll(consumeCarLevelCriteria, sortDataList);
    }
}
