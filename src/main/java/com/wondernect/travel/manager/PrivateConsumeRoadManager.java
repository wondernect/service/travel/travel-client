package com.wondernect.travel.manager;

import com.wondernect.elements.rdb.base.manager.BaseSortManager;
import com.wondernect.elements.rdb.criteria.Criteria;
import com.wondernect.elements.rdb.criteria.Restrictions;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.travel.model.PrivateConsumeRoad;
import com.wondernect.travel.repository.PrivateConsumeRoadDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: PrivateConsumeRoadManager
 * Author: chenxun
 * Date: 2019-10-06 10:30
 * Description: 包车服务路线配置
 */
@Service
public class PrivateConsumeRoadManager extends BaseSortManager<PrivateConsumeRoad, String> {

    @Autowired
    private PrivateConsumeRoadDao privateConsumeRoadDao;

    public void deleteAllByPrivateConsumeId(String privateConsumeId) {
        privateConsumeRoadDao.deleteAllByPrivateConsumeId(privateConsumeId);
    }

    public List<PrivateConsumeRoad> list(String privateConsumeId, List<SortData> sortDataList) {
        Criteria<PrivateConsumeRoad> privateConsumeRoadCriteria = new Criteria<>();
        privateConsumeRoadCriteria.add(Restrictions.eq("privateConsumeId", privateConsumeId));
        return super.findAll(privateConsumeRoadCriteria, sortDataList);
    }
}
