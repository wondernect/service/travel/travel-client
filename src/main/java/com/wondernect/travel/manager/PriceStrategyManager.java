package com.wondernect.travel.manager;

import com.wondernect.elements.common.exception.BusinessException;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.common.utils.ESStringUtils;
import com.wondernect.elements.rdb.base.manager.BaseStringManager;
import com.wondernect.travel.model.PriceStrategy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: PriceStrategyManager
 * Author: chenxun
 * Date: 2019/5/11 16:59
 * Description:
 */
@Service
public class PriceStrategyManager extends BaseStringManager<PriceStrategy> {

    @Transactional
    public PriceStrategy save(PriceStrategy priceStrategy) {
        PriceStrategy priceStrategySave;
        if (ESStringUtils.isNotBlank(priceStrategy.getId())) {
            priceStrategySave = super.findById(priceStrategy.getId());
            if (ESObjectUtils.isNull(priceStrategySave)) {
                throw new BusinessException("价格策略不存在");
            }
            priceStrategySave.setStrategyName(priceStrategy.getStrategyName());
            priceStrategySave.setStrategyDescription(priceStrategy.getStrategyDescription());
            priceStrategySave.setPlanType(priceStrategy.getPlanType());
            priceStrategySave.setPlanDescription(priceStrategy.getPlanDescription());
            priceStrategySave.setBasePrice(priceStrategy.getBasePrice());
            priceStrategySave.setKiloPrice(priceStrategy.getKiloPrice());
            priceStrategySave.setTimePrice(priceStrategy.getTimePrice());
            priceStrategySave.setStartKilo(priceStrategy.getStartKilo());
            priceStrategySave.setLimitKilo(priceStrategy.getLimitKilo());
            priceStrategySave.setLongPrice(priceStrategy.getLongPrice());
            priceStrategySave.setDistance(priceStrategy.getDistance());
            priceStrategySave.setNightPercent(priceStrategy.getNightPercent());
        } else {
            priceStrategySave = new PriceStrategy(
                    priceStrategy.getStrategyName(),
                    priceStrategy.getStrategyDescription(),
                    priceStrategy.getPlanType(),
                    priceStrategy.getPlanDescription(),
                    priceStrategy.getBasePrice(),
                    priceStrategy.getKiloPrice(),
                    priceStrategy.getTimePrice(),
                    priceStrategy.getStartKilo(),
                    priceStrategy.getLimitKilo(),
                    priceStrategy.getLongPrice(),
                    priceStrategy.getDistance(),
                    priceStrategy.getNightPercent()
            );
        }
        return super.save(priceStrategySave);
    }
}
