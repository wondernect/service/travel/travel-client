package com.wondernect.travel.manager;

import com.wondernect.elements.common.exception.BusinessException;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.common.utils.ESStringUtils;
import com.wondernect.elements.rdb.base.manager.BaseStringManager;
import com.wondernect.elements.rdb.criteria.Criteria;
import com.wondernect.elements.rdb.criteria.Restrictions;
import com.wondernect.elements.rdb.request.PageRequestData;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.travel.model.Address;
import org.hibernate.criterion.MatchMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: AddressManager
 * Author: chenxun
 * Date: 2019/5/11 16:54
 * Description:
 */
@Service
public class AddressManager extends BaseStringManager<Address> {

    @Transactional
    public Address save(Address address) {
        Address addressSave;
        if (ESStringUtils.isNotBlank(address.getId())) {
            addressSave = super.findById(address.getId());
            if (ESObjectUtils.isNull(addressSave)) {
                throw new BusinessException("地址不存在");
            }
            addressSave.setCityId(address.getCityId());
            addressSave.setType(address.getType());
            addressSave.setName(address.getName());
            addressSave.setDetail(address.getDetail());
            addressSave.setLongitude(address.getLongitude());
            addressSave.setLatitude(address.getLatitude());
        } else {
            addressSave = new Address(address.getCityId(), address.getType(), address.getName(), address.getDetail(), address.getLongitude(), address.getLatitude());
        }
        return super.save(addressSave);
    }

    public PageResponseData<Address> page(String cityId, String input, PageRequestData pageRequestData) {
        Criteria<Address> addressCriteria = new Criteria<>();
        addressCriteria.add(
                Restrictions.and(
                        Restrictions.eq("cityId", cityId),
                        Restrictions.or(
                                Restrictions.like("name", input, MatchMode.ANYWHERE),
                                Restrictions.like("detail", input, MatchMode.ANYWHERE)
                        )
                )
        );
        return super.findAll(addressCriteria, pageRequestData);
    }
}
