package com.wondernect.travel.manager;

import com.wondernect.elements.common.exception.BusinessException;
import com.wondernect.elements.common.utils.ESBeanUtils;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.common.utils.ESStringUtils;
import com.wondernect.elements.rdb.base.manager.BaseSortManager;
import com.wondernect.elements.rdb.criteria.Criteria;
import com.wondernect.elements.rdb.criteria.Restrictions;
import com.wondernect.elements.rdb.request.PageRequestData;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.travel.model.PrivateConsume;
import com.wondernect.travel.model.em.Scene;
import org.hibernate.criterion.MatchMode;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: PrivateConsumeManager
 * Author: chenxun
 * Date: 2019-10-05 16:40
 * Description: 包车服务
 */
@Service
public class PrivateConsumeManager extends BaseSortManager<PrivateConsume, String> {

    public PrivateConsume save(PrivateConsume privateConsume) {
        PrivateConsume privateConsumeSave;
        if (ESStringUtils.isNotBlank(privateConsume.getId())) {
            privateConsumeSave = super.findById(privateConsume.getId());
            if (ESObjectUtils.isNull(privateConsumeSave)) {
                throw new BusinessException("包车服务不存在");
            }
        } else {
            privateConsumeSave = new PrivateConsume();
        }
        ESBeanUtils.copyProperties(privateConsume, privateConsumeSave);
        if (ESObjectUtils.isNotNull(privateConsume.getWeight())) {
            privateConsumeSave.setWeight(privateConsume.getWeight());
        }
        return super.save(privateConsumeSave);
    }

    public PageResponseData<PrivateConsume> page(String cityId, List<Scene> sceneList, String value, Long start, Long end, PageRequestData pageRequestData) {
        Criteria<PrivateConsume> privateConsumeCriteria = new Criteria<>();
        privateConsumeCriteria.add(Restrictions.eq("cityId", cityId));
        privateConsumeCriteria.add(Restrictions.in("scene", sceneList));
        privateConsumeCriteria.add(Restrictions.like("name", value, MatchMode.ANYWHERE));
        privateConsumeCriteria.add(Restrictions.gte("createTime", start));
        privateConsumeCriteria.add(Restrictions.lte("createTime", end));
        return super.findAll(privateConsumeCriteria, pageRequestData);
    }

    public List<PrivateConsume> list(String cityId, List<Scene> sceneList, List<SortData> sortDataList) {
        Criteria<PrivateConsume> privateConsumeCriteria = new Criteria<>();
        privateConsumeCriteria.add(Restrictions.eq("cityId", cityId));
        privateConsumeCriteria.add(Restrictions.in("scene", sceneList));
        return super.findAll(privateConsumeCriteria, sortDataList);
    }
}
