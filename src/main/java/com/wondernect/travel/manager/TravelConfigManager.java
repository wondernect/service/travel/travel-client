package com.wondernect.travel.manager;

import com.wondernect.elements.common.exception.BusinessException;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.common.utils.ESStringUtils;
import com.wondernect.elements.rdb.base.manager.BaseSortManager;
import com.wondernect.elements.rdb.criteria.Criteria;
import com.wondernect.elements.rdb.criteria.Restrictions;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.travel.model.TravelConfig;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: TravelConfigManager
 * Author: chenxun
 * Date: 2019-05-12 16:24
 * Description:
 */
@Service
public class TravelConfigManager extends BaseSortManager<TravelConfig, String> {

    public TravelConfig save(TravelConfig travelConfig) {
        TravelConfig travelConfigSave;
        if (ESStringUtils.isNotBlank(travelConfig.getId())) {
            travelConfigSave = super.findById(travelConfig.getId());
            if (ESObjectUtils.isNull(travelConfigSave)) {
                throw new BusinessException("配置不存在");
            }
            travelConfigSave.setName(travelConfig.getName());
        } else {
            travelConfigSave = new TravelConfig(travelConfig.getCode(), travelConfig.getName());
        }
        if (ESObjectUtils.isNotNull(travelConfig.getWeight())) {
            travelConfigSave.setWeight(travelConfig.getWeight());
        }
        return super.save(travelConfigSave);
    }

    public List<TravelConfig> findAllByCode(String code, List<SortData> sortDataList) {
        Criteria<TravelConfig> travelConfigCriteria = new Criteria<>();
        travelConfigCriteria.add(Restrictions.eq("code", code));
        return super.findAll(travelConfigCriteria, sortDataList);
    }
}
