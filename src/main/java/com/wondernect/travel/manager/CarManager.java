package com.wondernect.travel.manager;

import com.wondernect.elements.common.exception.BusinessException;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.common.utils.ESStringUtils;
import com.wondernect.elements.rdb.base.manager.BaseStringManager;
import com.wondernect.elements.rdb.criteria.Criteria;
import com.wondernect.elements.rdb.criteria.Restrictions;
import com.wondernect.elements.rdb.request.PageRequestData;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.travel.model.Car;
import com.wondernect.travel.repository.CarDao;
import org.hibernate.criterion.MatchMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: CarManager
 * Author: chenxun
 * Date: 2019/5/11 16:54
 * Description:
 */
@Service
public class CarManager extends BaseStringManager<Car> {

    @Autowired
    private CarDao carDao;

    public Car save(Car car) {
        Car carSave;
        if (ESStringUtils.isNotBlank(car.getId())) {
            carSave = super.findById(car.getId());
            if (ESObjectUtils.isNull(carSave)) {
                throw new BusinessException("车辆信息不存在");
            }
            if (!ESStringUtils.equalsIgnoreCase(carSave.getDriverUserId(), car.getDriverUserId()) &&
                    ESObjectUtils.isNotNull(findByDriverUserId(car.getDriverUserId()))) {
                throw new BusinessException("司机已存在车辆信息");
            }
            carSave.setCityId(car.getCityId());
            carSave.setCarNo(car.getCarNo());
            carSave.setColor(car.getColor());
            carSave.setZuoweiId(car.getZuoweiId());
            carSave.setChexingId(car.getChexingId());
        } else {
            if (ESObjectUtils.isNotNull(findByDriverUserId(car.getDriverUserId()))) {
                throw new BusinessException("司机已存在车辆信息");
            }
            carSave = new Car(car.getCityId(), car.getCarNo(), car.getZuoweiId(), car.getChexingId(), car.getColor(), car.getDriverUserId());
        }
        return super.save(carSave);
    }

    public Car findByDriverUserId(String driverUserId) {
        return carDao.findByDriverUserId(driverUserId);
    }

    public PageResponseData<Car> page(String cityId, String carNo, PageRequestData pageRequestData) {
        Criteria<Car> carCriteria = new Criteria<>();
        carCriteria.add(Restrictions.eq("cityId", cityId));
        carCriteria.add(Restrictions.like("carNo", carNo, MatchMode.ANYWHERE));
        return super.findAll(carCriteria, pageRequestData);
    }
}
