package com.wondernect.travel.manager;

import com.wondernect.elements.rdb.base.manager.BaseSortManager;
import com.wondernect.elements.rdb.criteria.Criteria;
import com.wondernect.elements.rdb.criteria.Restrictions;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.travel.model.PrivateConsumeCarLevel;
import com.wondernect.travel.repository.PrivateConsumeCarLevelDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: PrivateConsumeCarLevelManager
 * Author: chenxun
 * Date: 2019-10-06 10:24
 * Description: 包车服务配置
 */
@Service
public class PrivateConsumeCarLevelManager extends BaseSortManager<PrivateConsumeCarLevel, String> {

    @Autowired
    private PrivateConsumeCarLevelDao privateConsumeCarLevelDao;

    public void deleteAllByPrivateConsumeId(String privateConsumeId) {
        privateConsumeCarLevelDao.deleteAllByPrivateConsumeId(privateConsumeId);
    }

    public boolean existsByZuoweiIdOrChexingId(String zuoweiId, String chexingId) {
        return privateConsumeCarLevelDao.existsByZuoweiIdOrChexingId(zuoweiId, chexingId);
    }

    public List<PrivateConsumeCarLevel> findAllByConsumeId(String privateConsumeId, List<SortData> sortDataList) {
        Criteria<PrivateConsumeCarLevel> privateConsumeCarLevelCriteria = new Criteria<>();
        privateConsumeCarLevelCriteria.add(Restrictions.eq("privateConsumeId", privateConsumeId));
        return super.findAll(privateConsumeCarLevelCriteria, sortDataList);
    }
}
