package com.wondernect.travel.manager;

import com.wondernect.elements.rdb.base.manager.BaseStringManager;
import com.wondernect.elements.rdb.criteria.Criteria;
import com.wondernect.elements.rdb.criteria.Restrictions;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.travel.model.TravelOrderHistory;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: TravelOrderHistoryManager
 * Author: chenxun
 * Date: 2019/7/5 10:06
 * Description:
 */
@Service
public class TravelOrderHistoryManager extends BaseStringManager<TravelOrderHistory> {

    public List<TravelOrderHistory> list(String travelOrderId) {
        Criteria<TravelOrderHistory> travelOrderHistoryCriteria = new Criteria<>();
        travelOrderHistoryCriteria.add(
                Restrictions.eq("orderId", travelOrderId)
        );
        return super.findAll(travelOrderHistoryCriteria, Arrays.asList(new SortData("createTime", "ASC")));
    }
}
