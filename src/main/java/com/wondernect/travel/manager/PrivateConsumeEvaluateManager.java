package com.wondernect.travel.manager;

import com.wondernect.elements.rdb.base.manager.BaseStringManager;
import com.wondernect.travel.model.PrivateConsumeEvaluate;
import org.springframework.stereotype.Service;

/**
 * Copyright (C), 2020, wondernect.com
 * FileName: PrivateConsumeEvaluateManager
 * Author: chenxun
 * Date: 2020-08-09 10:04
 * Description:
 */
@Service
public class PrivateConsumeEvaluateManager extends BaseStringManager<PrivateConsumeEvaluate> {
}
