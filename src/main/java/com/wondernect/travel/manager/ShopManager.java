package com.wondernect.travel.manager;

import com.wondernect.elements.rdb.base.manager.BaseStringManager;
import com.wondernect.elements.rdb.criteria.Criteria;
import com.wondernect.elements.rdb.criteria.Restrictions;
import com.wondernect.elements.rdb.criteria.em.LogicalOperator;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.travel.model.Shop;
import org.hibernate.criterion.MatchMode;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: ShopManager
 * Author: chenxun
 * Date: 2019/5/11 10:42
 * Description:
 */
@Service
public class ShopManager extends BaseStringManager<Shop> {

    public List<Shop> list(String name, String userId, List<SortData> sortDataList) {
        Criteria<Shop> shopCriteria = new Criteria<>(LogicalOperator.OR);
        shopCriteria.add(Restrictions.like("name", name, MatchMode.ANYWHERE));
        shopCriteria.add(Restrictions.eq("userId", userId));
        return super.findAll(shopCriteria, sortDataList);
    }
}
