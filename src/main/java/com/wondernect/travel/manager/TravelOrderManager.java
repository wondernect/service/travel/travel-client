package com.wondernect.travel.manager;

import com.wondernect.elements.common.utils.ESDistanceUtils;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.common.utils.ESSortUtils;
import com.wondernect.elements.common.utils.ESStringUtils;
import com.wondernect.elements.rdb.base.manager.BaseStringManager;
import com.wondernect.elements.rdb.criteria.Criteria;
import com.wondernect.elements.rdb.criteria.Restrictions;
import com.wondernect.elements.rdb.request.PageRequestData;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.travel.business.chexing.manager.ChexingManager;
import com.wondernect.travel.business.zuowei.manager.ZuoweiManager;
import com.wondernect.travel.cache.DriverAcceptedOrderCache;
import com.wondernect.travel.cache.DriverCurrentCache;
import com.wondernect.travel.cache.DriverNowCache;
import com.wondernect.travel.cache.DriverOrderCache;
import com.wondernect.travel.cache.model.DriverCurrent;
import com.wondernect.travel.constant.CarConfigConstant;
import com.wondernect.travel.dto.base.TravelOrderResponseDTO;
import com.wondernect.travel.model.OrderStrategy;
import com.wondernect.travel.model.TravelOrder;
import com.wondernect.travel.model.em.*;
import com.wondernect.travel.model.excel.GoodsOrderExcel;
import com.wondernect.travel.model.excel.SettledTravelOrderExcel;
import com.wondernect.travel.model.excel.TravelOrderExcel;
import com.wondernect.travel.model.excel.WarningTravelOrderExcel;
import com.wondernect.travel.repository.TravelOrderDao;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: TravelOrderManager
 * Author: chenxun
 * Date: 2019/5/11 17:00
 * Description:
 */
@Service
public class TravelOrderManager extends BaseStringManager<TravelOrder> {

    private static final Logger logger = LoggerFactory.getLogger(TravelOrderManager.class);

    @Autowired
    private TravelOrderDao travelOrderDao;

    @Autowired
    private DriverEvaluateManager driverEvaluateManager;

    @Autowired
    private DriverAcceptedOrderCache driverAcceptedOrderCache;

    @Autowired
    private DriverNowCache driverNowCache;

    @Autowired
    private DriverOrderCache driverOrderCache;

    @Autowired
    private DriverCurrentCache driverCurrentCache;

    @Autowired
    private ZuoweiManager zuoweiManager;

    @Autowired
    private ChexingManager chexingManager;

    @Autowired
    private ConsumeManager consumeManager;

    @Autowired
    private PrivateConsumeManager privateConsumeManager;

    public Long countAllByUserIdAndOrderStatusIn(String userId) {
        return travelOrderDao.countAllByUserIdAndOrderStatusIn(userId);
    }

    public PageResponseData<TravelOrder> driverOrderList(String driverUserId, PageRequestData pageRequestData) {
        DriverCurrent driverCurrent = driverCurrentCache.get(driverUserId);
        List<String> zuoweiIdList = new ArrayList<>();
        List<String> chexingIdList = new ArrayList<>();
        zuoweiIdList.add(CarConfigConstant.ALL_CAR_CONFIG);
        chexingIdList.add(CarConfigConstant.ALL_CAR_CONFIG);
        if (ESObjectUtils.isNotNull(driverCurrent)) {
            if (ESStringUtils.isNotBlank(driverCurrent.getZuoweiId())) {
                zuoweiIdList.add(driverCurrent.getZuoweiId());
            }
            if (ESStringUtils.isNotBlank(driverCurrent.getChexingId())) {
                chexingIdList.add(driverCurrent.getChexingId());
            }
        }
        return travelOrderDao.driverOrderList(driverCurrent.getCityId(), zuoweiIdList, chexingIdList, pageRequestData);
    }

    public PageResponseData<TravelOrder> driverOrderListTest(String driverUserId, PageRequestData pageRequestData) {
        DriverCurrent driverCurrent = driverCurrentCache.get(driverUserId);
        List<String> zuoweiIdList = new ArrayList<>();
        List<String> chexingIdList = new ArrayList<>();
        zuoweiIdList.add(CarConfigConstant.ALL_CAR_CONFIG);
        chexingIdList.add(CarConfigConstant.ALL_CAR_CONFIG);
        if (ESObjectUtils.isNotNull(driverCurrent)) {
            if (ESStringUtils.isNotBlank(driverCurrent.getZuoweiId())) {
                zuoweiIdList.add(driverCurrent.getZuoweiId());
            }
            if (ESStringUtils.isNotBlank(driverCurrent.getChexingId())) {
                chexingIdList.add(driverCurrent.getChexingId());
            }
        }
        return travelOrderDao.driverOrderList(driverCurrent.getCityId(), zuoweiIdList, chexingIdList, pageRequestData);
    }

    public List<TravelOrder> driverSettledOrderList(String driverUserId, Long startTime, Long endTime, List<SortData> sortDataList) {
        return travelOrderDao.driverSettledOrderList(driverUserId, startTime, endTime, sortDataList);
    }

    public PageResponseData<TravelOrderResponseDTO> userPage(String userId, List<Scene> sceneList, List<OrderStatus> statusList, Boolean evaluate, PageRequestData pageRequestData) {
        List<TravelOrderResponseDTO> travelOrderListDTO = new ArrayList<>();
        Criteria<TravelOrder> travelOrderCriteria = new Criteria<>();
        travelOrderCriteria.add(Restrictions.eq("userId", userId));
        travelOrderCriteria.add(Restrictions.in("scene", sceneList));
        travelOrderCriteria.add(Restrictions.in("orderStatus", statusList));
        PageResponseData<TravelOrder> travelOrderPageResponseData = super.findAll(travelOrderCriteria, pageRequestData);
        List<TravelOrder> travelOrderList = travelOrderPageResponseData.getDataList();
        if (CollectionUtils.isNotEmpty(travelOrderList)) {
            for (TravelOrder travelOrder : travelOrderList) {
                TravelOrderResponseDTO travelOrderResponseDTO = new TravelOrderResponseDTO();
                travelOrder.setOrderStatusInt(travelOrder.getOrderStatus().ordinal());
                if (ESObjectUtils.isNull(travelOrder.getEvaluate())) {
                    if (ESObjectUtils.isNotNull(driverEvaluateManager.findTopByUserIdAndOrderId(travelOrder.getUserId(), travelOrder.getId()))) {
                        travelOrder.setEvaluate(true);
                    } else {
                        travelOrder.setEvaluate(false);
                    }
                }
                travelOrderResponseDTO.setTravelOrder(travelOrder);
                travelOrderResponseDTO.setZuowei(zuoweiManager.findById(travelOrder.getZuoweiId()));
                travelOrderResponseDTO.setChexing(chexingManager.findById(travelOrder.getChexingId()));
                switch (travelOrder.getScene()) {
                    case JIEJI:
                    case SONGJI:
                    case DAY_PRIVATE:
                    case ORDER_SCENE:
                    {
                        if (ESStringUtils.isNotBlank(travelOrder.getConsumeId())) {
                            travelOrderResponseDTO.setConsume(consumeManager.findById(travelOrder.getConsumeId()));
                        }
                        break;
                    }
                    case ROAD_PRIVATE:
                    case MEISHI_PRIVATE:
                    case JINGDIAN_PRIVATE:
                    case BANSHOU_PRIVATE:
                    {
                        if (ESStringUtils.isNotBlank(travelOrder.getPrivateConsumeId())) {
                            travelOrderResponseDTO.setPrivateConsume(privateConsumeManager.findById(travelOrder.getPrivateConsumeId()));
                        }
                        break;
                    }
                }
                if (ESObjectUtils.isNotNull(evaluate)) {
                    if (travelOrder.getEvaluate() == evaluate) {
                        travelOrderListDTO.add(travelOrderResponseDTO);
                    }
                } else {
                    travelOrderListDTO.add(travelOrderResponseDTO);
                }

            }
        }
        return new PageResponseData<>(pageRequestData.getPage(), pageRequestData.getSize(), travelOrderPageResponseData.getTotalPages(), travelOrderPageResponseData.getTotalElements(), travelOrderListDTO);
    }

    public PageResponseData<TravelOrderResponseDTO> billPage(String userId, int type, PageRequestData pageRequestData) {
        List<TravelOrderResponseDTO> travelOrderListDTO = new ArrayList<>();
        Criteria<TravelOrder> travelOrderCriteria = new Criteria<>();
        travelOrderCriteria.add(Restrictions.eq("userId", userId));
        travelOrderCriteria.add(Restrictions.eq("orderStatus", OrderStatus.DONE));
        if (type == 1) {
            // 可开具发票
            travelOrderCriteria.add(Restrictions.isNull("billId"));
        } else {
            // 已开具发票
            travelOrderCriteria.add(Restrictions.isNotNull("billId"));
        }
        PageResponseData<TravelOrder> travelOrderPageResponseData = super.findAll(travelOrderCriteria, pageRequestData);
        List<TravelOrder> travelOrderList = travelOrderPageResponseData.getDataList();
        if (CollectionUtils.isNotEmpty(travelOrderList)) {
            for (TravelOrder travelOrder : travelOrderList) {
                TravelOrderResponseDTO travelOrderResponseDTO = new TravelOrderResponseDTO();
                travelOrder.setOrderStatusInt(travelOrder.getOrderStatus().ordinal());
                travelOrderResponseDTO.setTravelOrder(travelOrder);
                switch (travelOrder.getScene()) {
                    case JIEJI:
                    case SONGJI:
                    case DAY_PRIVATE:
                    case ORDER_SCENE:
                    {
                        if (ESStringUtils.isNotBlank(travelOrder.getConsumeId())) {
                            travelOrderResponseDTO.setConsume(consumeManager.findById(travelOrder.getConsumeId()));
                        }
                        break;
                    }
                    case ROAD_PRIVATE:
                    case MEISHI_PRIVATE:
                    case JINGDIAN_PRIVATE:
                    case BANSHOU_PRIVATE:
                    {
                        if (ESStringUtils.isNotBlank(travelOrder.getPrivateConsumeId())) {
                            travelOrderResponseDTO.setPrivateConsume(privateConsumeManager.findById(travelOrder.getPrivateConsumeId()));
                        }
                        break;
                    }
                }
                travelOrderListDTO.add(travelOrderResponseDTO);
            }
        }
        return new PageResponseData<>(pageRequestData.getPage(), pageRequestData.getSize(), travelOrderPageResponseData.getTotalPages(), travelOrderPageResponseData.getTotalElements(), travelOrderListDTO);
    }

    public List<TravelOrder> driverCancelList(String driverUserId, List<SortData> sortDataList) {
        Criteria<TravelOrder> travelOrderCriteria = new Criteria<>();
        travelOrderCriteria.add(
                Restrictions.and(
                        Restrictions.eq("driverUserId", driverUserId),
                        Restrictions.or(
                                Restrictions.eq("orderStatus", OrderStatus.CANCEL_CONSOLE),
                                Restrictions.eq("orderStatus", OrderStatus.CANCEL_SHOP),
                                Restrictions.eq("orderStatus", OrderStatus.CANCEL_USER)
                        )
                )
        );
        return super.findAll(travelOrderCriteria, sortDataList);
    }

    public PageResponseData<TravelOrder> driverDonePage(String driverUserId, PageRequestData pageRequestData) {
        Criteria<TravelOrder> travelOrderCriteria = new Criteria<>();
        travelOrderCriteria.add(
                Restrictions.and(
                        Restrictions.eq("driverUserId", driverUserId),
                        Restrictions.or(
                                Restrictions.eq("orderStatus", OrderStatus.DONE),
                                Restrictions.eq("orderStatus", OrderStatus.SETTLED)
                        )
                )
        );
        return super.findAll(travelOrderCriteria, pageRequestData);
    }

    public PageResponseData<TravelOrder> orderPage(
            List<Scene> sceneList,
            OrderStatus orderStatus,
            OrderSource orderSource,
            String shopId,
            String sourceShopId,
            String sourceDriverUserId,
            String driverUserId,
            Integer type,
            Long start,
            Long end,
            String value,
            PageRequestData pageRequestData
    ) {
        return travelOrderDao.orderPage(sceneList, orderStatus, orderSource, shopId, sourceShopId, sourceDriverUserId, driverUserId, type, start, end, value, pageRequestData);
    }

    public void exportOrderPage(
            List<Scene> sceneList,
            OrderStatus orderStatus,
            OrderSource orderSource,
            String shopId,
            String sourceShopId,
            String sourceDriverUserId,
            String driverUserId,
            Integer type,
            Long start,
            Long end,
            String value,
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        PageRequestData pageRequestData = new PageRequestData();
        pageRequestData.setPage(0);
        pageRequestData.setSize(10000);
        PageResponseData<TravelOrder> travelOrderPageResponseData = travelOrderDao.orderPage(sceneList, orderStatus, orderSource, shopId, sourceShopId, sourceDriverUserId, driverUserId, type, start, end, value, pageRequestData);
        if (sceneList.contains(Scene.BANSHOU_PRIVATE)) {
            GoodsOrderExcel.exportData(travelOrderPageResponseData.getDataList(), request, response);
        } else {
            TravelOrderExcel.exportData(travelOrderPageResponseData.getDataList(), request, response);
        }
    }

    public PageResponseData<TravelOrder> settledPage(Integer settledType, OrderSource orderSource, String shopId, String sourceShopId, String orderId, String value, String targetPlace, Long settledStart, Long settledEnd, Long createStart, Long createEnd, PageRequestData pageRequestData) {
        return travelOrderDao.settledPage(settledType, orderSource, shopId, sourceShopId, orderId, value, targetPlace, settledStart, settledEnd, createStart, createEnd, pageRequestData);
    }

    public void exportSettledPage(
            Integer settledType,
            OrderSource orderSource,
            String shopId,
            String sourceShopId,
            String orderId,
            String value,
            String targetPlace,
            Long settledStart,
            Long settledEnd,
            Long createStart,
            Long createEnd,
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        PageRequestData pageRequestData = new PageRequestData();
        pageRequestData.setPage(0);
        pageRequestData.setSize(10000);
        PageResponseData<TravelOrder> travelOrderPageResponseData = travelOrderDao.settledPage(settledType, orderSource, shopId, sourceShopId, orderId, value, targetPlace, settledStart, settledEnd, createStart, createEnd, pageRequestData);
        SettledTravelOrderExcel.exportData(travelOrderPageResponseData.getDataList(), request, response);
    }

    public PageResponseData<TravelOrder> warningPage(WarningStatus warningStatus, String username, String orderId, String startPlace, String targetPlace, Long createStart, Long createEnd, Long executeStart, Long executeEnd, Double lowPrice, Double highPrice, PageRequestData pageRequestData) {
        return travelOrderDao.warningPage(warningStatus, username, orderId, startPlace, targetPlace, createStart, createEnd, executeStart, executeEnd, lowPrice, highPrice, pageRequestData);
    }

    public void exportWarningPage(
            WarningStatus warningStatus,
            String username,
            String orderId,
            String startPlace,
            String targetPlace,
            Long createStart,
            Long createEnd,
            Long executeStart,
            Long executeEnd,
            Double lowPrice,
            Double highPrice,
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        PageRequestData pageRequestData = new PageRequestData();
        pageRequestData.setPage(0);
        pageRequestData.setSize(1000);
        PageResponseData<TravelOrder> travelOrderPageResponseData = travelOrderDao.warningPage(warningStatus, username, orderId, startPlace, targetPlace, createStart, createEnd, executeStart, executeEnd, lowPrice, highPrice, pageRequestData);
        WarningTravelOrderExcel.exportData(travelOrderPageResponseData.getDataList(), request, response);
    }

    public Long countOrder(WarningStatus warningStatus) {
        return travelOrderDao.countOrder(warningStatus);
    }

    public List<TravelOrder> findAcceptedOrderList(String driverUserId) {
        List<Object> objectList = driverAcceptedOrderCache.getOrderList(driverUserId);
        List<TravelOrder> travelOrderList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(objectList)) {
            for (Object object : objectList) {
                String orderId = "";
                if (object instanceof String) {
                    orderId = (String) object;
                }
                TravelOrder travelOrder = travelOrderDao.findById(orderId);
                if (ESObjectUtils.isNotNull(travelOrder)) {
                    if (travelOrder.getOrderStatus() == OrderStatus.ACCEPTED ||
                            travelOrder.getOrderStatus() == OrderStatus.ON_THE_WAY) {
                        travelOrderList.add(travelOrder);
                    } else {
                        driverAcceptedOrderCache.removeOrder(driverUserId, orderId);
                    }
                } else {
                    driverAcceptedOrderCache.removeOrder(driverUserId, orderId);
                }
            }
        }
        ESSortUtils.sort(travelOrderList, false, "startTime");
        return travelOrderList;
    }

    public List<TravelOrder> findNowAndOrder(String driverUserId) {
        List<TravelOrder> travelOrderList = new ArrayList<>();
        List<TravelOrder> travelOrderListNow = findNow(driverUserId);
        if (CollectionUtils.isNotEmpty(travelOrderListNow)) {
            travelOrderList.addAll(travelOrderListNow);
        }
        List<TravelOrder> travelOrderListOrder = findOrder(driverUserId);
        if (CollectionUtils.isNotEmpty(travelOrderListOrder)) {
            travelOrderList.addAll(travelOrderListOrder);
        }
        return travelOrderList;
    }

    public List<TravelOrder> findNow(String driverUserId) {
        List<String> orderList = driverNowCache.getOrderList(driverUserId);
        List<TravelOrder> travelOrderList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(orderList)) {
            for (String orderId : orderList) {
                TravelOrder travelOrder = travelOrderDao.findById(orderId);
                if (ESObjectUtils.isNotNull(travelOrder)) {
                    if (travelOrder.getOrderStatus() == OrderStatus.AUTO) {
                        travelOrderList.add(travelOrder);
                    } else {
                        driverNowCache.removeOrder(driverUserId, orderId);
                    }
                } else {
                    driverNowCache.removeOrder(driverUserId, orderId);
                }
            }

        }
        return travelOrderList;
    }

    public List<TravelOrder> findOrder(String driverUserId) {
        List<Object> objectList = driverOrderCache.getOrderList(driverUserId);
        List<TravelOrder> travelOrderList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(objectList)) {
            for (Object object : objectList) {
                String orderId = "";
                if (object instanceof String) {
                    orderId = (String) object;
                }
                TravelOrder travelOrder = travelOrderDao.findById(orderId);
                if (ESObjectUtils.isNotNull(travelOrder)) {
                    if (travelOrder.getOrderStatus() == OrderStatus.AUTO) {
                        travelOrderList.add(travelOrder);
                    } else {
                        driverOrderCache.removeOrder(driverUserId, orderId);
                    }
                } else {
                    driverOrderCache.removeOrder(driverUserId, orderId);
                }
            }
        }
        return travelOrderList;
    }

    public void driverDoneOrder(String driverUserId, String orderId) {
        driverAcceptedOrderCache.removeOrder(driverUserId, orderId);
    }

    public void driverAcceptOrder(String driverUserId, String orderId, CommonScene commonScene) {
        driverAcceptedOrderCache.addOrder(driverUserId, orderId);
        Set<String> driverList = driverCurrentCache.driverList();
        for (String driverId : driverList) {
            if (!ESStringUtils.equalsIgnoreCase(driverId, driverUserId)) {
                switch (commonScene) {
                    case NOW:
                    {
                        driverNowCache.removeOrder(driverUserId, orderId);
                        break;
                    }
                    case ORDER:
                    {
                        driverOrderCache.removeOrder(driverUserId, orderId);
                        break;
                    }
                }
            }
        }
    }

    public void sendAutoOrder(TravelOrder travelOrder, OrderStrategy orderStrategy) {
        // logger.info("订单{}分发,策略{}", travelOrder, orderStrategy);
        Set<String> driverList = driverCurrentCache.driverList();
        for (String driverUserId : driverList) {
            DriverCurrent driverCurrent = driverCurrentCache.get(driverUserId);
            if (ESObjectUtils.isNotNull(driverCurrent) && ESObjectUtils.isNotNull(orderStrategy)) {
                if (ESStringUtils.isNotBlank(travelOrder.getZuoweiId()) &&
                        ESStringUtils.isNotBlank(travelOrder.getChexingId())) {
                    if (checkCarConfigId(driverCurrent, travelOrder) &&
                            checkDriverStatus(driverCurrent, orderStrategy) &&
                            checkDriverDistance(driverCurrent, travelOrder, orderStrategy) &&
                            checkDriverEvaluate(driverCurrent, orderStrategy)) {
                        // 派单
                        switch (travelOrder.getCommonScene()) {
                            case NOW:
                            {
                                driverNowCache.addOrder(driverUserId, travelOrder.getId());
                                break;
                            }
                            case ORDER:
                            {
                                driverOrderCache.addOrder(driverUserId, travelOrder.getId());
                                break;
                            }
                        }
                    }
                } else {
                    if (checkDriverStatus(driverCurrent, orderStrategy) &&
                            checkDriverDistance(driverCurrent, travelOrder, orderStrategy) &&
                            checkDriverEvaluate(driverCurrent, orderStrategy)) {
                        // 派单
                        switch (travelOrder.getCommonScene()) {
                            case NOW:
                            {
                                driverNowCache.addOrder(driverUserId, travelOrder.getId());
                                break;
                            }
                            case ORDER:
                            {
                                driverOrderCache.addOrder(driverUserId, travelOrder.getId());
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    private Boolean checkCarConfigId(DriverCurrent driverCurrent, TravelOrder travelOrder) {
        if (!ESStringUtils.equalsIgnoreCase(travelOrder.getCityId(), driverCurrent.getCityId())) {
            return false;
        }
        if (travelOrder.getZuoweiId().equalsIgnoreCase(CarConfigConstant.ALL_CAR_CONFIG)) {
            if (travelOrder.getChexingId().equalsIgnoreCase(CarConfigConstant.ALL_CAR_CONFIG)) {
                return true;
            } else {
                if (ESStringUtils.equalsIgnoreCase(driverCurrent.getChexingId(), travelOrder.getChexingId())) {
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            if (travelOrder.getChexingId().equalsIgnoreCase(CarConfigConstant.ALL_CAR_CONFIG)) {
                if (ESStringUtils.equalsIgnoreCase(driverCurrent.getZuoweiId(), travelOrder.getZuoweiId())) {
                    return true;
                } else {
                    return false;
                }
            } else {
                if (ESStringUtils.equalsIgnoreCase(driverCurrent.getZuoweiId(), travelOrder.getZuoweiId()) &&
                        ESStringUtils.equalsIgnoreCase(driverCurrent.getChexingId(), travelOrder.getChexingId())) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }

    private Boolean checkDriverStatus(DriverCurrent driverCurrent, OrderStrategy orderStrategy) {
        if (orderStrategy.getDriverStatus() != null) {
            switch (orderStrategy.getDriverStatus()) {
                case IDLE:
                {
                    return driverCurrent.getStatus() == orderStrategy.getDriverStatus();
                }
            }
        }
        return true;
    }

    private Boolean checkDriverDistance(DriverCurrent driverCurrent, TravelOrder travelOrder, OrderStrategy orderStrategy) {
        if (ESObjectUtils.isNull(driverCurrent.getLongitude()) ||
                driverCurrent.getLongitude() == 0 ||
                ESObjectUtils.isNull(driverCurrent.getLatitude()) ||
                driverCurrent.getLatitude() == 0) {
            return false;
        }
        Double distance = ESDistanceUtils.distance(driverCurrent.getLongitude(), driverCurrent.getLatitude(), travelOrder.getStartLongitude(), travelOrder.getStartLatitude());
        if (orderStrategy.getDistanceScope() != null && orderStrategy.getDistanceScope() > 0) {
            return distance < orderStrategy.getDistanceScope();
        }
        return true;
    }

    private Boolean checkDriverEvaluate(DriverCurrent driverCurrent, OrderStrategy orderStrategy) {
        if (orderStrategy.getDriverEvaluate() > 0) {
            return driverCurrent.getEvaluate() > orderStrategy.getDriverEvaluate();
        }
        return true;
    }
}
