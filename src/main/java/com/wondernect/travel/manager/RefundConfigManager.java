package com.wondernect.travel.manager;

import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.rdb.base.manager.BaseManager;
import com.wondernect.travel.model.RefundConfig;
import org.springframework.stereotype.Service;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: RefundConfigManager
 * Author: chenxun
 * Date: 2019/5/11 17:00
 * Description:
 */
@Service
public class RefundConfigManager extends BaseManager<RefundConfig, String> {

    public RefundConfig save(RefundConfig refundConfig) {
        RefundConfig refundConfigGet = super.findById(refundConfig.getCode());
        if (ESObjectUtils.isNotNull(refundConfigGet)) {
            refundConfigGet.setBeforeFirstTime(refundConfig.getBeforeFirstTime());
            refundConfigGet.setFirstFund(refundConfig.getFirstFund());
            refundConfigGet.setBeforeSecondTime(refundConfig.getBeforeSecondTime());
            refundConfigGet.setSecondFund(refundConfig.getSecondFund());
            refundConfigGet.setLimitFund(refundConfig.getLimitFund());
            return super.save(refundConfigGet);
        } else {
            return super.save(new RefundConfig(
                    refundConfig.getCode(),
                    refundConfig.getBeforeFirstTime(),
                    refundConfig.getFirstFund(),
                    refundConfig.getBeforeSecondTime(),
                    refundConfig.getSecondFund(),
                    refundConfig.getLimitFund()
            ));
        }
    }

    public RefundConfig getOrderRefundConfig() {
        return super.findById(RefundConfig.ORDER_CODE);
    }

    public RefundConfig getPrivateRefundConfig() {
        return super.findById(RefundConfig.PRIVATE_CODE);
    }

    public RefundConfig getZeroRefundConfig() {
        return new RefundConfig(
                RefundConfig.ZERO_REFUND_CODE,
                null,
                null,
                null,
                null,
                null
        );
    }
}
