package com.wondernect.travel.manager.schedule;

import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.rdb.base.manager.BaseManager;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.services.user.model.User;
import com.wondernect.services.user.service.UserService;
import com.wondernect.travel.manager.CarManager;
import com.wondernect.travel.manager.TravelOrderManager;
import com.wondernect.travel.model.Car;
import com.wondernect.travel.model.TravelOrder;
import com.wondernect.travel.model.schedule.SMSSchedule;
import com.wondernect.travel.sms.TravelSMSService;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: SMSScheduleManager
 * Author: chenxun
 * Date: 2019-10-27 20:41
 * Description:
 */
@Service
public class SMSScheduleManager extends BaseManager<SMSSchedule, String> {

    private static final Logger logger = LoggerFactory.getLogger(SMSScheduleManager.class);

    @Autowired
    private TravelOrderManager travelOrderManager;

    @Autowired
    private UserService userService;

    @Autowired
    private CarManager carManager;

    @Autowired
    private TravelSMSService travelSMSService;

    public void process() {
        List<SMSSchedule> smsScheduleList = super.findAll(Arrays.asList(new SortData("createTime", "ASC")));
        if (CollectionUtils.isNotEmpty(smsScheduleList)) {
            for (SMSSchedule smsSchedule : smsScheduleList) {
                switch (smsSchedule.getSmsType()) {
                    case 0:
                    {
                        // 短信验证码发送
                        try {
                            // logger.info("短信验证码发送开始:{}", smsSchedule);
                            travelSMSService.sendCaptcha(smsSchedule.getPhoneNumber(), smsSchedule.getCaptcha());
                            super.deleteById(smsSchedule.getId());
                        } catch (Exception e) {
                            logger.error("短信验证码发送失败", e);
                            int retry = smsSchedule.getRetry();
                            if (retry < 2) {
                                retry++;
                                smsSchedule.setRetry(retry);
                                super.save(smsSchedule);
                            } else {
                                super.deleteById(smsSchedule.getId());
                            }
                        }
                        break;
                    }
                    case 1:
                    {
                        // 预约成功短信通知 - 乘客端
                        try {
                            // logger.info("预约成功短信通知 - 乘客端发送开始:{}", smsSchedule);
                            TravelOrder travelOrder = travelOrderManager.findById(smsSchedule.getOrderId());
                            if (ESObjectUtils.isNotNull(travelOrder)) {
                                // 发送短信给乘客
                                travelSMSService.orderCreateForUser(smsSchedule.getPhoneNumber(), travelOrder.getStartTime());
                                super.deleteById(smsSchedule.getId());
                            } else {
                                int retry = smsSchedule.getRetry();
                                logger.error("预约成功短信通知 - 乘客端发送失败，订单{}查询不存在，重试次数：{}", smsSchedule, retry);
                                if (retry < 2) {
                                    retry++;
                                    smsSchedule.setRetry(retry);
                                    super.save(smsSchedule);
                                } else {
                                    super.deleteById(smsSchedule.getId());
                                }
                            }
                        } catch (Exception e) {
                            logger.error("预约成功短信通知 - 乘客端发送失败", e);
                            int retry = smsSchedule.getRetry();
                            if (retry < 2) {
                                retry++;
                                smsSchedule.setRetry(retry);
                                super.save(smsSchedule);
                            } else {
                                super.deleteById(smsSchedule.getId());
                            }
                        }
                        break;
                    }
                    case 2:
                    {
                        // 司机接单短信通知 - 乘客端
                        try {
                            // logger.info("司机接单短信通知 - 乘客端发送开始:{}", smsSchedule);
                            TravelOrder travelOrder = travelOrderManager.findById(smsSchedule.getOrderId());
                            if (ESObjectUtils.isNotNull(travelOrder)) {
                                User driver = userService.findByUserId(travelOrder.getDriverUserId());
                                Car car = carManager.findByDriverUserId(travelOrder.getDriverUserId());
                                // 发送短信给乘客
                                travelSMSService.orderAcceptedForUser(
                                        smsSchedule.getPhoneNumber(),
                                        travelOrder.getStartTime(),
                                        ESObjectUtils.isNotNull(driver) ? driver.getName() : "",
                                        ESObjectUtils.isNotNull(car) ? car.getCarNo() : "",
                                        ESObjectUtils.isNotNull(car) ? car.getColor() : "",
                                        ESObjectUtils.isNotNull(driver) ? driver.getMobile() : "");
                                super.deleteById(smsSchedule.getId());
                            } else {
                                int retry = smsSchedule.getRetry();
                                logger.error("司机接单短信通知 - 乘客端发送失败，订单{}查询不存在，重试次数：{}", smsSchedule, retry);
                                if (retry < 2) {
                                    retry++;
                                    smsSchedule.setRetry(retry);
                                    super.save(smsSchedule);
                                } else {
                                    super.deleteById(smsSchedule.getId());
                                }
                            }
                        } catch (Exception e) {
                            logger.error("司机接单短信通知 - 乘客端发送失败", e);
                            int retry = smsSchedule.getRetry();
                            if (retry < 2) {
                                retry++;
                                smsSchedule.setRetry(retry);
                                super.save(smsSchedule);
                            } else {
                                super.deleteById(smsSchedule.getId());
                            }
                        }
                        break;
                    }
                    case 3:
                    {
                        // 司机接单短信通知 - 司机端
                        try {
                            // logger.info("司机接单短信通知 - 司机端发送开始:{}", smsSchedule);
                            TravelOrder travelOrder = travelOrderManager.findById(smsSchedule.getOrderId());
                            if (ESObjectUtils.isNotNull(travelOrder)) {
                                // 发送短信给司机
                                travelSMSService.orderAcceptedForDriver(
                                        smsSchedule.getPhoneNumber(),
                                        travelOrder.getStartTime(),
                                        travelOrder.getUsername(),
                                        travelOrder.getMobile());
                                super.deleteById(smsSchedule.getId());
                            } else {
                                int retry = smsSchedule.getRetry();
                                logger.error("司机接单短信通知 - 司机端发送失败，订单{}查询不存在，重试次数：{}", smsSchedule, retry);
                                if (retry < 2) {
                                    retry++;
                                    smsSchedule.setRetry(retry);
                                    super.save(smsSchedule);
                                } else {
                                    super.deleteById(smsSchedule.getId());
                                }
                            }
                        } catch (Exception e) {
                            logger.error("司机接单短信通知 - 司机端发送失败", e);
                            int retry = smsSchedule.getRetry();
                            if (retry < 2) {
                                retry++;
                                smsSchedule.setRetry(retry);
                                super.save(smsSchedule);
                            } else {
                                super.deleteById(smsSchedule.getId());
                            }
                        }
                        break;
                    }
                    case 4:
                    {
                        // 伴手礼下单成功短信通知 - 用户
                        try {
                            // logger.info("伴手礼下单成功短信通知 - 用户发送开始:{}", smsSchedule);
                            TravelOrder travelOrder = travelOrderManager.findById(smsSchedule.getOrderId());
                            if (ESObjectUtils.isNotNull(travelOrder)) {
                                // 发送短信给用户
                                travelSMSService.banshouOrderCreateForUser(smsSchedule.getPhoneNumber(), travelOrder.getStartTime());
                                super.deleteById(smsSchedule.getId());
                            } else {
                                int retry = smsSchedule.getRetry();
                                logger.error("伴手礼下单成功短信通知 - 用户发送失败，订单{}查询不存在，重试次数：{}", smsSchedule, retry);
                                if (retry < 2) {
                                    retry++;
                                    smsSchedule.setRetry(retry);
                                    super.save(smsSchedule);
                                } else {
                                    super.deleteById(smsSchedule.getId());
                                }
                            }
                        } catch (Exception e) {
                            logger.error("伴手礼下单成功短信通知 - 用户发送失败", e);
                            int retry = smsSchedule.getRetry();
                            if (retry < 2) {
                                retry++;
                                smsSchedule.setRetry(retry);
                                super.save(smsSchedule);
                            } else {
                                super.deleteById(smsSchedule.getId());
                            }
                        }
                        break;
                    }
                    case 5:
                    {
                        // 伴手礼发货成功短信通知 - 用户
                        try {
                            // logger.info("伴手礼发货成功短信通知 - 用户发送开始:{}", smsSchedule);
                            TravelOrder travelOrder = travelOrderManager.findById(smsSchedule.getOrderId());
                            if (ESObjectUtils.isNotNull(travelOrder)) {
                                // 发送短信给用户
                                travelSMSService.banshouOrderSendForUser(smsSchedule.getPhoneNumber(), travelOrder.getExecuteTime());
                                super.deleteById(smsSchedule.getId());
                            } else {
                                int retry = smsSchedule.getRetry();
                                logger.error("伴手礼发货成功短信通知 - 用户发送失败，订单{}查询不存在，重试次数：{}", smsSchedule, retry);
                                if (retry < 2) {
                                    retry++;
                                    smsSchedule.setRetry(retry);
                                    super.save(smsSchedule);
                                } else {
                                    super.deleteById(smsSchedule.getId());
                                }
                            }
                        } catch (Exception e) {
                            logger.error("伴手礼发货成功短信通知 - 用户发送失败", e);
                            int retry = smsSchedule.getRetry();
                            if (retry < 2) {
                                retry++;
                                smsSchedule.setRetry(retry);
                                super.save(smsSchedule);
                            } else {
                                super.deleteById(smsSchedule.getId());
                            }
                        }
                        break;
                    }
                }
            }
        }
    }
}
