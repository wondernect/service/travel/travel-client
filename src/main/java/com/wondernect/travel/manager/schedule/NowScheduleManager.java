package com.wondernect.travel.manager.schedule;

import com.wondernect.elements.common.utils.ESDateTimeUtils;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.rdb.base.manager.BaseManager;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.travel.manager.OrderStrategyManager;
import com.wondernect.travel.manager.TravelOrderManager;
import com.wondernect.travel.model.OrderStrategy;
import com.wondernect.travel.model.TravelOrder;
import com.wondernect.travel.model.em.OrderStatus;
import com.wondernect.travel.model.em.Scene;
import com.wondernect.travel.model.schedule.NowSchedule;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: NowScheduleManager
 * Author: chenxun
 * Date: 2019-10-27 20:22
 * Description:
 */
@Service
public class NowScheduleManager extends BaseManager<NowSchedule, String> {

    private static final Logger logger = LoggerFactory.getLogger(NowScheduleManager.class);

    @Autowired
    private TravelOrderManager travelOrderManager;

    @Autowired
    private OrderStrategyManager orderStrategyManager;

    public void process() {
        List<NowSchedule> nowScheduleList = super.findAll(Arrays.asList(new SortData("createTime", "ASC")));
        if (CollectionUtils.isNotEmpty(nowScheduleList)) {
            for (NowSchedule nowSchedule : nowScheduleList) {
                Long currentTime = ESDateTimeUtils.getCurrentTimestamp();
                TravelOrder travelOrder = travelOrderManager.findById(nowSchedule.getOrderId());
                if (ESObjectUtils.isNotNull(travelOrder)) {
                    if (ESObjectUtils.isNotNull(travelOrder.getScene()) && travelOrder.getScene() != Scene.BANSHOU_PRIVATE) {
                        if (travelOrder.getOrderStatus() == OrderStatus.AUTO) {
                            // 获取订单派送策略
                            OrderStrategy orderStrategy = orderStrategyManager.findById(nowSchedule.getOrderStrategyId());
                            if (ESObjectUtils.isNull(orderStrategy)) {
                                // 找不到自动派单时指定的对应策略
                                orderStrategy = orderStrategyManager.findNextOrderStrategy(null);
                                if (ESObjectUtils.isNull(orderStrategy)) {
                                    // logger.info("立即叫车订单，没有派单策略,继续循环:{}", nowSchedule);
                                } else {
                                    travelOrderManager.sendAutoOrder(travelOrder, orderStrategy);
                                    nowSchedule.setOrderStrategyId(orderStrategy.getId());
                                    nowSchedule.setLastAddTime(currentTime);
                                    super.save(nowSchedule);
                                    // logger.info("立即叫车订单，执行派单后，设置派单策略,继续循环:{}", nowSchedule);
                                }
                            } else {
                                Long changeTime = orderStrategy.getAutoTime() * 1000 + nowSchedule.getLastAddTime();
                                if (changeTime > currentTime) {
                                    travelOrderManager.sendAutoOrder(travelOrder, orderStrategy);
                                    logger.info("立即叫车订单，还没到策略切换时间,执行派单后继续循环:{}", nowSchedule);
                                } else {
                                    OrderStrategy orderStrategyNext = orderStrategyManager.findNextOrderStrategy(nowSchedule.getOrderStrategyId());
                                    travelOrderManager.sendAutoOrder(travelOrder, orderStrategyNext);
                                    nowSchedule.setOrderStrategyId(orderStrategyNext.getId());
                                    nowSchedule.setLastAddTime(currentTime);
                                    super.save(nowSchedule);
                                    logger.info("立即叫车订单，到时间切换策略，新策略执行派单后继续循环:{}", nowSchedule);
                                }
                            }
                        } else {
                            super.deleteById(nowSchedule.getOrderId());
                            logger.error("{}立即叫车订单id对应订单状态不是AUTO，已被处理", nowSchedule);
                        }
                    } else {
                        super.deleteById(nowSchedule.getOrderId());
                        logger.error("{}立即叫车订单id对应订单scene场景为空,或者scene场景不需要派单:{}", nowSchedule, travelOrder.getScene());
                    }
                } else {
                    super.deleteById(nowSchedule.getOrderId());
                    logger.error("{}立即叫车订单id对应订单不存在", nowSchedule);
                }
            }
        }
    }
}
