package com.wondernect.travel.manager.schedule;

import com.wondernect.elements.common.utils.ESDateTimeUtils;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.rdb.base.manager.BaseManager;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.travel.manager.OrderStrategyManager;
import com.wondernect.travel.manager.TravelOrderManager;
import com.wondernect.travel.model.OrderStrategy;
import com.wondernect.travel.model.TravelOrder;
import com.wondernect.travel.model.em.OrderStatus;
import com.wondernect.travel.model.em.Scene;
import com.wondernect.travel.model.schedule.OrderSchedule;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: OrderScheduleManager
 * Author: chenxun
 * Date: 2019-10-27 20:22
 * Description:
 */
@Service
public class OrderScheduleManager extends BaseManager<OrderSchedule, String> {

    private static final Logger logger = LoggerFactory.getLogger(OrderScheduleManager.class);

    @Autowired
    private TravelOrderManager travelOrderManager;

    @Autowired
    private OrderStrategyManager orderStrategyManager;

    public void process() {
        List<OrderSchedule> orderScheduleList = super.findAll(Arrays.asList(new SortData("createTime", "ASC")));
        if (CollectionUtils.isNotEmpty(orderScheduleList)) {
            for (OrderSchedule orderSchedule : orderScheduleList) {
                Long currentTime = ESDateTimeUtils.getCurrentTimestamp();
                TravelOrder travelOrder = travelOrderManager.findById(orderSchedule.getOrderId());
                if (ESObjectUtils.isNotNull(travelOrder)) {
                    if (ESObjectUtils.isNotNull(travelOrder.getScene()) && travelOrder.getScene() != Scene.BANSHOU_PRIVATE) {
                        if (travelOrder.getOrderStatus() == OrderStatus.AUTO) {
                            // 获取订单派送策略
                            OrderStrategy orderStrategy = orderStrategyManager.findNextOrderStrategy(orderSchedule.getOrderStrategyId());
                            if (ESObjectUtils.isNull(orderStrategy)) {
                                // 找不到自动派单时指定的对应策略
                                orderStrategy = orderStrategyManager.findNextOrderStrategy(null);
                                if (ESObjectUtils.isNull(orderStrategy)) {
                                    // logger.info("预约订单，没有派单策略,继续循环:{}", orderSchedule);
                                } else {
                                    travelOrderManager.sendAutoOrder(travelOrder, orderStrategy);
                                    orderSchedule.setOrderStrategyId(orderStrategy.getId());
                                    orderSchedule.setLastAddTime(currentTime);
                                    super.save(orderSchedule);
                                    // logger.info("预约订单，执行派单后，设置派单策略,继续循环:{}", orderSchedule);
                                }
                            } else {
                                Long changeTime = orderStrategy.getAutoTime() * 1000 + orderSchedule.getLastAddTime();
                                if (changeTime > currentTime) {
                                    travelOrderManager.sendAutoOrder(travelOrder, orderStrategy);
                                    // logger.info("预约订单，还没到策略切换时间,执行派单后继续循环:{}", orderSchedule);
                                } else {
                                    OrderStrategy orderStrategyNext = orderStrategyManager.findNextOrderStrategy(orderSchedule.getOrderStrategyId());
                                    travelOrderManager.sendAutoOrder(travelOrder, orderStrategyNext);
                                    orderSchedule.setOrderStrategyId(orderStrategyNext.getId());
                                    orderSchedule.setLastAddTime(currentTime);
                                    super.save(orderSchedule);
                                    // logger.info("预约订单，到时间切换策略，新策略执行派单后继续循环:{}", orderSchedule);
                                }
                            }
                        } else {
                            super.deleteById(orderSchedule.getOrderId());
                            logger.error("{}预约订单id对应订单状态不是AUTO，已被处理", orderSchedule);
                        }
                    } else {
                        super.deleteById(orderSchedule.getOrderId());
                        logger.error("{}预约订单id对应订单scene场景为空,或者scene场景不需要派单:{}", orderSchedule, travelOrder.getScene());
                    }
                } else {
                    super.deleteById(orderSchedule.getOrderId());
                    logger.error("{}预约订单id对应订单不存在", orderSchedule);
                }
            }
        }
    }
}
