package com.wondernect.travel.manager.schedule;

import com.wondernect.elements.common.utils.ESDateTimeUtils;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.rdb.base.manager.BaseManager;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.travel.manager.OrderStrategyManager;
import com.wondernect.travel.manager.TravelOrderManager;
import com.wondernect.travel.model.OrderStrategy;
import com.wondernect.travel.model.TravelOrder;
import com.wondernect.travel.model.em.OrderStatus;
import com.wondernect.travel.model.em.Scene;
import com.wondernect.travel.model.schedule.*;
import com.wondernect.travel.task.NowScheduleTask;
import com.wondernect.travel.task.OrderScheduleTask;
import com.wondernect.travel.task.UrgentScheduleTask;
import com.wondernect.travel.task.WarningScheduleTask;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: AutoScheduleManager
 * Author: chenxun
 * Date: 2019-10-27 20:21
 * Description:
 */
@Service
public class AutoScheduleManager extends BaseManager<AutoSchedule, String> {

    private static final Logger logger = LoggerFactory.getLogger(AutoScheduleManager.class);

    @Autowired
    private TravelOrderManager travelOrderManager;

    @Autowired
    private OrderStrategyManager orderStrategyManager;

    @Autowired
    private NowScheduleTask nowScheduleTask;

    @Autowired
    private OrderScheduleTask orderScheduleTask;

    @Autowired
    private UrgentScheduleTask urgentScheduleTask;

    @Autowired
    private WarningScheduleTask warningScheduleTask;

    public void process() {
        List<AutoSchedule> autoScheduleList = super.findAll(Arrays.asList(new SortData("createTime", "ASC")));
        if (CollectionUtils.isNotEmpty(autoScheduleList)) {
            for (AutoSchedule autoSchedule : autoScheduleList) {
                Long currentTime = ESDateTimeUtils.getCurrentTimestamp();
                TravelOrder travelOrder = travelOrderManager.findById(autoSchedule.getOrderId());
                if (ESObjectUtils.isNotNull(travelOrder)) {
                    if (ESObjectUtils.isNotNull(travelOrder.getScene()) && travelOrder.getScene() != Scene.BANSHOU_PRIVATE) {
                        if (travelOrder.getOrderStatus() == OrderStatus.WAIT_ACCEPT) {
                            // 获取订单派送策略
                            OrderStrategy orderStrategy = orderStrategyManager.findNextOrderStrategy(autoSchedule.getOrderStrategyId());
                            if (ESObjectUtils.isNull(orderStrategy)) {
                                autoSchedule.setLastAddTime(currentTime);
                                super.save(autoSchedule);
                                // logger.info("自动派单订单，没有派单策略,继续循环:{}", autoSchedule);
                            } else {
                                switch (travelOrder.getCommonScene()) {
                                    case NOW:
                                    {
                                        travelOrder.setOrderStatus(OrderStatus.AUTO);
                                        travelOrder.setAutoDispatchTime(currentTime);
                                        travelOrderManager.save(travelOrder);
                                        travelOrderManager.sendAutoOrder(travelOrder, orderStrategy);
                                        nowScheduleTask.add(new NowSchedule(autoSchedule.getOrderId(), orderStrategy.getId(), currentTime));
                                        warningScheduleTask.add(new WarningSchedule(autoSchedule.getOrderId()));
                                        urgentScheduleTask.add(new UrgentSchedule(autoSchedule.getOrderId()));
                                        super.deleteById(autoSchedule.getOrderId());
                                        // logger.info("自动派单订单，到自动派单时间，开始派立即叫车单:{}", autoSchedule);
                                        break;
                                    }
                                    case ORDER:
                                    {
                                        // 检查是否到达触发派单时间
                                        if (travelOrder.getAutoDispatchTime() <= currentTime) {
                                            travelOrder.setOrderStatus(OrderStatus.AUTO);
                                            travelOrder.setAutoDispatchTime(currentTime);
                                            travelOrderManager.save(travelOrder);
                                            travelOrderManager.sendAutoOrder(travelOrder, orderStrategy);
                                            orderScheduleTask.add(new OrderSchedule(autoSchedule.getOrderId(), orderStrategy.getId(), currentTime));
                                            warningScheduleTask.add(new WarningSchedule(autoSchedule.getOrderId()));
                                            urgentScheduleTask.add(new UrgentSchedule(autoSchedule.getOrderId()));
                                            super.deleteById(autoSchedule.getOrderId());
                                            // logger.info("自动派单订单，到自动派单时间，开始预约叫车派单:{}", autoSchedule);
                                        } else {
                                            // logger.info("自动派单订单，未到自动派单时间,继续循环:{}", autoSchedule);
                                        }
                                        break;
                                    }
                                }
                            }
                        } else {
                            super.deleteById(autoSchedule.getOrderId());
                            logger.error("{}自动派单id对应订单状态不是WAIT_ACCEPT，已被处理", autoSchedule);
                        }
                    } else {
                        super.deleteById(autoSchedule.getOrderId());
                        logger.error("{}自动派单id对应订单scene场景为空,或者scene场景不需要派单:{}", autoSchedule, travelOrder.getScene());
                    }
                } else {
                    super.deleteById(autoSchedule.getOrderId());
                    logger.error("{}自动派单id对应订单不存在", autoSchedule);
                }
            }
        }
    }
}
