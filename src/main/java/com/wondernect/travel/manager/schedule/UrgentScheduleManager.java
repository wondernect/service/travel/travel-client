package com.wondernect.travel.manager.schedule;

import com.wondernect.elements.common.utils.ESDateTimeUtils;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.rdb.base.manager.BaseManager;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.travel.manager.TravelOrderManager;
import com.wondernect.travel.model.TravelOrder;
import com.wondernect.travel.model.em.OrderStatus;
import com.wondernect.travel.model.em.Scene;
import com.wondernect.travel.model.em.WarningStatus;
import com.wondernect.travel.model.schedule.UrgentSchedule;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: UrgentScheduleManager
 * Author: chenxun
 * Date: 2019-10-27 20:23
 * Description:
 */
@Service
public class UrgentScheduleManager extends BaseManager<UrgentSchedule, String> {

    private static final Logger logger = LoggerFactory.getLogger(UrgentScheduleManager.class);

    @Autowired
    private TravelOrderManager travelOrderManager;

    public void process() {
        List<UrgentSchedule> urgentScheduleList = super.findAll(Arrays.asList(new SortData("createTime", "ASC")));
        if (CollectionUtils.isNotEmpty(urgentScheduleList)) {
            for (UrgentSchedule urgentSchedule : urgentScheduleList) {
                Long currentTime = ESDateTimeUtils.getCurrentTimestamp();
                TravelOrder travelOrder = travelOrderManager.findById(urgentSchedule.getOrderId());
                if (ESObjectUtils.isNotNull(travelOrder)) {
                    if (ESObjectUtils.isNotNull(travelOrder.getScene()) && travelOrder.getScene() != Scene.BANSHOU_PRIVATE) {
                        long useTime = currentTime - travelOrder.getAutoDispatchTime();
                        if (travelOrder.getOrderStatus() == OrderStatus.AUTO) {
                            if (useTime > 600000) {
                                // logger.info("{}紧急订单已到告警时间，设置紧急状态", urgentSchedule);
                                travelOrder.setWarningStatus(WarningStatus.URGENT);
                                travelOrderManager.save(travelOrder);
                                super.deleteById(urgentSchedule.getOrderId());
                            } else {
                                // logger.info("{}紧急订单没到告警时间，继续循环", urgentSchedule);
                            }
                        } else {
                            logger.error("{}紧急订单id对应订单状态不是AUTO，已被处理", urgentSchedule);
                            super.deleteById(urgentSchedule.getOrderId());
                        }
                    } else {
                        logger.error("{}紧急订单id对应订单scene场景为空,或者scene场景不需要派单:{}", urgentSchedule, travelOrder.getScene());
                        super.deleteById(urgentSchedule.getOrderId());
                    }
                } else {
                    logger.error("{}紧急订单id对应订单不存在", urgentSchedule);
                    super.deleteById(urgentSchedule.getOrderId());
                }
            }
        }
    }
}
