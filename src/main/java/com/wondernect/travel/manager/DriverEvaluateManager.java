package com.wondernect.travel.manager;

import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.rdb.base.manager.BaseStringManager;
import com.wondernect.elements.rdb.criteria.Criteria;
import com.wondernect.elements.rdb.criteria.Restrictions;
import com.wondernect.elements.rdb.request.PageRequestData;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.travel.model.DriverEvaluate;
import com.wondernect.travel.repository.DriverEvaluateDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: DriverEvaluateManager
 * Author: chenxun
 * Date: 2019/5/11 16:58
 * Description:
 */
@Service
public class DriverEvaluateManager extends BaseStringManager<DriverEvaluate> {

    @Autowired
    private DriverEvaluateDao driverEvaluateDao;

    public DriverEvaluate findTopByUserIdAndOrderId(String userId, String orderId) {
        return driverEvaluateDao.findTopByUserIdAndOrderId(userId, orderId);
    }

    public PageResponseData<DriverEvaluate> findAllByDriverUserId(String driverUserId, Integer type, Long start, Long end, PageRequestData pageRequestData) {
        Criteria<DriverEvaluate> driverEvaluateCriteria = new Criteria<>();
        driverEvaluateCriteria.add(Restrictions.eq("driverUserId", driverUserId));
        if (ESObjectUtils.isNotNull(type)) {
            switch (type) {
                case 1:
                {
                    driverEvaluateCriteria.add(Restrictions.gte("orderCreateTime", start));
                    driverEvaluateCriteria.add(Restrictions.lte("orderCreateTime", end));
                    break;
                }
                case 2:
                {
                    driverEvaluateCriteria.add(Restrictions.gte("orderExcuteTime", start));
                    driverEvaluateCriteria.add(Restrictions.lte("orderExcuteTime", end));
                    break;
                }
            }
        }
        return super.findAll(driverEvaluateCriteria, pageRequestData);
    }
}
