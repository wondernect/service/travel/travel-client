package com.wondernect.travel.manager;

import com.wondernect.elements.rdb.base.manager.BaseStringManager;
import com.wondernect.elements.rdb.criteria.Criteria;
import com.wondernect.elements.rdb.criteria.Restrictions;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.travel.model.UserShop;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: UserShopManager
 * Author: chenxun
 * Date: 2019/5/11 10:43
 * Description:
 */
@Service
public class UserShopManager extends BaseStringManager<UserShop> {

    public List<UserShop> findAllByUserId(String userId, List<SortData> sortDataList) {
        Criteria<UserShop> userShopCriteria = new Criteria<>();
        userShopCriteria.add(Restrictions.eq("userId", userId));
        return super.findAll(userShopCriteria, sortDataList);
    }

    public List<UserShop> findAllByShopId(String shopId, List<SortData> sortDataList) {
        Criteria<UserShop> userShopCriteria = new Criteria<>();
        userShopCriteria.add(Restrictions.eq("shopId", shopId));
        return super.findAll(userShopCriteria, sortDataList);
    }
}
