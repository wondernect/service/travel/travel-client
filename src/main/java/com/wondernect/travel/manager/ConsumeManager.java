package com.wondernect.travel.manager;

import com.wondernect.elements.common.exception.BusinessException;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.common.utils.ESStringUtils;
import com.wondernect.elements.rdb.base.manager.BaseStringManager;
import com.wondernect.elements.rdb.criteria.Criteria;
import com.wondernect.elements.rdb.criteria.Restrictions;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.travel.model.Consume;
import com.wondernect.travel.model.em.Scene;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: ConsumeManager
 * Author: chenxun
 * Date: 2019/5/11 16:55
 * Description:
 */
@Service
public class ConsumeManager extends BaseStringManager<Consume> {

    public Consume save(Consume consume) {
        Consume consumeSave;
        if (ESStringUtils.isNotBlank(consume.getId())) {
            consumeSave = super.findById(consume.getId());
            if (ESObjectUtils.isNull(consumeSave)) {
                throw new BusinessException("用车服务不存在");
            }
            consumeSave.setCityId(consume.getCityId());
            consumeSave.setScene(consume.getScene());
            consumeSave.setCommonScene(consume.getCommonScene());
            consumeSave.setTaocan(consume.getTaocan());
            consumeSave.setTitle(consume.getTitle());
            consumeSave.setShowPrice(consume.getShowPrice());
            consumeSave.setDescription(consume.getDescription());
            consumeSave.setContain(consume.getContain());
            consumeSave.setUncontain(consume.getUncontain());
        } else {
            consumeSave = new Consume(
                    consume.getCityId(),
                    consume.getScene(),
                    consume.getCommonScene(),
                    consume.getTaocan(),
                    consume.getTitle(),
                    consume.getShowPrice(),
                    consume.getDescription(),
                    consume.getContain(),
                    consume.getUncontain()
            );
        }
        return super.save(consumeSave);
    }

    public List<Consume> findAllByScene(String cityId, Scene scene, String taocan, List<SortData> sortDataList) {
        Criteria<Consume> consumeCriteria = new Criteria<>();
        consumeCriteria.add(Restrictions.eq("cityId", cityId));
        consumeCriteria.add(Restrictions.eq("scene", scene));
        consumeCriteria.add(Restrictions.eq("taocan", taocan));
        return super.findAll(consumeCriteria, sortDataList);
    }
}
