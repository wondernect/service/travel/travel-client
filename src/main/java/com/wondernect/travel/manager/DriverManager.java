package com.wondernect.travel.manager;

import com.wondernect.elements.rdb.base.manager.BaseStringManager;
import com.wondernect.elements.rdb.criteria.Criteria;
import com.wondernect.elements.rdb.criteria.Restrictions;
import com.wondernect.elements.rdb.criteria.em.LogicalOperator;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.travel.model.Driver;
import com.wondernect.travel.repository.DriverDao;
import org.hibernate.criterion.MatchMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: DriverManager
 * Author: chenxun
 * Date: 2019/5/11 10:41
 * Description:
 */
@Service
public class DriverManager extends BaseStringManager<Driver> {

    @Autowired
    private DriverDao driverDao;

    public Driver findByUserId(String userId) {
        return driverDao.findByUserId(userId);
    }

    public List<Driver> driverList(String value, List<SortData> sortDataList) {
        Criteria<Driver> driverCriteria = new Criteria<>(LogicalOperator.OR);
        driverCriteria.add(Restrictions.like("name", value, MatchMode.ANYWHERE));
        driverCriteria.add(Restrictions.like("mobile", value, MatchMode.ANYWHERE));
        return super.findAll(driverCriteria, sortDataList);
    }
}
