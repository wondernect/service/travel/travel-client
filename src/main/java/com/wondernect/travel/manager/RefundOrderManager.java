package com.wondernect.travel.manager;

import com.wondernect.elements.common.exception.BusinessException;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.common.utils.ESStringUtils;
import com.wondernect.elements.rdb.base.manager.BaseStringManager;
import com.wondernect.elements.rdb.criteria.Criteria;
import com.wondernect.elements.rdb.criteria.Restrictions;
import com.wondernect.elements.rdb.request.PageRequestData;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.travel.model.RefundOrder;
import com.wondernect.travel.repository.RefundOrderDao;
import org.hibernate.criterion.MatchMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: TravelOrderManager
 * Author: chenxun
 * Date: 2019/5/11 17:00
 * Description:
 */
@Service
public class RefundOrderManager extends BaseStringManager<RefundOrder> {

    @Autowired
    private RefundOrderDao refundOrderDao;

    public RefundOrder save(RefundOrder refundOrder) {
        RefundOrder refundOrderSave;
        if (ESStringUtils.isNotBlank(refundOrder.getId())) {
            refundOrderSave = super.findById(refundOrder.getId());
            if (ESObjectUtils.isNull(refundOrderSave)) {
                throw new BusinessException("退款订单不存在");
            }
            refundOrderSave.setHasRefund(refundOrder.getHasRefund());
        } else {
            refundOrderSave = findByOrderId(refundOrder.getOrderId());
            if (ESObjectUtils.isNotNull(refundOrderSave)) {
                throw new BusinessException("对应订单退款已存在");
            }
            refundOrderSave = new RefundOrder(
                    refundOrder.getOrderId(),
                    refundOrder.getUserId(),
                    refundOrder.getUsername(),
                    refundOrder.getMobile(),
                    refundOrder.getWechat(),
                    refundOrder.getPrice(),
                    refundOrder.getRefund(),
                    refundOrder.getBeforeTime(),
                    refundOrder.getRefundPercent(),
                    refundOrder.getFee(),
                    refundOrder.getHasRefund()
            );
        }
        return super.save(refundOrderSave);
    }

    public RefundOrder findByOrderId(String orderId) {
        return refundOrderDao.findByOrderId(orderId);
    }

    public PageResponseData<RefundOrder> page(String value, Boolean hasRefund, PageRequestData pageRequestData) {
        Criteria<RefundOrder> refundOrderCriteria = new Criteria<>();
        refundOrderCriteria.add(Restrictions.eq("hasRefund", hasRefund));
        refundOrderCriteria.add(
                Restrictions.or(
                        Restrictions.like("username", value, MatchMode.ANYWHERE),
                        Restrictions.like("mobile", value, MatchMode.ANYWHERE)
                )
        );
        return super.findAll(refundOrderCriteria, pageRequestData);
    }
}
