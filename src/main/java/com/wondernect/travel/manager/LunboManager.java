package com.wondernect.travel.manager;

import com.wondernect.elements.common.exception.BusinessException;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.common.utils.ESStringUtils;
import com.wondernect.elements.rdb.base.manager.BaseSortManager;
import com.wondernect.travel.model.Lunbo;
import org.springframework.stereotype.Service;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: LunboManager
 * Author: chenxun
 * Date: 2019-10-04 08:07
 * Description: 轮播图
 */
@Service
public class LunboManager extends BaseSortManager<Lunbo, String> {

    public Lunbo save(Lunbo lunbo) {
        Lunbo lunboSave;
        if (ESStringUtils.isNotBlank(lunbo.getId())) {
            lunboSave = super.findById(lunbo.getId());
            if (ESObjectUtils.isNull(lunboSave)) {
                throw new BusinessException("轮播配置不存在");
            }
            lunboSave.setImages(lunbo.getImages());
            lunboSave.setLink(lunbo.getLink());
        } else {
            lunboSave = new Lunbo(lunbo.getImages(), lunbo.getLink());
        }
        if (ESObjectUtils.isNotNull(lunbo.getWeight())) {
            lunboSave.setWeight(lunbo.getWeight());
        }
        return super.save(lunboSave);
    }
}
