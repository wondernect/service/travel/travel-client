package com.wondernect.travel.constant;

/**
 * Copyright (C), 2017-2020, wondernect.com
 * FileName: CarConfigConstant
 * Author: chenxun
 * Date: 2020/1/5 16:11
 * Description: 车型配置常量
 */
public interface CarConfigConstant {

    String ALL_CAR_CONFIG = "ALL"; // 所有座位或车型
}
