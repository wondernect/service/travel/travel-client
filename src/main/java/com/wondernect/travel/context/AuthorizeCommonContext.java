package com.wondernect.travel.context;

import com.wondernect.elements.authorize.context.impl.AbstractWondernectCommonContext;
import org.springframework.stereotype.Component;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: UserCommonContext
 * Author: chenxun
 * Date: 2019/3/28 17:50
 * Description: user common context
 */
@Component
public class AuthorizeCommonContext extends AbstractWondernectCommonContext {

}
