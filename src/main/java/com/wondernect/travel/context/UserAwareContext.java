package com.wondernect.travel.context;

import com.wondernect.elements.authorize.context.AuthorizeData;
import com.wondernect.elements.authorize.context.WondernectCommonContext;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.rdb.context.AuditorAwareContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: SessionAuditorAwareContext
 * Author: chenxun
 * Date: 2019/4/9 16:21
 * Description:
 */
@Component
public class UserAwareContext implements AuditorAwareContext {

    @Autowired
    private WondernectCommonContext wondernectCommonContext;

    @Override
    public String getCurrentUser() {
        AuthorizeData authorizeData = wondernectCommonContext.getAuthorizeData();
        if (ESObjectUtils.isNull(authorizeData)) {
            return null;
        }
        return wondernectCommonContext.getAuthorizeData().getUserId();
    }
}
