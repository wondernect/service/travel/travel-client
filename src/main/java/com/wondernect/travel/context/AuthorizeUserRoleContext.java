package com.wondernect.travel.context;

import com.wondernect.elements.authorize.context.impl.AbstractWondernectAuthorizeContext;
import com.wondernect.services.rbac.service.RBACOpenService;
import com.wondernect.services.session.service.impl.DefaultCodeSessionService;
import com.wondernect.services.session.service.impl.DefaultTokenSessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: UserAuthorizeContext
 * Author: chenxun
 * Date: 2019/4/4 11:40
 * Description:
 */
@Component
public class AuthorizeUserRoleContext extends AbstractWondernectAuthorizeContext {

    @Autowired
    private DefaultCodeSessionService codeSessionService;

    @Autowired
    private DefaultTokenSessionService tokenSessionService;

    @Autowired
    private RBACOpenService rbacOpenService;

    @Override
    public String authorizeExpiresToken(String authorizeToken) {
        return codeSessionService.authCodeSession(authorizeToken).getCreateUser();
    }

    @Override
    public String authorizeUnlimitedToken(String authorizeToken) {
        return tokenSessionService.authTokenSession(authorizeToken).getCreateUser();
    }

    @Override
    public String getUserRole(String userId) {
        return rbacOpenService.getUserRole(userId);
    }

    @Override
    public List<String> getCustomValidUserRoles() {
        return new ArrayList<>(); // 自定义合法的用户角色
    }

    @Override
    public List<String> getRequestValidUserRoles(String requestUrl, String requestMethod) {
        return rbacOpenService.getValidUserRoles(requestUrl, requestMethod);
    }
}
