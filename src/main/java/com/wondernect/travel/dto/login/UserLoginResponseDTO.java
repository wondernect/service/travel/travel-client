package com.wondernect.travel.dto.login;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.services.rbac.model.Role;
import com.wondernect.services.session.model.TokenSession;
import com.wondernect.services.user.model.User;
import com.wondernect.travel.model.Shop;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: UserLoginResponseDTO
 * Author: chenxun
 * Date: 2019/3/17 10:22
 * Description: user login response dto
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "user login response dto")
public class UserLoginResponseDTO implements Serializable {

    private static final long serialVersionUID = -3600048295677012606L;

    @JsonProperty("user")
    @ApiModelProperty(notes = "用户")
    private User user;

    @JsonProperty("roles")
    @ApiModelProperty(notes = "角色")
    private List<Role> roles;

    @JsonProperty("token_session")
    @ApiModelProperty(notes = "访问令牌")
    private TokenSession tokenSession;
}
