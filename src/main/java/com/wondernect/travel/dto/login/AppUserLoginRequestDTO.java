package com.wondernect.travel.dto.login;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.services.user.model.em.AppType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: UserLoginRequestDTO
 * Author: chenxun
 * Date: 2019/3/16 19:03
 * Description: user login request dto
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "user login request dto")
public class AppUserLoginRequestDTO implements Serializable {

    private static final long serialVersionUID = 376775934932473508L;

    @NotBlank(message = "用户登录密码不能为空")
    @JsonProperty("app_user_id")
    @ApiModelProperty(notes = "第三方用户id")
    private String appUserId;
}
