package com.wondernect.travel.dto.login;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: UserLoginRequestDTO
 * Author: chenxun
 * Date: 2019/3/16 19:03
 * Description: user login request dto
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "user login request dto")
public class CaptchaLoginRequestDTO implements Serializable {

    private static final long serialVersionUID = 376775934932473508L;

    @NotBlank(message = "验证码id不能为空")
    @JsonProperty("captcha_session_id")
    @ApiModelProperty(notes = "手机验证码会话id")
    private String captchaSessionId;

    @NotBlank(message = "验证码不能为空")
    @JsonProperty("captcha")
    @ApiModelProperty(notes = "手机验证码")
    private String captcha;

    @NotBlank(message = "用户登录名不能为空")
    @JsonProperty("username")
    @ApiModelProperty(notes = "用户名")
    private String username;
}
