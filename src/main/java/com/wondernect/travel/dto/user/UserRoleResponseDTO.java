package com.wondernect.travel.dto.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.services.rbac.model.Role;
import com.wondernect.services.user.model.User;
import com.wondernect.travel.model.Driver;
import com.wondernect.travel.model.Shop;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: UserAllResponseDTO
 * Author: chenxun
 * Date: 2019/4/10 11:56
 * Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "user role response dto")
public class UserRoleResponseDTO implements Serializable {

    private static final long serialVersionUID = 6726512770408653760L;

    @JsonProperty("user")
    @ApiModelProperty(notes = "用户")
    private User user;

    @JsonProperty("roles")
    @ApiModelProperty(notes = "角色列表")
    private List<Role> roles;
}
