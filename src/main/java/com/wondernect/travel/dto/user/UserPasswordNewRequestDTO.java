package com.wondernect.travel.dto.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: UserPasswordNewRequestDTO
 * Author: chenxun
 * Date: 2019/3/16 19:08
 * Description: user password new regist dto
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "user password new request dto")
public class UserPasswordNewRequestDTO implements Serializable {

    private static final long serialVersionUID = 2353943014717660579L;

    @NotBlank(message = "手机号码不能为空")
    @JsonProperty("username")
    @ApiModelProperty(notes = "手机号码")
    private String username;

    @NotBlank(message = "密码不能为空")
    @JsonProperty("password")
    @ApiModelProperty(notes = "密码")
    private String password;

    @NotBlank(message = "验证码id不能为空")
    @JsonProperty("captcha_session_id")
    @ApiModelProperty(notes = "手机验证码会话id")
    private String captchaSessionId;

    @NotBlank(message = "验证码不能为空")
    @JsonProperty("captcha")
    @ApiModelProperty(notes = "手机验证码")
    private String captcha;
}
