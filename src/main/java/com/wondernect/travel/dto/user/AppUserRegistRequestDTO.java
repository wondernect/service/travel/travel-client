package com.wondernect.travel.dto.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: AppUserRegistRequestDTO
 * Author: chenxun
 * Date: 2019/3/16 18:54
 * Description: user regist request dto
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "app user regist request dto")
public class AppUserRegistRequestDTO implements Serializable {

    private static final long serialVersionUID = 1536276648349947767L;

    @NotBlank(message = "第三方注册用户id不能为空")
    @JsonProperty("app_user_id")
    @ApiModelProperty(notes = "第三方注册用户id")
    private String appUserId;

    @NotBlank(message = "第三方注册用户name不能为空")
    @JsonProperty("app_user_name")
    @ApiModelProperty(notes = "第三方注册用户name")
    private String appUserName;

    @JsonProperty("app_user_avatar")
    @ApiModelProperty(notes = "第三方注册用户头像")
    private String appUserAvatar;

    @NotBlank(message = "手机号码不能为空")
    @JsonProperty("username")
    @ApiModelProperty(notes = "手机号码")
    private String username;
}
