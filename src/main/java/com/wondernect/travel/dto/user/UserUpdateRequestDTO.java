package com.wondernect.travel.dto.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.services.user.model.em.Gender;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: UserUpdateRequestDTO
 * Author: chenxun
 * Date: 2019/3/18 11:32
 * Description: user update request dto
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "user update request dto")
public class UserUpdateRequestDTO implements Serializable {

    private static final long serialVersionUID = 6980764356171542330L;

    @NotBlank(message = "用户id不能为空")
    @JsonProperty("user_id")
    @ApiModelProperty(notes = "用户id")
    private String userId;

    @JsonProperty("name")
    @ApiModelProperty(notes = "姓名")
    private String name;

    @JsonProperty("nick_name")
    @ApiModelProperty(notes = "昵称")
    private String nickName;

    @JsonProperty("avatar")
    @ApiModelProperty(notes = "头像")
    private String avatar;

    @JsonProperty("remark")
    @ApiModelProperty(notes = "个性签名")
    private String remark;

    @JsonProperty("mobile")
    @ApiModelProperty(notes = "手机号码")
    private String mobile;

    @JsonProperty("password")
    @ApiModelProperty(notes = "用户密码")
    private String password;

    @JsonProperty("gender")
    @ApiModelProperty(notes = "性别", allowableValues = "MALE,FEMALE,UNKNOWN")
    private Gender gender;

    @JsonProperty("location")
    @ApiModelProperty(notes = "常住地")
    private String location;
}
