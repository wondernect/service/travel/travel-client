package com.wondernect.travel.dto.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: UserCreateRequestDTO
 * Author: chenxun
 * Date: 2019/3/16 18:54
 * Description: user create request dto
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "user create request dto")
public class UserRoleCreateRequestDTO implements Serializable {

    private static final long serialVersionUID = 1536276648349947767L;

    @NotBlank(message = "姓名不能为空")
    @JsonProperty("name")
    @ApiModelProperty(notes = "姓名")
    private String name;

    @JsonProperty("nick_name")
    @ApiModelProperty(notes = "昵称")
    private String nickName;

    @NotBlank(message = "手机号码不能为空")
    @JsonProperty("mobile")
    @ApiModelProperty(notes = "手机号码")
    private String mobile;

    @NotBlank(message = "用户密码不能为空")
    @JsonProperty("password")
    @ApiModelProperty(notes = "用户密码")
    private String password;

    @NotBlank(message = "角色不能为空")
    @JsonProperty("role_id")
    @ApiModelProperty(notes = "角色id")
    private String roleId;

    @JsonProperty("shop_id")
    @ApiModelProperty(notes = "商家id(创建商家员工时不能为空)")
    private String shopId;

    @JsonProperty("shop_name")
    @ApiModelProperty(notes = "商家名称(创建商家时不能为空)")
    private String shopName;

    @JsonProperty("driver_evaluate")
    @ApiModelProperty(notes = "司机评分(创建司机时不能为空)")
    private int driverEvaluate;
}
