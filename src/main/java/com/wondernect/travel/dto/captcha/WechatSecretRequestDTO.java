package com.wondernect.travel.dto.captcha;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: UserLoginRequestDTO
 * Author: chenxun
 * Date: 2019/3/16 19:03
 * Description: user login request dto
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "微信加密信息请求对象")
public class WechatSecretRequestDTO implements Serializable {

    private static final long serialVersionUID = 376775934932473508L;

    @NotBlank(message = "用户加密信息不能为空")
    @JsonProperty("encrypted_data")
    @ApiModelProperty(notes = "用户信息")
    private String encryptedData;

    @NotBlank(message = "会话session_key不能为空")
    @JsonProperty("session_key")
    @ApiModelProperty(notes = "session_key")
    private String sessionKey;

    @NotBlank(message = "加密算法的初始向量不能为空")
    @JsonProperty("iv")
    @ApiModelProperty(notes = "加密算法的初始向量")
    private String iv;
}
