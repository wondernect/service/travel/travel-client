package com.wondernect.travel.dto.captcha;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: CaptchaRequestDTO
 * Author: chenxun
 * Date: 2019/4/7 10:08
 * Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "captcha request dto")
public class CaptchaRequestDTO implements Serializable {

    private static final long serialVersionUID = 6837783571929118297L;

    @JsonProperty("captcha_type")
    @ApiModelProperty(notes = "验证码类型", allowableValues = "MOBILE")
    private String captchaType;

    @NotBlank(message = "手机号码不能为空")
    @JsonProperty("username")
    @ApiModelProperty(notes = "手机号码")
    private String username;

    @JsonProperty("description")
    @ApiModelProperty(notes = "申请验证码原因")
    private String description;

    @JsonProperty("expires")
    @ApiModelProperty(notes = "验证码过期时间")
    private Long expires = 0L;
}
