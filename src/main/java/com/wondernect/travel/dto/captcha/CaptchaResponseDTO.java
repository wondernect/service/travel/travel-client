package com.wondernect.travel.dto.captcha;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: CaptchaResponseDTO
 * Author: chenxun
 * Date: 2019/3/17 10:12
 * Description: captcha session response dto
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "captcha session response dto")
public class CaptchaResponseDTO implements Serializable {

    private static final long serialVersionUID = 5003598586824211910L;

    @JsonProperty("username")
    @ApiModelProperty(notes = "目标手机号码")
    private String username;

    @JsonProperty("captcha_session_id")
    @ApiModelProperty(notes = "手机验证码会话id")
    private String captchaSessionId;

    @JsonProperty("captcha")
    @ApiModelProperty(notes = "验证码")
    private String captcha;

    @JsonProperty("expires")
    @ApiModelProperty(notes = "验证码过期时间(默认60s)")
    private Long expires;

    @JsonProperty("description")
    @ApiModelProperty(notes = "验证码使用描述")
    private String description;
}
