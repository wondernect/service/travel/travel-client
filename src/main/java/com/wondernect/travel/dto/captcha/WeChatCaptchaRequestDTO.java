package com.wondernect.travel.dto.captcha;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: CaptchaRequestDTO
 * Author: chenxun
 * Date: 2019/4/7 10:08
 * Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "captcha request dto")
public class WeChatCaptchaRequestDTO implements Serializable {

    private static final long serialVersionUID = 6837783571929118297L;

    @NotBlank(message = "微信授权code不能为空")
    @JsonProperty("code")
    @ApiModelProperty(notes = "微信授权code")
    private String code;
}
