package com.wondernect.travel.dto.base;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.travel.model.PrivateConsume;
import com.wondernect.travel.model.PrivateConsumeRoad;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: CaptchaRequestDTO
 * Author: chenxun
 * Date: 2019/4/7 10:08
 * Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "包车服务响应对象")
public class WeChatPrivateConsumeResponseDTO {

    @JsonProperty("private_consume")
    @ApiModelProperty(notes = "包车服务")
    private PrivateConsume privateConsume;

    @JsonProperty("car_levels")
    @ApiModelProperty(notes = "分级收费")
    private List<PrivateConsumeCarLevelResponseDTO> carLevels;

    @JsonProperty("roads")
    @ApiModelProperty(notes = "路线")
    private List<PrivateConsumeRoad> roads;
}
