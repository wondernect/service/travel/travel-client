package com.wondernect.travel.dto.base;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.travel.model.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: CaptchaRequestDTO
 * Author: chenxun
 * Date: 2019/4/7 10:08
 * Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "包车服务请求")
public class PrivateConsumeRequestDTO {

    @NotNull(message = "包车服务不能为空")
    @JsonProperty("private_consume")
    @ApiModelProperty(notes = "包车服务")
    private PrivateConsume privateConsume;

    @JsonProperty("car_levels")
    @ApiModelProperty(notes = "分级收费")
    private List<PrivateConsumeCarLevel> carLevels;

    @JsonProperty("roads")
    @ApiModelProperty(notes = "路线")
    private List<PrivateConsumeRoad> roads;
}
