package com.wondernect.travel.dto.base;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.travel.model.Consume;
import com.wondernect.travel.model.ConsumeCarLevel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: CaptchaRequestDTO
 * Author: chenxun
 * Date: 2019/4/7 10:08
 * Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "consume request dto")
public class ConsumeRequestDTO {

    @NotNull(message = "用车服务不能为空")
    @JsonProperty("consume")
    @ApiModelProperty(notes = "用车服务")
    private Consume consume;

    @JsonProperty("car_levels")
    @ApiModelProperty(notes = "分级收费")
    private List<ConsumeCarLevel> carLevels;
}
