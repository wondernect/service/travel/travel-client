package com.wondernect.travel.dto.base;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: CaptchaRequestDTO
 * Author: chenxun
 * Date: 2019/4/7 10:08
 * Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "driver settled request dto")
public class DriverSettledRequestDTO {

    @NotBlank(message = "司机id不能为空")
    @JsonProperty("driver_user_id")
    @ApiModelProperty(notes = "司机")
    private String driverUserId;

    @JsonProperty("start")
    @ApiModelProperty(notes = "开始时间")
    private Long start;

    @JsonProperty("end")
    @ApiModelProperty(notes = "结束时间")
    private Long end;
}
