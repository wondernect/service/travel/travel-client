package com.wondernect.travel.dto.base;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.travel.model.Consume;
import com.wondernect.travel.model.ConsumeCarLevel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: CaptchaRequestDTO
 * Author: chenxun
 * Date: 2019/4/7 10:08
 * Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "refund response dto")
public class RefundResponseDTO {

    @JsonProperty("order_id")
    @ApiModelProperty(notes = "订单id")
    private String orderId;

    @JsonProperty("before_time")
    @ApiModelProperty(notes = "提前多长时间")
    private Double beforeTime;

    @JsonProperty("refund_percent")
    @ApiModelProperty(notes = "退款收费百分比")
    private int refundPercent;

    @JsonProperty("fee")
    @ApiModelProperty(notes = "退款手续费")
    private Double fee;
}
