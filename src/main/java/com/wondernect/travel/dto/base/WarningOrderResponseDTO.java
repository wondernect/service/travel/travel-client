package com.wondernect.travel.dto.base;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.travel.model.TravelOrder;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: DriverEvaluateResponseDTO
 * Author: chenxun
 * Date: 2019-05-12 17:12
 * Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "warning order response dto")
public class WarningOrderResponseDTO {

    @JsonProperty("travel_order_page")
    @ApiModelProperty(notes = "车辆信息")
    private PageResponseData<TravelOrder> travelOrderPage;

    @JsonProperty("total")
    @ApiModelProperty(notes = "全部总数")
    private Long total;

    @JsonProperty("warning")
    @ApiModelProperty(notes = "警告总数")
    private Long warning;

    @JsonProperty("urgent")
    @ApiModelProperty(notes = "紧急总数")
    private Long urgent;
}
