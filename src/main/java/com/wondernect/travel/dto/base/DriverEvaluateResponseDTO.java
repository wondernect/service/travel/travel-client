package com.wondernect.travel.dto.base;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.services.user.model.User;
import com.wondernect.travel.model.Driver;
import com.wondernect.travel.model.DriverEvaluate;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: DriverEvaluateResponseDTO
 * Author: chenxun
 * Date: 2019-05-12 17:12
 * Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "driver evaluate response dto")
public class DriverEvaluateResponseDTO {

    @JsonProperty("driver_evaluate")
    @ApiModelProperty(notes = "司机评价")
    private DriverEvaluate driverEvaluate;

    @JsonProperty("driver")
    @ApiModelProperty(notes = "司机信息")
    private Driver driver;

    @JsonProperty("user")
    @ApiModelProperty(notes = "司机对应用户信息")
    private User user;

    @JsonProperty("order_user")
    @ApiModelProperty(notes = "订单对应用户信息")
    private User orderUser;
}
