package com.wondernect.travel.dto.base;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.services.user.model.User;
import com.wondernect.travel.business.chexing.model.Chexing;
import com.wondernect.travel.business.city.model.City;
import com.wondernect.travel.business.zuowei.model.Zuowei;
import com.wondernect.travel.model.Car;
import com.wondernect.travel.model.Driver;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: DriverEvaluateResponseDTO
 * Author: chenxun
 * Date: 2019-05-12 17:12
 * Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "car response dto")
public class CarResponseDTO {

    @JsonProperty("car")
    @ApiModelProperty(notes = "车辆信息")
    private Car car;

    @JsonProperty("city")
    @ApiModelProperty(notes = "城市信息")
    private City city;

    @JsonProperty("zuowei")
    @ApiModelProperty(notes = "座位配置信息")
    private Zuowei zuowei;

    @JsonProperty("chexing")
    @ApiModelProperty(notes = "车型配置信息")
    private Chexing chexing;

    @JsonProperty("driver")
    @ApiModelProperty(notes = "司机信息")
    private Driver driver;

    @JsonProperty("user")
    @ApiModelProperty(notes = "司机对应用户信息")
    private User user;
}
