package com.wondernect.travel.dto.base;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.travel.model.TravelOrder;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: DriverEvaluateResponseDTO
 * Author: chenxun
 * Date: 2019-05-12 17:12
 * Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "driver settled order response dto")
public class DriverSettledOrderResponseDTO {

    @JsonProperty("travel_order_list")
    @ApiModelProperty(notes = "订单信息")
    private List<TravelOrder> travelOrderList;

    @JsonProperty("total")
    @ApiModelProperty(notes = "总金额")
    private Double total;
}
