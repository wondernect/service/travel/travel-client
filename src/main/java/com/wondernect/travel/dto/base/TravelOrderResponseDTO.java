package com.wondernect.travel.dto.base;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.services.user.model.User;
import com.wondernect.travel.business.chexing.model.Chexing;
import com.wondernect.travel.business.city.model.City;
import com.wondernect.travel.business.zuowei.model.Zuowei;
import com.wondernect.travel.model.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: DriverEvaluateResponseDTO
 * Author: chenxun
 * Date: 2019-05-12 17:12
 * Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "订单响应信息")
public class TravelOrderResponseDTO {

    @JsonProperty("order")
    @ApiModelProperty(notes = "订单信息")
    private TravelOrder travelOrder;

    @JsonProperty("consume")
    @ApiModelProperty(notes = "接送机、按天包车用户服务信息")
    private Consume consume;

    @JsonProperty("private_consume")
    @ApiModelProperty(notes = "线路包车、景点、美食、伴手礼用户服务信息")
    private PrivateConsume privateConsume;

    @JsonProperty("zuowei")
    @ApiModelProperty(notes = "座位配置信息")
    private Zuowei zuowei;

    @JsonProperty("chexing")
    @ApiModelProperty(notes = "车型配置信息")
    private Chexing chexing;
}
