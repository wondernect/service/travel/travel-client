package com.wondernect.travel.dto.base;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: CaptchaRequestDTO
 * Author: chenxun
 * Date: 2019/4/7 10:08
 * Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "price count request dto")
public class PriceCountRequestDTO {

    @NotBlank(message = "价格策略id不能为空")
    @JsonProperty("price_strategy_id")
    @ApiModelProperty(notes = "价格策略id")
    private String priceStrategyId;

    @JsonProperty("start_time")
    @ApiModelProperty(notes = "用车时间")
    private Long startTime;

    @JsonProperty("kilo")
    @ApiModelProperty(notes = "公里数")
    private Double kilo;

    @JsonProperty("time")
    @ApiModelProperty(notes = "时长")
    private Double time;
}
