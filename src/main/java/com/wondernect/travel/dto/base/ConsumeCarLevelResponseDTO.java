package com.wondernect.travel.dto.base;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.travel.business.chexing.model.Chexing;
import com.wondernect.travel.business.zuowei.model.Zuowei;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: CaptchaRequestDTO
 * Author: chenxun
 * Date: 2019/4/7 10:08
 * Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "用车服务分级收费响应对象")
public class ConsumeCarLevelResponseDTO {

    @JsonProperty("zuowei")
    @ApiModelProperty(notes = "座位")
    private Zuowei zuowei;

    @JsonProperty("chexing")
    @ApiModelProperty(notes = "车型")
    private Chexing chexing;

    @JsonProperty("price")
    @ApiModelProperty(notes = "价格/每天")
    private Double price;

    @JsonProperty("price_strategy_id")
    @ApiModelProperty(notes = "价格策略id")
    private String priceStrategyId;

    @JsonProperty("price_strategy_name")
    @ApiModelProperty(notes = "价格策略名称")
    private String priceStrategyName;

    @JsonProperty("plan_description")
    @ApiModelProperty(notes = "价格方案描述")
    private String planDescription;
}
