package com.wondernect.travel.dto.base;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.travel.model.Consume;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: CaptchaRequestDTO
 * Author: chenxun
 * Date: 2019/4/7 10:08
 * Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "wechat consume response dto")
public class WeChatConsumeResponseDTO {

    @JsonProperty("consume")
    @ApiModelProperty(notes = "用车服务")
    private Consume consume;

    @JsonProperty("car_levels")
    @ApiModelProperty(notes = "分级收费")
    private List<ConsumeCarLevelResponseDTO> carLevels;

    @JsonProperty("count")
    @ApiModelProperty(notes = "用户未完成订单数")
    private Long count;
}
