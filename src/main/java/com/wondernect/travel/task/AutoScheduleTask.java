package com.wondernect.travel.task;

import com.wondernect.elements.common.utils.ESDateTimeUtils;
import com.wondernect.travel.manager.schedule.AutoScheduleManager;
import com.wondernect.travel.model.schedule.AutoSchedule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: TrainScheduleTask
 * Author: chenxun
 * Date: 2019/10/24 19:20
 * Description:
 */
@Component
public class AutoScheduleTask {

    private static final Logger logger = LoggerFactory.getLogger(AutoScheduleTask.class);

    @Autowired
    private AutoScheduleManager autoScheduleManager;

    @Async
    public void add(AutoSchedule autoSchedule) {
        long startTime = ESDateTimeUtils.getCurrentTimestamp();
        // logger.info("AUTO订单入队任务开始执行,现在时间是:{}", ESDateTimeUtils.formatDate(startTime, "yyyy-MM-dd HH:mm:ss"));
        autoScheduleManager.save(autoSchedule);
        long endTime = ESDateTimeUtils.getCurrentTimestamp();
        // logger.info("AUTO订单入队任务执行结束,现在时间是:{},耗时:{}", ESDateTimeUtils.formatDate(endTime, "yyyy-MM-dd HH:mm:ss"), endTime - startTime);
    }

    @Scheduled(initialDelay = 1000, fixedDelay = 10000)
    public void task() {
        long startTime = ESDateTimeUtils.getCurrentTimestamp();
        // logger.info("AUTO定时器开始执行,现在时间是:{}", ESDateTimeUtils.formatDate(startTime, "yyyy-MM-dd HH:mm:ss"));
        autoScheduleManager.process();
        long endTime = ESDateTimeUtils.getCurrentTimestamp();
        // logger.info("AUTO定时器执行结束,现在时间是:{},耗时:{}", ESDateTimeUtils.formatDate(endTime, "yyyy-MM-dd HH:mm:ss"), endTime - startTime);
    }
}
