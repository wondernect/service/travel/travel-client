package com.wondernect.travel.task;

import com.wondernect.elements.common.utils.ESDateTimeUtils;
import com.wondernect.travel.manager.schedule.NowScheduleManager;
import com.wondernect.travel.model.schedule.NowSchedule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: TrainScheduleTask
 * Author: chenxun
 * Date: 2019/10/24 19:20
 * Description:
 */
@Component
public class NowScheduleTask {

    private static final Logger logger = LoggerFactory.getLogger(NowScheduleTask.class);

    @Autowired
    private NowScheduleManager nowScheduleManager;

    @Async
    public void add(NowSchedule nowSchedule) {
        long startTime = ESDateTimeUtils.getCurrentTimestamp();
        // logger.info("NOW订单入队任务开始执行,现在时间是:{}", ESDateTimeUtils.formatDate(startTime, "yyyy-MM-dd HH:mm:ss"));
        nowScheduleManager.save(nowSchedule);
        long endTime = ESDateTimeUtils.getCurrentTimestamp();
        // logger.info("NOW订单入队任务执行结束,现在时间是:{},耗时:{}", ESDateTimeUtils.formatDate(endTime, "yyyy-MM-dd HH:mm:ss"), endTime - startTime);
    }

    @Scheduled(initialDelay = 1000, fixedDelay = 10000)
    public void task() {
        long startTime = ESDateTimeUtils.getCurrentTimestamp();
        // logger.info("NOW定时器开始执行,现在时间是:{}", ESDateTimeUtils.formatDate(startTime, "yyyy-MM-dd HH:mm:ss"));
        nowScheduleManager.process();
        long endTime = ESDateTimeUtils.getCurrentTimestamp();
        // logger.info("NOW定时器执行结束,现在时间是:{},耗时:{}", ESDateTimeUtils.formatDate(endTime, "yyyy-MM-dd HH:mm:ss"), endTime - startTime);
    }
}
