package com.wondernect.travel.task;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: TaskScheduleConfig
 * Author: chenxun
 * Date: 2019/10/25 10:16
 * Description:
 */
@Configuration
@EnableScheduling
public class TaskScheduleConfig implements SchedulingConfigurer {

    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.setScheduler(taskExecutor());
    }

    //spring容器关闭时,关掉定时任务
    @Bean
    public Executor taskExecutor() {
        //设置100个线程处理定时任务
        return Executors.newScheduledThreadPool(10);
    }
}
