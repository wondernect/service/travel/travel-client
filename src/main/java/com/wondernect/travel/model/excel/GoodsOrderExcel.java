package com.wondernect.travel.model.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.wondernect.elements.common.utils.ESDateTimeUtils;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.easyoffice.excel.EasyExcel;
import com.wondernect.travel.model.TravelOrder;
import com.wondernect.travel.model.em.OrderSource;
import com.wondernect.travel.model.em.Scene;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: TravelOrderExcel
 * Author: chenxun
 * Date: 2019/5/30 9:16
 * Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GoodsOrderExcel {

    @Excel(name = "订单状态")
    private String orderStatus;

    @Excel(name = "收货人姓名")
    private String username;

    @Excel(name = "联系电话")
    private String mobile;

    @Excel(name = "收货地址")
    private String receiveAddress;

    @Excel(name = "下单时间")
    private String createTime;

    @Excel(name = "订单id")
    private String orderId;

    @Excel(name = "价格")
    private String price;

    public static final String EXCEL_TITLE = "订单列表";

    public static final String EXCEL_SHEET = "订单列表";

    public static final String EXCEL_FILE_NAME_PREFIX = ESDateTimeUtils.formatDate(ESDateTimeUtils.getCurrentTimestamp(), "yyyy-MM-dd");

    public static final String EXCEL_FILE_NAME = "订单导出";

    private static GoodsOrderExcel build(TravelOrder travelOrder) {
        String orderStatus = "";
        switch (travelOrder.getOrderStatus()) {
            case WAIT_APPROVAL_OR_PAY:
            {
                if (ESObjectUtils.isNotNull(travelOrder.getScene()) &&
                        travelOrder.getScene() == Scene.BANSHOU_PRIVATE) {
                    orderStatus = "订单待付款";
                } else {
                    if (ESObjectUtils.isNotNull(travelOrder.getOrderSource()) &&
                            travelOrder.getOrderSource() == OrderSource.SHOP) {
                        orderStatus = "待商户审核";
                    } else {
                        orderStatus = "待付款";
                    }
                }
                break;
            }
            case WAIT_ACCEPT:
            {
                if (ESObjectUtils.isNotNull(travelOrder.getScene()) &&
                        travelOrder.getScene() == Scene.BANSHOU_PRIVATE) {
                    orderStatus = "订单已付款";
                } else {
                    orderStatus = "待接单";
                }
                break;
            }
            case AUTO:
            {
                orderStatus = "派单中";
                break;
            }
            case ACCEPTED:
            {
                orderStatus = "已接单";
                break;
            }
            case ON_THE_WAY:
            {
                if (ESObjectUtils.isNotNull(travelOrder.getScene()) &&
                        travelOrder.getScene() == Scene.BANSHOU_PRIVATE) {
                    orderStatus = "订单已发货";
                } else {
                    orderStatus = "行程中";
                }
                break;
            }
            case DONE:
            {
                if (ESObjectUtils.isNotNull(travelOrder.getScene()) &&
                        travelOrder.getScene() == Scene.BANSHOU_PRIVATE) {
                    orderStatus = "订单已完成";
                } else {
                    orderStatus = "已抵达目的地";
                }
                break;
            }
            case SETTLED:
            {
                orderStatus = "已结算";
                break;
            }
            case CANCEL_CONSOLE:
            {
                orderStatus = "已取消(后台)";
                break;
            }
            case CANCEL_SHOP:
            {
                orderStatus = "已取消(商家)";
                break;
            }
            case CANCEL_USER:
            {
                orderStatus = "已取消(用户)";
                break;
            }
        }
        String price = "";
        if (ESObjectUtils.isNotNull(travelOrder.getPrice())) {
            price = String.valueOf(travelOrder.getPrice());
        }
        return new GoodsOrderExcel(
                orderStatus,
                travelOrder.getUsername(),
                travelOrder.getMobile(),
                travelOrder.getReceiveAddress(),
                ESDateTimeUtils.formatDate(travelOrder.getCreateTime(), "yyyy-MM-dd HH:mm"),
                travelOrder.getId(),
                price
        );
    }

    public static void exportData(List<TravelOrder> travelOrderList, HttpServletRequest request, HttpServletResponse response) {
        List<GoodsOrderExcel> goodsOrderExcelList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(travelOrderList)) {
            for (TravelOrder travelOrder : travelOrderList) {
                goodsOrderExcelList.add(build(travelOrder));
            }
        }
        export(goodsOrderExcelList, request, response);
    }

    private static void export(List<GoodsOrderExcel> goodsOrderExcelList, HttpServletRequest request, HttpServletResponse response) {
        EasyExcel.exportExcel(
                goodsOrderExcelList,
                EXCEL_TITLE,
                EXCEL_SHEET,
                GoodsOrderExcel.class,
                EXCEL_FILE_NAME_PREFIX + EXCEL_FILE_NAME,
                request,
                response
        );
    }
}
