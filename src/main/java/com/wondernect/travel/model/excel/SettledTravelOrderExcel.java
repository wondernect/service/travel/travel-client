package com.wondernect.travel.model.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.wondernect.elements.common.utils.ESDateTimeUtils;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.easyoffice.excel.EasyExcel;
import com.wondernect.travel.model.TravelOrder;
import com.wondernect.travel.model.em.Scene;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: TravelOrderExcel
 * Author: chenxun
 * Date: 2019/5/30 9:16
 * Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SettledTravelOrderExcel {

    @Excel(name = "结算状态", orderNum = "0")
    private String settledStatus;

    @Excel(name = "来源商家", orderNum = "1")
    private String orderSource;

    @Excel(name = "费用", orderNum = "2")
    private String price;

    @Excel(name = "结算时间", orderNum = "3")
    private String settledTime;

    @Excel(name = "乘车人姓名", orderNum = "4")
    private String username;

    @Excel(name = "类型", orderNum = "5")
    private String scene;

    @Excel(name = "上车地点", orderNum = "6")
    private String startPlace;

    @Excel(name = "目的地", orderNum = "7")
    private String targetPlace;

    @Excel(name = "上车时间", orderNum = "8")
    private String startTime;

    @Excel(name = "航班号", orderNum = "9")
    private String airNo;

    @Excel(name = "联系电话", orderNum = "10")
    private String mobile;

    @Excel(name = "备注", orderNum = "11")
    private String remark;

    @Excel(name = "下单时间", orderNum = "12")
    private String createTime;

    @Excel(name = "订单id", orderNum = "13")
    private String orderId;

    @Excel(name = "司机姓名", orderNum = "14")
    private String driverUserName;

    @Excel(name = "司机手机号码", orderNum = "15")
    private String driverMobile;

    public static final String EXCEL_TITLE = "结算订单列表";

    public static final String EXCEL_SHEET = "结算订单列表";

    public static final String EXCEL_FILE_NAME_PREFIX = ESDateTimeUtils.formatDate(ESDateTimeUtils.getCurrentTimestamp(), "yyyy-MM-dd");

    public static final String EXCEL_FILE_NAME = "结算订单导出";

    private static SettledTravelOrderExcel build(TravelOrder travelOrder) {
        String orderSource = "";
        switch (travelOrder.getOrderSource()) {
            case SHOP:
            {
                orderSource = travelOrder.getShopName();
                break;
            }
            case USER:
            {
                orderSource = "用户本身";
                break;
            }
        }
        String orderStatus = "";
        switch (travelOrder.getOrderStatus()) {
            case DONE:
            {
                if (ESObjectUtils.isNotNull(travelOrder.getScene()) &&
                        travelOrder.getScene() == Scene.BANSHOU_PRIVATE) {
                    orderStatus = "订单已完成";
                } else {
                    orderStatus = "待结算";
                }
                break;
            }
            case SETTLED:
            {
                orderStatus = "已结算";
                break;
            }
        }
        String scene = "";
        switch (travelOrder.getScene()) {
            case JIEJI:
            {
                scene = "接机";
                break;
            }
            case SONGJI:
            {
                scene = "送机";
                break;
            }
            case ORDER_SCENE:
            {
                scene = "预约";
                break;
            }
            case DAY_PRIVATE:
            {
                scene = "按天包车";
                break;
            }
            case ROAD_PRIVATE:
            {
                scene = "线路包车";
                break;
            }
            case JINGDIAN_PRIVATE:
            {
                scene = "景点美食";
                break;
            }
            case BANSHOU_PRIVATE:
            {
                scene = "伴手礼";
                break;
            }
        }

        return new SettledTravelOrderExcel(
                orderStatus,
                orderSource,
                travelOrder.getPrice() + "元",
                ESObjectUtils.isNotNull(travelOrder.getSettledTime()) ? ESDateTimeUtils.formatDate(travelOrder.getSettledTime(), "yyyy-MM-dd HH:mm") : "",
                travelOrder.getUsername(),
                scene,
                travelOrder.getStartPlace(),
                travelOrder.getTargetPlace(),
                ESDateTimeUtils.formatDate(travelOrder.getExecuteTime(), "yyyy-MM-dd HH:mm"),
                travelOrder.getAirNo(),
                travelOrder.getMobile(),
                travelOrder.getRemark(),
                ESDateTimeUtils.formatDate(travelOrder.getCreateTime(), "yyyy-MM-dd HH:mm"),
                travelOrder.getId(),
                travelOrder.getDriverUserName(),
                travelOrder.getDriverMobile()
        );
    }

    public static void exportData(List<TravelOrder> travelOrderList, HttpServletRequest request, HttpServletResponse response) {
        List<SettledTravelOrderExcel> settledTravelOrderExcelList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(travelOrderList)) {
            for (TravelOrder travelOrder : travelOrderList) {
                settledTravelOrderExcelList.add(build(travelOrder));
            }
        }
        export(settledTravelOrderExcelList, request, response);
    }

    private static void export(List<SettledTravelOrderExcel> settledTravelOrderExcelList, HttpServletRequest request, HttpServletResponse response) {
        EasyExcel.exportExcel(
                settledTravelOrderExcelList,
                EXCEL_TITLE,
                EXCEL_SHEET,
                SettledTravelOrderExcel.class,
                EXCEL_FILE_NAME_PREFIX + EXCEL_FILE_NAME,
                request,
                response
        );
    }
}
