package com.wondernect.travel.model.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.wondernect.elements.common.utils.ESDateTimeUtils;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.easyoffice.excel.EasyExcel;
import com.wondernect.travel.model.TravelOrder;
import com.wondernect.travel.model.em.OrderSource;
import com.wondernect.travel.model.em.Scene;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: TravelOrderExcel
 * Author: chenxun
 * Date: 2019/5/30 9:16
 * Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TravelOrderExcel {

    @Excel(name = "来源", orderNum = "0")
    private String orderSource;

    @Excel(name = "订单状态", orderNum = "1")
    private String orderStatus;

    @Excel(name = "类型", orderNum = "2")
    private String scene;

    @Excel(name = "乘车人姓名", orderNum = "3")
    private String username;

    @Excel(name = "联系电话", orderNum = "4")
    private String mobile;

    @Excel(name = "紧急联系人", orderNum = "5")
    private String contact;

    @Excel(name = "注册手机号", orderNum = "6")
    private String userMobile;

    @Excel(name = "上车时间", orderNum = "7")
    private String startTime;

    @Excel(name = "航班号", orderNum = "8")
    private String airNo;

    @Excel(name = "上车地点", orderNum = "9")
    private String startPlace;

    @Excel(name = "目的地", orderNum = "10")
    private String targetPlace;

    @Excel(name = "备注", orderNum = "10")
    private String remark;

    @Excel(name = "下单时间", orderNum = "11")
    private String createTime;

    @Excel(name = "订单id", orderNum = "12")
    private String orderId;

    @Excel(name = "价格", orderNum = "13")
    private String price;

    @Excel(name = "取消手续费", orderNum = "14")
    private String refundFee;

    @Excel(name = "实收金额", orderNum = "15")
    private String exactPrice;

    @Excel(name = "司机姓名", orderNum = "16")
    private String driverUserName;

    @Excel(name = "司机手机号码", orderNum = "17")
    private String driverMobile;

    public static final String EXCEL_TITLE = "订单列表";

    public static final String EXCEL_SHEET = "订单列表";

    public static final String EXCEL_FILE_NAME_PREFIX = ESDateTimeUtils.formatDate(ESDateTimeUtils.getCurrentTimestamp(), "yyyy-MM-dd");

    public static final String EXCEL_FILE_NAME = "订单导出";

    private static TravelOrderExcel build(TravelOrder travelOrder) {
        String orderSource = "";
        switch (travelOrder.getOrderSource()) {
            case SHOP:
            {
                orderSource = travelOrder.getShopName();
                break;
            }
            case USER:
            {
                orderSource = "用户本身";
                break;
            }
        }
        String orderStatus = "";
        switch (travelOrder.getOrderStatus()) {
            case WAIT_APPROVAL_OR_PAY:
            {
                if (ESObjectUtils.isNotNull(travelOrder.getScene()) &&
                        travelOrder.getScene() == Scene.BANSHOU_PRIVATE) {
                    orderStatus = "订单待付款";
                } else {
                    if (ESObjectUtils.isNotNull(travelOrder.getOrderSource()) &&
                            travelOrder.getOrderSource() == OrderSource.SHOP) {
                        orderStatus = "待商户审核";
                    } else {
                        orderStatus = "待付款";
                    }
                }
                break;
            }
            case WAIT_ACCEPT:
            {
                if (ESObjectUtils.isNotNull(travelOrder.getScene()) &&
                        travelOrder.getScene() == Scene.BANSHOU_PRIVATE) {
                    orderStatus = "订单已付款";
                } else {
                    orderStatus = "待接单";
                }
                break;
            }
            case AUTO:
            {
                orderStatus = "派单中";
                break;
            }
            case ACCEPTED:
            {
                orderStatus = "已接单";
                break;
            }
            case ON_THE_WAY:
            {
                if (ESObjectUtils.isNotNull(travelOrder.getScene()) &&
                        travelOrder.getScene() == Scene.BANSHOU_PRIVATE) {
                    orderStatus = "订单已发货";
                } else {
                    orderStatus = "行程中";
                }
                break;
            }
            case DONE:
            {
                if (ESObjectUtils.isNotNull(travelOrder.getScene()) &&
                        travelOrder.getScene() == Scene.BANSHOU_PRIVATE) {
                    orderStatus = "订单已完成";
                } else {
                    orderStatus = "已抵达目的地";
                }
                break;
            }
            case SETTLED:
            {
                orderStatus = "已结算";
                break;
            }
            case CANCEL_CONSOLE:
            {
                orderStatus = "已取消(后台)";
                break;
            }
            case CANCEL_SHOP:
            {
                orderStatus = "已取消(商家)";
                break;
            }
            case CANCEL_USER:
            {
                orderStatus = "已取消(用户)";
                break;
            }
        }
        String scene = "";
        switch (travelOrder.getScene()) {
            case JIEJI:
            {
                scene = "接机";
                break;
            }
            case SONGJI:
            {
                scene = "送机";
                break;
            }
            case ORDER_SCENE:
            {
                scene = "预约";
                break;
            }
            case DAY_PRIVATE:
            {
                scene = "按天包车";
                break;
            }
            case ROAD_PRIVATE:
            {
                scene = "线路包车";
                break;
            }
            case JINGDIAN_PRIVATE:
            {
                scene = "景点美食";
                break;
            }
            case BANSHOU_PRIVATE:
            {
                scene = "伴手礼";
                break;
            }
        }
        String price = "";
        if (ESObjectUtils.isNotNull(travelOrder.getPrice())) {
            price = String.valueOf(travelOrder.getPrice());
        }
        String refundFee = "";
        if (ESObjectUtils.isNotNull(travelOrder.getRefundFee())) {
            refundFee = String.valueOf(travelOrder.getRefundFee());
        }

        return new TravelOrderExcel(
                orderSource,
                orderStatus,
                scene,
                travelOrder.getUsername(),
                travelOrder.getMobile(),
                travelOrder.getContact(),
                travelOrder.getUserMobile(),
                ESDateTimeUtils.formatDate(travelOrder.getStartTime(), "yyyy-MM-dd HH:mm"),
                travelOrder.getAirNo(),
                travelOrder.getStartPlace(),
                travelOrder.getTargetPlace(),
                travelOrder.getRemark(),
                ESDateTimeUtils.formatDate(travelOrder.getCreateTime(), "yyyy-MM-dd HH:mm"),
                travelOrder.getId(),
                price,
                refundFee,
                ESObjectUtils.isNotNull(travelOrder.getRefundFee()) ? refundFee : price,
                travelOrder.getDriverUserName(),
                travelOrder.getDriverMobile()
        );
    }

    public static void exportData(List<TravelOrder> travelOrderList, HttpServletRequest request, HttpServletResponse response) {
        List<TravelOrderExcel> travelOrderExcelList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(travelOrderList)) {
            for (TravelOrder travelOrder : travelOrderList) {
                travelOrderExcelList.add(build(travelOrder));
            }
        }
        export(travelOrderExcelList, request, response);
    }

    private static void export(List<TravelOrderExcel> travelOrderExcelList, HttpServletRequest request, HttpServletResponse response) {
        EasyExcel.exportExcel(
                travelOrderExcelList,
                EXCEL_TITLE,
                EXCEL_SHEET,
                TravelOrderExcel.class,
                EXCEL_FILE_NAME_PREFIX + EXCEL_FILE_NAME,
                request,
                response
        );
    }
}
