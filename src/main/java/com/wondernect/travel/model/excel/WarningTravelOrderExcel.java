package com.wondernect.travel.model.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.wondernect.elements.common.utils.ESDateTimeUtils;
import com.wondernect.elements.common.utils.ESNumberFormatUtils;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.easyoffice.excel.EasyExcel;
import com.wondernect.travel.model.TravelOrder;
import com.wondernect.travel.model.em.OrderSource;
import com.wondernect.travel.model.em.Scene;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: TravelOrderExcel
 * Author: chenxun
 * Date: 2019/5/30 9:16
 * Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WarningTravelOrderExcel {

    @Excel(name = "上车时间", orderNum = "0")
    private String startTime;

    @Excel(name = "用车类型", orderNum = "1")
    private String scene;

    @Excel(name = "当前时差", orderNum = "2")
    private String currentTime;

    @Excel(name = "状态", orderNum = "3")
    private String orderStatus;

    @Excel(name = "下单时间", orderNum = "4")
    private String createTime;

    @Excel(name = "上车地点", orderNum = "5")
    private String startPlace;

    @Excel(name = "目的地", orderNum = "6")
    private String targetPlace;

    @Excel(name = "一口价", orderNum = "7")
    private String price;

    @Excel(name = "预估时间", orderNum = "8")
    private String time;

    @Excel(name = "用户姓名", orderNum = "9")
    private String username;

    @Excel(name = "联系方式", orderNum = "10")
    private String mobile;

    @Excel(name = "订单id", orderNum = "11")
    private String orderId;

    public static final String EXCEL_TITLE = "预警订单列表";

    public static final String EXCEL_SHEET = "预警订单列表";

    public static final String EXCEL_FILE_NAME_PREFIX = ESDateTimeUtils.formatDate(ESDateTimeUtils.getCurrentTimestamp(), "yyyy-MM-dd");

    public static final String EXCEL_FILE_NAME = "预警订单导出";

    private static WarningTravelOrderExcel build(TravelOrder travelOrder) {
        Long time = ESDateTimeUtils.getCurrentTimestamp() - travelOrder.getAutoDispatchTime();
        String orderStatus = "";
        switch (travelOrder.getOrderStatus()) {
            case WAIT_APPROVAL_OR_PAY:
            {
                if (ESObjectUtils.isNotNull(travelOrder.getScene()) &&
                        travelOrder.getScene() == Scene.BANSHOU_PRIVATE) {
                    orderStatus = "订单待付款";
                } else {
                    if (ESObjectUtils.isNotNull(travelOrder.getOrderSource()) &&
                            travelOrder.getOrderSource() == OrderSource.SHOP) {
                        orderStatus = "待商户审核";
                    } else {
                        orderStatus = "待付款";
                    }
                }
                break;
            }
            case WAIT_ACCEPT:
            {
                if (ESObjectUtils.isNotNull(travelOrder.getScene()) &&
                        travelOrder.getScene() == Scene.BANSHOU_PRIVATE) {
                    orderStatus = "订单已付款";
                } else {
                    orderStatus = "待接单";
                }
                break;
            }
            case AUTO:
            {
                Long dispatchTime = time >= 0 ? time / 3600  : 0;
                orderStatus = "已派单" + ESNumberFormatUtils.formatDouble(dispatchTime.doubleValue(), 2) + "分钟";
                break;
            }
            case ACCEPTED:
            {
                orderStatus = "已接单";
                break;
            }
            case ON_THE_WAY:
            {
                if (ESObjectUtils.isNotNull(travelOrder.getScene()) &&
                        travelOrder.getScene() == Scene.BANSHOU_PRIVATE) {
                    orderStatus = "订单已发货";
                } else {
                    orderStatus = "行程中";
                }
                break;
            }
            case DONE:
            {
                if (ESObjectUtils.isNotNull(travelOrder.getScene()) &&
                        travelOrder.getScene() == Scene.BANSHOU_PRIVATE) {
                    orderStatus = "订单已完成";
                } else {
                    orderStatus = "已抵达目的地";
                }
                break;
            }
            case SETTLED:
            {
                orderStatus = "已结算";
                break;
            }
            case CANCEL_CONSOLE:
            {
                orderStatus = "已取消(后台)";
                break;
            }
            case CANCEL_SHOP:
            {
                orderStatus = "已取消(商家)";
                break;
            }
            case CANCEL_USER:
            {
                orderStatus = "已取消(用户)";
                break;
            }
        }
        String scene = "";
        switch (travelOrder.getScene()) {
            case JIEJI:
            {
                scene = "接机";
                break;
            }
            case SONGJI:
            {
                scene = "送机";
                break;
            }
            case ORDER_SCENE:
            {
                scene = "预约";
                break;
            }
            case DAY_PRIVATE:
            {
                scene = "按天包车";
                break;
            }
            case ROAD_PRIVATE:
            {
                scene = "线路包车";
                break;
            }
            case JINGDIAN_PRIVATE:
            {
                scene = "景点美食";
                break;
            }
            case BANSHOU_PRIVATE:
            {
                scene = "伴手礼";
                break;
            }
        }
        Long currentTime = travelOrder.getStartTime() - ESDateTimeUtils.getCurrentTimestamp();
        Long current = currentTime >= 0 ? currentTime / 3600 : 0;
        if (ESObjectUtils.isNull(travelOrder.getTime())) {
            travelOrder.setTime(0d);
        }
        Double useTime = travelOrder.getTime() >= 0 ? travelOrder.getTime() / 3600000 : 0;
        return new WarningTravelOrderExcel(
                ESDateTimeUtils.formatDate(travelOrder.getStartTime(), "yyyy-MM-dd HH:mm"),
                scene,
                ESNumberFormatUtils.formatDouble(current.doubleValue(), 2) + "分钟",
                orderStatus,
                ESDateTimeUtils.formatDate(travelOrder.getCreateTime(), "yyyy-MM-dd HH:mm"),
                travelOrder.getStartPlace(),
                travelOrder.getTargetPlace(),
                travelOrder.getPrice() + "元",
                ESNumberFormatUtils.formatDouble(useTime, 2) + "小时",
                travelOrder.getUsername(),
                travelOrder.getMobile(),
                travelOrder.getId()
        );
    }

    public static void exportData(List<TravelOrder> travelOrderList, HttpServletRequest request, HttpServletResponse response) {
        List<WarningTravelOrderExcel> warningTravelOrderExcelList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(travelOrderList)) {
            for (TravelOrder travelOrder : travelOrderList) {
                warningTravelOrderExcelList.add(build(travelOrder));
            }
        }
        export(warningTravelOrderExcelList, request, response);
    }

    private static void export(List<WarningTravelOrderExcel> warningTravelOrderExcelList, HttpServletRequest request, HttpServletResponse response) {
        EasyExcel.exportExcel(
                warningTravelOrderExcelList,
                EXCEL_TITLE,
                EXCEL_SHEET,
                WarningTravelOrderExcel.class,
                EXCEL_FILE_NAME_PREFIX + EXCEL_FILE_NAME,
                request,
                response
        );
    }
}
