package com.wondernect.travel.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.base.model.BaseStringModel;
import com.wondernect.elements.rdb.config.IDGenerateor;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: Address
 * Author: chenxun
 * Date: 2019/4/24 15:43
 * Description:
 */
@Entity
@Table(name = "address")
@GenericGenerator(name = "id", strategy = IDGenerateor.DISTRIBUTED_STRING_IDENTITY)
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(description = "常用接送地址")
public class Address extends BaseStringModel {

    @NotBlank(message = "城市id不能为空")
    @JsonProperty("city_id")
    @ApiModelProperty(value = "城市id")
    private String cityId;

    @NotNull(message = "常用地点类型(0-机场;1-火车站;2-其他;)不能为空")
    @JsonProperty("type")
    @ApiModelProperty(value = "常用地点类型(0-机场;1-火车站;2-其他;)")
    private Integer type;

    @NotBlank(message = "常用地点不能为空")
    @JsonProperty("name")
    @ApiModelProperty(notes = "常用地点")
    private String name;

    @NotBlank(message = "详细地址不能为空")
    @JsonProperty("detail")
    @ApiModelProperty(notes = "详细地址")
    private String detail;

    @NotNull(message = "经度不能为空")
    @JsonProperty("longitude")
    @ApiModelProperty(notes = "经度")
    private Double longitude;

    @NotNull(message = "纬度不能为空")
    @JsonProperty("latitude")
    @ApiModelProperty(notes = "纬度")
    private Double latitude;
}
