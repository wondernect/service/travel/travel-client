package com.wondernect.travel.model.em;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: WarningStatus
 * Author: chenxun
 * Date: 2019/5/16 16:48
 * Description:
 */
public enum WarningStatus {

    NORMAL, // 正常

    WARNING, // 警告

    URGENT, // 紧急
}

