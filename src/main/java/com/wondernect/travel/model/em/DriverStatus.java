package com.wondernect.travel.model.em;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: DriverStatus
 * Author: chenxun
 * Date: 2019/4/24 17:31
 * Description:
 */
public enum DriverStatus {

    UNKNOWN, // 未知

    WORK, // 接单工作中(接订单后系统设置)

    IDLE, // 空闲(司机手动设置或者完成订单后系统设置)
}

