package com.wondernect.travel.model.em;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: OrderStatus
 * Author: chenxun
 * Date: 2019/5/10 10:12
 * Description:
 */
public enum OrderStatus {

    WAIT_APPROVAL_OR_PAY, // 1.待商家审核(商家发起的订单);2.待用户付款(用户发起的订单);

    WAIT_ACCEPT, // 1.待接单(已发布,但未被司机接单,且未进入派单流程的订单)；2.已付款；

    AUTO, // 派单中(已发布,但未被司机接单,且触发派单条件的订单) - 每个司机对应有一个auto订单池

    ACCEPTED, // 已接单(司机已接单,乘客未上车)

    ON_THE_WAY, // 1.行程中(乘客已上车,未到达目的地)；2.已发货；

    DONE, // 1.已抵达目的地；2.已完成；

    SETTLED, // 后台财务人员点击已结算

    CANCEL_CONSOLE, // 已取消(后台)

    CANCEL_SHOP, // 已取消(商家执行)

    CANCEL_USER, // 取消用户订单(用户执行)
}

