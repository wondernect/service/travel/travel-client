package com.wondernect.travel.model.em;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: OrderSource
 * Author: chenxun
 * Date: 2019/5/10 10:35
 * Description:
 */
public enum OrderSource {

    SHOP,   // 商家

    USER,   // 用户
}

