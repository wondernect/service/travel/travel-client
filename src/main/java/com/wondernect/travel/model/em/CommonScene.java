package com.wondernect.travel.model.em;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: CommonConsumeScene
 * Author: chenxun
 * Date: 2019/4/14 19:11
 * Description:
 */
public enum CommonScene {

    NOW,    // 立即叫车

    ORDER,  // 预约
}

