package com.wondernect.travel.model.em;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: PricePlan
 * Author: chenxun
 * Date: 2019/4/14 19:41
 * Description:
 */
public enum PlanType {

    ONE, // 方案1

    TWO, // 方案2

    THREE, // 方案3

    FOUR, // 方案4

    FIVE, // 方案5

    SIX, // 方案6
}
