package com.wondernect.travel.model.em;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: ConsumeScene
 * Author: chenxun
 * Date: 2019/4/14 19:08
 * Description:
 */
public enum Scene {

    JIEJI, // 接机

    SONGJI, // 送机

    ORDER_SCENE, // 预约

    DAY_PRIVATE, // 按天包车

    ROAD_PRIVATE, // 按线路包车

    JINGDIAN_PRIVATE, // 景点

    MEISHI_PRIVATE, // 美食

    BANSHOU_PRIVATE, // 伴手礼
}

