package com.wondernect.travel.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.base.model.BaseStringModel;
import com.wondernect.elements.rdb.config.IDGenerateor;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: Order
 * Author: chenxun
 * Date: 2019/5/10 10:44
 * Description:
 */
@Entity
@Table(name = "refund_order")
@GenericGenerator(name = "id", strategy = IDGenerateor.DISTRIBUTED_STRING_IDENTITY)
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(description = "退款订单")
public class RefundOrder extends BaseStringModel {

    @JsonProperty("order_id")
    @ApiModelProperty(notes = "订单id")
    private String orderId;

    @JsonProperty("user_id")
    @ApiModelProperty(notes = "用户id")
    private String userId;

    @JsonProperty("username")
    @ApiModelProperty(notes = "乘车人姓名")
    private String username;

    @JsonProperty("mobile")
    @ApiModelProperty(notes = "联系电话")
    private String mobile;

    @NotBlank(message = "微信号不能为空")
    @JsonProperty("wechat")
    @ApiModelProperty(notes = "微信号")
    private String wechat;

    @JsonProperty("price")
    @ApiModelProperty(notes = "订单金额")
    private Double price;

    @JsonProperty("refund")
    @ApiModelProperty(notes = "退款金额")
    private Double refund;

    @JsonProperty("before_time")
    @ApiModelProperty(notes = "提前多长时间")
    private Double beforeTime;

    @JsonProperty("refund_percent")
    @ApiModelProperty(notes = "退款手续费百分比")
    private Double refundPercent;

    @JsonProperty("fee")
    @ApiModelProperty(notes = "退款手续费")
    private Double fee;

    @JsonProperty("has_refund")
    @ApiModelProperty(notes = "退款状态")
    private Boolean hasRefund;
}
