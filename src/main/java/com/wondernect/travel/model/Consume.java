package com.wondernect.travel.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.base.model.BaseStringModel;
import com.wondernect.elements.rdb.config.IDGenerateor;
import com.wondernect.travel.model.em.CommonScene;
import com.wondernect.travel.model.em.Scene;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: CommonConsume
 * Author: chenxun
 * Date: 2019/4/14 19:18
 * Description:
 */
@Entity
@Table(name = "consume")
@GenericGenerator(name = "id", strategy = IDGenerateor.DISTRIBUTED_STRING_IDENTITY)
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(description = "用车服务配置(适用于接送机&按天包车)")
public class Consume extends BaseStringModel {

    @NotBlank(message = "城市id不能为空")
    @JsonProperty("city_id")
    @ApiModelProperty(notes = "城市id")
    private String cityId;

    @NotNull(message = "用车场景不能为空")
    @Enumerated(EnumType.STRING)
    @JsonProperty("scene")
    @ApiModelProperty(notes = "用车场景", required = true)
    private Scene scene;

    @NotNull(message = "用车类型不能为空")
    @Enumerated(EnumType.STRING)
    @JsonProperty("common_scene")
    @ApiModelProperty(notes = "用车类型")
    private CommonScene commonScene;

    @JsonProperty("taocan")
    @ApiModelProperty(notes = "套餐标识(1-套餐1；2-套餐；...)")
    private String taocan;

    @JsonProperty("title")
    @ApiModelProperty(notes = "套餐标题")
    private String title;

    @JsonProperty("show_price")
    @ApiModelProperty(notes = "页面展示最低价格(699/天)")
    private String showPrice;

    @JsonProperty("description")
    @ApiModelProperty(notes = "备注")
    private String description;

    @JsonProperty("contain")
    @ApiModelProperty(notes = "套餐包含说明")
    private String contain;

    @JsonProperty("uncontain")
    @ApiModelProperty(notes = "套餐不包含说明")
    private String uncontain;
}
