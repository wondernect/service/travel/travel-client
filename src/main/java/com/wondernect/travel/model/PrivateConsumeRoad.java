package com.wondernect.travel.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.base.model.BaseSortModel;
import com.wondernect.elements.rdb.config.IDGenerateor;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: PrivateConsume
 * Author: chenxun
 * Date: 2019/9/23 7:58
 * Description: 包车服务行程路线
 */
@Entity
@Table(name = "private_consume_road")
@GenericGenerator(name = "id", strategy = IDGenerateor.DISTRIBUTED_STRING_IDENTITY)
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(description = "包车服务行程路线")
public class PrivateConsumeRoad extends BaseSortModel<String> {

    @JsonProperty("private_consume_id")
    @ApiModelProperty(notes = "包车服务标识id")
    private String privateConsumeId;

    @JsonProperty("name")
    @ApiModelProperty(notes = "行程名称")
    private String name;

    @JsonProperty("start_time")
    @ApiModelProperty(notes = "行程开始时间")
    private Long startTime;

    @Lob
    @JsonProperty("day_road")
    @ApiModelProperty(notes = "当日行程")
    private String dayRoad;

    @Lob
    @JsonProperty("images")
    @ApiModelProperty(notes = "行程图片")
    private String images;

    @JsonProperty("play_time")
    @ApiModelProperty(notes = "游玩时间")
    private String playTime;

    @Lob
    @JsonProperty("description")
    @ApiModelProperty(value = "行程说明")
    private String description;
}
