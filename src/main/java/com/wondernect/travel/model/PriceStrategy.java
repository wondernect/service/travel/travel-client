package com.wondernect.travel.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.base.model.BaseStringModel;
import com.wondernect.elements.rdb.config.IDGenerateor;
import com.wondernect.travel.model.em.PlanType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: PriceStrategy
 * Author: chenxun
 * Date: 2019/4/14 19:28
 * Description:
 */
@Entity
@Table(name = "price_strategy")
@GenericGenerator(name = "id", strategy = IDGenerateor.DISTRIBUTED_STRING_IDENTITY)
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(description = "价格策略")
public class PriceStrategy extends BaseStringModel {

    @NotBlank(message = "名称不能为空")
    @JsonProperty("strategy_name")
    @ApiModelProperty(notes = "策略名称")
    private String strategyName;

    @JsonProperty("strategy_description")
    @ApiModelProperty(notes = "备注")
    private String strategyDescription;

    @NotNull(message = "价格方案不能为空")
    @Enumerated(EnumType.STRING)
    @JsonProperty("plan_type")
    @ApiModelProperty(notes = "价格方案")
    private PlanType planType;

    @JsonProperty("plan_description")
    @ApiModelProperty(notes = "价格方案备注")
    private String planDescription;

    @JsonProperty("base_price")
    @ApiModelProperty(notes = "基础费")
    private Double basePrice;

    @JsonProperty("kilo_price")
    @ApiModelProperty(notes = "里程费")
    private Double kiloPrice;

    @JsonProperty("time_price")
    @ApiModelProperty(notes = "时长费")
    private Double timePrice;

    @JsonProperty("start_kilo")
    @ApiModelProperty(notes = "起步公里数")
    private Double startKilo;

    @JsonProperty("limit_kilo")
    @ApiModelProperty(notes = "界限公里数")
    private Double limitKilo;

    @JsonProperty("long_price")
    @ApiModelProperty(notes = "远途费")
    private Double longPrice;

    @JsonProperty("distance")
    @ApiModelProperty(notes = "区间价格Map<String, Double> => {\"10.0\":20.0,\"5.0\":10.0,\"30.0\":50.0}")
    private String distance;

    @JsonProperty("night_percent")
    @ApiModelProperty(notes = "夜间服务费百分比")
    private Double nightPercent;
}
