package com.wondernect.travel.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.base.model.BaseStringModel;
import com.wondernect.elements.rdb.config.IDGenerateor;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: Driver
 * Author: chenxun
 * Date: 2019/4/24 16:15
 * Description:
 */
@Entity
@Table(name = "driver")
@GenericGenerator(name = "id", strategy = IDGenerateor.DISTRIBUTED_STRING_IDENTITY)
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(description = "司机")
public class Driver extends BaseStringModel {

    @NotBlank(message = "对应用户id不能为空")
    @JsonProperty("user_id")
    @ApiModelProperty(notes = "司机对应用户id")
    private String userId;

    @JsonProperty("name")
    @ApiModelProperty(notes = "司机姓名")
    private String name;

    @JsonProperty("mobile")
    @ApiModelProperty(notes = "手机号码")
    private String mobile;

    @JsonProperty("evaluate")
    @ApiModelProperty(notes = "司机评分(有效评分平均值)")
    private int evaluate;

    @JsonProperty("total_evaluate_count")
    @ApiModelProperty(notes = "司机有效总评分数量")
    private int totalEvaluateCount;

    @JsonProperty("total_evaluate")
    @ApiModelProperty(notes = "司机有效总评分")
    private int totalEvaluate;
}
