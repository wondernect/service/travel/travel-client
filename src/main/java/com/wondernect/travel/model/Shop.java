package com.wondernect.travel.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.base.model.BaseStringModel;
import com.wondernect.elements.rdb.config.IDGenerateor;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: Shop
 * Author: chenxun
 * Date: 2019/4/24 16:18
 * Description:
 */
@Entity
@Table(name = "shop")
@GenericGenerator(name = "id", strategy = IDGenerateor.DISTRIBUTED_STRING_IDENTITY)
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(description = "店铺")
public class Shop extends BaseStringModel {

    @JsonProperty("name")
    @ApiModelProperty(notes = "店铺名称")
    private String name;

    @JsonProperty("user_id")
    @ApiModelProperty(notes = "店铺联系人id")
    private String userId;
}
