package com.wondernect.travel.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.base.model.BaseModel;
import com.wondernect.elements.rdb.config.IDGenerateor;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: Refund
 * Author: chenxun
 * Date: 2019/5/10 11:26
 * Description:
 */
@Entity
@Table(name = "refund_config")
@GenericGenerator(name = "id", strategy = IDGenerateor.DISTRIBUTED_STRING_IDENTITY)
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(description = "订单取消退款手续费")
public class RefundConfig extends BaseModel {

    public static final String ORDER_CODE = "order"; // 预约用车
    public static final String PRIVATE_CODE = "private"; // 线路包车
    public static final String ZERO_REFUND_CODE = "zero_refund"; // 0手续费退款

    @Id
    @JsonProperty("code")
    @ApiModelProperty(notes = "退款配置标识(order:预约用车;private:线路包车;zero_refund:0手续费退款)")
    private String code;

    @JsonProperty("before_first_time")
    @ApiModelProperty(notes = "用车前X小时")
    private Double beforeFirstTime;

    @JsonProperty("first_fund")
    @ApiModelProperty(notes = "用车前X小时,手续费X%")
    private Double firstFund;

    @JsonProperty("before_second_time")
    @ApiModelProperty(notes = "用车前X小时")
    private Double beforeSecondTime;

    @JsonProperty("second_fund")
    @ApiModelProperty(notes = "用车前X小时,手续费X%")
    private Double secondFund;

    @JsonProperty("limit_fund")
    @ApiModelProperty(notes = "退款金额超过X元,需人工审核")
    private Double limitFund;
}
