package com.wondernect.travel.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.base.model.BaseStringModel;
import com.wondernect.elements.rdb.config.IDGenerateor;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: DriverEvaluate
 * Author: chenxun
 * Date: 2019/4/24 15:47
 * Description:
 */
@Entity
@Table(name = "driver_evaluate")
@GenericGenerator(name = "id", strategy = IDGenerateor.DISTRIBUTED_STRING_IDENTITY)
@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(description = "司机评价")
public class DriverEvaluate extends BaseStringModel {

    @NotBlank(message = "司机不能为空")
    @JsonProperty("driver_user_id")
    @ApiModelProperty(notes = "司机id")
    private String driverUserId;

    @JsonProperty("evaluate")
    @ApiModelProperty(notes = "评价星级")
    private int evaluate;

    @Lob
    @JsonProperty("image")
    @ApiModelProperty(value = "图片")
    private String image;

    @Lob
    @JsonProperty("content")
    @ApiModelProperty(notes = "评价内容")
    private String content;

    @NotBlank(message = "乘客不能为空")
    @JsonProperty("user_id")
    @ApiModelProperty(notes = "乘客id")
    private String userId;

    @JsonProperty("order_create_time")
    @ApiModelProperty(notes = "订单下单时间")
    private Long orderCreateTime;

    @JsonProperty("order_excute_time")
    @ApiModelProperty(notes = "订单执行时间")
    private Long orderExcuteTime;

    @NotBlank(message = "订单不能为空")
    @JsonProperty("order_id")
    @ApiModelProperty(notes = "订单id")
    private String orderId;

    public DriverEvaluate() {
    }

    public DriverEvaluate(String driverUserId, int evaluate, String image, String content, String userId, String orderId) {
        this.driverUserId = driverUserId;
        this.evaluate = evaluate;
        this.image = image;
        this.content = content;
        this.userId = userId;
        this.orderId = orderId;
    }
}
