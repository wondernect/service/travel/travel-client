package com.wondernect.travel.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.base.model.BaseStringModel;
import com.wondernect.elements.rdb.config.IDGenerateor;
import com.wondernect.travel.model.em.OrderStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: Order
 * Author: chenxun
 * Date: 2019/5/10 10:44
 * Description:
 */
@Entity
@Table(name = "travel_order_history")
@GenericGenerator(name = "id", strategy = IDGenerateor.DISTRIBUTED_STRING_IDENTITY)
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(description = "订单历史记录")
public class TravelOrderHistory extends BaseStringModel {

    @JsonProperty("order_id")
    @ApiModelProperty(notes = "订单id")
    private String orderId;

    @Enumerated(EnumType.STRING)
    @JsonProperty("order_status")
    @ApiModelProperty(notes = "订单状态")
    private OrderStatus orderStatus;

    @JsonProperty("description")
    @ApiModelProperty(notes = "描述")
    private String description;
}
