package com.wondernect.travel.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.base.model.BaseSortModel;
import com.wondernect.elements.rdb.config.IDGenerateor;
import com.wondernect.travel.model.em.DriverStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: OrderStrategy
 * Author: chenxun
 * Date: 2019/4/14 20:01
 * Description:
 */
@Entity
@Table(name = "order_strategy")
@GenericGenerator(name = "id", strategy = IDGenerateor.DISTRIBUTED_STRING_IDENTITY)
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(description = "订单派送策略")
public class OrderStrategy extends BaseSortModel<String> {

    @NotBlank(message = "策略名称不能为空")
    @JsonProperty("strategy_name")
    @ApiModelProperty(notes = "策略名称")
    private String strategyName;

    @JsonProperty("strategy_description")
    @ApiModelProperty(notes = "备注")
    private String strategyDescription;

    @JsonProperty("before_use_dispatch_hour")
    @ApiModelProperty(notes = "用车前多少小时,自动派单")
    private Double beforeUseDispatchHour;

    @JsonProperty("driver_status")
    @ApiModelProperty(notes = "自动派单范围(非IDLE时表示无限制,为IDLE时表示派送给当前空闲状态的司机)")
    private DriverStatus driverStatus;

    @JsonProperty("distance_scope")
    @ApiModelProperty(notes = "自动派单范围(null或小于0时表示无限制,大于0时表示派送给距离上车地点在此范围内的司机)")
    private Double distanceScope;

    @JsonProperty("driver_evaluate")
    @ApiModelProperty(notes = "自动派单范围(为null或0时表示无限制,为具体数值时表示派送给评分不低于此数值的司机)")
    private int driverEvaluate;

    @JsonProperty("auto_time")
    @ApiModelProperty(notes = "该策略执行时间(时间内无人接单，则继续下一策略，直至无策略可用)")
    private Long autoTime;
}
