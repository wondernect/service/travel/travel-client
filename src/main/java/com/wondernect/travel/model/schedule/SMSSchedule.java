package com.wondernect.travel.model.schedule;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.common.exception.BusinessException;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.rdb.base.model.BaseModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: AutoSchedule
 * Author: chenxun
 * Date: 2019-10-27 20:02
 * Description: 短信发送队列
 */
@Entity
@Table(name = "sms_schedule")
@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(description = "短信发送队列")
public class SMSSchedule extends BaseModel {

    @Id
    @JsonProperty("id")
    @ApiModelProperty(notes = "唯一标识id")
    private String id;

    @JsonProperty("sms_type")
    @ApiModelProperty(notes = "短信发送类型(0-验证码发送;1-预约成功短信通知 - 乘客端;2-司机接单短信通知 - 乘客端;3-司机接单短信通知 - 司机端;4-伴手礼下单成功短信通知 - 用户;5-伴手礼发货成功短信通知 - 用户;)")
    private int smsType;

    @JsonProperty("phone_number")
    @ApiModelProperty(notes = "手机号码")
    private String phoneNumber;

    @JsonProperty("order_id")
    @ApiModelProperty(notes = "订单id")
    private String orderId;

    @JsonProperty("captcha")
    @ApiModelProperty(notes = "验证码")
    private String captcha;

    @JsonProperty("retry")
    @ApiModelProperty(notes = "发送次数")
    private int retry;

    public SMSSchedule() {
    }

    public SMSSchedule(int smsType, String phoneNumber, String orderId, String captcha) {
        this.id = generateIdentifier(smsType, phoneNumber, orderId, captcha);
        if (ESObjectUtils.isNull(this.id)) {
            throw new BusinessException("消息类型有误");
        }
        this.smsType = smsType;
        this.phoneNumber = phoneNumber;
        this.orderId = orderId;
        this.captcha = captcha;
        this.retry = 0;
    }

    private String generateIdentifier(int smsType, String phoneNumber, String orderId, String captcha) {
        switch (smsType) {
            case 0:
            {
                return smsType + phoneNumber + captcha;
            }
            case 1:
            case 2:
            case 3:
            case 4:
            {
                return smsType + phoneNumber + orderId;
            }
            case 5:
            {
                return smsType + phoneNumber + orderId;
            }
        }
        return null;
    }
}
