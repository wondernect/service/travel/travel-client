package com.wondernect.travel.model.schedule;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.base.model.BaseModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: AutoSchedule
 * Author: chenxun
 * Date: 2019-10-27 20:02
 * Description: 警告队列
 */
@Entity
@Table(name = "warning_schedule")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(description = "警告队列")
public class WarningSchedule extends BaseModel {

    @Id
    @JsonProperty("order_id")
    @ApiModelProperty(notes = "订单id")
    private String orderId;
}
