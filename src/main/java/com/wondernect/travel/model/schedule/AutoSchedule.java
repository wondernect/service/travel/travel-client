package com.wondernect.travel.model.schedule;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.base.model.BaseModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: AutoSchedule
 * Author: chenxun
 * Date: 2019-10-27 20:02
 * Description: 自动派单队列
 */
@Entity
@Table(name = "auto_schedule")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(description = "自动派单队列")
public class AutoSchedule extends BaseModel {

    @Id
    @JsonProperty("order_id")
    @ApiModelProperty(notes = "订单id")
    private String orderId;

    @JsonProperty("order_strategy_id")
    @ApiModelProperty(notes = "订单派送策略id")
    private String orderStrategyId;

    @JsonProperty("last_add_time")
    @ApiModelProperty(notes = "上次策略执行时间")
    private Long lastAddTime;
}
