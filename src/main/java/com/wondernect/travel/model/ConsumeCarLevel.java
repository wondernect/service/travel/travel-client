package com.wondernect.travel.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.base.model.BaseStringModel;
import com.wondernect.elements.rdb.config.IDGenerateor;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: ConsumeClassify
 * Author: chenxun
 * Date: 2019/4/14 19:55
 * Description:
 */
@Entity
@Table(name = "consume_car_level")
@GenericGenerator(name = "id", strategy = IDGenerateor.DISTRIBUTED_STRING_IDENTITY)
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(description = "用车服务-自定义分级")
public class ConsumeCarLevel extends BaseStringModel {

    @JsonProperty("consume_id")
    @ApiModelProperty(notes = "服务id")
    private String consumeId;

    @JsonProperty("zuowei_id")
    @ApiModelProperty(notes = "座位id")
    private String zuoweiId;

    @JsonProperty("chexing_id")
    @ApiModelProperty(notes = "车型id")
    private String chexingId;

    @JsonProperty("price")
    @ApiModelProperty(notes = "价格/每天")
    private Double price;

    @JsonProperty("price_strategy_id")
    @ApiModelProperty(notes = "价格策略id")
    private String priceStrategyId;
}
