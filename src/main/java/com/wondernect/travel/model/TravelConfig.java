package com.wondernect.travel.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.base.model.BaseSortModel;
import com.wondernect.elements.rdb.config.IDGenerateor;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: TravelConfig
 * Author: chenxun
 * Date: 2019-05-12 16:18
 * Description:
 */
@Entity
@Table(name = "travel_config")
@GenericGenerator(name = "id", strategy = IDGenerateor.DISTRIBUTED_STRING_IDENTITY)
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(description = "服务配置")
public class TravelConfig extends BaseSortModel<String> {

    @NotBlank(message = "配置代码不能为空")
    @JsonProperty("code")
    @ApiModelProperty(notes = "配置代码")
    private String code;

    @NotBlank(message = "配置不能为空")
    @JsonProperty("name")
    @ApiModelProperty(notes = "配置")
    private String name;
}
