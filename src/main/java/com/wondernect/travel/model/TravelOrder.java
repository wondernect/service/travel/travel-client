package com.wondernect.travel.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.base.model.BaseStringModel;
import com.wondernect.elements.rdb.config.IDGenerateor;
import com.wondernect.travel.model.em.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: Order
 * Author: chenxun
 * Date: 2019/5/10 10:44
 * Description:
 */
@Entity
@Table(name = "travel_order")
@GenericGenerator(name = "id", strategy = IDGenerateor.DISTRIBUTED_STRING_IDENTITY)
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(description = "订单")
public class TravelOrder extends BaseStringModel {

    @NotNull(message = "用车服务不能为空")
    @Enumerated(EnumType.STRING)
    @JsonProperty("scene")
    @ApiModelProperty(notes = "用车服务")
    private Scene scene;

    @NotNull(message = "用车类型不能为空")
    @Enumerated(EnumType.STRING)
    @JsonProperty("common_scene")
    @ApiModelProperty(notes = "用车类型")
    private CommonScene commonScene;

    @JsonProperty("consume_id")
    @ApiModelProperty(notes = "接机、送机、按天包车、景点、美食价格模板服务id")
    private String consumeId;

    @JsonProperty("private_consume_id")
    @ApiModelProperty(notes = "线路包车、景点、美食、伴手礼服务id")
    private String privateConsumeId;

    @NotNull(message = "订单来源不能为空")
    @Enumerated(EnumType.STRING)
    @JsonProperty("order_source")
    @ApiModelProperty(notes = "订单来源")
    private OrderSource orderSource;

    @Enumerated(EnumType.STRING)
    @JsonProperty("order_status")
    @ApiModelProperty(notes = "订单状态")
    private OrderStatus orderStatus;

    @Transient
    @JsonProperty("order_status_int")
    @ApiModelProperty(notes = "订单状态(数值)")
    private Integer orderStatusInt;

    @Enumerated(EnumType.STRING)
    @JsonProperty("warning_status")
    @ApiModelProperty(notes = "紧急状态")
    private WarningStatus warningStatus;

    @JsonProperty("username")
    @ApiModelProperty(notes = "乘车人姓名")
    private String username;

    @JsonProperty("mobile")
    @ApiModelProperty(notes = "联系电话")
    private String mobile;

    @JsonProperty("contact")
    @ApiModelProperty(notes = "紧急联系人")
    private String contact;

    @JsonProperty("contact_mobile")
    @ApiModelProperty(notes = "紧急联系人电话")
    private String contactMobile;

    @JsonProperty("start_time")
    @ApiModelProperty(notes = "上车时间")
    private Long startTime;

    @JsonProperty("start_place")
    @ApiModelProperty(notes = "上车地点")
    private String startPlace;

    @JsonProperty("start_longitude")
    @ApiModelProperty(notes = "上车地点经度")
    private Double startLongitude;

    @JsonProperty("start_latitude")
    @ApiModelProperty(notes = "上车地点纬度")
    private Double startLatitude;

    @JsonProperty("target_place")
    @ApiModelProperty(notes = "目的地")
    private String targetPlace;

    @JsonProperty("target_longitude")
    @ApiModelProperty(notes = "目的地经度")
    private Double targetLongitude;

    @JsonProperty("target_latitude")
    @ApiModelProperty(notes = "目的地纬度")
    private Double targetLatitude;

    @JsonProperty("air_no")
    @ApiModelProperty(notes = "航班号")
    private String airNo;

    @JsonProperty("remark")
    @ApiModelProperty(notes = "备注")
    private String remark;

    @JsonProperty("city_id")
    @ApiModelProperty(notes = "城市id")
    private String cityId;

    @JsonProperty("zuowei_id")
    @ApiModelProperty(notes = "座位id")
    private String zuoweiId;

    @JsonProperty("chexing_id")
    @ApiModelProperty(notes = "车型id")
    private String chexingId;

    @JsonProperty("kilo")
    @ApiModelProperty(notes = "订单总公里数")
    private Double kilo;

    @JsonProperty("time")
    @ApiModelProperty(notes = "订单总用时")
    private Double time;

    @JsonProperty("days")
    @ApiModelProperty(notes = "包车天数")
    private Double days;

    @JsonProperty("count")
    @ApiModelProperty(notes = "伴手礼数量")
    private Double count;

    @NotNull(message = "订单总金额不能为空")
    @JsonProperty("total_price")
    @ApiModelProperty(value = "订单总金额")
    private Double totalPrice;

    @JsonProperty("coupon_id")
    @ApiModelProperty(notes = "优惠券id")
    private String couponId;

    @JsonProperty("coupon_price")
    @ApiModelProperty(value = "优惠金额")
    private Double couponPrice;

    @NotNull(message = "订单金额不能为空")
    @JsonProperty("price")
    @ApiModelProperty(notes = "订单金额")
    private Double price;

    @JsonProperty("refund_fee")
    @ApiModelProperty(notes = "退款手续费")
    private Double refundFee;

    @JsonProperty("shop_id")
    @ApiModelProperty(notes = "商家id")
    private String shopId;

    @JsonProperty("shop_name")
    @ApiModelProperty(notes = "商家名称")
    private String shopName;

    @JsonProperty("user_id")
    @ApiModelProperty(notes = "下单用户id")
    private String userId;

    @JsonProperty("user_mobile")
    @ApiModelProperty(notes = "下单用户手机号码")
    private String userMobile;

    @JsonProperty("open_id")
    @ApiModelProperty(notes = "下单用户微信openId")
    private String openId;

    @JsonProperty("user_wxname")
    @ApiModelProperty(notes = "下单用户微信名")
    private String userWXName;

    @JsonProperty("driver_user_id")
    @ApiModelProperty(notes = "服务司机id")
    private String driverUserId;

    @JsonProperty("driver_user_name")
    @ApiModelProperty(notes = "服务司机姓名")
    private String driverUserName;

    @JsonProperty("driver_mobile")
    @ApiModelProperty(notes = "服务司机手机号码")
    private String driverMobile;

    @JsonProperty("driver_car_no")
    @ApiModelProperty(notes = "服务司机车牌号")
    private String driverCarNo;

    @JsonProperty("before_use_dispatch_hour")
    @ApiModelProperty(notes = "用车前多少小时,自动派单")
    private Double beforeUseDispatchHour;

    @JsonProperty("auto_dispatch_time")
    @ApiModelProperty(notes = "自动派单时间")
    private Long autoDispatchTime;

    @JsonProperty("force_accept")
    @ApiModelProperty(notes = "是否后台指派订单")
    private Boolean forceAccept;

    @JsonProperty("accepted_time")
    @ApiModelProperty(notes = "司机接单时间")
    private Long acceptedTime;

    @JsonProperty("execute_time")
    @ApiModelProperty(notes = "订单开始时间")
    private Long executeTime;

    @JsonProperty("done_time")
    @ApiModelProperty(notes = "订单完成时间")
    private Long doneTime;

    @JsonProperty("evaluate")
    @ApiModelProperty(notes = "订单是否评价")
    private Boolean evaluate = false;

    @JsonProperty("cancel_time")
    @ApiModelProperty(notes = "订单取消时间")
    private Long cancelTime;

    @JsonProperty("settled_time")
    @ApiModelProperty(notes = "订单结算时间")
    private Long settledTime;

    @JsonProperty("source_shop_id")
    @ApiModelProperty(notes = "来源商家id")
    private String sourceShopId;

    @JsonProperty("source_shop_name")
    @ApiModelProperty(notes = "来源商家名称")
    private String sourceShopName;

    @JsonProperty("source_driver_user_id")
    @ApiModelProperty(notes = "来源司机用户id")
    private String sourceDriverUserId;

    @JsonProperty("source_driver_user_name")
    @ApiModelProperty(notes = "来源司机用户名称")
    private String sourceDriverUserName;

    @JsonProperty("wechat_order_id")
    @ApiModelProperty(notes = "微信统一下单订单号")
    private String wechatOrderId;

    @JsonProperty("wechat_timestamp")
    @ApiModelProperty(notes = "微信统一下单时间戳")
    private String wechatTimestamp;

    @JsonProperty("wechat_nonce_str")
    @ApiModelProperty(notes = "微信统一下单随机字符串")
    private String wechatNonceStr;

    @JsonProperty("wechat_pay_sign")
    @ApiModelProperty(notes = "微信统一下单签名")
    private String wechatPaySign;

    @JsonProperty("receive_name")
    @ApiModelProperty(value = "收货姓名")
    private String receiveName;

    @JsonProperty("receive_mobile")
    @ApiModelProperty(value = "收货电话")
    private String receiveMobile;

    @JsonProperty("receive_address")
    @ApiModelProperty(notes = "收货地址")
    private String receiveAddress;

    @JsonProperty("express_number")
    @ApiModelProperty(notes = "快递单号")
    private String expressNumber;

    @JsonProperty("bill_id")
    @ApiModelProperty(notes = "发票id")
    private String billId;
}
