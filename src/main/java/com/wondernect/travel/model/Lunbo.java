package com.wondernect.travel.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.base.model.BaseSortModel;
import com.wondernect.elements.rdb.config.IDGenerateor;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: Lunbo
 * Author: chenxun
 * Date: 2019-10-04 08:02
 * Description: 轮播图
 */
@Entity
@Table(name = "lunbo")
@GenericGenerator(name = "id", strategy = IDGenerateor.DISTRIBUTED_STRING_IDENTITY)
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(description = "轮播图")
public class Lunbo extends BaseSortModel<String> {

    @NotBlank(message = "轮播图片不能为空")
    @Lob
    @JsonProperty("images")
    @ApiModelProperty(notes = "轮播图片")
    private String images;

    @NotBlank(message = "跳转链接不能为空")
    @JsonProperty("link")
    @ApiModelProperty(notes = "跳转链接")
    private String link;
}
