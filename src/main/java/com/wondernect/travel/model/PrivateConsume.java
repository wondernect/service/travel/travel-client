package com.wondernect.travel.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.base.model.BaseSortModel;
import com.wondernect.elements.rdb.config.IDGenerateor;
import com.wondernect.travel.model.em.CommonScene;
import com.wondernect.travel.model.em.Scene;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: PrivateConsume
 * Author: chenxun
 * Date: 2019/9/23 7:58
 * Description: 包车服务
 */
@Entity
@Table(name = "private_consume")
@GenericGenerator(name = "id", strategy = IDGenerateor.DISTRIBUTED_STRING_IDENTITY)
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(description = "包车服务配置(适用于线路包车&景点美食伴手礼)")
public class PrivateConsume extends BaseSortModel<String> {

    @NotBlank(message = "城市id不能为空")
    @JsonProperty("city_id")
    @ApiModelProperty(notes = "城市id")
    private String cityId;

    @NotNull(message = "用车场景不能为空")
    @Enumerated(EnumType.STRING)
    @JsonProperty("scene")
    @ApiModelProperty(notes = "用车场景")
    private Scene scene;

    @NotNull(message = "用车类型不能为空")
    @Enumerated(EnumType.STRING)
    @JsonProperty("common_scene")
    @ApiModelProperty(notes = "用车类型")
    private CommonScene commonScene;

    @NotBlank(message = "名称不能为空")
    @JsonProperty("name")
    @ApiModelProperty(notes = "名称")
    private String name;

    @JsonProperty("tag")
    @ApiModelProperty(notes = "线路标签")
    private String tag;

    @JsonProperty("show_days")
    @ApiModelProperty(notes = "预约时间XX天内")
    private Integer showDays;

    @JsonProperty("start_time")
    @ApiModelProperty(notes = "出发开始时间")
    private Long startTime;

    @JsonProperty("end_time")
    @ApiModelProperty(notes = "出发结束时间")
    private Long endTime;

    @JsonProperty("price")
    @ApiModelProperty(notes = "价格")
    private Double price;

    @Lob
    @JsonProperty("reason")
    @ApiModelProperty(notes = "推荐理由")
    private String reason;

    @Lob
    @JsonProperty("description")
    @ApiModelProperty(value = "详细介绍")
    private String description;

    @Lob
    @JsonProperty("images")
    @ApiModelProperty(notes = "线路图片")
    private String images;

    @Lob
    @JsonProperty("promise")
    @ApiModelProperty(notes = "成团承诺")
    private String promise;

    @Lob
    @JsonProperty("price_include")
    @ApiModelProperty(notes = "费用包含")
    private String priceInclude;

    @Lob
    @JsonProperty("price_exclude")
    @ApiModelProperty(notes = "费用不含")
    private String priceExclude;

    @Lob
    @JsonProperty("valid_time")
    @ApiModelProperty(value = "有效期")
    private String validTime;

    @Lob
    @JsonProperty("advance_notice")
    @ApiModelProperty(notes = "预定须知")
    private String advanceNotice;

    @Lob
    @JsonProperty("refund_rule")
    @ApiModelProperty(notes = "退订规则")
    private String refundRule;

    @JsonProperty("target_place")
    @ApiModelProperty(notes = "目的地")
    private String targetPlace;

    @JsonProperty("target_longitude")
    @ApiModelProperty(notes = "目的地经度")
    private Double targetLongitude;

    @JsonProperty("target_latitude")
    @ApiModelProperty(notes = "目的地纬度")
    private Double targetLatitude;

    @JsonProperty("showPrice")
    @ApiModelProperty(value = "展示价格")
    private String showPrice;

    @JsonProperty("evaluate_score")
    @ApiModelProperty(value = "评分")
    private Integer evaluateScore;

    @JsonProperty("evaluate_count")
    @ApiModelProperty(value = "点评数")
    private Integer evaluateCount;

    @JsonProperty("recommend_count")
    @ApiModelProperty(value = "推荐数")
    private Integer recommendCount;
}
