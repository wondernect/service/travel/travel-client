package com.wondernect.travel.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.base.model.BaseStringModel;
import com.wondernect.elements.rdb.config.IDGenerateor;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: UserShop
 * Author: chenxun
 * Date: 2019/4/24 16:23
 * Description:
 */
@Entity
@Table(name = "user_shop")
@GenericGenerator(name = "id", strategy = IDGenerateor.DISTRIBUTED_STRING_IDENTITY)
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(description = "店铺员工")
public class UserShop extends BaseStringModel {

    @JsonProperty("user_id")
    @ApiModelProperty(notes = "员工id")
    private String userId;

    @JsonProperty("shop_id")
    @ApiModelProperty(notes = "店铺id")
    private String shopId;
}
