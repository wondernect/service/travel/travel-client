package com.wondernect.travel.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.base.model.BaseStringModel;
import com.wondernect.elements.rdb.config.IDGenerateor;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: Car
 * Author: chenxun
 * Date: 2019/4/24 15:38
 * Description:
 */
@Entity
@Table(name = "car")
@GenericGenerator(name = "id", strategy = IDGenerateor.DISTRIBUTED_STRING_IDENTITY)
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(description = "车辆")
public class Car extends BaseStringModel {

    @NotBlank(message = "城市id不能为空")
    @JsonProperty("city_id")
    @ApiModelProperty(notes = "城市id")
    private String cityId;

    @NotBlank(message = "车牌号不能为空")
    @JsonProperty("car_no")
    @ApiModelProperty(notes = "车牌号")
    private String carNo;

    @NotBlank(message = "座位id不能为空")
    @JsonProperty("zuowei_id")
    @ApiModelProperty(value = "座位id")
    private String zuoweiId;

    @NotBlank(message = "车型id不能为空")
    @JsonProperty("chexing_id")
    @ApiModelProperty(value = "车型id")
    private String chexingId;

    @JsonProperty("color")
    @ApiModelProperty(notes = "颜色")
    private String color;

    @NotBlank(message = "司机不能为空")
    @JsonProperty("driver_user_id")
    @ApiModelProperty(notes = "司机对应用户id")
    private String driverUserId;
}
