package com.wondernect.travel.business.remen.controller;

import com.wondernect.elements.common.error.BusinessError;
import com.wondernect.elements.common.response.BusinessData;
import com.wondernect.elements.rdb.criteria.Criteria;
import com.wondernect.elements.rdb.criteria.Restrictions;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.travel.business.remen.dto.ListHotSearchRequestDTO;
import com.wondernect.travel.business.remen.dto.PageHotSearchRequestDTO;
import com.wondernect.travel.business.remen.manager.HotSearchManager;
import com.wondernect.travel.business.remen.model.HotSearch;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 热门搜索接口
 *
 * @author chenxun 2020-07-06 18:28:10
 **/
@RequestMapping(value = "/v5/travel/hot_search")
@RestController
@Validated
@Api(tags = "热门搜索接口")
public class HotSearchController {

    @Autowired
    private HotSearchManager hotSearchManager;

    @ApiOperation(value = "创建/更新", httpMethod = "POST")
    @PostMapping(value = "/save")
    public BusinessData<HotSearch> create(
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @Validated @RequestBody(required = false) HotSearch hotSearch
    ) {
        return new BusinessData<>(hotSearchManager.save(hotSearch));
    }

    @ApiOperation(value = "删除", httpMethod = "POST")
    @PostMapping(value = "/{id}/delete")
    public BusinessData delete(
            @ApiParam(required = true) @NotBlank(message = "对象id不能为空") @PathVariable(value = "id", required = false) String id
    ) {
        hotSearchManager.deleteById(id);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @ApiOperation(value = "获取详细信息", httpMethod = "GET")
    @GetMapping(value = "/{id}/detail")
    public BusinessData<HotSearch> detail(
            @ApiParam(required = true) @NotBlank(message = "对象id不能为空") @PathVariable(value = "id", required = false) String id
    ) {
        return new BusinessData<>(hotSearchManager.findById(id));
    }

    @ApiOperation(value = "列表", httpMethod = "POST")
    @PostMapping(value = "/list")
    public BusinessData<List<HotSearch>> list(
            @ApiParam(required = true) @NotNull(message = "列表请求参数不能为空") @Validated @RequestBody(required = false) ListHotSearchRequestDTO listHotSearchRequestDTO
    ) {
        Criteria<HotSearch> hotSearchCriteria = new Criteria<>();
        hotSearchCriteria.add(Restrictions.eq("cityId", listHotSearchRequestDTO.getCityId()));
        return new BusinessData<>(hotSearchManager.findAll(hotSearchCriteria, listHotSearchRequestDTO.getSortDataList()));
    }

    @ApiOperation(value = "分页", httpMethod = "POST")
    @PostMapping(value = "/page")
    public BusinessData<PageResponseData<HotSearch>> page(
            @ApiParam(required = true) @NotNull(message = "分页请求参数不能为空") @Validated @RequestBody(required = false) PageHotSearchRequestDTO pageHotSearchRequestDTO
    ) {
        Criteria<HotSearch> hotSearchCriteria = new Criteria<>();
        hotSearchCriteria.add(Restrictions.eq("cityId", pageHotSearchRequestDTO.getCityId()));
        return new BusinessData<>(hotSearchManager.findAll(hotSearchCriteria, pageHotSearchRequestDTO.getPageRequestData()));
    }
}