package com.wondernect.travel.business.remen.repository;

import com.wondernect.elements.rdb.base.repository.BaseStringRepository;
import com.wondernect.travel.business.remen.model.HotSearch;

/**
 * 热门搜索数据库操作类
 *
 * @author chenxun 2020-07-06 18:28:08
 **/
public interface HotSearchRepository extends BaseStringRepository<HotSearch> {
}