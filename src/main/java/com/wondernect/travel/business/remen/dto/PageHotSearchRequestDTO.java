package com.wondernect.travel.business.remen.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.request.PageRequestData;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * 热门搜索分页请求DTO
 *
 * @author chenxun 2020-07-06 18:28:09
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "热门搜索分页请求对象")
public class PageHotSearchRequestDTO {

    @JsonProperty("city_id")
    @ApiModelProperty(value = "城市id")
    private String cityId;

    @NotNull(message = "分页请求参数不能为空")
    @JsonProperty("page_request_data")
    @ApiModelProperty(notes = "分页请求参数")
    private PageRequestData pageRequestData;
}