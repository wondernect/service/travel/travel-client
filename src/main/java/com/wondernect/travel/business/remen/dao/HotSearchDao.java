package com.wondernect.travel.business.remen.dao;

import com.wondernect.elements.rdb.base.dao.BaseStringDao;
import com.wondernect.travel.business.remen.model.HotSearch;
import org.springframework.stereotype.Repository;

/**
 * 热门搜索数据库操作类
 *
 * @author chenxun 2020-07-06 18:28:09
 **/
@Repository
public class HotSearchDao extends BaseStringDao<HotSearch> {
}