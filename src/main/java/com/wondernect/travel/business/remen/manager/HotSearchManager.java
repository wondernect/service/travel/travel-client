package com.wondernect.travel.business.remen.manager;

import com.wondernect.elements.rdb.base.manager.BaseStringManager;
import com.wondernect.travel.business.remen.model.HotSearch;
import org.springframework.stereotype.Service;

/**
 * 热门搜索服务操作类
 *
 * @author chenxun 2020-07-06 18:28:09
 **/
@Service
public class HotSearchManager extends BaseStringManager<HotSearch> {
}