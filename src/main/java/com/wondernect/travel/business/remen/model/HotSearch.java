package com.wondernect.travel.business.remen.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.base.model.BaseStringModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

/**
 * Copyright (C), 2017-2020, wondernect.com
 * FileName: HotSearch
 * Author: chenxun
 * Date: 2020/6/13 14:40
 * Description: 热门搜索
 */
@Entity
@Table(name = "hot_search")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "热门搜索")
public class HotSearch extends BaseStringModel {

    @NotBlank(message = "城市id不能为空")
    @Column(columnDefinition = "varchar(255) not null")
    @JsonProperty("city_id")
    @ApiModelProperty(value = "城市id")
    private String cityId;

    @NotBlank(message = "名称不能为空")
    @Column(columnDefinition = "varchar(255) not null")
    @JsonProperty("name")
    @ApiModelProperty(value = "名称")
    private String name;
}
