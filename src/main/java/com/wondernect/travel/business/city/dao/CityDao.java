package com.wondernect.travel.business.city.dao;

import com.wondernect.elements.rdb.base.dao.BaseStringDao;
import com.wondernect.travel.business.city.model.City;
import org.springframework.stereotype.Repository;

/**
 * 城市数据库操作类
 *
 * @author chenxun 2020-07-06 18:24:34
 **/
@Repository
public class CityDao extends BaseStringDao<City> {
}