package com.wondernect.travel.business.city.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.request.SortData;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 城市列表请求DTO
 *
 * @author chenxun 2020-07-06 18:24:35
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "城市列表请求对象")
public class ListCityRequestDTO {

    @JsonProperty("sort_data_list")
    @ApiModelProperty(notes = "列表请求参数")
    private List<SortData> sortDataList;
}