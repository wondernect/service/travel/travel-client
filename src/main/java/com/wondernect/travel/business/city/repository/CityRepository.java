package com.wondernect.travel.business.city.repository;

import com.wondernect.elements.rdb.base.repository.BaseStringRepository;
import com.wondernect.travel.business.city.model.City;

/**
 * 城市数据库操作类
 *
 * @author chenxun 2020-07-06 18:24:34
 **/
public interface CityRepository extends BaseStringRepository<City> {
}