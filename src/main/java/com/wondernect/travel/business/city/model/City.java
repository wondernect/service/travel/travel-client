package com.wondernect.travel.business.city.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.base.model.BaseStringModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

/**
 * Copyright (C), 2017-2020, wondernect.com
 * FileName: City
 * Author: chenxun
 * Date: 2020/5/23 12:30
 * Description: 城市
 */
@Entity
@Table(name = "city")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "城市")
public class City extends BaseStringModel {

    @NotBlank(message = "名称不能为空")
    @Column(columnDefinition = "varchar(255) not null")
    @JsonProperty("name")
    @ApiModelProperty(value = "名称")
    private String name;
}
