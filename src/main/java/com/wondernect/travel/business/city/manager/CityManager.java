package com.wondernect.travel.business.city.manager;

import com.wondernect.elements.rdb.base.manager.BaseStringManager;
import com.wondernect.travel.business.city.model.City;
import org.springframework.stereotype.Service;

/**
 * 城市服务操作类
 *
 * @author chenxun 2020-07-06 18:24:34
 **/
@Service
public class CityManager extends BaseStringManager<City> {
}