package com.wondernect.travel.business.youhui.controller;

import com.wondernect.elements.authorize.context.interceptor.AuthorizeRoleType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeUserRole;
import com.wondernect.elements.common.error.BusinessError;
import com.wondernect.elements.common.exception.BusinessException;
import com.wondernect.elements.common.response.BusinessData;
import com.wondernect.elements.common.utils.ESDateTimeUtils;
import com.wondernect.elements.common.utils.ESStringUtils;
import com.wondernect.elements.rdb.criteria.Criteria;
import com.wondernect.elements.rdb.criteria.Restrictions;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.travel.business.youhui.dto.ListCouponPoolRequestDTO;
import com.wondernect.travel.business.youhui.dto.PageCouponPoolRequestDTO;
import com.wondernect.travel.business.youhui.manager.CouponPoolManager;
import com.wondernect.travel.business.youhui.model.CouponCategory;
import com.wondernect.travel.business.youhui.model.CouponPool;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;

/**
 * 优惠券池接口
 *
 * @author chenxun 2020-09-27 21:54:35
 **/
@RequestMapping(value = "/v5/travel/coupon_pool")
@RestController
@Validated
@Api(tags = "优惠券池接口")
public class CouponPoolController {

    @Autowired
    private CouponPoolManager couponPoolManager;

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "创建/更新", httpMethod = "POST")
    @PostMapping(value = "/create")
    public BusinessData<CouponPool> save(
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @Validated @RequestBody(required = false) CouponPool couponPool
    ) {
        if (ESStringUtils.isBlank(couponPool.getId())) {
            // 新增
            if (couponPool.getCouponCategory() == CouponCategory.XINYONGHU || couponPool.getCouponCategory() == CouponCategory.FANLI) {
                if (couponPoolManager.count() > 0) {
                    throw new BusinessException("优惠活动只能有一个，请直接编辑");
                }
            }
        }
        return new BusinessData<>(couponPoolManager.save(couponPool));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "删除", httpMethod = "POST")
    @PostMapping(value = "/{id}/delete")
    public BusinessData delete(
            @ApiParam(required = true) @NotBlank(message = "对象id不能为空") @PathVariable(value = "id", required = false) String id
    ) {
        couponPoolManager.deleteById(id);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "获取详细信息", httpMethod = "GET")
    @GetMapping(value = "/{id}/detail")
    public BusinessData<CouponPool> detail(
            @ApiParam(required = true) @NotBlank(message = "对象id不能为空") @PathVariable(value = "id", required = false) String id
    ) {
        return new BusinessData<>(couponPoolManager.findById(id));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "列表", httpMethod = "POST")
    @PostMapping(value = "/list")
    public BusinessData<List<CouponPool>> list(
            @ApiParam(required = true) @NotNull(message = "列表请求参数不能为空") @Validated @RequestBody(required = false) ListCouponPoolRequestDTO listCouponPoolRequestDTO
    ) {
        Criteria<CouponPool> couponPoolCriteria = new Criteria<>();
        couponPoolCriteria.add(Restrictions.eq("couponCategory", listCouponPoolRequestDTO.getCouponCategory()));
        if (listCouponPoolRequestDTO.getStatus() == 1) {
            Long currentTime = ESDateTimeUtils.getCurrentTimestamp();
            couponPoolCriteria.add(Restrictions.gt("startTime", currentTime));
            couponPoolCriteria.add(Restrictions.lt("endTime", currentTime));
        }
        if (CollectionUtils.isNotEmpty(listCouponPoolRequestDTO.getSortDataList())) {
            listCouponPoolRequestDTO.getSortDataList().add(new SortData("createTime", "DESC"));
        } else {
            listCouponPoolRequestDTO.setSortDataList(Arrays.asList(new SortData("createTime", "DESC")));
        }
        return new BusinessData<>(couponPoolManager.findAll(couponPoolCriteria, listCouponPoolRequestDTO.getSortDataList()));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "分页", httpMethod = "POST")
    @PostMapping(value = "/page")
    public BusinessData<PageResponseData<CouponPool>> page(
            @ApiParam(required = true) @NotNull(message = "分页请求参数不能为空") @Validated @RequestBody(required = false) PageCouponPoolRequestDTO pageCouponPoolRequestDTO
    ) {
        Criteria<CouponPool> couponPoolCriteria = new Criteria<>();
        couponPoolCriteria.add(Restrictions.eq("couponCategory", pageCouponPoolRequestDTO.getCouponCategory()));
        if (pageCouponPoolRequestDTO.getStatus() == 1) {
            Long currentTime = ESDateTimeUtils.getCurrentTimestamp();
            couponPoolCriteria.add(Restrictions.gt("startTime", currentTime));
            couponPoolCriteria.add(Restrictions.lt("endTime", currentTime));
        }
        if (CollectionUtils.isNotEmpty(pageCouponPoolRequestDTO.getPageRequestData().getSortDataList())) {
            pageCouponPoolRequestDTO.getPageRequestData().getSortDataList().add(new SortData("createTime", "DESC"));
        } else {
            pageCouponPoolRequestDTO.getPageRequestData().setSortDataList(Arrays.asList(new SortData("createTime", "DESC")));
        }
        return new BusinessData<>(couponPoolManager.findAll(couponPoolCriteria, pageCouponPoolRequestDTO.getPageRequestData()));
    }
}