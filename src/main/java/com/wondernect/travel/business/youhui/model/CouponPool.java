package com.wondernect.travel.business.youhui.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.base.model.BaseStringModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Copyright (C), 2017-2020, wondernect.com
 * FileName: CouponPool
 * Author: chenxun
 * Date: 2020/6/14 13:45
 * Description: 优惠券池
 */
@Entity
@Table(name = "coupon_pool")
@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "优惠券池")
public class CouponPool extends BaseStringModel {

    @NotNull(message = "优惠券分类不能为空")
    @Enumerated(EnumType.STRING)
    @JsonProperty("coupon_category")
    @ApiModelProperty(value = "优惠券分类")
    private CouponCategory couponCategory;

    @NotBlank(message = "优惠券名称不能为空")
    @JsonProperty("name")
    @ApiModelProperty(value = "优惠券名称")
    private String name;

    @NotNull(message = "自领取之日起多少天有效不能为空")
    @JsonProperty("days")
    @ApiModelProperty(value = "自领取之日起多少天有效")
    private Integer days;

    @NotNull(message = "满减金额不能为空")
    @JsonProperty("limit_price")
    @ApiModelProperty(value = "满减金额")
    private Double limitPrice;

    @NotNull(message = "优惠金额不能为空")
    @JsonProperty("price")
    @ApiModelProperty(value = "优惠金额")
    private Double price;

    @JsonProperty("fanli")
    @ApiModelProperty(value = "邀请者获得返利金额")
    private Double fanli;

    @JsonProperty("total")
    @ApiModelProperty(value = "优惠券剩余库存数量(不传或者传0代表没有限制)")
    private Integer total;

    @JsonProperty("receive")
    @ApiModelProperty(value = "优惠券已兑换数量")
    private Integer receive;

    @JsonProperty("start_time")
    @ApiModelProperty(value = "兑换开始时间(不传表示没有时间限制)")
    private Long startTime;

    @JsonProperty("end_time")
    @ApiModelProperty(value = "兑换结束时间(不传表示没有时间限制)")
    private Long endTime;

    public CouponPool() {
    }

    public CouponPool(CouponCategory couponCategory, String name, Integer days, Double limitPrice, Double price, Double fanli, Integer total, Long startTime, Long endTime) {
        this.couponCategory = couponCategory;
        this.name = name;
        this.days = days;
        this.limitPrice = limitPrice;
        this.price = price;
        this.fanli = fanli;
        this.total = total;
        this.receive = 0;
        this.startTime = startTime;
        this.endTime = endTime;
    }
}
