package com.wondernect.travel.business.youhui.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.travel.business.youhui.model.CouponCategory;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.List;

/**
 * 优惠券池列表请求DTO
 *
 * @author chenxun 2020-09-27 21:54:34
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "优惠券池列表请求对象")
public class ListCouponPoolRequestDTO {

    @Enumerated(EnumType.STRING)
    @JsonProperty("coupon_category")
    @ApiModelProperty(value = "优惠券分类")
    private CouponCategory couponCategory;

    @JsonProperty("status")
    @ApiModelProperty(value = "优惠券池状态（0-所有；1-当前可用;）")
    private int status = 0;

    @JsonProperty("sort_data_list")
    @ApiModelProperty(notes = "列表请求参数")
    private List<SortData> sortDataList;
}