package com.wondernect.travel.business.youhui.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.request.PageRequestData;
import com.wondernect.travel.business.youhui.model.CouponCategory;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

/**
 * 优惠券池分页请求DTO
 *
 * @author chenxun 2020-09-27 21:54:34
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "优惠券池分页请求对象")
public class PageCouponPoolRequestDTO {

    @Enumerated(EnumType.STRING)
    @JsonProperty("coupon_category")
    @ApiModelProperty(value = "优惠券分类")
    private CouponCategory couponCategory;

    @JsonProperty("status")
    @ApiModelProperty(value = "优惠券池状态（0-所有；1-当前可用;）")
    private int status = 0;

    @NotNull(message = "分页请求参数不能为空")
    @JsonProperty("page_request_data")
    @ApiModelProperty(notes = "分页请求参数")
    private PageRequestData pageRequestData;
}