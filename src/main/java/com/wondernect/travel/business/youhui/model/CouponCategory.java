package com.wondernect.travel.business.youhui.model;

/**
 * Copyright (C), 2017-2020, wondernect.com
 * FileName: FaxianCategory
 * Author: chenxun
 * Date: 2020/6/14 13:14
 * Description: 发现分类
 */
public enum CouponCategory {

    MANJIAN, // 通用满减优惠券

    XINYONGHU, // 新用户打车优惠券

    FANLI, // 邀请返利优惠券
}

