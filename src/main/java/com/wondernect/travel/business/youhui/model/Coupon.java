package com.wondernect.travel.business.youhui.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.base.model.BaseStringModel;
import com.wondernect.elements.rdb.config.IDGenerateor;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

/**
 * Copyright (C), 2017-2020, wondernect.com
 * FileName: Coupon
 * Author: chenxun
 * Date: 2020/6/14 13:39
 * Description: 优惠券
 */
@Entity
@Table(name = "coupon")
@GenericGenerator(name = "id", strategy = IDGenerateor.DISTRIBUTED_STRING_IDENTITY)
@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "优惠券")
public class Coupon extends BaseStringModel {

    @JsonProperty("coupon_pool_id")
    @ApiModelProperty(value = "优惠券池id")
    private String couponPoolId;

    @Enumerated(EnumType.STRING)
    @JsonProperty("coupon_category")
    @ApiModelProperty(value = "优惠券分类")
    private CouponCategory couponCategory;

    @JsonProperty("name")
    @ApiModelProperty(value = "优惠券名称")
    private String name;

    @JsonProperty("user_id")
    @ApiModelProperty(value = "邀请者id")
    private String userId;

    @JsonProperty("mobile")
    @ApiModelProperty(value = "手机号码")
    private String mobile;

    @JsonProperty("start_time")
    @ApiModelProperty(value = "开始时间")
    private Long startTime;

    @JsonProperty("end_time")
    @ApiModelProperty(value = "结束时间")
    private Long endTime;

    @JsonProperty("limit_price")
    @ApiModelProperty(value = "满减金额")
    private Double limitPrice;

    @JsonProperty("price")
    @ApiModelProperty(value = "优惠金额")
    private Double price;

    @JsonProperty("fanli")
    @ApiModelProperty(value = "邀请者获得返利金额")
    private Double fanli;

    @JsonProperty("has_used")
    @ApiModelProperty(value = "是否使用")
    private Boolean hasUsed;

    public Coupon() {
    }

    public Coupon(String couponPoolId, CouponCategory couponCategory, String name, String userId, String mobile, Long startTime, Long endTime, Double limitPrice, Double price, Double fanli) {
        this.couponPoolId = couponPoolId;
        this.couponCategory = couponCategory;
        this.name = name;
        this.userId = userId;
        this.mobile = mobile;
        this.startTime = startTime;
        this.endTime = endTime;
        this.limitPrice = limitPrice;
        this.price = price;
        this.fanli = fanli;
        this.hasUsed = false;
    }
}
