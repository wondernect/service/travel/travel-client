package com.wondernect.travel.business.youhui.controller;

import com.wondernect.elements.authorize.context.interceptor.AuthorizeRoleType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeUserRole;
import com.wondernect.elements.common.error.BusinessError;
import com.wondernect.elements.common.exception.BusinessException;
import com.wondernect.elements.common.response.BusinessData;
import com.wondernect.elements.common.utils.ESDateTimeUtils;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.rdb.criteria.Criteria;
import com.wondernect.elements.rdb.criteria.Restrictions;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.services.user.model.User;
import com.wondernect.services.user.service.UserService;
import com.wondernect.travel.business.youhui.dto.ListCouponRequestDTO;
import com.wondernect.travel.business.youhui.dto.PageCouponRequestDTO;
import com.wondernect.travel.business.youhui.manager.CouponManager;
import com.wondernect.travel.business.youhui.model.Coupon;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 优惠券接口
 *
 * @author chenxun 2020-09-27 21:54:59
 **/
@RequestMapping(value = "/v5/travel/coupon")
@RestController
@Validated
@Api(tags = "优惠券接口")
public class CouponController {

    @Autowired
    private UserService userService;

    @Autowired
    private CouponManager couponManager;

    @ApiOperation(value = "检查手机号码对应用户是否已领取优惠券", httpMethod = "GET")
    @GetMapping(value = "/obtain_check")
    public BusinessData<Boolean> checkObtain(
            @ApiParam(required = true) @NotBlank(message = "手机号码不能为空") @RequestParam(value = "mobile", required = false) String mobile,
            @ApiParam(required = true) @NotBlank(message = "优惠券池id不能为空") @RequestParam(value = "coupon_pool_id", required = false) String couponPoolId
    ) {
        return new BusinessData<>(couponManager.obtainCheck(mobile, couponPoolId));
    }

    @ApiOperation(value = "领取优惠券(user_id为分享者id)", httpMethod = "GET")
    @GetMapping(value = "/obtain")
    public BusinessData obtain(
            @ApiParam(required = true) @NotBlank(message = "手机号码不能为空") @RequestParam(value = "mobile", required = false) String mobile,
            @ApiParam(required = false) @RequestParam(value = "user_id", required = false) String userId,
            @ApiParam(required = true) @NotBlank(message = "优惠券池id不能为空") @RequestParam(value = "coupon_pool_id", required = false) String couponPoolId
    ) {
        couponManager.obtainCoupon(mobile, userId, couponPoolId);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "获取账单可用优惠券列表", httpMethod = "POST")
    @PostMapping(value = "/usable_list")
    public BusinessData<List<Coupon>> list(
            @ApiParam(required = true) @NotNull(message = "列表请求参数不能为空") @Validated @RequestBody(required = false) ListCouponRequestDTO listCouponRequestDTO
    ) {
        User user = userService.findByUserId(listCouponRequestDTO.getUserId());
        if (ESObjectUtils.isNull(user)) {
            throw new BusinessException("用户信息不存在");
        }
        Long currentTime = ESDateTimeUtils.getCurrentTimestamp();
        Criteria<Coupon> couponCriteria = new Criteria<>();
        couponCriteria.add(Restrictions.eq("mobile", user.getMobile()));
        couponCriteria.add(Restrictions.eq("hasUsed", false));
        couponCriteria.add(Restrictions.lte("limitPrice", listCouponRequestDTO.getPrice()));
        couponCriteria.add(Restrictions.lt("startTime", currentTime));
        couponCriteria.add(Restrictions.gt("endTime", currentTime));
        return new BusinessData<>(couponManager.findAll(couponCriteria, listCouponRequestDTO.getSortDataList()));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "我的优惠券分页(未使用、已使用、已过期)", httpMethod = "POST")
    @PostMapping(value = "/page")
    public BusinessData<PageResponseData<Coupon>> page(
            @ApiParam(required = true) @NotNull(message = "分页请求参数不能为空") @Validated @RequestBody(required = false) PageCouponRequestDTO pageCouponRequestDTO
    ) {
        User user = userService.findByUserId(pageCouponRequestDTO.getUserId());
        if (ESObjectUtils.isNull(user)) {
            throw new BusinessException("用户信息不存在");
        }
        Criteria<Coupon> couponCriteria = new Criteria<>();
        couponCriteria.add(Restrictions.eq("mobile", user.getMobile()));
        if (pageCouponRequestDTO.getStatus() == 1) {
            // 未使用
            couponCriteria.add(Restrictions.eq("hasUsed", false));
            Long currentTime = ESDateTimeUtils.getCurrentTimestamp();
            couponCriteria.add(Restrictions.lt("startTime", currentTime));
            couponCriteria.add(Restrictions.gt("endTime", currentTime));
        } else if (pageCouponRequestDTO.getStatus() == 2) {
            // 已使用
            couponCriteria.add(Restrictions.eq("hasUsed", true));
        } else if (pageCouponRequestDTO.getStatus() == 3) {
            // 已过期
            couponCriteria.add(Restrictions.eq("hasUsed", false));
            Long currentTime = ESDateTimeUtils.getCurrentTimestamp();
            couponCriteria.add(Restrictions.lt("endTime", currentTime));
        }
        return new BusinessData<>(couponManager.findAll(couponCriteria, pageCouponRequestDTO.getPageRequestData()));
    }
}