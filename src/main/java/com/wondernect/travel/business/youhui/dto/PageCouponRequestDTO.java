package com.wondernect.travel.business.youhui.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.request.PageRequestData;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * 优惠券分页请求DTO
 *
 * @author chenxun 2020-09-27 21:54:58
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "优惠券分页请求对象")
public class PageCouponRequestDTO {

    @JsonProperty("user_id")
    @ApiModelProperty(value = "当前登录用户id")
    private String userId;

    @JsonProperty("status")
    @ApiModelProperty(value = "优惠券池状态（0-所有；1-未使用;2-已使用;3-已过期;）")
    private int status = 0;

    @NotNull(message = "分页请求参数不能为空")
    @JsonProperty("page_request_data")
    @ApiModelProperty(notes = "分页请求参数")
    private PageRequestData pageRequestData;
}