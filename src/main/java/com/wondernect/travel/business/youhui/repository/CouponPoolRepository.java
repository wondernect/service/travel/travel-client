package com.wondernect.travel.business.youhui.repository;

import com.wondernect.elements.rdb.base.repository.BaseStringRepository;
import com.wondernect.travel.business.youhui.model.CouponPool;

/**
 * 优惠券池数据库操作类
 *
 * @author chenxun 2020-09-27 21:54:33
 **/
public interface CouponPoolRepository extends BaseStringRepository<CouponPool> {
}