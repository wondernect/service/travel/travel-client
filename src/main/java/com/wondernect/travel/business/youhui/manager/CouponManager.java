package com.wondernect.travel.business.youhui.manager;

import com.wondernect.elements.common.exception.BusinessException;
import com.wondernect.elements.common.utils.ESDateTimeUtils;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.common.utils.ESRegexUtils;
import com.wondernect.elements.rdb.base.manager.BaseStringManager;
import com.wondernect.elements.rdb.criteria.Criteria;
import com.wondernect.elements.rdb.criteria.Restrictions;
import com.wondernect.services.user.service.UserService;
import com.wondernect.travel.business.youhui.model.Coupon;
import com.wondernect.travel.business.youhui.model.CouponPool;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 优惠券服务操作类
 *
 * @author chenxun 2020-09-27 21:54:58
 **/
@Service
public class CouponManager extends BaseStringManager<Coupon> {

    @Autowired
    private UserService userService;

    @Autowired
    private CouponPoolManager couponPoolManager;

    public Coupon get(String mobile, String couponPoolId) {
        Criteria<Coupon> couponCriteria = new Criteria<>();
        couponCriteria.add(Restrictions.eq("couponPoolId", couponPoolId));
        couponCriteria.add(Restrictions.eq("mobile", mobile));
        List<Coupon> couponList = super.findAll(couponCriteria, new ArrayList<>());
        if (CollectionUtils.isEmpty(couponList)) {
            return null;
        }
        return couponList.get(0);
    }

    @Transactional
    public synchronized void obtainCoupon(String mobile, String userId, String couponPoolId) {
        if (!ESRegexUtils.isMobile(mobile)) {
            throw new BusinessException("请输入正确的手机号码");
        }
        CouponPool couponPool = couponPoolManager.findById(couponPoolId);
        if (ESObjectUtils.isNull(couponPool)) {
            throw new BusinessException("该活动已过期，谢谢您的参与");
        }
        if (ESObjectUtils.isNotNull(couponPool.getStartTime())) {
            if (ESObjectUtils.isNotNull(couponPool.getEndTime())) {
                Long currentTime = ESDateTimeUtils.getCurrentTimestamp();
                if (currentTime < couponPool.getStartTime()) {
                    throw new BusinessException("该活动尚未开始，敬请期待");
                } else if (currentTime > couponPool.getEndTime()) {
                    throw new BusinessException("该活动已过期，谢谢您的参与");
                }
            } else {
                Long currentTime = ESDateTimeUtils.getCurrentTimestamp();
                if (currentTime < couponPool.getStartTime()) {
                    throw new BusinessException("该活动尚未开始，敬请期待");
                }
            }
        } else {
            if (ESObjectUtils.isNotNull(couponPool.getEndTime())) {
                Long currentTime = ESDateTimeUtils.getCurrentTimestamp();
                if (currentTime > couponPool.getEndTime()) {
                    throw new BusinessException("该活动已过期，谢谢您的参与");
                }
            }
        }
        Coupon coupon = get(mobile, couponPoolId);
        if (ESObjectUtils.isNotNull(coupon)) {
            throw new BusinessException("您已参加过该活动");
        }
        int receiveCount = ESObjectUtils.isNotNull(couponPool.getReceive()) ? couponPool.getReceive() : 0;
        Integer receive = receiveCount + 1;
        if (ESObjectUtils.isNotNull(couponPool.getTotal()) && couponPool.getTotal() < receive) {
            throw new BusinessException("优惠券已领完，敬请期待下次活动");
        }
        Long startTime = ESDateTimeUtils.getCurrentTimestamp();
        Long endTime = startTime + couponPool.getDays() * 24 * 60 * 60 * 1000;
        super.save(new Coupon(couponPoolId, couponPool.getCouponCategory(), couponPool.getName(), userId, mobile, startTime, endTime, couponPool.getLimitPrice(), couponPool.getPrice(), couponPool.getFanli()));
        couponPool.setReceive(receive);
        couponPoolManager.save(couponPool);
    }

    @Transactional
    public boolean obtainCheck(String mobile, String couponPoolId) {
        if (!ESRegexUtils.isMobile(mobile)) {
            return true;
        }
        CouponPool couponPool = couponPoolManager.findById(couponPoolId);
        if (ESObjectUtils.isNull(couponPool)) {
            return true;
        }
        if (ESObjectUtils.isNotNull(couponPool.getStartTime())) {
            if (ESObjectUtils.isNotNull(couponPool.getEndTime())) {
                Long currentTime = ESDateTimeUtils.getCurrentTimestamp();
                if (currentTime < couponPool.getStartTime()) {
                    return true;
                } else if (currentTime > couponPool.getEndTime()) {
                    return true;
                }
            } else {
                Long currentTime = ESDateTimeUtils.getCurrentTimestamp();
                if (currentTime < couponPool.getStartTime()) {
                    return true;
                }
            }
        } else {
            if (ESObjectUtils.isNotNull(couponPool.getEndTime())) {
                Long currentTime = ESDateTimeUtils.getCurrentTimestamp();
                if (currentTime > couponPool.getEndTime()) {
                    return true;
                }
            }
        }
        Coupon coupon = get(mobile, couponPoolId);
        if (ESObjectUtils.isNotNull(coupon)) {
            return true;
        }
        return false;
    }
}