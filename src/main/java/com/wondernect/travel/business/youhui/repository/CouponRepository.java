package com.wondernect.travel.business.youhui.repository;

import com.wondernect.elements.rdb.base.repository.BaseStringRepository;
import com.wondernect.travel.business.youhui.model.Coupon;

/**
 * 优惠券数据库操作类
 *
 * @author chenxun 2020-09-27 21:54:58
 **/
public interface CouponRepository extends BaseStringRepository<Coupon> {
}