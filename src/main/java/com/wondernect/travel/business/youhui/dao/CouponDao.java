package com.wondernect.travel.business.youhui.dao;

import com.wondernect.elements.rdb.base.dao.BaseStringDao;
import com.wondernect.travel.business.youhui.model.Coupon;
import org.springframework.stereotype.Repository;

/**
 * 优惠券数据库操作类
 *
 * @author chenxun 2020-09-27 21:54:58
 **/
@Repository
public class CouponDao extends BaseStringDao<Coupon> {
}