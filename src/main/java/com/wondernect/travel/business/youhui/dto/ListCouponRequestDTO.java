package com.wondernect.travel.business.youhui.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.request.SortData;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 优惠券列表请求DTO
 *
 * @author chenxun 2020-09-27 21:54:58
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "优惠券列表请求对象")
public class ListCouponRequestDTO {

    @JsonProperty("user_id")
    @ApiModelProperty(value = "当前登录用户id")
    private String userId;

    @JsonProperty("price")
    @ApiModelProperty(value = "账单金额")
    private Double price;

    @JsonProperty("sort_data_list")
    @ApiModelProperty(notes = "列表请求参数")
    private List<SortData> sortDataList;
}