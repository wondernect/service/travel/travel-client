package com.wondernect.travel.business.youhui.manager;

import com.wondernect.elements.rdb.base.manager.BaseStringManager;
import com.wondernect.travel.business.youhui.model.CouponPool;
import org.springframework.stereotype.Service;

/**
 * 优惠券池服务操作类
 *
 * @author chenxun 2020-09-27 21:54:33
 **/
@Service
public class CouponPoolManager extends BaseStringManager<CouponPool> {
}