package com.wondernect.travel.business.youhui.dao;

import com.wondernect.elements.rdb.base.dao.BaseStringDao;
import com.wondernect.travel.business.youhui.model.CouponPool;
import org.springframework.stereotype.Repository;

/**
 * 优惠券池数据库操作类
 *
 * @author chenxun 2020-09-27 21:54:33
 **/
@Repository
public class CouponPoolDao extends BaseStringDao<CouponPool> {
}