package com.wondernect.travel.business.user.controller;

import com.wondernect.elements.authorize.context.interceptor.AuthorizeRoleType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeUserRole;
import com.wondernect.elements.common.error.BusinessError;
import com.wondernect.elements.common.response.BusinessData;
import com.wondernect.elements.rdb.criteria.Criteria;
import com.wondernect.elements.rdb.criteria.Restrictions;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.travel.business.user.dto.ListUserFapiaoRequestDTO;
import com.wondernect.travel.business.user.dto.PageUserFapiaoRequestDTO;
import com.wondernect.travel.business.user.manager.UserFapiaoManager;
import com.wondernect.travel.business.user.model.UserFapiao;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 用户地址接口
 *
 * @author chenxun 2020-07-06 18:29:45
 **/
@RequestMapping(value = "/v5/travel/user_fapiao")
@RestController
@Validated
@Api(tags = "用户发票抬头信息接口")
public class UserFapiaoController {

    @Autowired
    private UserFapiaoManager userFapiaoManager;

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "创建/更新", httpMethod = "POST")
    @PostMapping(value = "/save")
    public BusinessData<UserFapiao> create(
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @Validated @RequestBody(required = false) UserFapiao userFapiao
    ) {
        return new BusinessData<>(userFapiaoManager.save(userFapiao));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "删除", httpMethod = "POST")
    @PostMapping(value = "/{id}/delete")
    public BusinessData delete(
            @ApiParam(required = true) @NotBlank(message = "对象id不能为空") @PathVariable(value = "id", required = false) String id
    ) {
        userFapiaoManager.deleteById(id);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "获取详细信息", httpMethod = "GET")
    @GetMapping(value = "/{id}/detail")
    public BusinessData<UserFapiao> detail(
            @ApiParam(required = true) @NotBlank(message = "对象id不能为空") @PathVariable(value = "id", required = false) String id
    ) {
        return new BusinessData<>(userFapiaoManager.findById(id));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "列表", httpMethod = "POST")
    @PostMapping(value = "/list")
    public BusinessData<List<UserFapiao>> list(
            @ApiParam(required = true) @NotNull(message = "列表请求参数不能为空") @Validated @RequestBody(required = false) ListUserFapiaoRequestDTO listUserFapiaoRequestDTO
    ) {
        Criteria<UserFapiao> userFapiaoCriteria = new Criteria<>();
        userFapiaoCriteria.add(Restrictions.eq("userId", listUserFapiaoRequestDTO.getUserId()));
        return new BusinessData<>(userFapiaoManager.findAll(userFapiaoCriteria, listUserFapiaoRequestDTO.getSortDataList()));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "分页", httpMethod = "POST")
    @PostMapping(value = "/page")
    public BusinessData<PageResponseData<UserFapiao>> page(
            @ApiParam(required = true) @NotNull(message = "分页请求参数不能为空") @Validated @RequestBody(required = false) PageUserFapiaoRequestDTO pageUserFapiaoRequestDTO
    ) {
        Criteria<UserFapiao> userFapiaoCriteria = new Criteria<>();
        userFapiaoCriteria.add(Restrictions.eq("userId", pageUserFapiaoRequestDTO.getUserId()));
        return new BusinessData<>(userFapiaoManager.findAll(userFapiaoCriteria, pageUserFapiaoRequestDTO.getPageRequestData()));
    }
}