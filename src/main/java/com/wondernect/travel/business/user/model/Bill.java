package com.wondernect.travel.business.user.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.base.model.BaseStringModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

/**
 * Copyright (C), 2017-2020, wondernect.com
 * FileName: Bill
 * Author: chenxun
 * Date: 2020/6/14 14:11
 * Description: 发票
 */
@Entity
@Table(name = "bill")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "发票")
public class Bill extends BaseStringModel {

    @NotBlank(message = "用户id不能为空")
    @JsonProperty("user_id")
    @ApiModelProperty(value = "用户id")
    private String userId;

    @JsonProperty("user_name")
    @ApiModelProperty(value = "用户姓名")
    private String userName;

    @JsonProperty("fapiao_type")
    @ApiModelProperty(value = "抬头类型")
    private Integer fapiaoType;

    @JsonProperty("fapiao_name")
    @ApiModelProperty(value = "名称")
    private String fapiaoName;

    @JsonProperty("fapiao_num")
    @ApiModelProperty(value = "税务登记号码")
    private String fapiaoNum;

    @JsonProperty("fapiao_address")
    @ApiModelProperty(value = "注册场所地址")
    private String fapiaoAddress;

    @JsonProperty("fapiao_telphone")
    @ApiModelProperty(value = "注册场所电话")
    private String fapiaoTelphone;

    @JsonProperty("fapiao_bank")
    @ApiModelProperty(value = "开户银行")
    private String fapiaoBank;

    @JsonProperty("fapiao_card")
    @ApiModelProperty(value = "开户银行账号")
    private String fapiaoCard;

    @JsonProperty("express_name")
    @ApiModelProperty(value = "快递姓名")
    private String expressName;

    @JsonProperty("express_mobile")
    @ApiModelProperty(value = "快递电话")
    private String expressMobile;

    @JsonProperty("express_address")
    @ApiModelProperty(value = "快递地址")
    private String expressAddress;

    @JsonProperty("express_number")
    @ApiModelProperty(notes = "快递单号")
    private String expressNumber;

    @JsonProperty("price")
    @ApiModelProperty(value = "发票金额")
    private Double price;
}
