package com.wondernect.travel.business.user.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.base.model.BaseStringModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Copyright (C), 2017-2020, wondernect.com
 * FileName: UserDizhi
 * Author: chenxun
 * Date: 2020/6/13 17:34
 * Description: 用户地址
 */
@Entity
@Table(name = "user_fapiao")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "用户发票抬头信息")
public class UserFapiao extends BaseStringModel {

    @NotBlank(message = "用户id不能为空")
    @JsonProperty("user_id")
    @ApiModelProperty(value = "用户id")
    private String userId;

    @NotNull(message = "发票抬头类型不能为空")
    @JsonProperty("type")
    @ApiModelProperty(value = "抬头类型")
    private Integer type;

    @NotBlank(message = "名称不能为空")
    @JsonProperty("name")
    @ApiModelProperty(value = "名称")
    private String name;

    @JsonProperty("num")
    @ApiModelProperty(value = "税务登记号码")
    private String num;

    @JsonProperty("address")
    @ApiModelProperty(value = "注册场所地址")
    private String address;

    @JsonProperty("telphone")
    @ApiModelProperty(value = "注册场所电话")
    private String telphone;

    @JsonProperty("bank")
    @ApiModelProperty(value = "开户银行")
    private String bank;

    @JsonProperty("card")
    @ApiModelProperty(value = "开户银行账号")
    private String card;

    @JsonProperty("set_default")
    @ApiModelProperty(value = "设为默认地址")
    private Boolean setDefault = false;
}
