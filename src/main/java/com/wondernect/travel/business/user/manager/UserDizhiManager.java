package com.wondernect.travel.business.user.manager;

import com.wondernect.elements.rdb.base.manager.BaseStringManager;
import com.wondernect.elements.rdb.criteria.Criteria;
import com.wondernect.elements.rdb.criteria.Restrictions;
import com.wondernect.travel.business.user.model.UserDizhi;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户地址服务操作类
 *
 * @author chenxun 2020-07-06 18:29:44
 **/
@Service
public class UserDizhiManager extends BaseStringManager<UserDizhi> {

    @Override
    public UserDizhi save(UserDizhi userDizhi) {
        if (userDizhi.getSetDefault()) {
            Criteria<UserDizhi> userDizhiCriteria = new Criteria<>();
            userDizhiCriteria.add(Restrictions.eq("userId", userDizhi.getUserId()));
            userDizhiCriteria.add(Restrictions.eq("setDefault", true));
            List<UserDizhi> userDizhiList = super.findAll(userDizhiCriteria, new ArrayList<>());
            if (CollectionUtils.isNotEmpty(userDizhiList)) {
                for (UserDizhi userDizhi1 : userDizhiList) {
                    userDizhi1.setSetDefault(false);
                    super.save(userDizhi1);
                }
            }
        }
        return super.save(userDizhi);
    }
}