package com.wondernect.travel.business.user.dao;

import com.wondernect.elements.rdb.base.dao.BaseDao;
import com.wondernect.travel.business.user.model.Wallet;
import org.springframework.stereotype.Repository;

/**
 * 钱包数据库操作类
 *
 * @author chenxun 2020-07-08 17:46:05
 **/
@Repository
public class WalletDao extends BaseDao<Wallet, String> {
}