package com.wondernect.travel.business.user.repository;

import com.wondernect.elements.rdb.base.repository.BaseRepository;
import com.wondernect.travel.business.user.model.Wallet;

/**
 * 钱包数据库操作类
 *
 * @author chenxun 2020-07-08 17:46:05
 **/
public interface WalletRepository extends BaseRepository<Wallet, String> {
}