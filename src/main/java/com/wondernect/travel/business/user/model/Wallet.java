package com.wondernect.travel.business.user.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.base.model.BaseModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Copyright (C), 2017-2020, wondernect.com
 * FileName: Wallet
 * Author: chenxun
 * Date: 2020/6/14 14:21
 * Description: 钱包
 */
@Entity
@Table(name = "wallet")
@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "钱包")
public class Wallet extends BaseModel {

    @Id
    @JsonProperty("user_id")
    @ApiModelProperty(value = "用户id")
    private String userId;

    @JsonProperty("money")
    @ApiModelProperty(value = "钱包金额")
    private Double money;

    @JsonProperty("limit_money")
    @ApiModelProperty(value = "提现下限")
    private Double limitMoney;

    public Wallet() {
    }

    public Wallet(String userId, Double money) {
        this.userId = userId;
        this.money = money;
        this.limitMoney = 100d;
    }
}
