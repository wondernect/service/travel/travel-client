package com.wondernect.travel.business.user.dao;

import com.wondernect.elements.rdb.base.dao.BaseStringDao;
import com.wondernect.travel.business.user.model.UserDizhi;
import org.springframework.stereotype.Repository;

/**
 * 用户地址数据库操作类
 *
 * @author chenxun 2020-07-06 18:29:44
 **/
@Repository
public class UserDizhiDao extends BaseStringDao<UserDizhi> {
}