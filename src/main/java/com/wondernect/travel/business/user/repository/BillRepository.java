package com.wondernect.travel.business.user.repository;

import com.wondernect.elements.rdb.base.repository.BaseStringRepository;
import com.wondernect.travel.business.user.model.Bill;

/**
 * 用户地址数据库操作类
 *
 * @author chenxun 2020-07-06 18:29:44
 **/
public interface BillRepository extends BaseStringRepository<Bill> {
}