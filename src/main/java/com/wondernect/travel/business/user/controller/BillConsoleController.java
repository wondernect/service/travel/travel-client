package com.wondernect.travel.business.user.controller;

import com.wondernect.elements.authorize.context.interceptor.AuthorizeRoleType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeUserRole;
import com.wondernect.elements.common.error.BusinessError;
import com.wondernect.elements.common.response.BusinessData;
import com.wondernect.elements.rdb.criteria.Criteria;
import com.wondernect.elements.rdb.criteria.Restrictions;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.travel.business.user.dto.PageBillRequestDTO;
import com.wondernect.travel.business.user.dto.SaveExpressNumberRequestDTO;
import com.wondernect.travel.business.user.manager.BillManager;
import com.wondernect.travel.business.user.model.Bill;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Arrays;

/**
 * 用户地址接口
 *
 * @author chenxun 2020-07-06 18:29:45
 **/
@RequestMapping(value = "/v5/travel/console/bill")
@RestController
@Validated
@Api(tags = "用户开具发票接口(后台管理)")
public class BillConsoleController {

    @Autowired
    private BillManager billManager;

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "更新快递单号", httpMethod = "POST")
    @PostMapping(value = "/save_express_number")
    public BusinessData<Bill> saveExpressNumber(
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @Validated @RequestBody(required = false) SaveExpressNumberRequestDTO saveExpressNumberRequestDTO
    ) {
        return new BusinessData<>(billManager.saveExpressNumber(saveExpressNumberRequestDTO.getBillId(), saveExpressNumberRequestDTO.getExpressNumber()));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "删除发票信息", httpMethod = "POST")
    @PostMapping(value = "/{id}/delete")
    public BusinessData delete(
            @ApiParam(required = true) @NotBlank(message = "对象id不能为空") @PathVariable(value = "id", required = false) String id
    ) {
        billManager.deleteBill(id);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "获取详细信息", httpMethod = "GET")
    @GetMapping(value = "/{id}/detail")
    public BusinessData<Bill> detail(
            @ApiParam(required = true) @NotBlank(message = "对象id不能为空") @PathVariable(value = "id", required = false) String id
    ) {
        return new BusinessData<>(billManager.findById(id));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "分页", httpMethod = "POST")
    @PostMapping(value = "/page")
    public BusinessData<PageResponseData<Bill>> page(
            @ApiParam(required = true) @NotNull(message = "分页请求参数不能为空") @Validated @RequestBody(required = false) PageBillRequestDTO pageBillRequestDTO
    ) {
        Criteria<Bill> billCriteria = new Criteria<>();
        billCriteria.add(Restrictions.eq("userId", pageBillRequestDTO.getUserId()));
        if (CollectionUtils.isEmpty(pageBillRequestDTO.getPageRequestData().getSortDataList())) {
            pageBillRequestDTO.getPageRequestData().setSortDataList(Arrays.asList(new SortData("createTime", "DESC")));
        }
        return new BusinessData<>(billManager.findAll(billCriteria, pageBillRequestDTO.getPageRequestData()));
    }
}