package com.wondernect.travel.business.user.manager;

import com.wondernect.elements.rdb.base.manager.BaseStringManager;
import com.wondernect.elements.rdb.criteria.Criteria;
import com.wondernect.elements.rdb.criteria.Restrictions;
import com.wondernect.travel.business.user.model.UserFapiao;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户地址服务操作类
 *
 * @author chenxun 2020-07-06 18:29:44
 **/
@Service
public class UserFapiaoManager extends BaseStringManager<UserFapiao> {

    @Override
    public UserFapiao save(UserFapiao userFapiao) {
        if (userFapiao.getSetDefault()) {
            Criteria<UserFapiao> userFapiaoCriteria = new Criteria<>();
            userFapiaoCriteria.add(Restrictions.eq("userId", userFapiao.getUserId()));
            userFapiaoCriteria.add(Restrictions.eq("setDefault", true));
            List<UserFapiao> userFapiaoList = super.findAll(userFapiaoCriteria, new ArrayList<>());
            if (CollectionUtils.isNotEmpty(userFapiaoList)) {
                for (UserFapiao userFapiao1 : userFapiaoList) {
                    userFapiao1.setSetDefault(false);
                    super.save(userFapiao1);
                }
            }
        }
        return super.save(userFapiao);
    }
}