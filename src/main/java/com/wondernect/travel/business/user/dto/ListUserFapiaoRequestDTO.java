package com.wondernect.travel.business.user.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.request.SortData;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 用户地址列表请求DTO
 *
 * @author chenxun 2020-07-06 18:29:44
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "用户发票抬头列表请求对象")
public class ListUserFapiaoRequestDTO {

    @JsonProperty("user_id")
    @ApiModelProperty(value = "用户id")
    private String userId;

    @JsonProperty("sort_data_list")
    @ApiModelProperty(notes = "列表请求参数")
    private List<SortData> sortDataList;
}