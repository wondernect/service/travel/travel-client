package com.wondernect.travel.business.user.controller;

import com.wondernect.elements.authorize.context.WondernectCommonContext;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeRoleType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeUserRole;
import com.wondernect.elements.common.response.BusinessData;
import com.wondernect.elements.common.utils.ESStringUtils;
import com.wondernect.elements.rdb.criteria.Criteria;
import com.wondernect.elements.rdb.criteria.Restrictions;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.travel.business.user.dto.BillCreateRequestDTO;
import com.wondernect.travel.business.user.dto.PageBillRequestDTO;
import com.wondernect.travel.business.user.manager.BillManager;
import com.wondernect.travel.business.user.model.Bill;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Arrays;

/**
 * 用户地址接口
 *
 * @author chenxun 2020-07-06 18:29:45
 **/
@RequestMapping(value = "/v5/travel/bill")
@RestController
@Validated
@Api(tags = "用户开具发票接口")
public class BillController {

    @Autowired
    private BillManager billManager;

    @Autowired
    private WondernectCommonContext wondernectCommonContext;

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "可开具发票总金额", httpMethod = "GET")
    @GetMapping(value = "/total_price")
    public BusinessData<Double> totalPrice() {
        return new BusinessData<>(billManager.getTotalPrice(wondernectCommonContext.getAuthorizeData().getUserId()));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "开具发票", httpMethod = "POST")
    @PostMapping(value = "/create")
    public BusinessData<Bill> create(
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @Validated @RequestBody(required = false) BillCreateRequestDTO billCreateRequestDTO
    ) {
        return new BusinessData<>(billManager.createBill(wondernectCommonContext.getAuthorizeData().getUserId(), billCreateRequestDTO.getUserFapiaoId(), billCreateRequestDTO.getUserDizhiId(), billCreateRequestDTO.getOrderIdList()));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "获取详细信息", httpMethod = "GET")
    @GetMapping(value = "/{id}/detail")
    public BusinessData<Bill> detail(
            @ApiParam(required = true) @NotBlank(message = "对象id不能为空") @PathVariable(value = "id", required = false) String id
    ) {
        return new BusinessData<>(billManager.findById(id));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "分页", httpMethod = "POST")
    @PostMapping(value = "/page")
    public BusinessData<PageResponseData<Bill>> page(
            @ApiParam(required = true) @NotNull(message = "分页请求参数不能为空") @Validated @RequestBody(required = false) PageBillRequestDTO pageBillRequestDTO
    ) {
        if (ESStringUtils.isBlank(pageBillRequestDTO.getUserId())) {
            pageBillRequestDTO.setUserId(wondernectCommonContext.getAuthorizeData().getUserId());
        }
        Criteria<Bill> billCriteria = new Criteria<>();
        billCriteria.add(Restrictions.eq("userId", pageBillRequestDTO.getUserId()));
        if (CollectionUtils.isEmpty(pageBillRequestDTO.getPageRequestData().getSortDataList())) {
            pageBillRequestDTO.getPageRequestData().setSortDataList(Arrays.asList(new SortData("createTime", "DESC")));
        }
        return new BusinessData<>(billManager.findAll(billCriteria, pageBillRequestDTO.getPageRequestData()));
    }
}