package com.wondernect.travel.business.user.manager;

import com.wondernect.elements.rdb.base.manager.BaseManager;
import com.wondernect.travel.business.user.model.Wallet;
import org.springframework.stereotype.Service;

/**
 * 钱包服务操作类
 *
 * @author chenxun 2020-07-08 17:46:05
 **/
@Service
public class WalletManager extends BaseManager<Wallet, String> {
}