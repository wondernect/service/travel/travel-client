package com.wondernect.travel.business.user.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 用户地址分页请求DTO
 *
 * @author chenxun 2020-07-06 18:29:44
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "用户发票创建请求对象")
public class BillCreateRequestDTO {

    @NotBlank(message = "用户发票抬头信息不能为空")
    @JsonProperty("user_fapiao_id")
    @ApiModelProperty(value = "用户发票抬头信息id")
    private String userFapiaoId;

    @NotBlank(message = "用户地址信息不能为空")
    @JsonProperty("user_dizhi_id")
    @ApiModelProperty(value = "用户地址信息id")
    private String userDizhiId;

    @NotNull(message = "用户账单id列表不能为空")
    @JsonProperty("order_id_list")
    @ApiModelProperty(notes = "用户账单id列表")
    private List<String> orderIdList;
}