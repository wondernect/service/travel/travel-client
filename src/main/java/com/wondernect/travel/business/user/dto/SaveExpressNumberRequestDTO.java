package com.wondernect.travel.business.user.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * 用户地址分页请求DTO
 *
 * @author chenxun 2020-07-06 18:29:44
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "用户更新快递单号请求对象")
public class SaveExpressNumberRequestDTO {

    @NotBlank(message = "发票id不能为空")
    @JsonProperty("bill_id")
    @ApiModelProperty(value = "用户发票id")
    private String billId;

    @NotBlank(message = "快递单号不能为空")
    @JsonProperty("express_number")
    @ApiModelProperty(value = "快递单号")
    private String expressNumber;
}