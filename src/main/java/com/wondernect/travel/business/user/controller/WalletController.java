package com.wondernect.travel.business.user.controller;

import com.wondernect.elements.authorize.context.interceptor.AuthorizeRoleType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeUserRole;
import com.wondernect.elements.common.error.BusinessError;
import com.wondernect.elements.common.response.BusinessData;
import com.wondernect.elements.rdb.criteria.Criteria;
import com.wondernect.elements.rdb.criteria.Restrictions;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.travel.business.user.dto.ListWalletRequestDTO;
import com.wondernect.travel.business.user.dto.PageWalletRequestDTO;
import com.wondernect.travel.business.user.manager.WalletManager;
import com.wondernect.travel.business.user.model.Wallet;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 钱包接口
 *
 * @author chenxun 2020-07-08 17:46:06
 **/
@RequestMapping(value = "/v5/travel/wallet")
@RestController
@Validated
@Api(tags = "钱包接口")
public class WalletController {

    @Autowired
    private WalletManager walletManager;

    // @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    // @ApiOperation(value = "创建/更新", httpMethod = "POST")
    // @PostMapping(value = "/create")
    // public BusinessData<Wallet> create(
    //         @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @Validated @RequestBody(required = false) Wallet wallet
    // ) {
    //     return new BusinessData<>(walletManager.save(wallet));
    // }
    //
    // @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    // @ApiOperation(value = "删除", httpMethod = "POST")
    // @PostMapping(value = "/{id}/delete")
    // public BusinessData delete(
    //         @ApiParam(required = true) @NotBlank(message = "对象id不能为空") @PathVariable(value = "id", required = false) String id
    // ) {
    //     walletManager.deleteById(id);
    //     return new BusinessData(BusinessError.SUCCESS);
    // }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "我的余额", httpMethod = "GET")
    @GetMapping(value = "/{user_id}/detail")
    public BusinessData<Wallet> detail(
            @ApiParam(required = true) @NotBlank(message = "用户id不能为空") @PathVariable(value = "user_id", required = false) String userId
    ) {
        return new BusinessData<>(walletManager.findById(userId));
    }

    // @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    // @ApiOperation(value = "列表", httpMethod = "POST")
    // @PostMapping(value = "/list")
    // public BusinessData<List<Wallet>> list(
    //         @ApiParam(required = true) @NotNull(message = "列表请求参数不能为空") @Validated @RequestBody(required = false) ListWalletRequestDTO listWalletRequestDTO
    // ) {
    //     Criteria<Wallet> walletCriteria = new Criteria<>();
    //     walletCriteria.add(Restrictions.eq("userId", listWalletRequestDTO.getUserId()));
    //     return new BusinessData<>(walletManager.findAll(walletCriteria, listWalletRequestDTO.getSortDataList()));
    // }
    //
    // @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    // @ApiOperation(value = "分页", httpMethod = "POST")
    // @PostMapping(value = "/page")
    // public BusinessData<PageResponseData<Wallet>> page(
    //         @ApiParam(required = true) @NotNull(message = "分页请求参数不能为空") @Validated @RequestBody(required = false) PageWalletRequestDTO pageWalletRequestDTO
    // ) {
    //     Criteria<Wallet> walletCriteria = new Criteria<>();
    //     walletCriteria.add(Restrictions.eq("userId", pageWalletRequestDTO.getUserId()));
    //     return new BusinessData<>(walletManager.findAll(walletCriteria, pageWalletRequestDTO.getPageRequestData()));
    // }
}