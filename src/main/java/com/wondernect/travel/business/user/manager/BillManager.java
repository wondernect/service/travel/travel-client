package com.wondernect.travel.business.user.manager;

import com.wondernect.elements.common.exception.BusinessException;
import com.wondernect.elements.common.utils.ESNumberFormatUtils;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.common.utils.ESStringUtils;
import com.wondernect.elements.rdb.base.manager.BaseStringManager;
import com.wondernect.elements.rdb.criteria.Criteria;
import com.wondernect.elements.rdb.criteria.Restrictions;
import com.wondernect.services.user.manager.UserManager;
import com.wondernect.services.user.model.User;
import com.wondernect.travel.business.user.model.Bill;
import com.wondernect.travel.business.user.model.UserDizhi;
import com.wondernect.travel.business.user.model.UserFapiao;
import com.wondernect.travel.manager.TravelOrderManager;
import com.wondernect.travel.model.TravelOrder;
import com.wondernect.travel.model.em.OrderSource;
import com.wondernect.travel.model.em.OrderStatus;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户地址服务操作类
 *
 * @author chenxun 2020-07-06 18:29:44
 **/
@Service
public class BillManager extends BaseStringManager<Bill> {

    @Autowired
    private UserManager userManager;

    @Autowired
    private UserFapiaoManager userFapiaoManager;

    @Autowired
    private UserDizhiManager userDizhiManager;

    @Autowired
    private TravelOrderManager travelOrderManager;

    // 可开具发票总金额
    public Double getTotalPrice(String userId) {
        Criteria<TravelOrder> travelOrderCriteria = new Criteria<>();
        travelOrderCriteria.add(Restrictions.eq("orderSource", OrderSource.USER));
        travelOrderCriteria.add(Restrictions.eq("userId", userId));
        travelOrderCriteria.add(Restrictions.eq("orderStatus", OrderStatus.DONE));
        travelOrderCriteria.add(Restrictions.isNull("billId"));
        List<TravelOrder> travelOrderList = travelOrderManager.findAll(travelOrderCriteria, new ArrayList<>());
        double price = 0;
        if (CollectionUtils.isNotEmpty(travelOrderList)) {
            for (TravelOrder travelOrder : travelOrderList) {
                price += travelOrder.getPrice();
            }
        }
        return ESNumberFormatUtils.formatDouble(price, 2);
    }

    // 开票
    @Transactional
    public Bill createBill(String userId, String fapiaoId, String dizhiId, List<String> orderIdList) {
        if (CollectionUtils.isEmpty(orderIdList)) {
            throw new BusinessException("请选择账单列表");
        }
        List<TravelOrder> travelOrderList = new ArrayList<>();
        Double price = 0d;
        for (String orderId : orderIdList) {
            TravelOrder travelOrder = travelOrderManager.findById(orderId);
            if (ESObjectUtils.isNull(travelOrder) || ESStringUtils.isNotBlank(travelOrder.getBillId())) {
                throw new BusinessException("部分账单信息为空或已开票");
            }
            if (travelOrder.getOrderStatus() != OrderStatus.DONE) {
                throw new BusinessException("部分行程尚未结束，不可开具发票");
            }
            price += travelOrder.getPrice();
            travelOrderList.add(travelOrder);
        }
        User user = userManager.findById(userId);
        if (ESObjectUtils.isNull(user)) {
            throw new BusinessException("用户信息不存在");
        }
        UserFapiao userFapiao = userFapiaoManager.findById(fapiaoId);
        if (ESObjectUtils.isNull(userFapiao)) {
            throw new BusinessException("用户发票抬头信息不存在");
        }
        UserDizhi userDizhi = userDizhiManager.findById(dizhiId);
        if (ESObjectUtils.isNull(userDizhi)) {
            throw new BusinessException("用户地址信息不存在");
        }
        Bill bill = super.save(
                new Bill(
                        userId,
                        user.getName(),
                        userFapiao.getType(),
                        userFapiao.getName(),
                        userFapiao.getNum(),
                        userFapiao.getAddress(),
                        userFapiao.getTelphone(),
                        userFapiao.getBank(),
                        userFapiao.getCard(),
                        userDizhi.getName(),
                        userDizhi.getMobile(),
                        userDizhi.getAddress(),
                        null,
                        ESNumberFormatUtils.formatDouble(price, 2)
                )
        );
        for (TravelOrder travelOrder : travelOrderList) {
            travelOrder.setBillId(bill.getId());
            travelOrderManager.save(travelOrder);
        }
        return bill;
    }

    // 添加/更新快递单号
    @Transactional
    public Bill saveExpressNumber(String id, String expressNumber) {
        Bill bill = super.findById(id);
        if (ESObjectUtils.isNull(bill)) {
            throw new BusinessException("用户发票信息不存在");
        }
        bill.setExpressNumber(expressNumber);
        return super.save(bill);
    }

    // 删除发票信息
    @Transactional
    public void deleteBill(String billId) {
        Bill bill = super.findById(billId);
        if (ESObjectUtils.isNull(bill)) {
            throw new BusinessException("发票信息不存在");
        }
        Criteria<TravelOrder> travelOrderCriteria = new Criteria<>();
        travelOrderCriteria.add(Restrictions.eq("billId", billId));
        List<TravelOrder> travelOrderList = travelOrderManager.findAll(travelOrderCriteria, new ArrayList<>());
        if (CollectionUtils.isNotEmpty(travelOrderList)) {
            for (TravelOrder travelOrder : travelOrderList) {
                travelOrder.setBillId(null);
                travelOrderManager.save(travelOrder);
            }
        }
        super.deleteById(billId);
    }
}