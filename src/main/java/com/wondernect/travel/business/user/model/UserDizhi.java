package com.wondernect.travel.business.user.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.base.model.BaseStringModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

/**
 * Copyright (C), 2017-2020, wondernect.com
 * FileName: UserDizhi
 * Author: chenxun
 * Date: 2020/6/13 17:34
 * Description: 用户地址
 */
@Entity
@Table(name = "user_dizhi")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "用户地址")
public class UserDizhi extends BaseStringModel {

    @NotBlank(message = "用户id不能为空")
    @JsonProperty("user_id")
    @ApiModelProperty(value = "用户id")
    private String userId;

    @JsonProperty("name")
    @ApiModelProperty(value = "姓名")
    private String name;

    @JsonProperty("mobile")
    @ApiModelProperty(value = "电话")
    private String mobile;

    @JsonProperty("address")
    @ApiModelProperty(value = "地址")
    private String address;

    @JsonProperty("set_default")
    @ApiModelProperty(value = "设为默认地址")
    private Boolean setDefault = false;
}
