package com.wondernect.travel.business.faxian.controller;

import com.wondernect.elements.authorize.context.interceptor.AuthorizeRoleType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeUserRole;
import com.wondernect.elements.common.error.BusinessError;
import com.wondernect.elements.common.response.BusinessData;
import com.wondernect.elements.common.utils.ESBeanUtils;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.rdb.criteria.Criteria;
import com.wondernect.elements.rdb.criteria.Restrictions;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.services.user.manager.UserManager;
import com.wondernect.services.user.model.User;
import com.wondernect.travel.business.faxian.dto.FaxianCollectResponseDTO;
import com.wondernect.travel.business.faxian.dto.PageFaxianCollectRequestDTO;
import com.wondernect.travel.business.faxian.manager.FaxianCollectManager;
import com.wondernect.travel.business.faxian.manager.FaxianManager;
import com.wondernect.travel.business.faxian.manager.FaxianZanManager;
import com.wondernect.travel.business.faxian.model.Faxian;
import com.wondernect.travel.business.faxian.model.FaxianCollect;
import com.wondernect.travel.business.faxian.model.FaxianZan;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * 我的收藏接口
 *
 * @author chenxun 2020-10-31 14:37:09
 **/
@RequestMapping(value = "/v5/travel/faxian_collect")
@RestController
@Validated
@Api(tags = "我的收藏接口")
public class FaxianCollectController {

    @Autowired
    private FaxianCollectManager faxianCollectManager;

    @Autowired
    private UserManager userManager;

    @Autowired
    private FaxianManager faxianManager;

    @Autowired
    private FaxianZanManager faxianZanManager;

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "创建/更新", httpMethod = "POST")
    @PostMapping(value = "/save")
    public BusinessData<FaxianCollect> save(
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @Validated @RequestBody(required = false) FaxianCollect faxianCollect
    ) {
        return new BusinessData<>(faxianCollectManager.save(faxianCollect));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "删除", httpMethod = "POST")
    @PostMapping(value = "/{id}/delete")
    public BusinessData delete(
            @ApiParam(required = true) @NotBlank(message = "对象id不能为空") @PathVariable(value = "id", required = false) String id
    ) {
        faxianCollectManager.deleteById(id);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @ApiOperation(value = "获取详细信息", httpMethod = "GET")
    @GetMapping(value = "/{id}/detail")
    public BusinessData<FaxianCollect> detail(
            @ApiParam(required = true) @NotBlank(message = "对象id不能为空") @PathVariable(value = "id", required = false) String id
    ) {
        return new BusinessData<>(faxianCollectManager.findById(id));
    }

    // @ApiOperation(value = "列表", httpMethod = "POST")
    // @PostMapping(value = "/list")
    // public BusinessData<List<FaxianCollect>> list(
    //         @ApiParam(required = true) @NotNull(message = "列表请求参数不能为空") @Validated @RequestBody(required = false) ListFaxianCollectRequestDTO listFaxianCollectRequestDTO
    // ) {
    //     return new BusinessData<>(faxianCollectManager.findAll(listFaxianCollectRequestDTO.getSortDataList()));
    // }

    // @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "分页", httpMethod = "POST")
    @PostMapping(value = "/page")
    public BusinessData<PageResponseData<FaxianCollectResponseDTO>> page(
            @ApiParam(required = true) @NotNull(message = "分页请求参数不能为空") @Validated @RequestBody(required = false) PageFaxianCollectRequestDTO pageFaxianCollectRequestDTO
    ) {
        Criteria<FaxianCollect> faxianCollectCriteria = new Criteria<>();
        faxianCollectCriteria.add(Restrictions.eq("faxianCategory", pageFaxianCollectRequestDTO.getFaxianCategory()));
        faxianCollectCriteria.add(Restrictions.eq("userId", pageFaxianCollectRequestDTO.getUserId()));
        faxianCollectCriteria.add(Restrictions.eq("findId", pageFaxianCollectRequestDTO.getFindId()));
        PageResponseData<FaxianCollect> pageResponseData = faxianCollectManager.findAll(faxianCollectCriteria, pageFaxianCollectRequestDTO.getPageRequestData());
        List<FaxianCollect> faxianCollectList = pageResponseData.getDataList();
        List<FaxianCollectResponseDTO> faxianCollectResponseDTOList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(faxianCollectList)) {
            for (FaxianCollect faxianCollect : faxianCollectList) {
                FaxianCollectResponseDTO faxianCollectResponseDTO = new FaxianCollectResponseDTO();
                ESBeanUtils.copyProperties(faxianCollect, faxianCollectResponseDTO);
                faxianCollectResponseDTO.setId(faxianCollect.getId());
                faxianCollectResponseDTO.setCreateTime(faxianCollect.getCreateTime());
                faxianCollectResponseDTO.setCreateUser(faxianCollect.getCreateUser());
                faxianCollectResponseDTO.setUpdateTime(faxianCollect.getUpdateTime());
                faxianCollectResponseDTO.setUpdateUser(faxianCollect.getUpdateUser());
                User user = userManager.findById(faxianCollect.getUserId());
                Faxian faxian = faxianManager.findById(faxianCollect.getFindId());
                FaxianZan faxianZan = faxianZanManager.findById(faxianCollect.getUserId() + faxian.getId());
                faxianCollectResponseDTO.setUser(ESObjectUtils.isNotNull(user) ? user : null);
                faxianCollectResponseDTO.setFind(ESObjectUtils.isNotNull(faxian) ? faxian : null);
                faxianCollectResponseDTO.setFaxianZanId(ESObjectUtils.isNotNull(faxianZan) ? faxianZan.getId() : null);
                faxianCollectResponseDTOList.add(faxianCollectResponseDTO);
            }
        }
        return new BusinessData<>(
                new PageResponseData<>(
                        pageResponseData.getPage(),
                        pageResponseData.getPage(),
                        pageResponseData.getTotalPages(),
                        pageResponseData.getTotalElements(),
                        faxianCollectResponseDTOList
                )
        );
    }
}