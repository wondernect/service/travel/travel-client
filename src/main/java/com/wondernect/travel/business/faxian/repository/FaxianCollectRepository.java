package com.wondernect.travel.business.faxian.repository;

import com.wondernect.elements.rdb.base.repository.BaseStringRepository;
import com.wondernect.travel.business.faxian.model.FaxianCollect;

/**
 * 我的收藏数据库操作类
 *
 * @author chenxun 2020-10-31 14:37:08
 **/
public interface FaxianCollectRepository extends BaseStringRepository<FaxianCollect> {
}