package com.wondernect.travel.business.faxian.dao;

import com.wondernect.elements.rdb.base.dao.BaseStringDao;
import com.wondernect.travel.business.faxian.model.Faxian;
import org.springframework.stereotype.Repository;

/**
 * 发现(景点、美食、视频、攻略)数据库操作类
 *
 * @author chenxun 2020-10-31 14:36:13
 **/
@Repository
public class FaxianDao extends BaseStringDao<Faxian> {
}