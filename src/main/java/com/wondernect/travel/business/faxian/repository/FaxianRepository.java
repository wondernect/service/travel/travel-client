package com.wondernect.travel.business.faxian.repository;

import com.wondernect.elements.rdb.base.repository.BaseStringRepository;
import com.wondernect.travel.business.faxian.model.Faxian;

/**
 * 发现(景点、美食、视频、攻略)数据库操作类
 *
 * @author chenxun 2020-10-31 14:36:13
 **/
public interface FaxianRepository extends BaseStringRepository<Faxian> {
}