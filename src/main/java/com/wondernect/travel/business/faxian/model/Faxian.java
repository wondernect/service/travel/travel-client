package com.wondernect.travel.business.faxian.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.base.model.BaseStringModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Copyright (C), 2017-2020, wondernect.com
 * FileName: Faxian
 * Author: chenxun
 * Date: 2020/6/14 13:11
 * Description: 发现
 */
@Entity
@Table(name = "faxian")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "发现(景点、美食、视频、攻略)")
public class Faxian extends BaseStringModel {

    @NotBlank(message = "城市id不能为空")
    @JsonProperty("city_id")
    @ApiModelProperty(value = "城市id")
    private String cityId;

    @NotNull(message = "发现分类不能为空")
    @Enumerated(EnumType.STRING)
    @JsonProperty("faxian_category")
    @ApiModelProperty(value = "发现分类")
    private FaxianCategory faxianCategory;

    @NotBlank(message = "名称不能为空")
    @JsonProperty("name")
    @ApiModelProperty(value = "名称")
    private String name;

    @Lob
    @JsonProperty("content")
    @ApiModelProperty(value = "内容")
    private String content;

    @Lob
    @JsonProperty("images")
    @ApiModelProperty(value = "图片或视频地址")
    private String images;

    @JsonProperty("target_place")
    @ApiModelProperty(value = "目的地")
    private String targetPlace;

    @JsonProperty("target_longitude")
    @ApiModelProperty(value = "目的地经度")
    private Double targetLongitude;

    @JsonProperty("target_latitude")
    @ApiModelProperty(value = "目的地纬度")
    private Double targetLatitude;

    @JsonProperty("valid")
    @ApiModelProperty(value = "是否合法")
    private Boolean valid = false;

    @JsonProperty("zan_count")
    @ApiModelProperty(value = "点赞数")
    private Integer zanCount = 0;

    @JsonProperty("evaluate_count")
    @ApiModelProperty(value = "评论数")
    private Integer evaluateCount = 0;

    @JsonProperty("collect_count")
    @ApiModelProperty(value = "收藏数")
    private Integer collectCount = 0;
}
