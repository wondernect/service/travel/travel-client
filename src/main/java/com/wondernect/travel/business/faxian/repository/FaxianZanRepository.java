package com.wondernect.travel.business.faxian.repository;

import com.wondernect.elements.rdb.base.repository.BaseStringRepository;
import com.wondernect.travel.business.faxian.model.FaxianZan;

/**
 * 我的点赞数据库操作类
 *
 * @author chenxun 2020-10-31 14:37:51
 **/
public interface FaxianZanRepository extends BaseStringRepository<FaxianZan> {
}