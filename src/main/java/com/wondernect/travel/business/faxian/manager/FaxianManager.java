package com.wondernect.travel.business.faxian.manager;

import com.wondernect.elements.rdb.base.manager.BaseStringManager;
import com.wondernect.travel.business.faxian.model.Faxian;
import org.springframework.stereotype.Service;

/**
 * 发现(景点、美食、视频、攻略)服务操作类
 *
 * @author chenxun 2020-10-31 14:36:13
 **/
@Service
public class FaxianManager extends BaseStringManager<Faxian> {
}