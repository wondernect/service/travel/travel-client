package com.wondernect.travel.business.faxian.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.base.model.BaseStringModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

/**
 * Copyright (C), 2017-2020, wondernect.com
 * FileName: FindCollect
 * Author: chenxun
 * Date: 2020/6/14 13:26
 * Description: 发现收藏
 */
@Entity
@Table(name = "faxian_zan")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "我的点赞")
public class FaxianZan extends BaseStringModel {

    @NotBlank(message = "用户id不能为空")
    @JsonProperty("user_id")
    @ApiModelProperty(value = "用户id")
    private String userId;

    @NotBlank(message = "发现id不能为空")
    @JsonProperty("find_id")
    @ApiModelProperty(value = "发现id")
    private String findId;

    @Enumerated(EnumType.STRING)
    @JsonProperty("faxian_category")
    @ApiModelProperty(value = "发现分类")
    private FaxianCategory faxianCategory;
}
