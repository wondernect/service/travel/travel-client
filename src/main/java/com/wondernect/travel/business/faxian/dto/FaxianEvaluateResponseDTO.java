package com.wondernect.travel.business.faxian.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.services.user.model.User;
import com.wondernect.travel.business.faxian.model.Faxian;
import com.wondernect.travel.business.faxian.model.FaxianCategory;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 我的评论响应DTO
 *
 * @author chenxun 2020-10-31 14:37:37
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "我的评论响应对象")
public class FaxianEvaluateResponseDTO {

    @JsonProperty("id")
    @ApiModelProperty(notes = "我的评论id")
    private String id;

    @JsonProperty("user_id")
    @ApiModelProperty(notes = "用户id")
    private String userId;

    @JsonProperty("user")
    @ApiModelProperty(notes = "用户")
    private User user;

    @JsonProperty("find_id")
    @ApiModelProperty(notes = "发现id")
    private String findId;

    @JsonProperty("find")
    @ApiModelProperty(notes = "发现详细信息")
    private Faxian find;

    @JsonProperty("faxian_category")
    @ApiModelProperty(notes = "发现分类")
    private FaxianCategory faxianCategory;

    @JsonProperty("content")
    @ApiModelProperty(notes = "评论内容")
    private String content;

    @JsonProperty("valid")
    @ApiModelProperty(notes = "是否合法")
    private Boolean valid;

    @JsonProperty("create_time")
    @ApiModelProperty(notes = "创建时间")
    private Long createTime;

    @JsonProperty("update_time")
    @ApiModelProperty(notes = "更新时间")
    private Long updateTime;

    @JsonProperty("create_user")
    @ApiModelProperty(notes = "创建用户")
    private String createUser;

    @JsonProperty("update_user")
    @ApiModelProperty(notes = "更新用户")
    private String updateUser;

    @JsonProperty("find_collect_id")
    @ApiModelProperty(value = "收藏id")
    private String faxianCollectId;

    @JsonProperty("find_zan_id")
    @ApiModelProperty(value = "赞id")
    private String faxianZanId;
}