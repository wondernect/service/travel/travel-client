package com.wondernect.travel.business.faxian.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.base.model.BaseStringModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

/**
 * Copyright (C), 2017-2020, wondernect.com
 * FileName: FaxianEvaluate
 * Author: chenxun
 * Date: 2020/6/14 13:21
 * Description: 发现评论
 */
@Entity
@Table(name = "faxian_evaluate")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "我的评论")
public class FaxianEvaluate extends BaseStringModel {

    @NotBlank(message = "用户id不能为空")
    @JsonProperty("user_id")
    @ApiModelProperty(value = "用户id")
    private String userId;

    @NotBlank(message = "发现id不能为空")
    @JsonProperty("find_id")
    @ApiModelProperty(value = "发现id")
    private String findId;

    @Column(columnDefinition = "varchar(255) not null")
    @Enumerated(EnumType.STRING)
    @JsonProperty("faxian_category")
    @ApiModelProperty(value = "发现分类")
    private FaxianCategory faxianCategory;

    @Lob
    @JsonProperty("content")
    @ApiModelProperty(value = "评论内容")
    private String content;

    @JsonProperty("valid")
    @ApiModelProperty(value = "是否合法")
    private Boolean valid = false;
}
