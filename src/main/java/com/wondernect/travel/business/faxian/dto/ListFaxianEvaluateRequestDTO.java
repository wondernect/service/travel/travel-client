package com.wondernect.travel.business.faxian.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.request.SortData;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 我的评论列表请求DTO
 *
 * @author chenxun 2020-10-31 14:37:37
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "我的评论列表请求对象")
public class ListFaxianEvaluateRequestDTO {

    @JsonProperty("sort_data_list")
    @ApiModelProperty(notes = "列表请求参数")
    private List<SortData> sortDataList;
}