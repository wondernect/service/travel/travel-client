package com.wondernect.travel.business.faxian.manager;

import com.wondernect.elements.common.exception.BusinessException;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.common.utils.ESStringUtils;
import com.wondernect.elements.rdb.base.manager.BaseStringManager;
import com.wondernect.travel.business.faxian.model.Faxian;
import com.wondernect.travel.business.faxian.model.FaxianCollect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 我的收藏服务操作类
 *
 * @author chenxun 2020-10-31 14:37:08
 **/
@Service
public class FaxianCollectManager extends BaseStringManager<FaxianCollect> {

    @Autowired
    private FaxianManager faxianManager;

    @Transactional
    @Override
    public FaxianCollect save(FaxianCollect faxianCollect) {
        Faxian faxian = faxianManager.findById(faxianCollect.getFindId());
        if (ESObjectUtils.isNull(faxian)) {
            throw new BusinessException("发现信息不存在");
        }
        faxianCollect.setFaxianCategory(faxian.getFaxianCategory());
        if (ESStringUtils.isBlank(faxianCollect.getId())) {
            faxianCollect.setId(faxianCollect.getUserId() + faxianCollect.getFindId());
        }
        faxianCollect = super.save(faxianCollect);
        Integer count = ESObjectUtils.isNotNull(faxian.getCollectCount()) ? faxian.getCollectCount() : 0;
        count++;
        faxian.setCollectCount(count);
        faxianManager.save(faxian);
        return faxianCollect;
    }

    @Transactional
    @Override
    public void deleteById(String id) {
        FaxianCollect faxianCollect = super.findById(id);
        if (ESObjectUtils.isNull(faxianCollect)) {
            throw new BusinessException("收藏记录不存在");
        }
        Faxian faxian = faxianManager.findById(faxianCollect.getFindId());
        if (ESObjectUtils.isNull(faxian)) {
            throw new BusinessException("发现信息不存在");
        }
        super.deleteById(id);
        Integer count = ESObjectUtils.isNotNull(faxian.getCollectCount()) ? faxian.getCollectCount() : 1;
        if (count <= 0) {
            count = 1;
        }
        count--;
        faxian.setCollectCount(count);
        faxianManager.save(faxian);
    }
}