package com.wondernect.travel.business.faxian.controller;

import com.wondernect.elements.authorize.context.WondernectCommonContext;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeRoleType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeUserRole;
import com.wondernect.elements.common.error.BusinessError;
import com.wondernect.elements.common.exception.BusinessException;
import com.wondernect.elements.common.response.BusinessData;
import com.wondernect.elements.common.utils.ESBeanUtils;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.common.utils.ESStringUtils;
import com.wondernect.elements.rdb.criteria.Criteria;
import com.wondernect.elements.rdb.criteria.Restrictions;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.services.user.manager.UserManager;
import com.wondernect.services.user.model.User;
import com.wondernect.travel.business.faxian.dto.FaxianResponseDTO;
import com.wondernect.travel.business.faxian.dto.PageFaxianRequestDTO;
import com.wondernect.travel.business.faxian.manager.FaxianCollectManager;
import com.wondernect.travel.business.faxian.manager.FaxianManager;
import com.wondernect.travel.business.faxian.manager.FaxianZanManager;
import com.wondernect.travel.business.faxian.model.Faxian;
import com.wondernect.travel.business.faxian.model.FaxianCollect;
import com.wondernect.travel.business.faxian.model.FaxianZan;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.criterion.MatchMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * 发现(景点、美食、视频、攻略)接口
 *
 * @author chenxun 2020-10-31 14:36:14
 **/
@RequestMapping(value = "/v5/travel/faxian")
@RestController
@Validated
@Api(tags = "发现(景点、美食、视频、攻略)接口")
public class FaxianController {

    @Autowired
    private WondernectCommonContext wondernectCommonContext;

    @Autowired
    private FaxianManager faxianManager;

    @Autowired
    private FaxianCollectManager faxianCollectManager;

    @Autowired
    private FaxianZanManager faxianZanManager;

    @Autowired
    private UserManager userManager;

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "创建/更新", httpMethod = "POST")
    @PostMapping(value = "/save")
    public BusinessData<Faxian> create(
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @Validated @RequestBody(required = false) Faxian faxian
    ) {
        return new BusinessData<>(faxianManager.save(faxian));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "删除", httpMethod = "POST")
    @PostMapping(value = "/{id}/delete")
    public BusinessData delete(
            @ApiParam(required = true) @NotBlank(message = "对象id不能为空") @PathVariable(value = "id", required = false) String id
    ) {
        Faxian faxian = faxianManager.findById(id);
        if (ESObjectUtils.isNull(faxian)) {
            throw new BusinessException("发现信息不存在");
        }
        faxianManager.deleteById(id);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "更新发现合法性", httpMethod = "POST")
    @PostMapping(value = "/{id}/valid")
    public BusinessData<Faxian> valid(
            @ApiParam(required = true) @NotBlank(message = "对象id不能为空") @PathVariable(value = "id", required = false) String id,
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @RequestParam(value = "valid", required = false) Boolean valid
    ) {
        Faxian faxian = faxianManager.findById(id);
        if (ESObjectUtils.isNull(faxian)) {
            throw new BusinessException("发现信息不存在");
        }
        faxian.setValid(valid);
        return new BusinessData<>(faxianManager.save(faxian));
    }

    @ApiOperation(value = "获取详细信息", httpMethod = "GET")
    @GetMapping(value = "/{id}/detail")
    public BusinessData<Faxian> detail(
            @ApiParam(required = true) @NotBlank(message = "对象id不能为空") @PathVariable(value = "id", required = false) String id
    ) {
        return new BusinessData<>(faxianManager.findById(id));
    }

    // @ApiOperation(value = "列表", httpMethod = "POST")
    // @PostMapping(value = "/list")
    // public BusinessData<List<Faxian>> list(
    //         @ApiParam(required = true) @NotNull(message = "列表请求参数不能为空") @Validated @RequestBody(required = false) ListFaxianRequestDTO listFaxianRequestDTO
    // ) {
    //     return new BusinessData<>(faxianManager.findAll(listFaxianRequestDTO.getSortDataList()));
    // }

    // @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "分页", httpMethod = "POST")
    @PostMapping(value = "/page")
    public BusinessData<PageResponseData<FaxianResponseDTO>> page(
            @ApiParam(required = true) @NotNull(message = "分页请求参数不能为空") @Validated @RequestBody(required = false) PageFaxianRequestDTO pageFaxianRequestDTO
    ) {
        String userId = ESStringUtils.isBlank(pageFaxianRequestDTO.getUserId()) ? "" : pageFaxianRequestDTO.getUserId();
        Criteria<Faxian> faxianCriteria = new Criteria<>();
        faxianCriteria.add(Restrictions.eq("faxianCategory", pageFaxianRequestDTO.getFaxianCategory()));
        faxianCriteria.add(Restrictions.eq("cityId", pageFaxianRequestDTO.getCityId()));
        faxianCriteria.add(Restrictions.like("name", pageFaxianRequestDTO.getName(), MatchMode.ANYWHERE));
        faxianCriteria.add(Restrictions.eq("valid", pageFaxianRequestDTO.getValid()));
        PageResponseData<Faxian> pageResponseData = faxianManager.findAll(faxianCriteria, pageFaxianRequestDTO.getPageRequestData());
        List<Faxian> faxianList = pageResponseData.getDataList();
        List<FaxianResponseDTO> faxianResponseDTOList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(faxianList)) {
            for (Faxian faxian : faxianList) {
                FaxianResponseDTO faxianResponseDTO = new FaxianResponseDTO();
                ESBeanUtils.copyProperties(faxian, faxianResponseDTO);
                faxianResponseDTO.setId(faxian.getId());
                faxianResponseDTO.setCreateTime(faxian.getCreateTime());
                faxianResponseDTO.setCreateUser(faxian.getCreateUser());
                faxianResponseDTO.setUpdateTime(faxian.getUpdateTime());
                faxianResponseDTO.setUpdateUser(faxian.getUpdateUser());
                User user = userManager.findById(faxian.getCreateUser());
                FaxianCollect faxianCollect = faxianCollectManager.findById(userId + faxian.getId());
                FaxianZan faxianZan = faxianZanManager.findById(userId + faxian.getId());
                faxianResponseDTO.setUser(ESObjectUtils.isNotNull(user) ? user : null);
                faxianResponseDTO.setFaxianCollectId(ESObjectUtils.isNotNull(faxianCollect) ? faxianCollect.getId() : null);
                faxianResponseDTO.setFaxianZanId(ESObjectUtils.isNotNull(faxianZan) ? faxianZan.getId() : null);
                faxianResponseDTOList.add(faxianResponseDTO);
            }
        }
        return new BusinessData<>(
                new PageResponseData<>(
                        pageResponseData.getPage(),
                        pageResponseData.getPage(),
                        pageResponseData.getTotalPages(),
                        pageResponseData.getTotalElements(),
                        faxianResponseDTOList
                )
        );
    }
}