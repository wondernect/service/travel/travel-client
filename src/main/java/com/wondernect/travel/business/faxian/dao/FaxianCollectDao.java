package com.wondernect.travel.business.faxian.dao;

import com.wondernect.elements.rdb.base.dao.BaseStringDao;
import com.wondernect.travel.business.faxian.model.FaxianCollect;
import org.springframework.stereotype.Repository;

/**
 * 我的收藏数据库操作类
 *
 * @author chenxun 2020-10-31 14:37:08
 **/
@Repository
public class FaxianCollectDao extends BaseStringDao<FaxianCollect> {
}