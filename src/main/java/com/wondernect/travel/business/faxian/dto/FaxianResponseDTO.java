package com.wondernect.travel.business.faxian.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.services.user.model.User;
import com.wondernect.travel.business.faxian.model.FaxianCategory;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 发现(景点、美食、视频、攻略)响应DTO
 *
 * @author chenxun 2020-10-31 14:36:13
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "发现(景点、美食、视频、攻略)响应对象")
public class FaxianResponseDTO {

    @JsonProperty("id")
    @ApiModelProperty(notes = "发现(景点、美食、视频、攻略)id")
    private String id;

    @JsonProperty("city_id")
    @ApiModelProperty(notes = "城市id")
    private String cityId;

    @JsonProperty("faxian_category")
    @ApiModelProperty(notes = "发现分类")
    private FaxianCategory faxianCategory;

    @JsonProperty("name")
    @ApiModelProperty(notes = "名称")
    private String name;

    @JsonProperty("content")
    @ApiModelProperty(notes = "内容")
    private String content;

    @JsonProperty("images")
    @ApiModelProperty(notes = "图片或视频地址")
    private String images;

    @JsonProperty("target_place")
    @ApiModelProperty(notes = "目的地")
    private String targetPlace;

    @JsonProperty("target_longitude")
    @ApiModelProperty(notes = "目的地经度")
    private Double targetLongitude;

    @JsonProperty("target_latitude")
    @ApiModelProperty(notes = "目的地纬度")
    private Double targetLatitude;

    @JsonProperty("valid")
    @ApiModelProperty(value = "是否合法")
    private Boolean valid;

    @JsonProperty("zan_count")
    @ApiModelProperty(notes = "点赞数")
    private Integer zanCount;

    @JsonProperty("evaluate_count")
    @ApiModelProperty(notes = "评论数")
    private Integer evaluateCount;

    @JsonProperty("collect_count")
    @ApiModelProperty(notes = "收藏数")
    private Integer collectCount;

    @JsonProperty("create_time")
    @ApiModelProperty(notes = "创建时间")
    private Long createTime;

    @JsonProperty("update_time")
    @ApiModelProperty(notes = "更新时间")
    private Long updateTime;

    @JsonProperty("create_user")
    @ApiModelProperty(notes = "创建用户")
    private String createUser;

    @JsonProperty("user")
    @ApiModelProperty(notes = "用户")
    private User user;

    @JsonProperty("update_user")
    @ApiModelProperty(notes = "更新用户")
    private String updateUser;

    @JsonProperty("find_collect_id")
    @ApiModelProperty(value = "收藏id")
    private String faxianCollectId;

    @JsonProperty("find_zan_id")
    @ApiModelProperty(value = "赞id")
    private String faxianZanId;
}