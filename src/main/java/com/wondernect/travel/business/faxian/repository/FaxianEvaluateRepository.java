package com.wondernect.travel.business.faxian.repository;

import com.wondernect.elements.rdb.base.repository.BaseStringRepository;
import com.wondernect.travel.business.faxian.model.FaxianEvaluate;

/**
 * 我的评论数据库操作类
 *
 * @author chenxun 2020-10-31 14:37:36
 **/
public interface FaxianEvaluateRepository extends BaseStringRepository<FaxianEvaluate> {
}