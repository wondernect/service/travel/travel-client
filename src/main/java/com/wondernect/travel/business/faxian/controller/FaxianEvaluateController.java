package com.wondernect.travel.business.faxian.controller;

import com.wondernect.elements.authorize.context.interceptor.AuthorizeRoleType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeUserRole;
import com.wondernect.elements.common.error.BusinessError;
import com.wondernect.elements.common.response.BusinessData;
import com.wondernect.elements.common.utils.ESBeanUtils;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.rdb.criteria.Criteria;
import com.wondernect.elements.rdb.criteria.Restrictions;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.services.user.manager.UserManager;
import com.wondernect.services.user.model.User;
import com.wondernect.travel.business.faxian.dto.FaxianEvaluateResponseDTO;
import com.wondernect.travel.business.faxian.dto.PageFaxianEvaluateRequestDTO;
import com.wondernect.travel.business.faxian.manager.FaxianCollectManager;
import com.wondernect.travel.business.faxian.manager.FaxianEvaluateManager;
import com.wondernect.travel.business.faxian.manager.FaxianManager;
import com.wondernect.travel.business.faxian.manager.FaxianZanManager;
import com.wondernect.travel.business.faxian.model.Faxian;
import com.wondernect.travel.business.faxian.model.FaxianCollect;
import com.wondernect.travel.business.faxian.model.FaxianEvaluate;
import com.wondernect.travel.business.faxian.model.FaxianZan;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * 我的评论接口
 *
 * @author chenxun 2020-10-31 14:37:37
 **/
@RequestMapping(value = "/v5/travel/faxian_evaluate")
@RestController
@Validated
@Api(tags = "我的评论接口")
public class FaxianEvaluateController {

    @Autowired
    private FaxianEvaluateManager faxianEvaluateManager;

    @Autowired
    private UserManager userManager;

    @Autowired
    private FaxianManager faxianManager;

    @Autowired
    private FaxianCollectManager faxianCollectManager;

    @Autowired
    private FaxianZanManager faxianZanManager;

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "创建/更新", httpMethod = "POST")
    @PostMapping(value = "/save")
    public BusinessData<FaxianEvaluate> save(
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @Validated @RequestBody(required = false) FaxianEvaluate faxianEvaluate
    ) {
        return new BusinessData<>(faxianEvaluateManager.save(faxianEvaluate));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "删除", httpMethod = "POST")
    @PostMapping(value = "/{id}/delete")
    public BusinessData delete(
            @ApiParam(required = true) @NotBlank(message = "对象id不能为空") @PathVariable(value = "id", required = false) String id
    ) {
        faxianEvaluateManager.deleteById(id);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "更新评论合法性", httpMethod = "POST")
    @PostMapping(value = "/{id}/valid")
    public BusinessData<FaxianEvaluate> valid(
            @ApiParam(required = true) @NotBlank(message = "对象id不能为空") @PathVariable(value = "id", required = false) String id,
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @Validated @RequestParam(value = "valid", required = false) Boolean valid
    ) {
        return new BusinessData<>(faxianEvaluateManager.valid(id, valid));
    }

    @ApiOperation(value = "获取详细信息", httpMethod = "GET")
    @GetMapping(value = "/{id}/detail")
    public BusinessData<FaxianEvaluate> detail(
            @ApiParam(required = true) @NotBlank(message = "对象id不能为空") @PathVariable(value = "id", required = false) String id
    ) {
        return new BusinessData<>(faxianEvaluateManager.findById(id));
    }

    // @ApiOperation(value = "列表", httpMethod = "POST")
    // @PostMapping(value = "/list")
    // public BusinessData<List<FaxianEvaluate>> list(
    //         @ApiParam(required = true) @NotNull(message = "列表请求参数不能为空") @Validated @RequestBody(required = false) ListFaxianEvaluateRequestDTO listFaxianEvaluateRequestDTO
    // ) {
    //     return new BusinessData<>(faxianEvaluateManager.findAll(listFaxianEvaluateRequestDTO.getSortDataList()));
    // }

    // @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "分页", httpMethod = "POST")
    @PostMapping(value = "/page")
    public BusinessData<PageResponseData<FaxianEvaluateResponseDTO>> page(
            @ApiParam(required = true) @NotNull(message = "分页请求参数不能为空") @Validated @RequestBody(required = false) PageFaxianEvaluateRequestDTO pageFaxianEvaluateRequestDTO
    ) {
        Criteria<FaxianEvaluate> faxianEvaluateCriteria = new Criteria<>();
        faxianEvaluateCriteria.add(Restrictions.eq("faxianCategory", pageFaxianEvaluateRequestDTO.getFaxianCategory()));
        faxianEvaluateCriteria.add(Restrictions.eq("userId", pageFaxianEvaluateRequestDTO.getUserId()));
        faxianEvaluateCriteria.add(Restrictions.eq("findId", pageFaxianEvaluateRequestDTO.getFindId()));
        faxianEvaluateCriteria.add(Restrictions.eq("valid", pageFaxianEvaluateRequestDTO.getValid()));
        PageResponseData<FaxianEvaluate> pageResponseData = faxianEvaluateManager.findAll(faxianEvaluateCriteria, pageFaxianEvaluateRequestDTO.getPageRequestData());
        List<FaxianEvaluate> faxianEvaluateList = pageResponseData.getDataList();
        List<FaxianEvaluateResponseDTO> faxianEvaluateResponseDTOList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(faxianEvaluateList)) {
            for (FaxianEvaluate faxianEvaluate : faxianEvaluateList) {
                FaxianEvaluateResponseDTO faxianEvaluateResponseDTO = new FaxianEvaluateResponseDTO();
                ESBeanUtils.copyProperties(faxianEvaluate, faxianEvaluateResponseDTO);
                faxianEvaluateResponseDTO.setId(faxianEvaluate.getId());
                faxianEvaluateResponseDTO.setCreateTime(faxianEvaluate.getCreateTime());
                faxianEvaluateResponseDTO.setCreateUser(faxianEvaluate.getCreateUser());
                faxianEvaluateResponseDTO.setUpdateTime(faxianEvaluate.getUpdateTime());
                faxianEvaluateResponseDTO.setUpdateUser(faxianEvaluate.getUpdateUser());
                User user = userManager.findById(faxianEvaluate.getUserId());
                Faxian faxian = faxianManager.findById(faxianEvaluate.getFindId());
                FaxianCollect faxianCollect = faxianCollectManager.findById(faxianEvaluate.getUserId() + faxian.getId());
                FaxianZan faxianZan = faxianZanManager.findById(faxianEvaluate.getUserId() + faxian.getId());
                faxianEvaluateResponseDTO.setUser(ESObjectUtils.isNotNull(user) ? user : null);
                faxianEvaluateResponseDTO.setFind(ESObjectUtils.isNotNull(faxian) ? faxian : null);
                faxianEvaluateResponseDTO.setFaxianCollectId(ESObjectUtils.isNotNull(faxianCollect) ? faxianCollect.getId() : null);
                faxianEvaluateResponseDTO.setFaxianZanId(ESObjectUtils.isNotNull(faxianZan) ? faxianZan.getId() : null);
                faxianEvaluateResponseDTOList.add(faxianEvaluateResponseDTO);
            }
        }
        return new BusinessData<>(
                new PageResponseData<>(
                        pageResponseData.getPage(),
                        pageResponseData.getPage(),
                        pageResponseData.getTotalPages(),
                        pageResponseData.getTotalElements(),
                        faxianEvaluateResponseDTOList
                )
        );
    }
}