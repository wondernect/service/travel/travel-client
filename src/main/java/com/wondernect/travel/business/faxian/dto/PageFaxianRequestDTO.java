package com.wondernect.travel.business.faxian.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.request.PageRequestData;
import com.wondernect.travel.business.faxian.model.FaxianCategory;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

/**
 * 发现(景点、美食、视频、攻略)分页请求DTO
 *
 * @author chenxun 2020-10-31 14:36:13
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "发现(景点、美食、视频、攻略)分页请求对象")
public class PageFaxianRequestDTO {

    @JsonProperty("user_id")
    @ApiModelProperty(value = "用户id")
    private String userId;

    @JsonProperty("city_id")
    @ApiModelProperty(value = "城市id")
    private String cityId;

    @Enumerated(EnumType.STRING)
    @JsonProperty("faxian_category")
    @ApiModelProperty(value = "发现分类")
    private FaxianCategory faxianCategory;

    @JsonProperty("name")
    @ApiModelProperty(value = "名称")
    private String name;

    @JsonProperty("valid")
    @ApiModelProperty(value = "是否合法")
    private Boolean valid;

    @NotNull(message = "分页请求参数不能为空")
    @JsonProperty("page_request_data")
    @ApiModelProperty(notes = "分页请求参数")
    private PageRequestData pageRequestData;
}