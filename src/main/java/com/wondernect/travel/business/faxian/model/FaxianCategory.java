package com.wondernect.travel.business.faxian.model;

/**
 * Copyright (C), 2017-2020, wondernect.com
 * FileName: FaxianCategory
 * Author: chenxun
 * Date: 2020/6/14 13:14
 * Description: 发现分类
 */
public enum FaxianCategory {

    JINGDIAN, // 景点

    MEISHI, // 美食

    SHIPIN, //视频

    GONGLUE, // 攻略
}

