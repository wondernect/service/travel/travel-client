package com.wondernect.travel.business.faxian.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.request.PageRequestData;
import com.wondernect.travel.business.faxian.model.FaxianCategory;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

/**
 * 我的点赞分页请求DTO
 *
 * @author chenxun 2020-10-31 14:37:52
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "我的点赞分页请求对象")
public class PageFaxianZanRequestDTO {

    @JsonProperty("user_id")
    @ApiModelProperty(value = "用户id")
    private String userId;

    @JsonProperty("find_id")
    @ApiModelProperty(value = "发现id")
    private String findId;

    @Enumerated(EnumType.STRING)
    @JsonProperty("faxian_category")
    @ApiModelProperty(value = "发现分类")
    private FaxianCategory faxianCategory;

    @NotNull(message = "分页请求参数不能为空")
    @JsonProperty("page_request_data")
    @ApiModelProperty(notes = "分页请求参数")
    private PageRequestData pageRequestData;
}