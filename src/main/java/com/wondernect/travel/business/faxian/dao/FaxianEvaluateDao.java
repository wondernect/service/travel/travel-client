package com.wondernect.travel.business.faxian.dao;

import com.wondernect.elements.rdb.base.dao.BaseStringDao;
import com.wondernect.travel.business.faxian.model.FaxianEvaluate;
import org.springframework.stereotype.Repository;

/**
 * 我的评论数据库操作类
 *
 * @author chenxun 2020-10-31 14:37:36
 **/
@Repository
public class FaxianEvaluateDao extends BaseStringDao<FaxianEvaluate> {
}