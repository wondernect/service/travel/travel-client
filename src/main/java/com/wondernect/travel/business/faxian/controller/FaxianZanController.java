package com.wondernect.travel.business.faxian.controller;

import com.wondernect.elements.authorize.context.interceptor.AuthorizeRoleType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeType;
import com.wondernect.elements.authorize.context.interceptor.AuthorizeUserRole;
import com.wondernect.elements.common.error.BusinessError;
import com.wondernect.elements.common.response.BusinessData;
import com.wondernect.elements.common.utils.ESBeanUtils;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.rdb.criteria.Criteria;
import com.wondernect.elements.rdb.criteria.Restrictions;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.services.user.manager.UserManager;
import com.wondernect.services.user.model.User;
import com.wondernect.travel.business.faxian.dto.FaxianZanResponseDTO;
import com.wondernect.travel.business.faxian.dto.PageFaxianZanRequestDTO;
import com.wondernect.travel.business.faxian.manager.FaxianCollectManager;
import com.wondernect.travel.business.faxian.manager.FaxianManager;
import com.wondernect.travel.business.faxian.manager.FaxianZanManager;
import com.wondernect.travel.business.faxian.model.Faxian;
import com.wondernect.travel.business.faxian.model.FaxianCollect;
import com.wondernect.travel.business.faxian.model.FaxianZan;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * 我的点赞接口
 *
 * @author chenxun 2020-10-31 14:37:52
 **/
@RequestMapping(value = "/v5/travel/faxian_zan")
@RestController
@Validated
@Api(tags = "我的点赞接口")
public class FaxianZanController {

    @Autowired
    private FaxianZanManager faxianZanManager;

    @Autowired
    private UserManager userManager;

    @Autowired
    private FaxianManager faxianManager;

    @Autowired
    private FaxianCollectManager faxianCollectManager;

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "创建/更新", httpMethod = "POST")
    @PostMapping(value = "/save")
    public BusinessData<FaxianZan> save(
            @ApiParam(required = true) @NotNull(message = "请求参数不能为空") @Validated @RequestBody(required = false) FaxianZan faxianZan
    ) {
        return new BusinessData<>(faxianZanManager.save(faxianZan));
    }

    @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "删除", httpMethod = "POST")
    @PostMapping(value = "/{id}/delete")
    public BusinessData delete(
            @ApiParam(required = true) @NotBlank(message = "对象id不能为空") @PathVariable(value = "id", required = false) String id
    ) {
        faxianZanManager.deleteById(id);
        return new BusinessData(BusinessError.SUCCESS);
    }

    @ApiOperation(value = "获取详细信息", httpMethod = "GET")
    @GetMapping(value = "/{id}/detail")
    public BusinessData<FaxianZan> detail(
            @ApiParam(required = true) @NotBlank(message = "对象id不能为空") @PathVariable(value = "id", required = false) String id
    ) {
        return new BusinessData<>(faxianZanManager.findById(id));
    }

    // @ApiOperation(value = "列表", httpMethod = "POST")
    // @PostMapping(value = "/list")
    // public BusinessData<List<FaxianZan>> list(
    //         @ApiParam(required = true) @NotNull(message = "列表请求参数不能为空") @Validated @RequestBody(required = false) ListFaxianZanRequestDTO listFaxianZanRequestDTO
    // ) {
    //     return new BusinessData<>(faxianZanManager.findAll(listFaxianZanRequestDTO.getSortDataList()));
    // }

    // @AuthorizeUserRole(authorizeType = AuthorizeType.UNLIMITED_TOKEN, authorizeRoleType = AuthorizeRoleType.ONLY_AUTHORIZE)
    @ApiOperation(value = "分页", httpMethod = "POST")
    @PostMapping(value = "/page")
    public BusinessData<PageResponseData<FaxianZanResponseDTO>> page(
            @ApiParam(required = true) @NotNull(message = "分页请求参数不能为空") @Validated @RequestBody(required = false) PageFaxianZanRequestDTO pageFaxianZanRequestDTO
    ) {
        Criteria<FaxianZan> faxianZanCriteria = new Criteria<>();
        faxianZanCriteria.add(Restrictions.eq("faxianCategory", pageFaxianZanRequestDTO.getFaxianCategory()));
        faxianZanCriteria.add(Restrictions.eq("userId", pageFaxianZanRequestDTO.getUserId()));
        faxianZanCriteria.add(Restrictions.eq("findId", pageFaxianZanRequestDTO.getFindId()));
        PageResponseData<FaxianZan> pageResponseData = faxianZanManager.findAll(faxianZanCriteria, pageFaxianZanRequestDTO.getPageRequestData());
        List<FaxianZan> faxianZanList = pageResponseData.getDataList();
        List<FaxianZanResponseDTO> faxianZanResponseDTOList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(faxianZanList)) {
            for (FaxianZan faxianZan : faxianZanList) {
                FaxianZanResponseDTO faxianZanResponseDTO = new FaxianZanResponseDTO();
                ESBeanUtils.copyProperties(faxianZan, faxianZanResponseDTO);
                faxianZanResponseDTO.setId(faxianZan.getId());
                faxianZanResponseDTO.setCreateTime(faxianZan.getCreateTime());
                faxianZanResponseDTO.setCreateUser(faxianZan.getCreateUser());
                faxianZanResponseDTO.setUpdateTime(faxianZan.getUpdateTime());
                faxianZanResponseDTO.setUpdateUser(faxianZan.getUpdateUser());
                User user = userManager.findById(faxianZan.getUserId());
                Faxian faxian = faxianManager.findById(faxianZan.getFindId());
                FaxianCollect faxianCollect = faxianCollectManager.findById(faxianZan.getUserId() + faxian.getId());
                faxianZanResponseDTO.setUser(ESObjectUtils.isNotNull(user) ? user : null);
                faxianZanResponseDTO.setFind(ESObjectUtils.isNotNull(faxian) ? faxian : null);
                faxianZanResponseDTO.setFaxianCollectId(ESObjectUtils.isNotNull(faxianCollect) ? faxianCollect.getId() : null);
                faxianZanResponseDTOList.add(faxianZanResponseDTO);
            }
        }
        return new BusinessData<>(
                new PageResponseData<>(
                        pageResponseData.getPage(),
                        pageResponseData.getPage(),
                        pageResponseData.getTotalPages(),
                        pageResponseData.getTotalElements(),
                        faxianZanResponseDTOList
                )
        );
    }
}