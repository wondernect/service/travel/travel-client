package com.wondernect.travel.business.faxian.manager;

import com.wondernect.elements.common.exception.BusinessException;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.common.utils.ESStringUtils;
import com.wondernect.elements.rdb.base.manager.BaseStringManager;
import com.wondernect.travel.business.faxian.model.Faxian;
import com.wondernect.travel.business.faxian.model.FaxianEvaluate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 我的评论服务操作类
 *
 * @author chenxun 2020-10-31 14:37:36
 **/
@Service
public class FaxianEvaluateManager extends BaseStringManager<FaxianEvaluate> {

    @Autowired
    private FaxianManager faxianManager;

    @Transactional
    @Override
    public FaxianEvaluate save(FaxianEvaluate faxianEvaluate) {
        Faxian faxian = faxianManager.findById(faxianEvaluate.getFindId());
        if (ESObjectUtils.isNull(faxian)) {
            throw new BusinessException("发现信息不存在");
        }
        faxianEvaluate.setFaxianCategory(faxian.getFaxianCategory());
        faxianEvaluate = super.save(faxianEvaluate);
        if (faxianEvaluate.getValid()) {
            Integer count = ESObjectUtils.isNotNull(faxian.getEvaluateCount()) ? faxian.getEvaluateCount() : 0;
            count++;
            faxian.setEvaluateCount(count);
            faxianManager.save(faxian);
        }
        return faxianEvaluate;
    }

    @Transactional
    @Override
    public void deleteById(String id) {
        FaxianEvaluate faxianEvaluate = super.findById(id);
        if (ESObjectUtils.isNull(faxianEvaluate)) {
            throw new BusinessException("评价记录不存在");
        }
        Faxian faxian = faxianManager.findById(faxianEvaluate.getFindId());
        if (ESObjectUtils.isNull(faxian)) {
            throw new BusinessException("发现信息不存在");
        }
        super.deleteById(id);
        Integer count = ESObjectUtils.isNotNull(faxian.getEvaluateCount()) ? faxian.getEvaluateCount() : 1;
        if (count <= 0) {
            count = 1;
        }
        count--;
        faxian.setEvaluateCount(count);
        faxianManager.save(faxian);
    }

    @Transactional
    public FaxianEvaluate valid(String id, Boolean valid) {
        FaxianEvaluate faxianEvaluate = super.findById(id);
        if (ESObjectUtils.isNull(faxianEvaluate)) {
            throw new BusinessException("评价记录不存在");
        }
        Faxian faxian = faxianManager.findById(faxianEvaluate.getFindId());
        if (ESObjectUtils.isNull(faxian)) {
            throw new BusinessException("发现信息不存在");
        }
        boolean originValid = faxianEvaluate.getValid();
        if (originValid) {
            if (!valid) {
                // -1
                Integer count = ESObjectUtils.isNotNull(faxian.getEvaluateCount()) ? faxian.getEvaluateCount() : 1;
                if (count <= 0) {
                    count = 1;
                }
                count--;
                faxian.setEvaluateCount(count);
                faxianManager.save(faxian);
            }
        } else {
            if (valid) {
                // +1
                Integer count = ESObjectUtils.isNotNull(faxian.getEvaluateCount()) ? faxian.getEvaluateCount() : 0;
                count++;
                faxian.setEvaluateCount(count);
                faxianManager.save(faxian);
            }
        }
        faxianEvaluate.setValid(valid);
        faxianEvaluate = super.save(faxianEvaluate);
        return faxianEvaluate;
    }
}