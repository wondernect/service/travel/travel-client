package com.wondernect.travel.business.faxian.manager;

import com.wondernect.elements.common.exception.BusinessException;
import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.common.utils.ESStringUtils;
import com.wondernect.elements.rdb.base.manager.BaseStringManager;
import com.wondernect.travel.business.faxian.model.Faxian;
import com.wondernect.travel.business.faxian.model.FaxianZan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 我的点赞服务操作类
 *
 * @author chenxun 2020-10-31 14:37:51
 **/
@Service
public class FaxianZanManager extends BaseStringManager<FaxianZan> {

    @Autowired
    private FaxianManager faxianManager;

    @Transactional
    @Override
    public FaxianZan save(FaxianZan faxianZan) {
        Faxian faxian = faxianManager.findById(faxianZan.getFindId());
        if (ESObjectUtils.isNull(faxian)) {
            throw new BusinessException("发现信息不存在");
        }
        faxianZan.setFaxianCategory(faxian.getFaxianCategory());
        if (ESStringUtils.isBlank(faxianZan.getId())) {
            faxianZan.setId(faxianZan.getUserId() + faxianZan.getFindId());
        }
        faxianZan = super.save(faxianZan);
        Integer count = ESObjectUtils.isNotNull(faxian.getZanCount()) ? faxian.getZanCount() : 0;
        count++;
        faxian.setZanCount(count);
        faxianManager.save(faxian);
        return faxianZan;
    }

    @Transactional
    @Override
    public void deleteById(String id) {
        FaxianZan faxianZan = super.findById(id);
        if (ESObjectUtils.isNull(faxianZan)) {
            throw new BusinessException("点赞记录不存在");
        }
        Faxian faxian = faxianManager.findById(faxianZan.getFindId());
        if (ESObjectUtils.isNull(faxian)) {
            throw new BusinessException("发现信息不存在");
        }
        super.deleteById(id);
        Integer count = ESObjectUtils.isNotNull(faxian.getZanCount()) ? faxian.getZanCount() : 1;
        if (count <= 0) {
            count = 1;
        }
        count--;
        faxian.setZanCount(count);
        faxianManager.save(faxian);
    }
}