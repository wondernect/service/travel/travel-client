package com.wondernect.travel.business.faxian.dao;

import com.wondernect.elements.rdb.base.dao.BaseStringDao;
import com.wondernect.travel.business.faxian.model.FaxianZan;
import org.springframework.stereotype.Repository;

/**
 * 我的点赞数据库操作类
 *
 * @author chenxun 2020-10-31 14:37:51
 **/
@Repository
public class FaxianZanDao extends BaseStringDao<FaxianZan> {
}