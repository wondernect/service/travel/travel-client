package com.wondernect.travel.business.zuowei.manager;

import com.wondernect.elements.rdb.base.manager.BaseStringManager;
import com.wondernect.travel.business.zuowei.model.Zuowei;
import org.springframework.stereotype.Service;

/**
 * 座位配置服务操作类
 *
 * @author chenxun 2020-07-06 18:39:13
 **/
@Service
public class ZuoweiManager extends BaseStringManager<Zuowei> {
}