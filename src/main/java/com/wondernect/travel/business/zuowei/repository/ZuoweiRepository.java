package com.wondernect.travel.business.zuowei.repository;

import com.wondernect.elements.rdb.base.repository.BaseStringRepository;
import com.wondernect.travel.business.zuowei.model.Zuowei;

/**
 * 座位配置数据库操作类
 *
 * @author chenxun 2020-07-06 18:39:13
 **/
public interface ZuoweiRepository extends BaseStringRepository<Zuowei> {
}