package com.wondernect.travel.business.zuowei.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.base.model.BaseStringModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

/**
 * Copyright (C), 2017-2020, wondernect.com
 * FileName: Zuowei
 * Author: chenxun
 * Date: 2020/6/13 15:48
 * Description: 座位配置
 */
@Entity
@Table(name = "zuowei")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "座位配置")
public class Zuowei extends BaseStringModel {

    @NotBlank(message = "名称不能为空")
    @Column(columnDefinition = "varchar(255) not null")
    @JsonProperty("name")
    @ApiModelProperty(value = "名称")
    private String name;
}
