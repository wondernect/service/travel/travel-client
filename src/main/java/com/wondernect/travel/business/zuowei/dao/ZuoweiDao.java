package com.wondernect.travel.business.zuowei.dao;

import com.wondernect.elements.rdb.base.dao.BaseStringDao;
import com.wondernect.travel.business.zuowei.model.Zuowei;
import org.springframework.stereotype.Repository;

/**
 * 座位配置数据库操作类
 *
 * @author chenxun 2020-07-06 18:39:13
 **/
@Repository
public class ZuoweiDao extends BaseStringDao<Zuowei> {
}