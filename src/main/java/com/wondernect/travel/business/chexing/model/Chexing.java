package com.wondernect.travel.business.chexing.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.base.model.BaseStringModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

/**
 * Copyright (C), 2017-2020, wondernect.com
 * FileName: Chexing
 * Author: chenxun
 * Date: 2020/6/13 16:35
 * Description: 车型配置
 */
@Entity
@Table(name = "chexing")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "车型配置")
public class Chexing extends BaseStringModel {

    @NotBlank(message = "名称不能为空")
    @Column(columnDefinition = "varchar(255) not null")
    @JsonProperty("name")
    @ApiModelProperty(value = "名称")
    private String name;

    @JsonProperty("description")
    @ApiModelProperty(value = "描述")
    private String description;

    @JsonProperty("cover_image")
    @ApiModelProperty(value = "封面路片")
    private String coverImage;

    @JsonProperty("passengers")
    @ApiModelProperty(value = "乘客说明")
    private String passengers;

    @JsonProperty("baggages")
    @ApiModelProperty(value = "行李说明")
    private String baggages;
}
