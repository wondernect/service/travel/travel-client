package com.wondernect.travel.business.chexing.repository;

import com.wondernect.elements.rdb.base.repository.BaseStringRepository;
import com.wondernect.travel.business.chexing.model.Chexing;

/**
 * 车型配置数据库操作类
 *
 * @author chenxun 2020-07-06 18:25:02
 **/
public interface ChexingRepository extends BaseStringRepository<Chexing> {
}