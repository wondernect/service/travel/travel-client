package com.wondernect.travel.business.chexing.dao;

import com.wondernect.elements.rdb.base.dao.BaseStringDao;
import com.wondernect.travel.business.chexing.model.Chexing;
import org.springframework.stereotype.Repository;

/**
 * 车型配置数据库操作类
 *
 * @author chenxun 2020-07-06 18:25:03
 **/
@Repository
public class ChexingDao extends BaseStringDao<Chexing> {
}