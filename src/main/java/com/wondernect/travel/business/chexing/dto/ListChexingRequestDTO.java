package com.wondernect.travel.business.chexing.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wondernect.elements.rdb.request.SortData;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 车型配置列表请求DTO
 *
 * @author chenxun 2020-07-06 18:25:03
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "车型配置列表请求对象")
public class ListChexingRequestDTO {

    @JsonProperty("sort_data_list")
    @ApiModelProperty(notes = "列表请求参数")
    private List<SortData> sortDataList;
}