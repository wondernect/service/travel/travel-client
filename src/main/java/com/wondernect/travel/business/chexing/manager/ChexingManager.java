package com.wondernect.travel.business.chexing.manager;

import com.wondernect.elements.rdb.base.manager.BaseStringManager;
import com.wondernect.travel.business.chexing.model.Chexing;
import org.springframework.stereotype.Service;

/**
 * 车型配置服务操作类
 *
 * @author chenxun 2020-07-06 18:25:03
 **/
@Service
public class ChexingManager extends BaseStringManager<Chexing> {
}