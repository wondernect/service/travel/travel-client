package com.wondernect.travel.repository.crud;

import com.wondernect.elements.rdb.base.repository.BaseStringRepository;
import com.wondernect.travel.model.Shop;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: ShopRepository
 * Author: chenxun
 * Date: 2019/5/11 10:24
 * Description:
 */
public interface ShopRepository extends BaseStringRepository<Shop> {

}
