package com.wondernect.travel.repository.crud;

import com.wondernect.elements.rdb.base.repository.BaseSortRepository;
import com.wondernect.travel.model.PrivateConsume;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: PrivateConsumeRepository
 * Author: chenxun
 * Date: 2019-10-05 16:38
 * Description: 包车服务配置
 */
public interface PrivateConsumeRepository extends BaseSortRepository<PrivateConsume, String> {

}
