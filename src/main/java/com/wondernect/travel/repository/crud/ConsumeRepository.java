package com.wondernect.travel.repository.crud;

import com.wondernect.elements.rdb.base.repository.BaseStringRepository;
import com.wondernect.travel.model.Consume;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: ConsumeRepository
 * Author: chenxun
 * Date: 2019/5/11 16:03
 * Description:
 */
public interface ConsumeRepository extends BaseStringRepository<Consume> {

}
