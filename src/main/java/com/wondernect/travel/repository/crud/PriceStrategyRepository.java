package com.wondernect.travel.repository.crud;

import com.wondernect.elements.rdb.base.repository.BaseStringRepository;
import com.wondernect.travel.model.PriceStrategy;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: PriceStrategyRepository
 * Author: chenxun
 * Date: 2019/5/11 16:05
 * Description:
 */
public interface PriceStrategyRepository extends BaseStringRepository<PriceStrategy> {

}
