package com.wondernect.travel.repository.crud;

import com.wondernect.elements.rdb.base.repository.BaseStringRepository;
import com.wondernect.travel.model.UserShop;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: UserShopRepository
 * Author: chenxun
 * Date: 2019/5/11 10:25
 * Description:
 */
public interface UserShopRepository extends BaseStringRepository<UserShop> {

}
