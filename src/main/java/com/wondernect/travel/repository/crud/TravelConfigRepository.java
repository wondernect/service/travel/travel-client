package com.wondernect.travel.repository.crud;

import com.wondernect.elements.rdb.base.repository.BaseSortRepository;
import com.wondernect.travel.model.TravelConfig;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: CarLevelRepository
 * Author: chenxun
 * Date: 2019/5/11 16:40
 * Description:
 */
public interface TravelConfigRepository extends BaseSortRepository<TravelConfig, String> {

    TravelConfig findByCode(String code);
}
