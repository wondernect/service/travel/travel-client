package com.wondernect.travel.repository.crud;

import com.wondernect.elements.rdb.base.repository.BaseStringRepository;
import com.wondernect.travel.model.Car;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: CarRepository
 * Author: chenxun
 * Date: 2019/5/11 16:42
 * Description:
 */
public interface CarRepository extends BaseStringRepository<Car> {

    Car findByDriverUserId(String driverUserId);
}
