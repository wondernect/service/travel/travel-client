package com.wondernect.travel.repository.crud;

import com.wondernect.elements.rdb.base.repository.BaseStringRepository;
import com.wondernect.travel.model.ConsumeCarLevel;
import org.springframework.data.jpa.repository.Modifying;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: ConsumeCarLevelRepository
 * Author: chenxun
 * Date: 2019/5/11 16:42
 * Description:
 */
public interface ConsumeCarLevelRepository extends BaseStringRepository<ConsumeCarLevel> {

    @Modifying
    void deleteAllByConsumeId(String consumeId);

    boolean existsByZuoweiIdOrChexingId(String zuoweiId, String chexingId);

    boolean existsByPriceStrategyId(String priceStrategyId);
}
