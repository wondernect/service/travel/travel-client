package com.wondernect.travel.repository.crud;

import com.wondernect.elements.rdb.base.repository.BaseStringRepository;
import com.wondernect.travel.model.TravelOrder;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: TravelOrderRepository
 * Author: chenxun
 * Date: 2019/5/11 16:46
 * Description:
 */
public interface TravelOrderRepository extends BaseStringRepository<TravelOrder> {

}
