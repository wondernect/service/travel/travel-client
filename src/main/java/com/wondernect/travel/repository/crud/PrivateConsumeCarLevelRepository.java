package com.wondernect.travel.repository.crud;

import com.wondernect.elements.rdb.base.repository.BaseSortRepository;
import com.wondernect.travel.model.PrivateConsumeCarLevel;
import org.springframework.data.jpa.repository.Modifying;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: PrivateConsumeCarLevelRepository
 * Author: chenxun
 * Date: 2019-10-05 17:06
 * Description: 包车服务
 */
public interface PrivateConsumeCarLevelRepository extends BaseSortRepository<PrivateConsumeCarLevel, String> {

    @Modifying
    void deleteAllByPrivateConsumeId(String privateConsumeId);

    boolean existsByZuoweiIdOrChexingId(String zuoweiId, String chexingId);
}
