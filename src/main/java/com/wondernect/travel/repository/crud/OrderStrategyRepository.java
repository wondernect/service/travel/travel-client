package com.wondernect.travel.repository.crud;

import com.wondernect.elements.rdb.base.repository.BaseSortRepository;
import com.wondernect.travel.model.OrderStrategy;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: OrderStrategyRepository
 * Author: chenxun
 * Date: 2019/5/11 16:39
 * Description:
 */
public interface OrderStrategyRepository extends BaseSortRepository<OrderStrategy, String> {

}
