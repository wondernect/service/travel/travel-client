package com.wondernect.travel.repository.crud;

import com.wondernect.elements.rdb.base.repository.BaseStringRepository;
import com.wondernect.travel.model.RefundOrder;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: TravelOrderRepository
 * Author: chenxun
 * Date: 2019/5/11 16:46
 * Description:
 */
public interface RefundOrderRepository extends BaseStringRepository<RefundOrder> {

    RefundOrder findByOrderId(String orderId);
}
