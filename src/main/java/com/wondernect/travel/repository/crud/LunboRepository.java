package com.wondernect.travel.repository.crud;

import com.wondernect.elements.rdb.base.repository.BaseSortRepository;
import com.wondernect.travel.model.Lunbo;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: LunboRepository
 * Author: chenxun
 * Date: 2019-10-04 08:06
 * Description: 轮播图
 */
public interface LunboRepository extends BaseSortRepository<Lunbo, String> {

}
