package com.wondernect.travel.repository.crud;

import com.wondernect.elements.rdb.base.repository.BaseRepository;
import com.wondernect.travel.model.RefundConfig;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: RefundConfigRepository
 * Author: chenxun
 * Date: 2019/5/11 16:46
 * Description:
 */
public interface RefundConfigRepository extends BaseRepository<RefundConfig, String> {

}
