package com.wondernect.travel.repository.crud;

import com.wondernect.elements.rdb.base.repository.BaseStringRepository;
import com.wondernect.travel.model.PrivateConsumeEvaluate;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: PrivateConsumeRepository
 * Author: chenxun
 * Date: 2019-10-05 16:38
 * Description:
 */
public interface PrivateConsumeEvaluateRepository extends BaseStringRepository<PrivateConsumeEvaluate> {

}
