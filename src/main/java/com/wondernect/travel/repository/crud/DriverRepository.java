package com.wondernect.travel.repository.crud;

import com.wondernect.elements.rdb.base.repository.BaseStringRepository;
import com.wondernect.travel.model.Driver;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: DriverRepository
 * Author: chenxun
 * Date: 2019/5/11 10:23
 * Description:
 */
public interface DriverRepository extends BaseStringRepository<Driver> {

    Driver findByUserId(String userId);
}
