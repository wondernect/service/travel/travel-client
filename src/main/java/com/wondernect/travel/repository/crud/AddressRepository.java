package com.wondernect.travel.repository.crud;

import com.wondernect.elements.rdb.base.repository.BaseStringRepository;
import com.wondernect.travel.model.Address;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: AddressRepository
 * Author: chenxun
 * Date: 2019/5/11 16:41
 * Description:
 */
public interface AddressRepository extends BaseStringRepository<Address> {

}
