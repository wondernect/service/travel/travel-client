package com.wondernect.travel.repository.crud;

import com.wondernect.elements.rdb.base.repository.BaseSortRepository;
import com.wondernect.travel.model.PrivateConsumeRoad;
import org.springframework.data.jpa.repository.Modifying;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: PrivateConsumeRoadRepository
 * Author: chenxun
 * Date: 2019-10-05 17:30
 * Description: 包车服务行程路线
 */
public interface PrivateConsumeRoadRepository extends BaseSortRepository<PrivateConsumeRoad, String> {

    @Modifying
    void deleteAllByPrivateConsumeId(String privateConsumeId);
}
