package com.wondernect.travel.repository.crud;

import com.wondernect.elements.rdb.base.repository.BaseStringRepository;
import com.wondernect.travel.model.DriverEvaluate;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: DriverEvaluateRepository
 * Author: chenxun
 * Date: 2019/5/11 16:45
 * Description:
 */
public interface DriverEvaluateRepository extends BaseStringRepository<DriverEvaluate> {

    DriverEvaluate findTopByUserIdAndOrderId(String userId, String orderId);
}
