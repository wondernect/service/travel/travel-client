package com.wondernect.travel.repository;

import com.wondernect.elements.rdb.base.dao.BaseStringDao;
import com.wondernect.elements.rdb.common.error.RDBErrorEnum;
import com.wondernect.elements.rdb.common.exception.RDBException;
import com.wondernect.travel.model.Driver;
import com.wondernect.travel.repository.crud.DriverRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: DriverDao
 * Author: chenxun
 * Date: 2019/5/11 10:31
 * Description:
 */
@Repository
public class DriverDao extends BaseStringDao<Driver> {

    @Autowired
    private DriverRepository driverRepository;

    public Driver findByUserId(String userId) {
        Driver driver;
        try {
            driver = driverRepository.findByUserId(userId);
        } catch (RuntimeException e) {
            throw new RDBException(RDBErrorEnum.RDB_NOT_FOUND);
        }
        return driver;
    }
}
