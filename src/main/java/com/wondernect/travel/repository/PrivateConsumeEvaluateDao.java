package com.wondernect.travel.repository;

import com.wondernect.elements.rdb.base.dao.BaseStringDao;
import com.wondernect.travel.model.PrivateConsumeEvaluate;
import org.springframework.stereotype.Repository;

/**
 * Copyright (C), 2020, wondernect.com
 * FileName: PrivateConsumeEvaluateDao
 * Author: chenxun
 * Date: 2020-08-09 10:03
 * Description:
 */
@Repository
public class PrivateConsumeEvaluateDao extends BaseStringDao<PrivateConsumeEvaluate> {
}
