package com.wondernect.travel.repository;

import com.wondernect.elements.rdb.base.dao.BaseStringDao;
import com.wondernect.elements.rdb.common.error.RDBErrorEnum;
import com.wondernect.elements.rdb.common.exception.RDBException;
import com.wondernect.travel.model.ConsumeCarLevel;
import com.wondernect.travel.repository.crud.ConsumeCarLevelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: ConsumeCarLevelDao
 * Author: chenxun
 * Date: 2019/5/11 16:49
 * Description:
 */
@Repository
public class ConsumeCarLevelDao extends BaseStringDao<ConsumeCarLevel> {

    @Autowired
    private ConsumeCarLevelRepository consumeCarLevelRepository;

    public void deleteAllByConsumeId(String consumeId) {
        try {
            consumeCarLevelRepository.deleteAllByConsumeId(consumeId);
        } catch (RuntimeException e) {
            throw new RDBException(RDBErrorEnum.RDB_DELETE_ALL_FAILED);
        }
    }

    public boolean existsByZuoweiIdOrChexingId(String zuoweiId, String chexingId) {
        boolean exist;
        try {
            exist = consumeCarLevelRepository.existsByZuoweiIdOrChexingId(zuoweiId, chexingId);
        } catch (RuntimeException e) {
            throw new RDBException(RDBErrorEnum.RDB_EXIST_GET_FAILED);
        }
        return exist;
    }

    public boolean existsByPriceStrategyId(String priceStrategyId) {
        boolean exist;
        try {
            exist = consumeCarLevelRepository.existsByPriceStrategyId(priceStrategyId);
        } catch (RuntimeException e) {
            throw new RDBException(RDBErrorEnum.RDB_EXIST_GET_FAILED);
        }
        return exist;
    }
}
