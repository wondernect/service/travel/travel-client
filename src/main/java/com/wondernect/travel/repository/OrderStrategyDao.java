package com.wondernect.travel.repository;

import com.wondernect.elements.algorithm.snowflake.SnowFlakeAlgorithm;
import com.wondernect.elements.rdb.base.dao.BaseSortDao;
import com.wondernect.travel.model.OrderStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: OrderStrategyDao
 * Author: chenxun
 * Date: 2019/5/11 16:51
 * Description:
 */
@Repository
public class OrderStrategyDao extends BaseSortDao<OrderStrategy, String> {

    @Autowired
    private SnowFlakeAlgorithm snowFlakeAlgorithm;

    @Override
    protected String generateIdentifier() {
        return String.valueOf(snowFlakeAlgorithm.getSnowflake().nextId());
    }
}
