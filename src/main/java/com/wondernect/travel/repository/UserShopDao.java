package com.wondernect.travel.repository;

import com.wondernect.elements.rdb.base.dao.BaseStringDao;
import com.wondernect.travel.model.UserShop;
import org.springframework.stereotype.Repository;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: UserShopDao
 * Author: chenxun
 * Date: 2019/5/11 10:36
 * Description:
 */
@Repository
public class UserShopDao extends BaseStringDao<UserShop> {

}
