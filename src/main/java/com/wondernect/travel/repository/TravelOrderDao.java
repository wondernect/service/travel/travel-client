package com.wondernect.travel.repository;

import com.wondernect.elements.common.utils.ESObjectUtils;
import com.wondernect.elements.common.utils.ESStringUtils;
import com.wondernect.elements.rdb.base.dao.BaseStringDao;
import com.wondernect.elements.rdb.criteria.Criteria;
import com.wondernect.elements.rdb.criteria.Restrictions;
import com.wondernect.elements.rdb.request.PageRequestData;
import com.wondernect.elements.rdb.request.SortData;
import com.wondernect.elements.rdb.response.PageResponseData;
import com.wondernect.travel.model.TravelOrder;
import com.wondernect.travel.model.em.OrderSource;
import com.wondernect.travel.model.em.OrderStatus;
import com.wondernect.travel.model.em.Scene;
import com.wondernect.travel.model.em.WarningStatus;
import org.hibernate.criterion.MatchMode;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: TravelOrderDao
 * Author: chenxun
 * Date: 2019/5/11 16:53
 * Description:
 */
@Repository
public class TravelOrderDao extends BaseStringDao<TravelOrder> {

    public Long countAllByUserIdAndOrderStatusIn(String userId) {
        Criteria<TravelOrder> travelOrderCriteria = new Criteria<>();
        travelOrderCriteria.add(
                Restrictions.and(
                        Restrictions.eq("userId", userId),
                        Restrictions.or(
                                Restrictions.eq("orderStatus", OrderStatus.WAIT_APPROVAL_OR_PAY),
                                Restrictions.eq("orderStatus", OrderStatus.WAIT_ACCEPT),
                                Restrictions.eq("orderStatus", OrderStatus.AUTO),
                                Restrictions.eq("orderStatus", OrderStatus.ACCEPTED),
                                Restrictions.eq("orderStatus", OrderStatus.ON_THE_WAY)
                        )
                )
        );
        return super.count(travelOrderCriteria);
    }

    public PageResponseData<TravelOrder> driverOrderList(String cityId, List<String> zuoweiIdList, List<String> chexingIdList,PageRequestData pageRequestData) {
        Criteria<TravelOrder> travelOrderCriteria = new Criteria<>();
        travelOrderCriteria.add(
                Restrictions.and(
                        Restrictions.eq("cityId", cityId),
                        Restrictions.in("zuoweiId", zuoweiIdList),
                        Restrictions.in("chexingId", chexingIdList),
                        Restrictions.ne("scene", Scene.BANSHOU_PRIVATE),
                        Restrictions.or(
                                Restrictions.eq("orderStatus", OrderStatus.WAIT_ACCEPT),
                                Restrictions.eq("orderStatus", OrderStatus.AUTO)
                        )
                )
        );
        return super.findAll(travelOrderCriteria, pageRequestData);
    }

    public List<TravelOrder> driverSettledOrderList(String driverUserId, Long startTime, Long endTime, List<SortData> sortDataList) {
        Criteria<TravelOrder> travelOrderCriteria = new Criteria<>();
        travelOrderCriteria.add(
                Restrictions.and(
                        Restrictions.eq("driverUserId", driverUserId),
                        Restrictions.gte("doneTime", startTime),
                        Restrictions.lte("doneTime", endTime),
                        Restrictions.or(
                                Restrictions.eq("orderStatus", OrderStatus.DONE),
                                Restrictions.eq("orderStatus", OrderStatus.SETTLED)
                        )
                )
        );
        return super.findAll(travelOrderCriteria, sortDataList);
    }

    public PageResponseData<TravelOrder> orderPage(
            List<Scene> sceneList,
            OrderStatus orderStatus,
            OrderSource orderSource,
            String shopId,
            String sourceShopId,
            String sourceDriverUserId,
            String driverUserId,
            Integer type,
            Long start,
            Long end,
            String value,
            PageRequestData pageRequestData
    ) {
        Criteria<TravelOrder> travelOrderCriteria = new Criteria<>();
        travelOrderCriteria.add(Restrictions.in("scene", sceneList));
        travelOrderCriteria.add(Restrictions.eq("orderStatus", orderStatus));
        travelOrderCriteria.add(Restrictions.eq("orderSource", orderSource));
        travelOrderCriteria.add(Restrictions.eq("driverUserId", driverUserId));
        travelOrderCriteria.add(Restrictions.eq("shopId", shopId));
        travelOrderCriteria.add(Restrictions.eq("sourceShopId", sourceShopId));
        travelOrderCriteria.add(Restrictions.eq("sourceDriverUserId", sourceDriverUserId));
        if (ESObjectUtils.isNotNull(type)) {
            switch (type) {
                case 1:
                {
                    travelOrderCriteria.add(Restrictions.gte("createTime", start));
                    travelOrderCriteria.add(Restrictions.lte("createTime", end));
                    break;
                }
                case 2:
                {
                    travelOrderCriteria.add(Restrictions.gte("startTime", start));
                    travelOrderCriteria.add(Restrictions.lte("startTime", end));
                    break;
                }
            }
        }
        travelOrderCriteria.add(
                Restrictions.or(
                        Restrictions.like("username", value, MatchMode.ANYWHERE),
                        Restrictions.like("mobile", value, MatchMode.ANYWHERE),
                        Restrictions.like("id", value, MatchMode.ANYWHERE)
                )
        );
        return super.findAll(travelOrderCriteria, pageRequestData);
    }

    public PageResponseData<TravelOrder> settledPage(Integer settledType, OrderSource orderSource, String shopId, String sourceShopId, String orderId, String value, String targetPlace, Long settledStart, Long settledEnd, Long createStart, Long createEnd, PageRequestData pageRequestData) {
        Criteria<TravelOrder> travelOrderCriteria = new Criteria<>();
        if (ESObjectUtils.isNull(settledType)) {
            travelOrderCriteria.add(
                    Restrictions.or(
                            Restrictions.eq("orderStatus", OrderStatus.DONE),
                            Restrictions.eq("orderStatus", OrderStatus.SETTLED)
                    )
            );
        } else {
            switch (settledType) {
                case 1:
                {
                    // 已结算
                    travelOrderCriteria.add(
                            Restrictions.eq("orderStatus", OrderStatus.SETTLED)
                    );
                    break;
                }
                case 2:
                {
                    // 待结算
                    travelOrderCriteria.add(Restrictions.eq("orderStatus", OrderStatus.DONE));
                    break;
                }
            }
        }
        travelOrderCriteria.add(Restrictions.eq("orderSource", orderSource));
        travelOrderCriteria.add(Restrictions.eq("shopId", shopId));
        travelOrderCriteria.add(Restrictions.eq("sourceShopId", sourceShopId));
        travelOrderCriteria.add(Restrictions.like("id", orderId, MatchMode.ANYWHERE));
        travelOrderCriteria.add(
                Restrictions.or(
                        Restrictions.like("username", value, MatchMode.ANYWHERE),
                        Restrictions.like("mobile", value, MatchMode.ANYWHERE)
                )
        );
        travelOrderCriteria.add(Restrictions.like("targetPlace", targetPlace, MatchMode.ANYWHERE));
        travelOrderCriteria.add(Restrictions.gte("settledTime", settledStart));
        travelOrderCriteria.add(Restrictions.lte("settledTime", settledEnd));
        travelOrderCriteria.add(Restrictions.gte("createTime", createStart));
        travelOrderCriteria.add(Restrictions.lte("createTime", createEnd));
        return super.findAll(travelOrderCriteria, pageRequestData);
    }

    public PageResponseData<TravelOrder> warningPage(WarningStatus warningStatus, String username, String orderId, String startPlace, String targetPlace, Long createStart, Long createEnd, Long executeStart, Long executeEnd, Double lowPrice, Double highPrice, PageRequestData pageRequestData) {
        Criteria<TravelOrder> travelOrderCriteria = new Criteria<>();
        if (ESObjectUtils.isNull(warningStatus)) {
            travelOrderCriteria.add(
                    Restrictions.or(
                            Restrictions.eq("warningStatus", WarningStatus.WARNING),
                            Restrictions.eq("warningStatus", WarningStatus.URGENT)
                    )
            );
        } else {
            travelOrderCriteria.add(Restrictions.eq("warningStatus", warningStatus));
        }
        travelOrderCriteria.add(Restrictions.like("username", username, MatchMode.ANYWHERE));
        travelOrderCriteria.add(Restrictions.like("id", orderId, MatchMode.ANYWHERE));
        travelOrderCriteria.add(Restrictions.like("startPlace", startPlace, MatchMode.ANYWHERE));
        travelOrderCriteria.add(Restrictions.like("targetPlace", targetPlace, MatchMode.ANYWHERE));
        travelOrderCriteria.add(Restrictions.gte("createTime", createStart));
        travelOrderCriteria.add(Restrictions.lte("createTime", createEnd));
        travelOrderCriteria.add(Restrictions.gte("startTime", executeStart));
        travelOrderCriteria.add(Restrictions.lte("startTime", executeEnd));
        travelOrderCriteria.add(Restrictions.gte("price", lowPrice));
        travelOrderCriteria.add(Restrictions.lte("price", highPrice));
        return super.findAll(travelOrderCriteria, pageRequestData);
    }

    public Long countOrder(WarningStatus warningStatus) {
        Criteria<TravelOrder> travelOrderCriteria = new Criteria<>();
        travelOrderCriteria.add(Restrictions.eq("warningStatus", warningStatus));
        return super.count(travelOrderCriteria);
    }
}
