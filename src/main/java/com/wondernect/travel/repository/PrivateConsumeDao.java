package com.wondernect.travel.repository;

import com.wondernect.elements.algorithm.snowflake.SnowFlakeAlgorithm;
import com.wondernect.elements.rdb.base.dao.BaseSortDao;
import com.wondernect.travel.model.PrivateConsume;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: PrivateConsumeDao
 * Author: chenxun
 * Date: 2019-10-05 16:39
 * Description: 包车服务
 */
@Repository
public class PrivateConsumeDao extends BaseSortDao<PrivateConsume, String> {

    @Autowired
    private SnowFlakeAlgorithm snowFlakeAlgorithm;

    @Override
    protected String generateIdentifier() {
        return String.valueOf(snowFlakeAlgorithm.getSnowflake().nextId());
    }
}
