package com.wondernect.travel.repository;

import com.wondernect.elements.algorithm.snowflake.SnowFlakeAlgorithm;
import com.wondernect.elements.rdb.base.dao.BaseSortDao;
import com.wondernect.elements.rdb.common.error.RDBErrorEnum;
import com.wondernect.elements.rdb.common.exception.RDBException;
import com.wondernect.travel.model.PrivateConsumeRoad;
import com.wondernect.travel.repository.crud.PrivateConsumeRoadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: PrivateConsumeRoadDao
 * Author: chenxun
 * Date: 2019-10-06 10:19
 * Description: 包车服务路线配置
 */
@Repository
public class PrivateConsumeRoadDao extends BaseSortDao<PrivateConsumeRoad, String> {

    @Autowired
    private PrivateConsumeRoadRepository privateConsumeRoadRepository;

    @Autowired
    private SnowFlakeAlgorithm snowFlakeAlgorithm;

    @Override
    protected String generateIdentifier() {
        return String.valueOf(snowFlakeAlgorithm.getSnowflake().nextId());
    }

    public void deleteAllByPrivateConsumeId(String privateConsumeId) {
        try {
            privateConsumeRoadRepository.deleteAllByPrivateConsumeId(privateConsumeId);
        } catch (RuntimeException e) {
            throw new RDBException(RDBErrorEnum.RDB_DELETE_ALL_FAILED);
        }
    }
}
