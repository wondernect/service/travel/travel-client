package com.wondernect.travel.repository;

import com.wondernect.elements.rdb.base.dao.BaseStringDao;
import com.wondernect.elements.rdb.common.error.RDBErrorEnum;
import com.wondernect.elements.rdb.common.exception.RDBException;
import com.wondernect.travel.model.RefundOrder;
import com.wondernect.travel.repository.crud.RefundOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: TravelOrderDao
 * Author: chenxun
 * Date: 2019/5/11 16:53
 * Description:
 */
@Repository
public class RefundOrderDao extends BaseStringDao<RefundOrder> {

    @Autowired
    private RefundOrderRepository refundOrderRepository;

    public RefundOrder findByOrderId(String orderId) {
        RefundOrder refundOrder;
        try {
            refundOrder = refundOrderRepository.findByOrderId(orderId);
        } catch (RuntimeException e) {
            throw new RDBException(RDBErrorEnum.RDB_GET_FAILED);
        }
        return refundOrder;
    }
}
