package com.wondernect.travel.repository;

import com.wondernect.elements.rdb.base.dao.BaseStringDao;
import com.wondernect.elements.rdb.common.error.RDBErrorEnum;
import com.wondernect.elements.rdb.common.exception.RDBException;
import com.wondernect.travel.model.DriverEvaluate;
import com.wondernect.travel.repository.crud.DriverEvaluateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: DriverEvaluateDao
 * Author: chenxun
 * Date: 2019/5/11 16:51
 * Description:
 */
@Repository
public class DriverEvaluateDao extends BaseStringDao<DriverEvaluate> {

    @Autowired
    private DriverEvaluateRepository driverEvaluateRepository;

    public DriverEvaluate findTopByUserIdAndOrderId(String userId, String orderId) {
        DriverEvaluate driverEvaluate;
        try {
            driverEvaluate = driverEvaluateRepository.findTopByUserIdAndOrderId(userId, orderId);
        } catch (RuntimeException e) {
            throw new RDBException(RDBErrorEnum.RDB_GET_FAILED);
        }
        return driverEvaluate;
    }
}
