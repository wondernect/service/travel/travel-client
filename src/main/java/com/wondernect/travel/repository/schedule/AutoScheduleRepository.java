package com.wondernect.travel.repository.schedule;

import com.wondernect.elements.rdb.base.repository.BaseRepository;
import com.wondernect.travel.model.schedule.AutoSchedule;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: AutoScheduleRepository
 * Author: chenxun
 * Date: 2019-10-27 20:15
 * Description:
 */
public interface AutoScheduleRepository extends BaseRepository<AutoSchedule, String> {
}
