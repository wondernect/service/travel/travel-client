package com.wondernect.travel.repository.schedule;

import com.wondernect.elements.rdb.base.dao.BaseDao;
import com.wondernect.travel.model.schedule.AutoSchedule;
import org.springframework.stereotype.Repository;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: AutoScheduleDao
 * Author: chenxun
 * Date: 2019-10-27 20:16
 * Description:
 */
@Repository
public class AutoScheduleDao extends BaseDao<AutoSchedule, String> {
}
