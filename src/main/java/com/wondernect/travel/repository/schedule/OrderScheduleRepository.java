package com.wondernect.travel.repository.schedule;

import com.wondernect.elements.rdb.base.repository.BaseRepository;
import com.wondernect.travel.model.schedule.OrderSchedule;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: OrderScheduleRepository
 * Author: chenxun
 * Date: 2019-10-27 20:18
 * Description:
 */
public interface OrderScheduleRepository extends BaseRepository<OrderSchedule, String> {
}
