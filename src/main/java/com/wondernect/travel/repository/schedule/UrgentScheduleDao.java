package com.wondernect.travel.repository.schedule;

import com.wondernect.elements.rdb.base.dao.BaseDao;
import com.wondernect.travel.model.schedule.UrgentSchedule;
import org.springframework.stereotype.Repository;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: UrgentScheduleDao
 * Author: chenxun
 * Date: 2019-10-27 20:19
 * Description:
 */
@Repository
public class UrgentScheduleDao extends BaseDao<UrgentSchedule, String> {
}
