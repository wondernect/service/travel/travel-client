package com.wondernect.travel.repository.schedule;

import com.wondernect.elements.rdb.base.repository.BaseRepository;
import com.wondernect.travel.model.schedule.NowSchedule;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: NowScheduleRepository
 * Author: chenxun
 * Date: 2019-10-27 20:17
 * Description:
 */
public interface NowScheduleRepository extends BaseRepository<NowSchedule, String> {
}
