package com.wondernect.travel.repository.schedule;

import com.wondernect.elements.rdb.base.dao.BaseDao;
import com.wondernect.travel.model.schedule.WarningSchedule;
import org.springframework.stereotype.Repository;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: WarningScheduleDao
 * Author: chenxun
 * Date: 2019-10-27 20:20
 * Description:
 */
@Repository
public class WarningScheduleDao extends BaseDao<WarningSchedule, String> {
}
