package com.wondernect.travel.repository.schedule;

import com.wondernect.elements.rdb.base.repository.BaseRepository;
import com.wondernect.travel.model.schedule.WarningSchedule;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: WarningScheduleRepository
 * Author: chenxun
 * Date: 2019-10-27 20:20
 * Description:
 */
public interface WarningScheduleRepository extends BaseRepository<WarningSchedule, String> {
}
