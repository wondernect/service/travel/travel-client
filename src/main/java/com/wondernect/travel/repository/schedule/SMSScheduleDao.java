package com.wondernect.travel.repository.schedule;

import com.wondernect.elements.rdb.base.dao.BaseDao;
import com.wondernect.travel.model.schedule.SMSSchedule;
import org.springframework.stereotype.Repository;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: SMSScheduleDao
 * Author: chenxun
 * Date: 2019/10/25 8:44
 * Description:
 */
@Repository
public class SMSScheduleDao extends BaseDao<SMSSchedule, String> {

}
