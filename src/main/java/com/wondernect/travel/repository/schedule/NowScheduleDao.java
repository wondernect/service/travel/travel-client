package com.wondernect.travel.repository.schedule;

import com.wondernect.elements.rdb.base.dao.BaseDao;
import com.wondernect.travel.model.schedule.NowSchedule;
import org.springframework.stereotype.Repository;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: NowScheduleDao
 * Author: chenxun
 * Date: 2019-10-27 20:17
 * Description:
 */
@Repository
public class NowScheduleDao extends BaseDao<NowSchedule, String> {
}
