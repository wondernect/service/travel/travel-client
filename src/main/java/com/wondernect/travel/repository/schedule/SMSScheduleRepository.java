package com.wondernect.travel.repository.schedule;

import com.wondernect.elements.rdb.base.repository.BaseRepository;
import com.wondernect.travel.model.schedule.SMSSchedule;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: SMSScheduleRepository
 * Author: chenxun
 * Date: 2019/10/25 8:41
 * Description: 短信发送
 */
public interface SMSScheduleRepository extends BaseRepository<SMSSchedule, String> {

}
