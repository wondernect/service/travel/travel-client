package com.wondernect.travel.repository.schedule;

import com.wondernect.elements.rdb.base.repository.BaseRepository;
import com.wondernect.travel.model.schedule.UrgentSchedule;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: UrgentScheduleRepository
 * Author: chenxun
 * Date: 2019-10-27 20:19
 * Description:
 */
public interface UrgentScheduleRepository extends BaseRepository<UrgentSchedule, String> {
}
