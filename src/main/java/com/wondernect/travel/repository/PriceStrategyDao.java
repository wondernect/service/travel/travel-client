package com.wondernect.travel.repository;

import com.wondernect.elements.rdb.base.dao.BaseStringDao;
import com.wondernect.travel.model.PriceStrategy;
import org.springframework.stereotype.Repository;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: PriceStrategyDao
 * Author: chenxun
 * Date: 2019/5/11 16:52
 * Description:
 */
@Repository
public class PriceStrategyDao extends BaseStringDao<PriceStrategy> {

}
