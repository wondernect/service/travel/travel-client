package com.wondernect.travel.repository;

import com.wondernect.elements.algorithm.snowflake.SnowFlakeAlgorithm;
import com.wondernect.elements.rdb.base.dao.BaseSortDao;
import com.wondernect.travel.model.Lunbo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: LunboDao
 * Author: chenxun
 * Date: 2019-10-04 08:07
 * Description: 轮播图
 */
@Repository
public class LunboDao extends BaseSortDao<Lunbo, String> {

    @Autowired
    private SnowFlakeAlgorithm snowFlakeAlgorithm;

    @Override
    protected String generateIdentifier() {
        return String.valueOf(snowFlakeAlgorithm.getSnowflake().nextId());
    }
}
