package com.wondernect.travel.repository;

import com.wondernect.elements.algorithm.snowflake.SnowFlakeAlgorithm;
import com.wondernect.elements.rdb.base.dao.BaseSortDao;
import com.wondernect.travel.model.TravelConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: TravelConfigDao
 * Author: chenxun
 * Date: 2019-05-12 16:21
 * Description:
 */
@Repository
public class TravelConfigDao extends BaseSortDao<TravelConfig, String> {

    @Autowired
    private SnowFlakeAlgorithm snowFlakeAlgorithm;

    @Override
    protected String generateIdentifier() {
        return String.valueOf(snowFlakeAlgorithm.getSnowflake().nextId());
    }
}
