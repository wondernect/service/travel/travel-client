package com.wondernect.travel.repository;

import com.wondernect.elements.rdb.base.dao.BaseStringDao;
import com.wondernect.elements.rdb.common.error.RDBErrorEnum;
import com.wondernect.elements.rdb.common.exception.RDBException;
import com.wondernect.travel.model.Car;
import com.wondernect.travel.repository.crud.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: CarDao
 * Author: chenxun
 * Date: 2019/5/11 16:48
 * Description:
 */
@Repository
public class CarDao extends BaseStringDao<Car> {

    @Autowired
    private CarRepository carRepository;

    public Car findByDriverUserId(String driverUserId) {
        Car car;
        try {
            car = carRepository.findByDriverUserId(driverUserId);
        } catch (RuntimeException e) {
            throw new RDBException(RDBErrorEnum.RDB_GET_FAILED);
        }
        return car;
    }
}
