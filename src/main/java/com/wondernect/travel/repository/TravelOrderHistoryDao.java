package com.wondernect.travel.repository;

import com.wondernect.elements.rdb.base.dao.BaseStringDao;
import com.wondernect.travel.model.TravelOrderHistory;
import org.springframework.stereotype.Repository;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: TravelOrderHistoryDao
 * Author: chenxun
 * Date: 2019/7/5 10:05
 * Description:
 */
@Repository
public class TravelOrderHistoryDao extends BaseStringDao<TravelOrderHistory> {

}
