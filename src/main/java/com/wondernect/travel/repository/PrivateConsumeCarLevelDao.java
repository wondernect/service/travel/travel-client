package com.wondernect.travel.repository;

import com.wondernect.elements.algorithm.snowflake.SnowFlakeAlgorithm;
import com.wondernect.elements.rdb.base.dao.BaseSortDao;
import com.wondernect.elements.rdb.common.error.RDBErrorEnum;
import com.wondernect.elements.rdb.common.exception.RDBException;
import com.wondernect.travel.model.PrivateConsumeCarLevel;
import com.wondernect.travel.repository.crud.PrivateConsumeCarLevelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Copyright (C), 2019, wondernect.com
 * FileName: PrivateConsumeCarLevelDao
 * Author: chenxun
 * Date: 2019-10-06 10:18
 * Description: 包车服务配置
 */
@Repository
public class PrivateConsumeCarLevelDao extends BaseSortDao<PrivateConsumeCarLevel, String> {

    @Autowired
    private PrivateConsumeCarLevelRepository privateConsumeCarLevelRepository;

    @Autowired
    private SnowFlakeAlgorithm snowFlakeAlgorithm;

    @Override
    protected String generateIdentifier() {
        return String.valueOf(snowFlakeAlgorithm.getSnowflake().nextId());
    }

    public void deleteAllByPrivateConsumeId(String privateConsumeId) {
        try {
            privateConsumeCarLevelRepository.deleteAllByPrivateConsumeId(privateConsumeId);
        } catch (RuntimeException e) {
            throw new RDBException(RDBErrorEnum.RDB_DELETE_ALL_FAILED);
        }
    }

    public boolean existsByZuoweiIdOrChexingId(String zuoweiId, String chexingId) {
        boolean exist;
        try {
            exist = privateConsumeCarLevelRepository.existsByZuoweiIdOrChexingId(zuoweiId, chexingId);
        } catch (RuntimeException e) {
            throw new RDBException(RDBErrorEnum.RDB_EXIST_GET_FAILED);
        }
        return exist;
    }
}
