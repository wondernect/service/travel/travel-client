package com.wondernect.travel.repository;

import com.wondernect.elements.rdb.base.dao.BaseStringDao;
import com.wondernect.travel.model.Consume;
import org.springframework.stereotype.Repository;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: ConsumeDao
 * Author: chenxun
 * Date: 2019/5/11 16:49
 * Description:
 */
@Repository
public class ConsumeDao extends BaseStringDao<Consume> {

}
