package com.wondernect.travel.repository;

import com.wondernect.elements.rdb.base.dao.BaseStringDao;
import com.wondernect.travel.model.Address;
import org.springframework.stereotype.Repository;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: AddressDao
 * Author: chenxun
 * Date: 2019/5/11 16:47
 * Description:
 */
@Repository
public class AddressDao extends BaseStringDao<Address> {

}
