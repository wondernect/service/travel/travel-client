package com.wondernect.travel.repository;

import com.wondernect.elements.rdb.base.dao.BaseStringDao;
import com.wondernect.travel.model.Shop;
import org.springframework.stereotype.Repository;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: ShopDao
 * Author: chenxun
 * Date: 2019/5/11 10:33
 * Description:
 */
@Repository
public class ShopDao extends BaseStringDao<Shop> {

}
