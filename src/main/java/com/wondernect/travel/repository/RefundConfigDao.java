package com.wondernect.travel.repository;

import com.wondernect.elements.rdb.base.dao.BaseDao;
import com.wondernect.travel.model.RefundConfig;
import org.springframework.stereotype.Repository;

/**
 * Copyright (C), 2017-2019, wondernect.com
 * FileName: RefundConfigDao
 * Author: chenxun
 * Date: 2019/5/11 16:52
 * Description:
 */
@Repository
public class RefundConfigDao extends BaseDao<RefundConfig, String> {

}
